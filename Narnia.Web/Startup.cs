﻿using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Logging;
using Narnia.Business;
using Narnia.Core.Infrastructure;
using Narnia.Web.Framework.Mvc;
using System;
using System.IO;
using Narnia.Extensions.DependencyInjection;
using Narnia.Data.Base;
using Hangfire;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Narnia.Core;
using Narnia.Web.BackgroundServices;
using Narnia.Web.Framework.Mvc.ModelBinding;
using Narnia.Web.Framework.Mvc.Routing;
using FluentValidation.AspNetCore;
using Narnia.Web.Framework.FluentValidation;
using Narnia.Web.Areas.Payments.Validators;

namespace Narnia.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
              .AddCookie(options => options.LoginPath = new PathString("/Account/Login"));

            //services.AddSignalR(options => {
            //    options.NegotiateTimeout = TimeSpan.FromSeconds(2);
            //    options.KeepAliveInterval = TimeSpan.FromSeconds(5);
            //});

            services.AddHangfire(config => config.UseSqlServerStorage(Configuration.GetConnectionString("CoinDb")));

            //Edit view path

            services.Configure<RazorViewEngineOptions>(options =>
            {
                options.ViewLocationExpanders.Add(new CustomViewLocator());
            });
            var mvcBuilder = services.AddMvc().AddSessionStateTempDataProvider();
            mvcBuilder.AddMvcOptions(options => options.ModelMetadataDetailsProviders.Add(new NarniaMetadataProvider()));

            // Adds a default in-memory implementation of IDistributedCache.
            services.AddDistributedMemoryCache();

            services.AddSession(options =>
            {
                // Set a short timeout for easy testing.
                options.IdleTimeout = TimeSpan.FromHours(1);
                options.Cookie.HttpOnly = true;
                options.Cookie.Name = ".Vinaex.Session";
            });

            services.Configure<AppSettings>(Configuration.GetSection("AppSettings"));

            //services.AddMvc().AddFluentValidation(configuration => {
            //    configuration.ValidatorFactoryType = typeof(NarniaValidatorFactory);
            //    configuration.ImplicitlyValidateChildProperties = true;
            //});

            services.AddMvc().AddFluentValidation(configuration =>
            {
                configuration.RegisterValidatorsFromAssemblyContaining<ManualBankDepositValidator>();
                configuration.ImplicitlyValidateChildProperties = true;
            })
            .AddViewOptions(op => op.HtmlHelperOptions.ClientValidationEnabled = true);

            //
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.TryAddSingleton<IActionContextAccessor, ActionContextAccessor>();

            services.AddDbContextFactory<IDbContextFactory, DbContextFactory>(new DbContextFactory(Configuration.GetConnectionString("CoinDb")));
            services.AddDbContextScopeFactory<IDbContextScopeFactory, DbContextScopeFactory>();

            //services.AddSingleton<Microsoft.Extensions.Hosting.IHostedService, OrderMatchingBackgroundService>();
            //services.AddSingleton<Microsoft.Extensions.Hosting.IHostedService, Scheduler.ScheduleTask>();
            var engine = EngineContext.Create();
            engine.Initialize(services);

            return engine.ConfigureServices(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }
            app.UseHangfireServer();
            app.UseHangfireDashboard();
            app.UseSession();
            app.UseStaticFiles();
            app.UseAuthentication();

            loggerFactory.AddEventSourceLogger();
            app.UseCors(policy => policy.AllowAnyOrigin());
            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(
                Path.Combine(Directory.GetCurrentDirectory(), "Areas/Admin")),
                RequestPath = "/Areas/Admin"
            });
            app.UseMvc(routes =>
            {


                routes.MapRoute(
                    name: "areaRoute",
                    template: "{area:exists}/{controller=Home}/{action=Index}/{id?}");


                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");

                EngineContext.Current.Resolve<IRoutePublisher>().RegisterRoutes(routes);

            });
            EngineContext.Current.ConfigureRequestPipeline(app);
            var settingService = EngineContext.Current.Resolve<ISettingService>();
            var allSettings = settingService.LoadAllSettings();
            EngineContext.Current.InitDbSetting(allSettings);

            //EngineContext.Current.StartOrderbook();
        }
    }
}
