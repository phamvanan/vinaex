﻿using FastMapper;
using Microsoft.AspNetCore.Mvc;
using Narnia.Business.Impl;
using Narnia.Core.Domain;
using Narnia.Web.Models.Feedbacks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Web.Components
{
    public class FeedbackViewComponent: ViewComponent
    {
        private readonly IFeedbackBusiness _feedbackBusiness;
        public FeedbackViewComponent(IFeedbackBusiness feedbackBusiness)
        {
            _feedbackBusiness = feedbackBusiness;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var feedbacks = _feedbackBusiness.GetTopFeedbacks(10);
            var model = TypeAdapter.Adapt<IList<Feedback>, IList<FeedbackViewModel>>(feedbacks);
            return View(model);
        }

    }
}
