﻿using Microsoft.AspNetCore.Mvc;
using Narnia.Business;
using Narnia.Core;
using Narnia.Core.Domain;
using Narnia.Core.Infrastructure;
using Narnia.Core.Models;
using System.Threading.Tasks;

namespace Narnia.Web.Components
{
    public class DepositViewComponent : ViewComponent
    {
        private readonly IWorkContext _workContext;
        private readonly ICoinWalletBusiness _coinWalletBusiness;
        private readonly User _currentUser = null;
        public DepositViewComponent(IWorkContext workContext,
                                    ICoinWalletBusiness coinWalletBusiness)
        {
            _workContext = workContext;
            _coinWalletBusiness = coinWalletBusiness;
            _currentUser = EngineContext.Current.Resolve<IWorkContext>().CurrentUser;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var coinWallet = _coinWalletBusiness.GetCurrentCoinWalletByUserId(_currentUser != null ? _currentUser.Id : 0, _workContext.WorkingSymbol.AssetIdBase);
            var deposit = new DepositViewModel();
            if (coinWallet != null)
            {
                deposit.AddressWallet = coinWallet.WalletAddress;
            }
            deposit.CoinCode = _workContext.WorkingSymbol.AssetIdBase;

            return View(deposit);
        }
    }
}
