﻿using Microsoft.AspNetCore.Mvc;
using Narnia.Business.Impl;
using Narnia.Core;
using Narnia.Core.Domain;
using Narnia.Core.Infrastructure;
using Narnia.Core.Models;
using System.Threading.Tasks;

namespace Narnia.Web.Components
{
    public class WithdrawViewComponent : ViewComponent
    {
        private readonly IWorkContext _workContext;
        private readonly ISiteSetting _siteSetting;
        private readonly User _currentUser = null;
        private readonly IAdvertisementFeeBusiness _advertisementFeeBusiness;
        private readonly IAssetBalanceBusiness _assetBalanceBusiness;

        public WithdrawViewComponent(IWorkContext workContext,
                                    ISiteSetting siteSetting,
                                    IAdvertisementFeeBusiness advertisementFeeBusiness,
                                    IAssetBalanceBusiness assetBalanceBusiness)
        {
            _workContext = workContext;
            _siteSetting = siteSetting;
            _advertisementFeeBusiness = advertisementFeeBusiness;
            _assetBalanceBusiness = assetBalanceBusiness;
            _currentUser = EngineContext.Current.Resolve<IWorkContext>().CurrentUser;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var assetBalance = _assetBalanceBusiness.GetByUserIdAssetType(_currentUser !=null ? _currentUser.Id : 0, _workContext.WorkingSymbol.AssetIdBase);

            var withdrawModel = new WithdrawViewModel()
            {
                CoinCode = _workContext.WorkingSymbol.AssetIdBase
            };

            if (assetBalance == null)
            {
                return View(withdrawModel);
            }

            withdrawModel.Balance = assetBalance.Balance;

            if (_currentUser != null)
            {
                var freeze = _advertisementFeeBusiness.GetAmountFreezeByUserIdCoinType(_currentUser.Id, assetBalance.AssetTypeId);
                decimal freezeAmout = freeze?.Amount ?? 0;
                withdrawModel.MaximumWithdrawAmount = withdrawModel.Balance - _siteSetting.FeePercentage - freezeAmout;
            }

            if (withdrawModel.MaximumWithdrawAmount < 0)
            {
                withdrawModel.MaximumWithdrawAmount = 0;
            }

            return View(withdrawModel);
        }
    }
}
