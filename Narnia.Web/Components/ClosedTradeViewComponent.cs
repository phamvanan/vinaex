﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Narnia.Web.Components
{
    public class ClosedTradeViewComponent : ViewComponent
    {
        public ClosedTradeViewComponent()
        {

        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            return View(null);
        }
    }
}
