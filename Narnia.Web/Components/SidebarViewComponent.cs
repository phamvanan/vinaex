﻿using Microsoft.AspNetCore.Mvc;
using Narnia.Business;
using Narnia.Core;
using Narnia.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Web.Components
{
    public class SidebarViewComponent : ViewComponent
    {
        private readonly ICoinSymbolBiz _coinSymbolBiz;
        private readonly IWorkContext _workContext;
        private readonly ISiteSetting _siteSetting;
        public SidebarViewComponent(IWorkContext workContext, ICoinSymbolBiz coinSymbolBiz, ISiteSetting siteSetting)
        {
            _workContext = workContext;
            _siteSetting = siteSetting;
            _coinSymbolBiz = coinSymbolBiz;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            SidebarViewModel model = new SidebarViewModel();
            model.IsAuthenticated = _workContext.CurrentUser != null;
            model.CurrentSymbol = _workContext.WorkingSymbol.AssetIdBase;
            var supportedSymbols = _coinSymbolBiz.GetAll().Select(s => s.AssetIdBase).ToList();
            foreach(var s in supportedSymbols)
            {
                model.Symbols.Add(new SymbolViewModel(){
                    Name = s,
                    Symbol = s
                });
            }
            return View(model);
        }
    }
}
