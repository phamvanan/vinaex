﻿using Microsoft.AspNetCore.Mvc;
using Narnia.Business.Localization;
using Narnia.Core;
using Narnia.Core.Caching;
using Narnia.Web.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Web.Components
{
    public class LanguageSelectorViewComponent : ViewComponent
    {
        private readonly ILanguageService _languageService;
        private readonly ICacheManager _cacheManager;
        private readonly IWorkContext _workContext;
        public LanguageSelectorViewComponent(ICacheManager cacheManager, ILanguageService languageService, IWorkContext workContext)
        {
            this._cacheManager = cacheManager;
            this._languageService = languageService;
            this._workContext = workContext;
        }
        public IViewComponentResult Invoke()
        {
            var availableLanguages = _cacheManager.Get(string.Format(LocalizationCacheKeys.LanguagesAllCacheKey, false), () =>
            {
                var result = _languageService
                    .GetAllLanguages();
                return result;
            });

            var model = new LanguageSelectorModel
            {
                CurrentLanguageId = _workContext.WorkingLanguage.Id,
                AvailableLanguages = availableLanguages.Select(x => new LanguageModel
                {
                    Id = x.Id,
                    Name = x.Name,
                    FlagImageFileName = x.FlagImageFileName,
                }).ToList()
            };

            if (model.AvailableLanguages.Count == 1)
                return Content("");

            return View(model);
        }
    }
}
