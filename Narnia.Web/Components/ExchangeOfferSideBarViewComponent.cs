﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FastMapper;
using Microsoft.AspNetCore.Mvc;
using Narnia.Business;
using Narnia.Web.Models;

namespace Narnia.Web.Components
{
    public class ExchangeOfferSideBarViewComponent : ViewComponent
    {
        private readonly ICoinSymbolBiz _coinSymbolBiz;
        public ExchangeOfferSideBarViewComponent(ICoinSymbolBiz coinSymbolBiz)
        {
            _coinSymbolBiz = coinSymbolBiz;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var coinSymbols = _coinSymbolBiz.GetAll().ToList();
            var coinSymbolVM = TypeAdapter.Adapt<List<CoinSymbolViewModel>>(coinSymbols);

            return View(coinSymbolVM);
        }
    }
}
