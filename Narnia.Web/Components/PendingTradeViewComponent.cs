﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FastMapper;
using Microsoft.AspNetCore.Mvc;
using Narnia.Business;
using Narnia.Core;
using Narnia.Core.Domain;
using Narnia.Core.Enums;
using Narnia.Core.Infrastructure;
using Narnia.Core.Models;
using Narnia.Web.Models;
using Narnia.Web.Models.Feedbacks;

namespace Narnia.Web.Components
{
    public class PendingTradeViewComponent : ViewComponent
    {
        public readonly ITransactionBiz _transaction;
        private readonly User _currentUser = null;
        public PendingTradeViewComponent(ITransactionBiz transaction)
        {
            _transaction = transaction;
            _currentUser = EngineContext.Current.Resolve<IWorkContext>().CurrentUser;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var user = EngineContext.Current.Resolve<IWorkContext>().CurrentUser;
            var transactionHistory = _transaction.GetTransactionHistoryForUser(_currentUser?.Id ?? 0, TransactionStatusClient.Complete, 0, 20);
            var transactionViewModel = TypeAdapter.Adapt<IList<TransactionHistoryUserModel>, IList<TransactionHistoryViewModel>>(transactionHistory.Data.ToList());
            return View();
        }
    }
}
