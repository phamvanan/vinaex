﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Web.Events
{
    public class KickUserOutEvent
    {
        public int UserId { get; set; }
    }
}
