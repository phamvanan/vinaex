﻿using DryIoc;
using Narnia.Core.Infrastructure;
using Narnia.Data.Repository.Contracts;
using Narnia.Data.Repository.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Web
{
    public class DependencyRegistrar : DryIocModule
    {
        protected override void Load(IRegistrator builder)
        {            
            builder.Register<IAggregateRepository, AggregateRepository>(Reuse.InWebRequest);
        }
    }
}
