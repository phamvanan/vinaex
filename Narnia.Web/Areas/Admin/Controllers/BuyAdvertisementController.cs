﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FastMapper;
using Microsoft.AspNetCore.Mvc;
using Narnia.Business;
using Narnia.Core.Domain;
using Narnia.Core.Models;
using Narnia.Web.Areas.Admin.Models.AjaxDataTables;
using Narnia.Web.Models;

namespace Narnia.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class BuyAdvertisementController : BaseAdminController
    {
        private readonly IBuyAdvertisementBiz _buyAdvertisementBiz;
        public BuyAdvertisementController(IBuyAdvertisementBiz buyAdvertisementBiz)
        {
            _buyAdvertisementBiz = buyAdvertisementBiz;
        }

        public IActionResult Index()
        {
            return RedirectToAction("List");
        }

        public ActionResult List()
        {
            return View();
        }

        [HttpGet]
        public ActionResult BuyDetail(int Id)
        {
            AdvertiseViewModel info = new AdvertiseViewModel();
            try
            {
                var buyInfo = _buyAdvertisementBiz.LoadBuyAdvertisementDetail(Id);
                info = TypeAdapter.Adapt<BuyAdvertisement, AdvertiseViewModel>(buyInfo);
            }
            catch { }

            return View("BuyDetail", info);            
        }

        [HttpPost]
        public JsonResult LoadBuyAdData(DataTableAjaxPostModel model, BuySellAdvertiseSearchCriteria searchCriteria)
        {
            int totalResultsCount = 0, filteredResultsCount = 0;
            var pageSize = model.length;
            var pageIndex = model.start;
            String sortString = "";
            

            var buyReport = _buyAdvertisementBiz.LoadBuyAdvertisement(pageIndex, pageSize, out totalResultsCount, out filteredResultsCount,true, sortString, searchCriteria).ToList();
            var buyViewModel = TypeAdapter.Adapt<List<BuyAdvertisement>, List<AdvertiseViewModel>>(buyReport);

            return Json(new DataTableAjaxReturnModel<AdvertiseViewModel>()
            {
                draw = model.draw,
                recordsFiltered = filteredResultsCount,
                recordsTotal = totalResultsCount,
                data = buyViewModel
            });
        }

        [HttpGet]
        public JsonResult ApproveBuyAdvertise(int Id)
        {
            bool isSuccess = false;
            try {
                var buyInfo = _buyAdvertisementBiz.GetBuyAdvertisementById(Id);
                if (buyInfo != null)
                {
                    buyInfo.Approved = true;
                    isSuccess = _buyAdvertisementBiz.UpdateBuyAdvertisement(buyInfo);
                }
            } catch { }

            return Json(isSuccess);
        }       
    }
}