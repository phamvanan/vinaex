﻿using FastMapper;
using Microsoft.AspNetCore.Mvc;
using Narnia.Business;
using Narnia.Core.Domain;
using Narnia.Web.Areas.Admin.Models.Settings;
using Narnia.Web.Framework.KendoUI;
using Narnia.Web.Framework.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Web.Areas.Admin.Controllers
{
    public class SettingController: BaseAdminController
    {
        #region Fields

        private readonly ISettingService _settingService;

        #endregion

        #region Ctor

        public SettingController(ISettingService settingService)
        {
            this._settingService = settingService;
        }

        #endregion

        #region Methods

        public virtual IActionResult AllSettings()
        {
            var model = new SettingSearchModel();
            return View(model);
        }

        [HttpPost]
        public virtual IActionResult AllSettings(SettingSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //get settings
            var settings = _settingService.SearchSettings(searchModel.SearchSettingName, searchModel.SearchSettingValue,
                searchModel.Page - 1, searchModel.PageSize, true);
            var model = new PagedListModel<SettingModel>
            {
                Data = settings.Data.Select(s => {
                    return TypeAdapter.Adapt<Setting, SettingModel>(s);
                }),
                Total = settings.TotalCount
            };

            return Json(model);
        }

        [HttpPost]
        public virtual IActionResult SettingUpdate(SettingModel model)
        {
            if (model.Name != null)
                model.Name = model.Name.Trim();

            if (model.Value != null)
                model.Value = model.Value.Trim();

            if (model.Description != null)
                model.Description = model.Description.Trim();

            if (!ModelState.IsValid)
                return Json(new DataSourceResult { Errors = ModelState.SerializeErrors() });

            var setting = _settingService.GetSettingById(model.Id)
                ?? throw new ArgumentException("No setting found with the specified id");

            setting.Value = model.Value;
            setting.Name = model.Name;
            setting.Description = model.Description;

            _settingService.Update(setting);

            return new JsonResult(null);
        }

        [HttpPost]
        public virtual IActionResult SettingAdd(SettingModel model)
        {
            if (model.Name != null)
                model.Name = model.Name.Trim();

            if (model.Value != null)
                model.Value = model.Value.Trim();

            if (model.Description != null)
                model.Description = model.Description.Trim();

            if (!ModelState.IsValid)
                return Json(new DataSourceResult { Errors = ModelState.SerializeErrors() });

            var setting = TypeAdapter.Adapt<SettingModel, Setting>(model);
            _settingService.AddNewSetting(setting);

            return new JsonResult(null);
        }

        [HttpPost]
        public virtual IActionResult SettingDelete(int id)
        {
            //try to get a setting with the specified id
            var setting = _settingService.GetSettingById(id)
                ?? throw new ArgumentException("No setting found with the specified id", nameof(id));

            _settingService.Delete(id);

            return new JsonResult(null);
        }

        #endregion
    }
}
