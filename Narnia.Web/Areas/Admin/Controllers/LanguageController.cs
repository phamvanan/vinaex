﻿using FastMapper;
using Microsoft.AspNetCore.Mvc;
using Narnia.Business.Localization;
using Narnia.Core.Domain.Localization;
using Narnia.Web.Areas.Admin.Models.AjaxDataTables;
using Narnia.Web.Areas.Admin.Models.Localization;
using Narnia.Web.Framework.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Narnia.Web.Areas.Admin.Extensions;
using System.IO;
using System.Text;
using Microsoft.AspNetCore.Http;
using Narnia.Web.Framework.KendoUI;
using Narnia.Web.Framework.Extensions;
using Narnia.Core.Infrastructure;
using Microsoft.AspNetCore.Mvc.Rendering;
using Narnia.Web.Framework.Models;

namespace Narnia.Web.Areas.Admin.Controllers
{
    public class LanguageController: BaseAdminController
    {
        private const string FLAGS_PATH = @"images\flags";

        private readonly ILanguageService _languageService;
        private readonly ILocalizationService _localizationService;
        private readonly INarniaFileProvider _fileProvider;
        public LanguageController(ILanguageService languageService, ILocalizationService localizationService, INarniaFileProvider narniaFileProvider)
        {
            _languageService = languageService;
            _localizationService = localizationService;
            _fileProvider = narniaFileProvider;
        }

        #region Languages

        public virtual IActionResult Index()
        {
            return RedirectToAction("List");
        }

        public virtual IActionResult List()
        {
            var model = new LanguageSearchModel();

            return View(model);
        }

        [HttpPost]
        public virtual JsonResult List(LanguageSearchModel searchCriteria)
        {
            var languages = _languageService.GetAllLanguages(true);
            
            var model = new PagedListModel<LanguageModel>
            {
                Data = languages.PaginationByRequestModel(searchCriteria).Select(language => TypeAdapter.Adapt<Language, LanguageModel>(language)),
                Total = languages.Count
            };


            return Json(model);
        }

        public virtual IActionResult Create()
        {
            var model = new LanguageModel();

            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public virtual IActionResult Create(LanguageModel model, bool continueEditing)
        {
            if (ModelState.IsValid)
            {
                var language = model.ToEntity();
                _languageService.InsertLanguage(language);

                if (!continueEditing)
                    return RedirectToAction("List");

                return RedirectToAction("Edit", new { id = language.Id });
            }

            //if we got this far, something failed, redisplay form
            return View(model);
        }

        public virtual IActionResult Edit(int id)
        {
            //try to get a language with the specified id
            var language = _languageService.GetLanguageById(id);
            if (language == null)
                return RedirectToAction("List");

            //prepare model
            var model = language.ToModel();

            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public virtual IActionResult Edit(LanguageModel model, bool continueEditing)
        {
            //try to get a language with the specified id
            var language = _languageService.GetLanguageById(model.Id);
            if (language == null)
                return RedirectToAction("List");

            if (ModelState.IsValid)
            {
                //ensure we have at least one published language
                var allLanguages = _languageService.GetAllLanguages();
                if (allLanguages.Count == 1 && allLanguages[0].Id == language.Id && !model.Published)
                {
                    ErrorNotification(_localizationService.GetResource("Admin.Configuration.Languages.PublishedLanguageRequired"));
                    return RedirectToAction("Edit", new { id = language.Id });
                }

                //update
                language = model.ToEntity(language);
                _languageService.UpdateLanguage(language);

                //notification
                SuccessNotification(_localizationService.GetResource("Admin.Configuration.Languages.Updated"));

                if (!continueEditing)
                    return RedirectToAction("List");


                return RedirectToAction("Edit", new { id = language.Id });
            }

            //if we got this far, something failed, redisplay form
            return View(model);
        }

        [HttpPost]
        public virtual IActionResult Delete(int id)
        {
            var language = _languageService.GetLanguageById(id);
            if (language == null)
                return RedirectToAction("List");

            //ensure we have at least one published language
            var allLanguages = _languageService.GetAllLanguages();
            if (allLanguages.Count == 1 && allLanguages[0].Id == language.Id)
            {
                ErrorNotification(_localizationService.GetResource("Admin.Configuration.Languages.PublishedLanguageRequired"));
                return RedirectToAction("Edit", new { id = language.Id });
            }

            //delete
            _languageService.DeleteLanguage(language);

            //notification
            SuccessNotification(_localizationService.GetResource("Admin.Configuration.Languages.Deleted"));

            return RedirectToAction("List");
        }

        [HttpPost]
        public virtual JsonResult GetAvailableFlagFileNames()
        {
            var flagNames = _fileProvider
                .EnumerateFiles(_fileProvider.GetAbsolutePath(FLAGS_PATH), "*.png")
                .Select(_fileProvider.GetFileName)
                .ToList();

            var availableFlagFileNames = flagNames.Select(flagName => new SelectListItem
            {
                Text = flagName,
                Value = flagName
            }).ToList();

            return Json(availableFlagFileNames);
        }

        #endregion

        #region Resources

        [HttpPost]
        public virtual IActionResult Resources(LocaleResourceSearchModel searchModel)
        {
            //try to get a language with the specified id
            var language = _languageService.GetLanguageById(searchModel.LanguageId);
            if (language == null)
                return RedirectToAction("List");

            //prepare model
            var model = PrepareLocaleResourceListModel(searchModel, language);

            return Json(model);
        }

        protected LocaleResourceListModel PrepareLocaleResourceListModel(LocaleResourceSearchModel searchModel, Language language)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            if (language == null)
                throw new ArgumentNullException(nameof(language));

            //get locale resources
            var localeResources = _localizationService.GetAllResourceValues(language.Id, loadPublicLocales: null)
                .OrderBy(localeResource => localeResource.Key).AsQueryable();

            
            if (!string.IsNullOrEmpty(searchModel.SearchResourceName))
                localeResources = localeResources.Where(l => l.Key.ToLowerInvariant().Contains(searchModel.SearchResourceName.ToLowerInvariant()));
            if (!string.IsNullOrEmpty(searchModel.SearchResourceValue))
                localeResources = localeResources.Where(l => l.Value.Value.ToLowerInvariant().Contains(searchModel.SearchResourceValue.ToLowerInvariant()));

            //prepare list model
            var model = new LocaleResourceListModel
            {
                Data = localeResources.PaginationByRequestModel(searchModel).Select(localeResource => new LocaleStringResourceModel
                {
                    LanguageId = language.Id,
                    Id = localeResource.Value.Key,
                    ResourceName = localeResource.Key,
                    ResourceValue = localeResource.Value.Value
                }),
                Total = localeResources.Count()
            };

            return model;
        }

        [HttpPost]
        public virtual IActionResult ResourceUpdate(LocaleStringResourceModel model)
        {

            if (model.ResourceName != null)
                model.ResourceName = model.ResourceName.Trim();
            if (model.ResourceValue != null)
                model.ResourceValue = model.ResourceValue.Trim();

            if (!ModelState.IsValid)
            {
                return Json(new DataSourceResult { Errors = ModelState.SerializeErrors() });
            }

            var resource = _localizationService.GetLocaleStringResourceById(model.Id);
            // if the resourceName changed, ensure it isn't being used by another resource
            if (!resource.ResourceName.Equals(model.ResourceName, StringComparison.InvariantCultureIgnoreCase))
            {
                var res = _localizationService.GetLocaleStringResourceByName(model.ResourceName, model.LanguageId);
                if (res != null && res.Id != resource.Id)
                {
                    return Json(new DataSourceResult { Errors = string.Format(_localizationService.GetResource("Admin.Configuration.Languages.Resources.NameAlreadyExists"), res.ResourceName) });
                }
            }

            resource.ResourceName = model.ResourceName;
            resource.ResourceValue = model.ResourceValue;
            _localizationService.UpdateLocaleStringResource(resource);

            return new JsonResult(null);
        }

        [HttpPost]
        public virtual IActionResult ResourceAdd(int languageId, LocaleStringResourceModel model)
        {

            if (model.ResourceName != null)
                model.ResourceName = model.ResourceName.Trim();
            if (model.ResourceValue != null)
                model.ResourceValue = model.ResourceValue.Trim();

            if (!ModelState.IsValid)
            {
                return Json(new DataSourceResult { Errors = ModelState.SerializeErrors() });
            }

            var res = _localizationService.GetLocaleStringResourceByName(model.ResourceName, model.LanguageId);
            if (res == null)
            {
                var resource = new LocaleStringResource
                {
                    LanguageId = languageId,
                    ResourceName = model.ResourceName,
                    ResourceValue = model.ResourceValue
                };

                _localizationService.InsertLocaleStringResource(resource);
            }
            else
            {
                return Json(new DataSourceResult { Errors = string.Format(_localizationService.GetResource("Admin.Configuration.Languages.Resources.NameAlreadyExists"), model.ResourceName) });
            }

            return new JsonResult(null);
        }

        [HttpPost]
        public virtual IActionResult ResourceDelete(int id)
        {
            //try to get a locale resource with the specified id
            var resource = _localizationService.GetLocaleStringResourceById(id)
                ?? throw new ArgumentException("No resource found with the specified id", nameof(id));

            _localizationService.DeleteLocaleStringResource(resource);

            return new JsonResult(null);
        }

        #endregion

        #region Export / Import

        public virtual IActionResult ExportXml(int id)
        {
            //try to get a language with the specified id
            var language = _languageService.GetLanguageById(id);
            if (language == null)
                return RedirectToAction("List");

            try
            {
                var xml = _localizationService.ExportResourcesToXml(language);
                return File(Encoding.UTF8.GetBytes(xml), "application/xml", "language_pack.xml");
            }
            catch (Exception exc)
            {
                ErrorNotification(exc);
                return RedirectToAction("List");
            }
        }

        [HttpPost]
        public virtual IActionResult ImportXml(int id, IFormFile importxmlfile)
        {
            //try to get a language with the specified id
            var language = _languageService.GetLanguageById(id);
            if (language == null)
                return RedirectToAction("List");

            try
            {
                if (importxmlfile != null && importxmlfile.Length > 0)
                {
                    using (var sr = new StreamReader(importxmlfile.OpenReadStream(), Encoding.UTF8))
                    {
                        _localizationService.ImportResourcesFromXml(language, sr.ReadToEnd());
                    }
                }
                else
                {
                    ErrorNotification(_localizationService.GetResource("Admin.Common.UploadFile"));
                    return RedirectToAction("Edit", new { id = language.Id });
                }

                SuccessNotification(_localizationService.GetResource("Admin.Configuration.Languages.Imported"));
                return RedirectToAction("Edit", new { id = language.Id });
            }
            catch (Exception exc)
            {
                ErrorNotification(exc);
                return RedirectToAction("Edit", new { id = language.Id });
            }
        }

        #endregion
    }
}
