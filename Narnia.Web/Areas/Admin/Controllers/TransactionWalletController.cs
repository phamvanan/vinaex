﻿using FastMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Narnia.Business;
using Narnia.Core;
using Narnia.Core.Domain;
using Narnia.Core.Enums;
using Narnia.Core.Infrastructure;
using Narnia.Core.Models;
using Narnia.Web.Areas.Admin.Models.TransactionWallet;
using Narnia.Web.Framework.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Narnia.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class TransactionWalletController : BaseAdminController
    {
        private readonly ITransactionWalletBiz _transactionWalletBiz;
        private User _currentUser = null;
        private int _userIdDefault = 0;

        public TransactionWalletController(ITransactionWalletBiz transactionWalletBiz)
        {
            _transactionWalletBiz = transactionWalletBiz;
            _currentUser = EngineContext.Current.Resolve<IWorkContext>().CurrentUser;
        }

        public IActionResult Index()
        {
            return RedirectToAction("List");
        }

        public IActionResult List()
        {
            var model = new TransactionWalletSearchModel
            {
            };
            model.AvailableAssetTypeIds.Add(new SelectListItem { Text = "All", Value = "" });

            IList<SelectListItem> AssetTypeIds = Enum.GetValues(typeof(AssetTypeEnum)).Cast<AssetTypeEnum>().
                Select(x => new SelectListItem { Text = x.ToString(), Value = x.ToString() }).ToList();
            model.AvailableAssetTypeIds.AddRange(AssetTypeIds);
            model.AvailableStatuses.Add(new SelectListItem { Text = "All", Value = "0" });

            IList<SelectListItem> Transtatus = Enum.GetValues(typeof(TransactionStautus)).Cast<TransactionStautus>().
                Select(x => new SelectListItem { Text = x.ToString(), Value = ((int)x).ToString() }).ToList();
            model.AvailableStatuses.AddRange(Transtatus);

            return View(model);
        }

        [HttpPost]
        public virtual IActionResult List(TransactionWalletSearchModel searchModel)
        {
            var transactions = _transactionWalletBiz.SearchTransactions(searchModel.StartDate, searchModel.EndDate, searchModel.TransactionCode,
                searchModel.Statuses, searchModel.AssetTypeIds, searchModel.UserName, searchModel.Page - 1, searchModel.PageSize);

            var model = new PagedListModel<TransactionWalletAdminSearchViewModel>
            {
                Data = transactions.Data.Select(t =>
                {
                    var result = TypeAdapter.Adapt<TransactionWalletAdminSearchModel, TransactionWalletAdminSearchViewModel>(t);
                    return result;
                }),
                Total = transactions.TotalCount
            };
            return Json(model);
        }

        [HttpGet]
        public virtual IActionResult Edit(int id)
        {
            var response = _transactionWalletBiz.GetDetailAdmin(id);
            if (response.HasError)
                return RedirectToAction("List");
            var result = TypeAdapter.Adapt<TransactionWallletDetailAdminModel, TransactionWalletDetailAdminViewModel>(response.Data);

            if (result.CanChangeStatus)
            {
                result.ValuesValidToExclude = _transactionWalletBiz.GetTransactionStatusValid(result.Status, result.TransactionType, result.Unit);
            }

            return View(result);
        }

        [HttpPost]
        public virtual IActionResult Edit(TransactionWalletDetailAdminViewModel model, List<IFormFile> Image)
        {
            var entityInput = TypeAdapter.Adapt<TransactionWalletDetailAdminViewModel, TransactionWallet>(model);

            entityInput.ConfirmBy = _currentUser?.Id ?? _userIdDefault;
            var result = _transactionWalletBiz.AdminConfirm(entityInput, Image);
            if (result.HasError)
            {
                TempData["error"] = result.Message;
            }
            else
            {
                TempData["success"] = CommonConst.UpdateTransactionSuccess;
            }
            return RedirectToAction("Edit", new { id = model.TransactionId });
        }
    }
}