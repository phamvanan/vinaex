﻿using FastMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Narnia.Business;
using Narnia.Business.Messaging;
using Narnia.Core.Domain;
using Narnia.Web.Areas.Admin.Extensions;
using Narnia.Web.Areas.Admin.Models.AjaxDataTables;
using Narnia.Web.Areas.Admin.Models.Messaging;
using Narnia.Web.Areas.Admin.Models.Wallets;
using Narnia.Web.Framework.KendoUI;
using Narnia.Web.Framework.Models;
using Narnia.Web.Framework.Mvc.Attributes;
using System.Collections.Generic;
using System.Linq;

namespace Narnia.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class WalletController: BaseAdminController
    {
        private ICoinWalletBusiness _coinWalletBusiness;
        public WalletController(ICoinWalletBusiness coinWalletBusiness)
        {
            _coinWalletBusiness = coinWalletBusiness;
        }

        public virtual IActionResult Index()
        {
            return RedirectToAction("List");
        }

        public virtual IActionResult List()
        {
            var model = new WalletSearchModel();
            model.AvailableAssetTypeIds.Add(new SelectListItem { Text = "All", Value = "" });
            var allSymbols = _coinWalletBusiness.GetAllSymbols(true).ToList();
            if (allSymbols != null && allSymbols.Count > 0)
            {
                model.AvailableAssetTypeIds.AddRange(allSymbols.Select(item => new SelectListItem() {Text = item.AssetIdBase,Value = item.AssetIdBase}));
            }
            return View(model);
        }

        [HttpPost]
        public JsonResult List(WalletSearchModel searchCriteria)
        {
            var wallets = _coinWalletBusiness.FindWallets(searchCriteria.StartDate,searchCriteria.EndDate,searchCriteria.AssetTypeIds,searchCriteria.UserName, searchCriteria.UserId,
                searchCriteria.Page - 1,searchCriteria.PageSize);

            var model = new PagedListModel<WalletModel>
            {
                Data = wallets.Data.Select(w =>
                {
                    return new WalletModel
                    {
                        Id = w.Id,
                        WalletAddress = w.WalletAddress,
                        UserName = w.User.UserName,
                        Amount = w.Amount,
                        AssetTypeId = w.AssetTypeId,
                        CreatedDate = w.CreatedDate,
                        UpdatedDate = w.UpdatedDate,
                        WalletPassword = w.WalletPassword,
                        UserId = w.UserId
                    };
                }),
                Total = wallets.TotalCount
            };
            return Json(model);
        }
    }
}
