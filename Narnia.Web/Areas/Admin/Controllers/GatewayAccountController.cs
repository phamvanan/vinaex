﻿using FastMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Narnia.Business.Impl;
using Narnia.Business.Transactions;
using Narnia.Core;
using Narnia.Core.Domain.Payments;
using Narnia.Core.Enums;
using Narnia.Web.Areas.Admin.Extensions;
using Narnia.Web.Areas.Admin.Models.Payments;
using Narnia.Web.Framework.KendoUI;
using Narnia.Web.Framework.Models;
using Narnia.Web.Framework.Mvc.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Web.Areas.Admin.Controllers
{
    public class GatewayAccountController: BaseAdminController
    {
        private readonly IPaymentBiz _paymentBiz;
        private readonly IAssetTypeBusiness _assetTypeBiz;
        private readonly IWorkContext _workContext;
        public GatewayAccountController(IPaymentBiz paymentBiz, IAssetTypeBusiness assetTypeBiz, IWorkContext workContext)
        {
            _paymentBiz = paymentBiz;
            _assetTypeBiz = assetTypeBiz;
            _workContext = workContext;
        }

        public virtual IActionResult Index()
        {
            return RedirectToAction("List");
        }

        public virtual IActionResult List()
        {
            var model = new GatewayAccountSearchModel();
            return View(model);

        }

        [HttpPost]
        public virtual IActionResult List(GatewayAccountSearchModel searchModel)
        {
            var allAccounts = _paymentBiz.SearchGatewayAccounts(searchModel.StartDate,searchModel.EndDate,searchModel.UserName,searchModel.AccountType > 0, searchModel.Page - 1, searchModel.PageSize, true);

            var model = new PagedListModel<GatewayAccountModel>
            {
                Data = allAccounts.Data.Select(t => {
                    var gatewayAcc = TypeAdapter.Adapt<GatewayAccount, GatewayAccountModel>(t);
                    return gatewayAcc;
                }),
                Total = allAccounts.TotalCount
            };

            return Json(model);
        }


        [HttpGet]
        public ActionResult Create()
        {
            var model = new GatewayAccountModel();
            model.IsSystemAccount = true;
            var assetTypes = _assetTypeBiz.GetAll();
            var availableAssetTypeIds = new List<SelectListItem>();
            availableAssetTypeIds.Add(new SelectListItem { Text = "None", Value = "" });
            foreach (var assettype in assetTypes)
            {
                availableAssetTypeIds.Add(new SelectListItem { Text = assettype.AssetTypeId, Value = assettype.AssetTypeId });
            }
            ViewBag.AvailableAssetTypeIds = availableAssetTypeIds;

            var gateways = _paymentBiz.GetAllGateways();

            var availableGateways = new List<SelectListItem>();
            availableGateways.Add(new SelectListItem { Text = "None", Value = "" });
            foreach (var gateway in gateways)
            {
                availableGateways.Add(new SelectListItem { Text = gateway.GatewayFriendlyName, Value = gateway.Id.ToString() });
            }

            ViewBag.AvailableGateways = availableGateways;
           
            return View(model);
        }
        [HttpPost]
        public virtual IActionResult Create(GatewayAccountModel model)
        {
            if (ModelState.IsValid)
            {
                var gatewayAccount = model.ToEntity();
                gatewayAccount.CreatedDate = DateTime.Now;
                gatewayAccount.UserId = null;

                _paymentBiz.AddGatewayAccount(gatewayAccount);
                return RedirectToAction("List");
            }
            //if we got this far, something failed, redisplay form
            return View(model);
        }

        [HttpGet]
        public ActionResult Edit(int Id)
        {
            var assetTypes = _assetTypeBiz.GetAll();
            var availableAssetTypeIds = new List<SelectListItem>();
            availableAssetTypeIds.Add(new SelectListItem { Text = "None", Value = "" });
            foreach (var assettype in assetTypes)
            {
                availableAssetTypeIds.Add(new SelectListItem { Text = assettype.AssetTypeId, Value = assettype.AssetTypeId });
            }
            ViewBag.AvailableAssetTypeIds = availableAssetTypeIds;

            var gateways = _paymentBiz.GetAllGateways();

            var availableGateways = new List<SelectListItem>();
            availableGateways.Add(new SelectListItem { Text = "None", Value = "" });
            foreach (var gateway in gateways)
            {
                availableGateways.Add(new SelectListItem { Text = gateway.GatewayFriendlyName, Value = gateway.Id.ToString() });
            }
            ViewBag.AvailableGateways = availableGateways;

            var currentGatewayAcc = _paymentBiz.GetGatewayAccountById(Id);
            if (currentGatewayAcc == null)
            {
                return Json("Cannot retreive data");
            }
            var model = currentGatewayAcc.ToModel();

            return View("Edit", model);
        }
        [HttpPost]
        [FormValueRequired("save")]
        public ActionResult Edit(GatewayAccountModel model)
        {
            var currentGatewayAcc = _paymentBiz.GetGatewayAccountById(model.GatewayId);

            currentGatewayAcc.Description = model.Description;
            

            _paymentBiz.UpdateGatewayAccount(currentGatewayAcc);

            // return Json(true);
            return RedirectToAction("List");
        }

        [HttpPost]
        public virtual IActionResult Delete(int id)
        {
            var currentGatewayAccount = _paymentBiz.GetGatewayAccountById(id);
            if (currentGatewayAccount != null)
                _paymentBiz.DeleteGatewayAccount(currentGatewayAccount);
            return RedirectToAction("List");
        }
    }
}
