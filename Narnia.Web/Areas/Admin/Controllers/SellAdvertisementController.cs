﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FastMapper;
using Microsoft.AspNetCore.Mvc;
using Narnia.Business;
using Narnia.Core.Domain;
using Narnia.Core.Models;
using Narnia.Web.Areas.Admin.Models.AjaxDataTables;
using Narnia.Web.Models;

namespace Narnia.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class SellAdvertisementController : BaseAdminController
    {
        private readonly ISellAdvertisementBiz _sellAdvertisementBiz;
        public SellAdvertisementController(ISellAdvertisementBiz sellAdvertisementBiz)
        {
            _sellAdvertisementBiz = sellAdvertisementBiz;
        }

        public IActionResult Index()
        {
            return RedirectToAction("List");
        }

        public ActionResult List()
        {
            return View();
        }

        [HttpGet]
        public ActionResult SellDetail(int Id)
        {
            AdvertiseViewModel info = new AdvertiseViewModel();
            try
            {
                var buyInfo = _sellAdvertisementBiz.LoadSellAdvertisementDetail(Id);
                info = TypeAdapter.Adapt<SellAdvertisement, AdvertiseViewModel>(buyInfo);
            }
            catch { }

            return View("SellDetail", info);
        }

        [HttpPost]
        public JsonResult LoadSellAdData(DataTableAjaxPostModel model, BuySellAdvertiseSearchCriteria searchCriteria)
        {
            int totalResultsCount = 0, filteredResultsCount = 0;
            var pageSize = model.length;
            var pageIndex = model.start;
            String sortString = "";


            var sellReport = _sellAdvertisementBiz.LoadSellAdvertisement(pageIndex, pageSize, out totalResultsCount, out filteredResultsCount, true, sortString, searchCriteria).ToList();
            var sellViewModel = TypeAdapter.Adapt<List<SellAdvertisement>, List<AdvertiseViewModel>>(sellReport);

            return Json(new DataTableAjaxReturnModel<AdvertiseViewModel>()
            {
                draw = model.draw,
                recordsFiltered = filteredResultsCount,
                recordsTotal = totalResultsCount,
                data = sellViewModel
            });
        }

        [HttpGet]
        public JsonResult ApproveSellAdvertise(int Id)
        {
            bool isSuccess = false;
            try
            {
                var sellInfo = _sellAdvertisementBiz.GetById(Id);
                if (sellInfo != null)
                {
                    sellInfo.Approved = true;
                    isSuccess = _sellAdvertisementBiz.UpdateSellAdvertisement(sellInfo);
                }
            }
            catch { }

            return Json(isSuccess);
        }
    }
}