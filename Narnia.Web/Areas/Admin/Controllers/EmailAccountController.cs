﻿using Mercedes.Admin.Models.Configuration;
using Microsoft.AspNetCore.Mvc;
using Narnia.Business;
using Narnia.Business.Messaging;
using Narnia.Web.Areas.Admin.Extensions;
using Narnia.Web.Areas.Admin.Models.Configuration;
using Narnia.Web.Framework.KendoUI;
using Narnia.Web.Framework.Mvc.Attributes;
using System;
using System.Linq;

namespace Narnia.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class EmailAccountController: BaseAdminController
    {
        private readonly IEmailAccountBiz _emailAccountService;
        private readonly IEmailSender _emailSender;
        public EmailAccountController(IEmailAccountBiz emailAccountService, IEmailSender emailSender)//, IEmailService emailService
        {
            _emailAccountService = emailAccountService;
            _emailSender = emailSender;
        }
        // GET: Account
        public ActionResult Index()
        {
            return RedirectToAction("List");
        }
     
        public ActionResult List()
        {
            var model = new EmailListModel();
            return View();
        }
        [HttpPost]
        public JsonResult List(DataSourceRequest command, EmailListModel model)
        {
            var ls = _emailAccountService.GetAllEmailAccounts();
            var dataModel = ls.Select(x => x.ToModel());
            return Json(new { data = dataModel });
        }

        [HttpGet]
        public ActionResult Add()
        {
            var model = new EmailAccountModel();
            return View("Create", model);
        }
        [HttpPost]
        public JsonResult Add(EmailAccountModel model)
        {
            var email = new Core.Domain.EmailAccount();
            model.ToEntity(email);
            _emailAccountService.InsertEmailAccount(email);
            return Json(true);
        }
        [HttpGet]
        public ActionResult Edit(int Id)
        {
            var currentEmailAccount = _emailAccountService.GetEmailAccountById(Id);
            if (currentEmailAccount == null)
            {
                return Json("Cannot retreive data");
            }
            var model = currentEmailAccount.ToModel();
            return View("Edit", model);
        }
        [HttpPost]
        [FormValueRequired("save")]
        public JsonResult Edit(EmailAccountModel model)
        {
            var currentEmailAccount = _emailAccountService.GetEmailAccountById(model.Id);
            model.ToEntity(currentEmailAccount);
            _emailAccountService.UpdateEmailAccount(currentEmailAccount);
            
            return Json(true);
        }

        [HttpPost, ActionName("Edit")]
        [FormValueRequired("changepassword")]
        public virtual JsonResult ChangePassword(EmailAccountModel model)
        {
            var emailAccount = _emailAccountService.GetEmailAccountById(model.Id);
            if (emailAccount == null)
                return Json(false);
            
            emailAccount.Password = model.Password;
            _emailAccountService.UpdateEmailAccount(emailAccount);
            return Json(true);
        }
        [HttpPost, ActionName("Edit")]
        [FormValueRequired("sendtestemail")]
        public virtual JsonResult SendTestEmail(EmailAccountModel model)
        {
            var emailAccount = _emailAccountService.GetEmailAccountById(model.Id);
            if (emailAccount == null)
                return Json(false);
            
            try
            {
                if (String.IsNullOrWhiteSpace(model.SendTestEmailTo))
                    throw new Exception("Enter test email address");

                string subject = "Testing email functionality.";
                string body = "Email works fine.";
                _emailSender.SendEmailAsync(emailAccount, subject, body, emailAccount.Email, emailAccount.DisplayName, model.SendTestEmailTo, null).RunSynchronously();
            }
            catch (Exception exc)
            {
                
            }
            return Json(true);
        }
        [HttpPost]
        public virtual JsonResult Delete(int id)
        {
            var emailAccount = _emailAccountService.GetEmailAccountById(id);
            if (emailAccount == null)
                return Json(false);

            try
            {
                _emailAccountService.DeleteEmailAccount(emailAccount);
                return Json(true);
            }
            catch (Exception exc)
            {
                return Json(false);
            }
        }
    }
}