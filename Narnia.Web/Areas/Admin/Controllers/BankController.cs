﻿using FastMapper;
using Microsoft.AspNetCore.Mvc;
using Narnia.Business;
using Narnia.Core;
using Narnia.Core.Domain;
using Narnia.Core.Infrastructure;
using Narnia.Core.Models;
using Narnia.Web.Areas.Admin.Models.AjaxDataTables;
using Narnia.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Narnia.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class BankController : BaseAdminController
    {
        private User _currentUser = null;
        private readonly IBanksBiz _banksBiz;

        public BankController(IBanksBiz banksBiz)
        {
            _banksBiz = banksBiz;
            _currentUser = EngineContext.Current.Resolve<IWorkContext>().CurrentUser;
        }

        public IActionResult Index()
        {
            return RedirectToAction("List");
        }

        public IActionResult List()
        {
            return View();
        }

        [HttpPost]
        public JsonResult Loadbank(DataTableAjaxPostModel model, BankSearchCriteria searchCriteria)
        {
            int totalResultsCount = 0;
            var pageSize = model.length;
            var pageIndex = model.start;
            string sortString = model.order != null && model.order.Any() ? model.order[0].column + "-" + model.order[0].dir.ToUpper() : "";

            var resultData =
                _banksBiz.GetBankAdminPagings(pageIndex, pageSize, sortString, out totalResultsCount);
            var result = TypeAdapter.Adapt<List<BankAdminPagingModel>, List<BankAdminPagingViewModel>>(resultData);

            return Json(new DataTableAjaxReturnModel<BankAdminPagingViewModel>()
            {
                draw = model.draw,
                recordsFiltered = totalResultsCount,
                recordsTotal = totalResultsCount,
                data = result
            });
        }

        [HttpGet]
        public ActionResult Add()
        {
            var model = new BankCreateModel();
            return View("Create", model);
        }

        [HttpPost]
        public JsonResult Add(BankCreateModel model)
        {
            if (string.IsNullOrEmpty(model.BankName))
            {
                return Json(new { Error = true, Message = "Chưa nhập tên ngân hàng" });
            }
            if (string.IsNullOrEmpty(model.BankCode))
            {
                return Json(new { Error = true, Message = "Chưa nhập mã ngân hàng" });
            }
            var banksModel = new Banks
            {
                BankName = model.BankName,
                BankCode = model.BankCode,
                Description = model.Description,
                CreatedDate = DateTime.Now,
                IsDeleted = false,
                DeletedDate = (DateTime?)null
            };
            var result = _banksBiz.CreateBank(banksModel);
            if (result.HasError)
            {
                return Json(new { Error = true, Message = result.Message });
            }
            return Json(new { Error = false, Message = "" });
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var model = new BankCreateModel();
            var bank = _banksBiz.GetById(id);
            if (bank == null)
            {
                return View("Edit", model);
            }
            model.BankCode = bank.BankCode;
            model.BankName = bank.BankName;
            model.Description = bank.Description;
            model.Id = bank.Id;
            return View("Edit", model);
        }

        [HttpPost]
        public JsonResult Edit(BankCreateModel model)
        {
            if (string.IsNullOrEmpty(model.BankName))
            {
                return Json(new { Error = true, Message = "Chưa nhập tên ngân hàng" });
            }
            if (string.IsNullOrEmpty(model.BankCode))
            {
                return Json(new { Error = true, Message = "Chưa nhập mã ngân hàng" });
            }
            var bank = new Banks()
            {
                Id = model.Id,
                BankName = model.BankName,
                BankCode = model.BankCode,
                Description = model.Description
            };
            var result = _banksBiz.UpdateBank(bank);
            if (result.HasError)
            {
                return Json(new { Error = true, Message = result.Message });
            }
            return Json(new { Error = false, Message = "" });
        }
    }
}