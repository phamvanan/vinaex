﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Narnia.Core;
using Narnia.Core.Caching;
using Narnia.Core.Infrastructure;
using Narnia.Web.Areas.Admin.Mvc.Attributes;

namespace Narnia.Web.Areas.Admin.Controllers
{
    [AdminAuthorize]
    [Area("Admin")]
    public class HomeController : Controller
    {
        private readonly ICacheManager _cacheManager;
        public HomeController(ICacheManager cacheManager)
        {
            _cacheManager = cacheManager;
        }
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult RestartApp()
        {
            var appLifetime = EngineContext.Current.Resolve<IApplicationLifetime>();
            appLifetime.StopApplication();
            //var webHelper = EngineContext.Current.Resolve<IWebHelper>();
            //webHelper.RestartAppDomain();
            return Ok();
        }
        [HttpPost]
        public virtual IActionResult ClearCache(string returnUrl = "")
        {
            _cacheManager.Clear();

            return Ok();
        }

    }
}