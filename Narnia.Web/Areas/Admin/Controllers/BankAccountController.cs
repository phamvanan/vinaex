﻿using System;
using FastMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Narnia.Business;
using Narnia.Core;
using Narnia.Core.Domain;
using Narnia.Core.Infrastructure;
using Narnia.Core.Models;
using Narnia.Web.Areas.Admin.Models.AjaxDataTables;
using Narnia.Web.Models;
using System.Collections.Generic;
using System.Linq;

namespace Narnia.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class BankAccountController : BaseAdminController
    {
        private User _currentUser = null;
        private readonly IBankAccountBiz _bankAccountBiz;
        private readonly IBanksBiz _banksBiz;

        public BankAccountController(IBankAccountBiz bankAccountBiz,
            IBanksBiz banksBiz)
        {
            _bankAccountBiz = bankAccountBiz;
            _currentUser = EngineContext.Current.Resolve<IWorkContext>().CurrentUser;
            _banksBiz = banksBiz;
        }

        public IActionResult Index()
        {
            return RedirectToAction("List");
        }

        public IActionResult List()
        {
            return View();
        }

        [HttpPost]
        public JsonResult LoadBankAccount(DataTableAjaxPostModel model, BankAccountSearchCriteria searchCriteria)
        {
            int totalResultsCount = 0;
            var pageSize = model.length;
            var pageIndex = model.start;
            string sortString =  model.order != null && model.order.Any() ? model.order[0].column + "-" + model.order[0].dir.ToUpper() : "";

            var resultData =
                _bankAccountBiz.GetBankAccountAdminPagings(pageIndex, pageSize, sortString, out totalResultsCount);
            var result = TypeAdapter.Adapt<List<BankAccountAdminPagingModel>, List<BankAccountAdminPagingViewModel>>(resultData);

            return Json(new DataTableAjaxReturnModel<BankAccountAdminPagingViewModel>()
            {
                draw = model.draw,
                recordsFiltered = totalResultsCount,
                recordsTotal = totalResultsCount,
                data = result
            });
        }

        [HttpGet]
        public ActionResult Add()
        {
            var model = new BankAccountCreateModel();
            var banks = _banksBiz.GetList();
            if (banks != null && banks.Count > 0)
            {
                model.banks = banks.Select(x => new SelectListItem
                {
                    Value = x.Id.ToString(),
                    Text = x.BankName
                }).ToList();
            }
            model.IsActived = true;
            return View("Create", model);
        }

        [HttpPost]
        public JsonResult Add(BankAccountCreateModel model)
        {
            if (model.BankId == 0)
            {
                return Json(new { Error = true, Message = "Chưa chọn ngân hàng" });
            }
            if (string.IsNullOrEmpty(model.AccountName))
            {
                return Json(new { Error = true, Message = "Chưa nhập tên tài khoản" });
            }
            if (string.IsNullOrEmpty(model.AccountNo))
            {
                return Json(new { Error = true, Message = "Chưa nhập số tài khoản" });
            }
            var bankAccount = new BankAccount
            {
                UserId = CommonConst.UserIdAdmin,
                AccountName = model.AccountName,
                BankId = model.BankId,
                AccountNo = model.AccountNo,
                Branch = model.Branch,
                CreatedDate = DateTime.Now,
                IsActived = model.IsActived,
                IsDeleted = false
            };
            var result = _bankAccountBiz.CreateBankAccountAdmin(bankAccount);
            if (result.HasError)
            {
                return Json(new { Error=true,Message=result.Message });
            }
            return Json(new { Error = false, Message = "" });
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var model = new BankAccountCreateModel();
            var bankAccount = _bankAccountBiz.GetById(id);
            var banks = _banksBiz.GetList();
            if (banks != null && banks.Count > 0)
            {
                model.banks = banks.Select(x => new SelectListItem
                {
                    Value = x.Id.ToString(),
                    Text = x.BankName
                }).ToList();
            }
            if (bankAccount == null)
            {
                return View("Edit", model);
            }
            model.BankId = bankAccount.BankId;
            model.bankAccountId = bankAccount.Id;
            model.AccountName = bankAccount.AccountName;
            model.AccountNo = bankAccount.AccountNo;
            model.Branch = bankAccount.Branch;
            model.IsActived = bankAccount.IsActived;
            return View("Edit", model);
        }

        [HttpPost]
        public JsonResult Edit(BankAccountCreateModel model)
        {
            if (model.BankId == 0)
            {
                return Json(new { Error = true, Message = "Chưa chọn ngân hàng" });
            }
            if (string.IsNullOrEmpty(model.AccountName))
            {
                return Json(new { Error = true, Message = "Chưa nhập tên tài khoản" });
            }
            if (string.IsNullOrEmpty(model.AccountNo))
            {
                return Json(new { Error = true, Message = "Chưa nhập số tài khoản" });
            }
            var bankAccount = new BankAccount
            {
                Id = model.bankAccountId,
                AccountName = model.AccountName,
                BankId = model.BankId,
                AccountNo = model.AccountNo,
                Branch = model.Branch,
                IsActived = model.IsActived,
            };
            var result = _bankAccountBiz.UpdateBankAccountAdmin(bankAccount);
            if (result.HasError)
            {
                return Json(new { Error = true, Message = result.Message });
            }
            return Json(new { Error = false, Message = "" });
        }
    }
}