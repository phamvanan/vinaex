﻿using Microsoft.AspNetCore.Mvc;
using Narnia.Core;
using Narnia.Core.Caching;
using Narnia.Core.Infrastructure;
using Narnia.Web.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Web.Areas.Admin.Controllers
{
    public class CommonController:BaseAdminController
    {
        private readonly INarniaFileProvider _fileProvider;
        private readonly IWebHelper _webHelper;
        private readonly IWorkContext _workContext;
        private readonly ICacheManager _cacheManager;

        public CommonController(ICacheManager cacheManager, INarniaFileProvider fileProvider, IWebHelper webHelper, IWorkContext workContext)
        {
            _fileProvider = fileProvider;
            _webHelper = webHelper;
            _workContext = workContext;
            _cacheManager = cacheManager;
        }
        [HttpPost]
        public virtual IActionResult ClearCache(string returnUrl = "")
        {
            _cacheManager.Clear();

            //home page
            if (string.IsNullOrEmpty(returnUrl))
                return RedirectToAction("Index", "Home", new { area = AreaNames.Admin });

            //prevent open redirection attack
            if (!Url.IsLocalUrl(returnUrl))
                return RedirectToAction("Index", "Home", new { area = AreaNames.Admin });

            return Redirect(returnUrl);
        }

        [HttpPost]
        public virtual IActionResult RestartApplication(string returnUrl = "")
        {
            //restart application
            _webHelper.RestartAppDomain();

            //home page
            if (string.IsNullOrEmpty(returnUrl))
                return RedirectToAction("Index", "Home", new { area = AreaNames.Admin });

            //prevent open redirection attack
            if (!Url.IsLocalUrl(returnUrl))
                return RedirectToAction("Index", "Home", new { area = AreaNames.Admin });

            return Redirect(returnUrl);
        }
    }
}
