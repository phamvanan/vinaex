﻿using Microsoft.AspNetCore.Mvc;
using Narnia.Business.Messaging;
using Narnia.Web.Areas.Admin.Extensions;
using Narnia.Web.Areas.Admin.Models.Messaging;
using Narnia.Web.Framework.KendoUI;
using Narnia.Web.Framework.Mvc.Attributes;
using System;
using System.Linq;

namespace Narnia.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class MessageTemplateController: BaseAdminController
    {
        private IMessageTemplateBiz _messageTemplateBiz;
        private readonly IMessageTokenProvider _messageTokenProvider;
        public MessageTemplateController(IMessageTemplateBiz messageTemplateBiz, IMessageTokenProvider messageTokenProvider)
        {
            _messageTemplateBiz = messageTemplateBiz;
            _messageTokenProvider = messageTokenProvider;
        }

        public virtual IActionResult Index()
        {
            return RedirectToAction("List");
        }

        public virtual IActionResult List()
        {
            var model = new MessageTemplateSearchModel();
            model.SetGridPageSize();
            return View(model);
        }

        [HttpPost]
        public JsonResult List(DataSourceRequest command, MessageTemplateSearchModel model)
        {
            var ls = _messageTemplateBiz.GetAllMessageTemplates();
            var dataModel = ls.Select(x => x.ToModel());
            return Json(new { data = dataModel });
        }
        [HttpGet]
        public ActionResult Create()
        {
            var model = new MessageTemplateModel();
            return View(model);
        }
        [HttpPost]
        public virtual IActionResult Create(MessageTemplateModel model)
        {
            if (ModelState.IsValid)
            {
                var messageTemplate = model.ToEntity();
                _messageTemplateBiz.InsertMessageTemplate(messageTemplate);
                return RedirectToAction("List");
            }
            //if we got this far, something failed, redisplay form
            return View(model);
        }

        [HttpGet]
        public ActionResult Edit(int Id)
        {
            var currentMessageTemplate = _messageTemplateBiz.GetMessageTemplateById(Id);
            if (currentMessageTemplate == null)
            {
                return Json("Cannot retreive data");
            }
            var model = currentMessageTemplate.ToModel();

            var allowedTokens = string.Join(", ", _messageTokenProvider.GetListOfAllowedTokens(_messageTokenProvider.GetTokenGroups(currentMessageTemplate)));
            model.AllowedTokens = $"{allowedTokens}{Environment.NewLine}{Environment.NewLine}" +
                $"All tokens{Environment.NewLine}";
            return View("Edit", model);
        }
        [HttpPost]
        [FormValueRequired("save")]
        public ActionResult Edit(MessageTemplateModel model)
        {
            var currentMessageTemplate = _messageTemplateBiz.GetMessageTemplateById(model.Id);

            if (string.IsNullOrEmpty(model.Body))
                model.Body = string.Empty;
            currentMessageTemplate.Body = model.Body;
            currentMessageTemplate.Subject = model.Subject;

            _messageTemplateBiz.UpdateMessageTemplate(currentMessageTemplate);

            // return Json(true);
            return RedirectToAction("List");
        }

        [HttpPost]
        public virtual IActionResult Delete(int id)
        {
            var currentMessageTemplate = _messageTemplateBiz.GetMessageTemplateById(id);
            if (currentMessageTemplate != null)
                _messageTemplateBiz.DeleteMessageTemplate(currentMessageTemplate);
            return RedirectToAction("List");
        }

        [HttpPost, ActionName("Edit")]
        public virtual IActionResult CopyTemplate(MessageTemplateModel model)
        {
            return View();
        }

        public virtual IActionResult TestTemplate(int id)
        {
            var currentMessageTemplate = _messageTemplateBiz.GetMessageTemplateById(id);
            var model = currentMessageTemplate.ToModel();
            return View(model);
        }

        [HttpPost, ActionName("TestTemplate")]
        public virtual IActionResult TestTemplate(MessageTemplateModel model)
        {
            return View(model);
        }

    }
}
