﻿using FastMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Narnia.Business.Impl;
using Narnia.Business.Transactions;
using Narnia.Core.Domain.Payments;
using Narnia.Core.Enums;
using Narnia.Web.Areas.Admin.Extensions;
using Narnia.Web.Areas.Admin.Models.Payments;
using Narnia.Web.Framework.KendoUI;
using Narnia.Web.Framework.Models;
using Narnia.Web.Framework.Mvc.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Web.Areas.Admin.Controllers
{
    public class GatewayController : BaseAdminController
    {
        private IPaymentBiz _paymentBiz;
        private IAssetTypeBusiness _assetTypeBiz;
        public GatewayController(IPaymentBiz paymentBiz, IAssetTypeBusiness assetTypeBiz)
        {
            _paymentBiz = paymentBiz;
            _assetTypeBiz = assetTypeBiz;
        }

        public virtual IActionResult Index()
        {
            return RedirectToAction("List");
        }

        public virtual IActionResult List()
        {
            GatewaySearchModel model = new GatewaySearchModel();
            return View(model);

        }

        [HttpPost]
        public virtual JsonResult List(DataSourceRequest dataSourceRequest)
        {
            var allGateways = _paymentBiz.GetAllGateways();
            
            var dataModel = allGateways.Select(x => x.ToModel());
            return Json(new { data = dataModel });
        }


        [HttpGet]
        public ActionResult Create()
        {
            var model = new GatewayModel();
            return View(model);
        }
        [HttpPost]
        public virtual IActionResult Create(GatewayModel model)
        {
            if (ModelState.IsValid)
            {
                var gateway = model.ToEntity();
                _paymentBiz.AddGateway(gateway);
                return RedirectToAction("List");
            }
            //if we got this far, something failed, redisplay form
            return View(model);
        }

        [HttpGet]
        public ActionResult Edit(int Id)
        {
            var currentGateway = _paymentBiz.GetGatewayById(Id);
            if (currentGateway == null)
            {
                return Json("Cannot retreive data");
            }
            var model = currentGateway.ToModel();

            return View("Edit", model);
        }
        [HttpPost]
        [FormValueRequired("save")]
        public ActionResult Edit(GatewayModel model)
        {
            var currentGateway = _paymentBiz.GetGatewayById(model.Id);

            currentGateway.Description = model.Description;
            currentGateway.GatewayFriendlyName = model.GatewayFriendlyName;

            _paymentBiz.UpdateGateway(currentGateway);

            return RedirectToAction("List");
        }

        [HttpPost]
        public virtual IActionResult Delete(int id)
        {
            var currentGateway = _paymentBiz.GetGatewayById(id);
            if (currentGateway != null)
                _paymentBiz.DeleteGateway(currentGateway);
            return RedirectToAction("List");
        }
    }
}
