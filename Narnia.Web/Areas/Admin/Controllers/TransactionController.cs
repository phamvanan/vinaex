﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using FastMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Narnia.Business;
using Narnia.Core;
using Narnia.Core.Domain;
using Narnia.Core.Enums;
using Narnia.Core.Infrastructure;
using Narnia.Core.Models;
using Narnia.Web.Areas.Admin.Controllers;
using Narnia.Web.Areas.Admin.Models.AjaxDataTables;
using Narnia.Web.Areas.Admin.Models.Transactions;
using Narnia.Web.Framework.Models;
using Narnia.Web.Models;

namespace BitCommunity.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class TransactionController : BaseAdminController
    {
        private readonly ITransactionBiz _transactionBiz;
        private readonly IEvidenceBiz _evidenceBiz;
        private User _currentUser = null;
        private int _userIdDefault = 0;

        public TransactionController(ITransactionBiz transactionBiz, IEvidenceBiz evidenceBiz)
        {
            _transactionBiz = transactionBiz;
            _evidenceBiz = evidenceBiz;
            _currentUser = EngineContext.Current.Resolve<IWorkContext>().CurrentUser;
        }
        public IActionResult Index()
        {
            return RedirectToAction("List");
        }

        public ActionResult List()
        {
            var model = new TransactionSearchModel
            {
            };
            model.AvailableAssetTypeIds.Add(new SelectListItem { Text = "All", Value = "" });

            IList<SelectListItem> AssetTypeIds = Enum.GetValues(typeof(AssetTypeEnum)).Cast<AssetTypeEnum>().
                                    Select(x => new SelectListItem { Text = x.ToString(), Value = x.ToString() }).ToList();
            model.AvailableAssetTypeIds.AddRange(AssetTypeIds);
            model.AvailableStatuses.Add(new SelectListItem { Text = "All", Value = "0" });

            IList<SelectListItem> Transtatus = Enum.GetValues(typeof(TransactionStautus)).Cast<TransactionStautus>().
        Select(x => new SelectListItem { Text = x.ToString(), Value = ((int)x).ToString() }).ToList();
            model.AvailableStatuses.AddRange(Transtatus);

            return View(model);
        }
        [HttpPost]
        public virtual IActionResult List(TransactionSearchModel searchModel)
        {
            var transactions = _transactionBiz.SearchTransactions(searchModel.StartDate, searchModel.EndDate, searchModel.TransactionCode,
                searchModel.Statuses,searchModel.AssetTypeIds,searchModel.UserName, searchModel.Page - 1, searchModel.PageSize, true);

            var model = new PagedListModel<TransactionModel>
            {
                Data = transactions.Data.Select(t => {
                    var tranModel = TypeAdapter.Adapt<Transaction, TransactionModel>(t);
                    if(t.BuyAdvertisement != null)
                    {
                        tranModel.Seller = t.User.UserName;
                        tranModel.Buyer = t.BuyAdvertisement.AdvertiseUser.UserName;
                    }
                    else if(t.SellAdvertisement != null)
                    {
                        tranModel.Buyer = t.User.UserName;
                        tranModel.Seller = t.SellAdvertisement.AdvertiseUser.UserName;
                    }
                    return tranModel;
                }),
                Total = transactions.TotalCount
            };

            return Json(model);
        }

        #region Edit, Delete
        [HttpGet]
        public virtual IActionResult Edit(int id)
        {
            var response = _transactionBiz.GetTransactionById(id);
            if (response.HasError)
                return RedirectToAction("List");
            var model = TypeAdapter.Adapt<Transaction, TransactionModel>(response.Data);
            if (response.Data.BuyAdvertisement != null)
            {
                model.Seller = response.Data.User.UserName;
                model.Buyer = response.Data.BuyAdvertisement.AdvertiseUser.UserName;
            }
            else if (response.Data.SellAdvertisement != null)
            {
                model.Buyer = response.Data.User.UserName;
                model.Seller = response.Data.SellAdvertisement.AdvertiseUser.UserName;
            }
            //Evidence
            if (model.EvidenceId.HasValue)
            {
                var evidenceEntity = _evidenceBiz.GetById(model.EvidenceId.Value);
                if (evidenceEntity != null)
                {
                    model.EvidenceCode = evidenceEntity.Code;
                }
            }

            //TransactionStatus for changeStatus
            if (model.CanChangeStatus)
            {
                model.ValuesValidToExclude = _transactionBiz.GetValuesValidToExclude(model.Status);
            }
            return View(model);
        }

        [HttpPost]
        public virtual IActionResult Edit(TransactionModel model)
        {
            var entityInput = TypeAdapter.Adapt<TransactionModel,Transaction>(model);

            entityInput.ConfirmBy = _currentUser?.Id ?? _userIdDefault;
            var result = _transactionBiz.AdminConfirm(entityInput);
            if (result.HasError)
            {
                TempData["error"] = result.Message;
            }
            else
            {
                TempData["success"] = CommonConst.UpdateTransactionSuccess;
            }
            return RedirectToAction("Edit", new {id = model.TransactionId});
        }
        #endregion

        [HttpPost]
        public JsonResult LoadTransactiondData(DataTableAjaxPostModel model, TransactionSearchCriteria searchCriteria)
        {
            int totalResultsCount = 0;
            var pageSize = model.length;
            var pageIndex = model.start;
            String sortString = model.order != null && model.order.Any() ? model.order[0].column+"-"+model.order[0].dir.ToUpper() : "";


            var resultData = _transactionBiz.SearchAdmin(pageIndex,pageSize,searchCriteria.TransMode,
                                                        searchCriteria.AssetTypeId,searchCriteria.Status,
                                                        sortString,searchCriteria.FromDate,searchCriteria.ToDate,searchCriteria.TransactionCode, out totalResultsCount);
            var result = TypeAdapter.Adapt<List<TransactionAdminSearchModel>, List<TransactionAdminSearchViewModel>>(resultData);

            return Json(new DataTableAjaxReturnModel<TransactionAdminSearchViewModel>()
            {
                draw = model.draw,
                recordsFiltered = totalResultsCount,
                recordsTotal = totalResultsCount,
                data = result
            });
        }

        [HttpGet]
        public IActionResult TransactionDetail(int transactionId)
        {
            var resultData = _transactionBiz.GetDetailAdmin(transactionId);
            if (resultData == null && resultData.TransactionId == 0) return  Json("Cannot retreive data");
            var result = TypeAdapter.Adapt<TransactionDetailAdminModel, TransactionDetailAdminViewModel>(resultData);
            return View("_TransactionDetail", result);
        }
        public FileStreamResult GetImage(string code)
        {
            var result = _evidenceBiz.GetByCode(code);
            if (result == null)
            {
                return null;
            }
            byte[] imgBytes = result.Image;

            Stream stream = new MemoryStream(result.Image);
            return new FileStreamResult(stream, result.ContentType);
        }

        public async Task<IActionResult> Download(string code)
        {
            if (string.IsNullOrEmpty(code))
                return Content("filename not present");

            var result = _evidenceBiz.GetByCode(code);
            if (result == null)
            {
                return Content("filename not present");
            }

            byte[] imgBytes = result.Image;

            Stream stream = new MemoryStream(result.Image);

            var memory = new MemoryStream();
            memory.Position = 0;
            return File(memory, result.ContentType, result.FileName);
        }
    }
}