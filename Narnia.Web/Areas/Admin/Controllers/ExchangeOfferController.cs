﻿using FastMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Narnia.Business;
using Narnia.Business.Impl;
using Narnia.Core.Domain;
using Narnia.Core.Enums;
using Narnia.Web.Areas.Admin.Models.ExchangeOffers;
using Narnia.Web.Areas.Admin.Models.Offers;
using Narnia.Web.Framework.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Web.Areas.Admin.Controllers
{
    public class ExchangeOfferController: BaseAdminController
    {
        private readonly IAssetTypeBusiness _assetTypeBiz;
        private readonly IExchangeOfferBusiness _exchangeOfferBiz;
        public ExchangeOfferController(IAssetTypeBusiness assetTypeBiz, IExchangeOfferBusiness exchangeOfferBiz)
        {
            _assetTypeBiz = assetTypeBiz;
            _exchangeOfferBiz = exchangeOfferBiz;
        }
        public virtual IActionResult Index(string type = "sell")
        {
            return RedirectToAction("List", new { type = type});
        }

        public virtual IActionResult List(string type = "sell")
        {
            var model = new ExchangeOfferSearchModel
            {
                Type = type
            };
            model.AvailableAssetTypeIds.Add(new SelectListItem { Text = "All", Value = "all" });
            var allAssetTypes = _assetTypeBiz.GetAll();
            foreach (var assetType in allAssetTypes)
            {
                model.AvailableAssetTypeIds.Add(new SelectListItem { Text = assetType.AssetTypeId, Value = assetType.AssetTypeId });
            }

            model.AvailableStatuses.Add(new SelectListItem { Text = "All", Value = "0" });
            IList<SelectListItem> statuses = Enum.GetValues(typeof(ExchangeOfferStatus)).Cast<ExchangeOfferStatus>().
                        Select(x => new SelectListItem { Text = x.ToString(), Value = ((int)x).ToString() }).ToList();
            model.AvailableStatuses.AddRange(statuses);

            return View(model);
        }

        [HttpPost]
        public virtual IActionResult List(ExchangeOfferSearchModel searchModel)
        {
            PagedListModel<ExchangeOfferModel> model;
            var offers = _exchangeOfferBiz.SearchExchangeOffers(searchModel.StartDate, searchModel.EndDate,
                        searchModel.Statuses, searchModel.AssetTypeIds, searchModel.Code, searchModel.Type == "sell" ? (int)ExchangeOfferType.Sell : (int)ExchangeOfferType.Buy, 
                        searchModel.Page - 1, searchModel.PageSize, true);

            model = new PagedListModel<ExchangeOfferModel>
            {
                Data = offers.Data.Select(s =>
                {
                    var item = TypeAdapter.Adapt<ExchangeOffer, ExchangeOfferModel>(s);
                    item.StatusString = ((ExchangeOfferStatus)item.Status).ToString();
                    return item;
                }),
                Total = offers.TotalCount
            };

            return Json(model);
        }

        [HttpPost]
        public JsonResult ApproveOffer(int Id)
        {
            bool success = false;
            try
            {
                var currentItem = _exchangeOfferBiz.GetById(Id);
                if (currentItem != null && currentItem.Status == (int)ExchangeOfferStatus.Pending)
                {
                    currentItem.Status = (int)ExchangeOfferStatus.Complete;
                    success = _exchangeOfferBiz.Update(currentItem);
                }
            }
            catch { }

            return Json(success);
        }

        [HttpPost]
        public JsonResult RejectOffer(int Id)
        {
            bool success = false;
            try
            {
                var currentItem = _exchangeOfferBiz.GetById(Id);
                if (currentItem != null && currentItem.Status == (int)ExchangeOfferStatus.Pending)
                {
                    currentItem.Status = (int)ExchangeOfferStatus.Cancel;
                    success = _exchangeOfferBiz.Update(currentItem);
                }
            }
            catch { }

            return Json(success);
        }
    }
}
