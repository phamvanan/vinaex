﻿using System;
using System.Collections.Generic;
using System.Linq;
using FastMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Narnia.Business.Impl;
using Narnia.Business.Transactions;
using Narnia.Core.Domain;
using Narnia.Core.Domain.Payments;
using Narnia.Core.Enums;
using Narnia.Web.Areas.Admin.Models.Deposits;
using Narnia.Web.Framework.Models;

namespace Narnia.Web.Areas.Admin.Controllers
{
    public class DepositController : BaseAdminController
    {
        private readonly IPaymentBiz _paymentBiz;
        private IAssetTypeBusiness _assetTypeBiz;
        public DepositController(IPaymentBiz paymentBiz, IAssetTypeBusiness assetTypeBiz)
        {
            _paymentBiz = paymentBiz;
            _assetTypeBiz = assetTypeBiz;
        }
       
        public virtual IActionResult Index()
        {
            return RedirectToAction("List");
        }

        public virtual IActionResult List()
        {
            var model = new DepositSearchModel();

            model.AvailableAssetTypeIds.Add(new SelectListItem { Text = "All", Value = "all" });

            var allAssetTypes = _assetTypeBiz.GetAll();
            foreach(var assetType in allAssetTypes)
            {
                model.AvailableAssetTypeIds.Add(new SelectListItem { Text = assetType.AssetTypeId, Value = assetType.AssetTypeId });
            }

            model.AvailableStatuses.Add(new SelectListItem { Text = "All", Value = "0" });

            IList<SelectListItem> depositStatuses = Enum.GetValues(typeof(DepositStatus)).Cast<DepositStatus>().
        Select(x => new SelectListItem { Text = x.ToString(), Value = ((int)x).ToString() }).ToList();
            model.AvailableStatuses.AddRange(depositStatuses);

            return View(model);
        }

        [HttpPost]
        public virtual IActionResult List(DepositSearchModel searchModel)
        {
            PagedListModel<DepositModel> model;
            if(searchModel.AssetTypeIds != null)
            {
                searchModel.AssetTypeIds.RemoveAll(item => item == null);
            }
          
            var deposit = _paymentBiz.SearchDeposits(searchModel.StartDate, searchModel.EndDate,searchModel.UserName,
                        searchModel.AssetTypeIds, searchModel.Statuses,searchModel.Page - 1, searchModel.PageSize);

            model = new PagedListModel<DepositModel>
            {
                Data = deposit.Data.Select(s =>
                {
                    var depositModel = TypeAdapter.Adapt<Deposit, DepositModel>(s);                    
                    return depositModel;
                }),
                Total = deposit.TotalCount
            };

            return Json(model);
        }
    }
}