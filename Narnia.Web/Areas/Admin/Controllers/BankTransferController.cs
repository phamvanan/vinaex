﻿using FastMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Narnia.Business.Impl;
using Narnia.Business.Multimedia;
using Narnia.Business.Transactions;
using Narnia.Core;
using Narnia.Core.Domain;
using Narnia.Core.Domain.Payments;
using Narnia.Core.Enums;
using Narnia.Web.Areas.Admin.Extensions;
using Narnia.Web.Areas.Admin.Models.Payments;
using Narnia.Web.Framework.KendoUI;
using Narnia.Web.Framework.Models;
using Narnia.Web.Framework.Mvc.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Web.Areas.Admin.Controllers
{
    public class BankTransferController : BaseAdminController
    {
        private readonly IPictureService _pictureService;
        private readonly IPaymentBiz _paymentBiz;
        private readonly IAssetTypeBusiness _assetTypeBiz;
        private readonly IWorkContext _workContext;
        public BankTransferController(IPaymentBiz paymentBiz, IAssetTypeBusiness assetTypeBiz, IPictureService pictureService, IWorkContext workContext)
        {
            _paymentBiz = paymentBiz;
            _assetTypeBiz = assetTypeBiz;
            _pictureService = pictureService;
            _workContext = workContext;
        }

        public virtual IActionResult Index()
        {
            return RedirectToAction("List");
        }

        public virtual IActionResult List()
        {
            var model = new BankTransferSearchModel();
            model.AvailableAssetTypeIds.Add(new SelectListItem { Text = "All", Value = "all" });

            var allAssetTypes = _assetTypeBiz.GetAll();
            foreach (var assetType in allAssetTypes)
            {
                model.AvailableAssetTypeIds.Add(new SelectListItem { Text = assetType.AssetTypeId, Value = assetType.AssetTypeId });
            }

            model.AvailableStatuses.Add(new SelectListItem { Text = "All", Value = "0" });

            IList<SelectListItem> depositStatuses = Enum.GetValues(typeof(BankTransferStatus)).Cast<BankTransferStatus>().
        Select(x => new SelectListItem { Text = x.ToString(), Value = ((int)x).ToString() }).ToList();
            model.AvailableStatuses.AddRange(depositStatuses);
            return View(model);

        }

        [HttpPost]
        public virtual IActionResult List(BankTransferSearchModel searchModel)
        {
            if (searchModel.AssetTypeIds != null)
            {
                searchModel.AssetTypeIds.RemoveAll(item => item == null);
            }

            var bankDeposits = _paymentBiz.SearchBankTransfers(searchModel.StartDate,searchModel.EndDate,searchModel.UserName,searchModel.BillingDate,
                searchModel.AssetTypeIds,searchModel.Statuses, searchModel.Page - 1, searchModel.PageSize, true);

            var model = new PagedListModel<BankTransferModel>
            {
                Data = bankDeposits.Data.Select(t => {
                    var gatewayAcc = TypeAdapter.Adapt<BankTransfer, BankTransferModel>(t);
                    return gatewayAcc;
                }),
                Total = bankDeposits.TotalCount
            };

            return Json(model);
        }


        [HttpGet]
        public ActionResult Edit(int Id)
        {
            var bankTransfer = _paymentBiz.GetBankTransferById(Id);
            if (bankTransfer == null)
            {
                return Json("Cannot retreive data");
            }
            var model = bankTransfer.ToModel();
            if (model.EvidencePictureId.HasValue && model.EvidencePictureId.Value > 0)
                model.EvidencePictureUrl = _pictureService.GetPictureUrl(model.EvidencePictureId.Value);
            else
                model.EvidencePictureUrl = _pictureService.GetDefaultPictureUrl();
            return View("Edit", model);
        }
        [HttpPost,ActionName("Edit")]
        [FormValueRequired("cancel-payment")]
        public ActionResult CancelPayment(BankTransferModel model)
        {
            var response = _paymentBiz.CancelBankDeposit(model.Id);
            if (response.HasError)
            {
                ModelState.AddModelError("Error_1", response.Message);
            }
            return RedirectToAction("List");
        }
        [HttpPost,ActionName("Edit")]
        [FormValueRequired("complete-payment")]
        public ActionResult CompletePayment(BankTransferModel model)
        {
            var response = _paymentBiz.CompleteBankDeposit(model.Id);
            if(response.HasError)
            {
                ModelState.AddModelError("Error_1", response.Message);
            }
            return RedirectToAction("List");
        }
    }
}
