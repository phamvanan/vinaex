﻿using Microsoft.AspNetCore.Mvc;
using Narnia.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Web.Areas.Admin.Controllers
{
    public class SecurityController: BaseAdminController
    {
        private readonly IWorkContext _workContext;

        public SecurityController(IWorkContext workContext)
        {
            this._workContext = workContext;
        }
        public virtual IActionResult AccessDenied(string pageUrl)
        {
            return View();
        }
    }
}
