﻿using Microsoft.AspNetCore.Mvc;
using Narnia.Web.Areas.Admin.Models;
using Narnia.Web.Areas.Admin.Models.Account;
using Narnia.Business.Impl;
using FastMapper;
using Narnia.Core.Domain;
using System.Collections.Generic;
using Narnia.Web.Areas.Admin.Mvc.Attributes;
using Narnia.Core.Models;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using Narnia.Business;
using Narnia.Web.Models;
using Narnia.Web.Framework.Models;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Narnia.Core;
using Narnia.Core.Infrastructure;
using Narnia.Web.Extensions;

namespace Narnia.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class AccountController : Controller
    {
        private readonly IUserBusiness _userBusiness;
        private readonly IBankAccountBiz _bankAccountBiz;
        public AccountController(IUserBusiness userBusiness, IBankAccountBiz bankAccountBiz)
        {
            _userBusiness = userBusiness;
            _bankAccountBiz = bankAccountBiz;
        }

        public IActionResult Index()
        {
            return RedirectToAction("List");
        }

        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            if (HttpContext.Session.GetObject<AdminUserInfo>("AccAdmin") != null)
            {
                return RedirectToAction("Index", "Home", new {area = "Admin"});
            }

            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Login(UserLoginViewModel model)
        {
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", "Username or password is not valid!");

                return View(model);
            }

            try
            {
                var resultResponse = _userBusiness.LoginAdmin(model.Username, model.Password);
                if (resultResponse.HasError)
                {
                    TempData["error"] = resultResponse.Message;
                    return View(model);
                }

                var adminInfo = new AdminUserInfo
                {
                    Username = model.Username,
                    Role = AdminType.Admin
                };
                HttpContext.Session.SetObject("AccAdmin", adminInfo);
            }
            catch (Exception ex)
            {
                return View(model);
            }

            return RedirectToAction("Index", "Home", new { area = "Admin" });
        }

        [HttpGet]
        [AdminAuthorize]
        public IActionResult ChangePassword()
        {
            try
            {
                var vm = new ChangePassViewModel();
                var userInfo = HttpContext.Session.GetObject<AdminUserInfo>("AccAdmin");
                vm.UserName = userInfo != null ? userInfo.Username : "";

                return View(vm);
            }
            catch (Exception ex)
            {
                return View(new ChangePassViewModel());
            }
        }

        [AdminAuthorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult ChangePassword(ChangePassViewModel model)
        {
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", "Data is not valid!");
                return View(model);
            }

            try
            {
                var user = new User()
                {
                    UserName = model.UserName.Trim(),
                    PasswordHash = model.NewPassWord.Trim()
                };

                var changePassword = _userBusiness.ChangePassword(user);
                if (changePassword.HasError)
                {
                    TempData["error"] = changePassword.Message;
                }
                else
                {
                    var adminInfo = new AdminUserInfo
                    {
                        Username = model.UserName,
                        Role = AdminType.Admin
                    };
                    HttpContext.Session.SetObject("AccAdmin", adminInfo);

                    TempData["success"] = "Cập nhật mật khẩu thành công.";
                    return RedirectToAction("Index", "Home", new {area = "Admin"});
                }
            }
            catch (Exception ex)
            {
                TempData["error"] = CommonConst.HasErrPleaseContactAdmin;
            }

            return View(model);
        }

        public IActionResult Logout()
        {
            HttpContext.Session.Remove("AccAdmin");
            return RedirectToAction("Login", "Account", new { area = "Admin" });
        }

        [AdminAuthorize]
        public IActionResult List()
        {
            var model = new UserSearchModel();
            model.SetGridPageSize();
            return View(model);
        }
        [AdminAuthorize]
        [HttpPost]
        public virtual IActionResult List(UserSearchModel searchModel)
        {
            var commissions = _userBusiness.FindUsers(searchModel.StartDate, searchModel.EndDate,searchModel.UserName,searchModel.PhoneNumber, searchModel.Page - 1, searchModel.PageSize, true);

            var model = new PagedListModel<UserModel>
            {
                Data = commissions.Data.Select(c => {
                    return new UserModel
                    {
                        Id = c.Id,
                        Email = c.Email,
                        UserName = c.UserName,
                        Phone = c.Phone,
                        CreatedDate = c.CreatedDate,
                        IDNumber = c.UserInfo != null ? c.UserInfo.IDNumber : string.Empty,
                        ReferenceCode = c.ReferenceCode,
                        IsVerify = c.IsVerify,
                       // VerifyDisplay = c.IsVerify ? "Yes" : "No"
                    };
                }),
                Total = commissions.TotalCount
            };

            return Json(model);
        }

        [AdminAuthorize]
        [HttpGet]
        public IActionResult Edit(int Id)
        {
            List<User> allUplines = null;
            var user = _userBusiness.GetUserById(Id);
            if(user.UplineId > 0)
            {
                allUplines = _userBusiness.GetUplineUsers(Id);
            }
            if (user == null)
                return RedirectToAction("List");
            var userModel = TypeAdapter.Adapt<User, UserModel>(user);
            
            userModel.UserAssetBalanceSearchModel.UserId = Id;
            userModel.UserAssetBalanceSearchModel.SetGridPageSize();

            userModel.UserDepositSearchModel.UserId = Id;
            userModel.UserDepositSearchModel.SetGridPageSize();

            userModel.UserWithdrawalSearchModel.UserId = Id;
            userModel.UserWithdrawalSearchModel.SetGridPageSize();

            userModel.UserLoginHistorySearchModel.UserId = Id;
            userModel.UserLoginHistorySearchModel.SetGridPageSize();

            userModel.CommissionHistorySearchModel.UserId = Id;
            userModel.CommissionHistorySearchModel.SetGridPageSize();

            if(allUplines != null && allUplines.Count > 0)
            {
                userModel.UplineUsers = TypeAdapter.Adapt<List<User>, List<UserAbbrModel>>(allUplines);
                userModel.UplineUsers.Reverse();
            }
            return View(userModel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Edit(UserInforModel model)
        {
            try
            {
                bool result = _userBusiness.UpdateVerify(model.UserId);
                if (result)
                {
                    return Json(true);
                }

                return Json(false);
            }
            catch (Exception ex)
            {
                return Json(false);
            }
        }

        [AdminAuthorize]
        [HttpPost]
        public IActionResult VerifyEvidence(UserInforModel model)
        {
            try
            {
                bool result = _userBusiness.UpdateVerify(model.UserId);
                if (result)
                {
                    TempData["success"] = "Verify was successfully!";
                }
                else
                {
                    TempData["warning"] = "Verify was unsuccessfully!";
                }

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index");
            }
        }

        [AdminAuthorize]
        [HttpGet]
        public IActionResult VerifyEvidence(int userId)
        {
            var user = _userBusiness.GetUserInfor(userId);
            if (user == null)
                return RedirectToAction("List");
            var userModel = TypeAdapter.Adapt<UserInforModel, UserModel>(user);

            return View(userModel);
        }

        [AdminAuthorize]
        [HttpGet]
        public IActionResult GetBanksAccount(int userId)
        {
            var listBanks = _bankAccountBiz.GetListByUserId(userId);
            var model = TypeAdapter.Adapt<List<BankAccountModel>, List<BankAccountViewModel>>(listBanks);
            return View(model);
        }

        public FileStreamResult GetFileThumbnail(int userId, int width, int height)
        {
            var user = _userBusiness.GetUserInfor(userId);
            if (user == null)
            {
                return null;
            }
            byte[] imgBytes = getThumbNail(user.Image, width, height);

            Stream stream = new MemoryStream(imgBytes);
            return new FileStreamResult(stream, user.ContentType);
        }

        private byte[] getThumbNail(byte[] data, int width, int height)
        {
            using (var file = new MemoryStream(data))
            {
                using (var image = Image.FromStream(file, true, true)) /* Creates Image from specified data stream */
                {
                    using (var thumb = image.GetThumbnailImage(width, height, () => false, IntPtr.Zero))
                    {
                        var jpgInfo = ImageCodecInfo.GetImageEncoders()
                            .Where(codecInfo => codecInfo.MimeType == "image/png").First();
                        using (var encParams = new EncoderParameters(1))
                        {
                            using (var samllfile = new MemoryStream())
                            {
                                long quality = 100;
                                encParams.Param[0] = new EncoderParameter(Encoder.Quality, quality);
                                thumb.Save(samllfile, jpgInfo, encParams);
                                return samllfile.ToArray();
                            }
                        };
                    };
                };
            };
        }
    }
}