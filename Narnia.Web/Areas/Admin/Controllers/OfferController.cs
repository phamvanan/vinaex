﻿using FastMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Narnia.Business;
using Narnia.Core.Domain;
using Narnia.Core.Enums;
using Narnia.Web.Areas.Admin.Models.Offers;
using Narnia.Web.Framework.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Web.Areas.Admin.Controllers
{
    public class OfferController: BaseAdminController
    {
        private readonly ISellAdvertisementBiz _sellAdvertisementBiz;
        private readonly IBuyAdvertisementBiz _buyAdvertisementBiz;
        public OfferController(ISellAdvertisementBiz sellAdvertisementBiz, IBuyAdvertisementBiz buyAdvertisementBiz)
        {
            _buyAdvertisementBiz = buyAdvertisementBiz;
            _sellAdvertisementBiz = sellAdvertisementBiz;
        }
        public virtual IActionResult Index(string type = "sell")
        {
            return RedirectToAction("List", new { type = type});
        }

        public virtual IActionResult List(string type = "sell")
        {
            var model = new OfferSearchModel
            {
                Type = !string.IsNullOrEmpty(type) && type.Trim().ToLower() == "buy" ? "buy" : "sell"
            };
            model.AvailableAssetTypeIds.Add(new SelectListItem { Text = "All", Value = "" });

            IList<SelectListItem> AssetTypeIds = Enum.GetValues(typeof(AssetTypeEnum)).Cast<AssetTypeEnum>().
                                    Select(x => new SelectListItem { Text = x.ToString(), Value = x.ToString() }).ToList();
            model.AvailableAssetTypeIds.AddRange(AssetTypeIds);
            model.AvailableStatuses.Add(new SelectListItem { Text = "All", Value = "0" });

            IList<SelectListItem> Transtatus = Enum.GetValues(typeof(AdvertisementStatus)).Cast<AdvertisementStatus>().
        Select(x => new SelectListItem { Text = x.ToString(), Value = ((int)x).ToString() }).ToList();
            model.AvailableStatuses.AddRange(Transtatus);

            return View(model);
        }

        [HttpPost]
        public virtual IActionResult List(OfferSearchModel searchModel)
        {
            PagedListModel<OfferModel> model;

            if (searchModel.Type == "sell")
            {
                var advertisments = _sellAdvertisementBiz.SearchAdvertisments(searchModel.StartDate, searchModel.EndDate,
                        searchModel.Statuses, searchModel.AssetTypeIds, searchModel.UserName, searchModel.Page - 1, searchModel.PageSize, true);

                model = new PagedListModel<OfferModel>
                {
                    Data = advertisments.Data.Select(s =>
                    {
                        var sellAdModel = TypeAdapter.Adapt<SellAdvertisement, OfferModel>(s);
                        sellAdModel.AmountProcessed = s.AmountSell;
                        return sellAdModel;
                    }),
                    Total = advertisments.TotalCount
                }; 
            }
            else
            {
                var advertisments = _buyAdvertisementBiz.SearchAdvertisments(searchModel.StartDate, searchModel.EndDate,
                searchModel.Statuses, searchModel.AssetTypeIds, searchModel.UserName, searchModel.Page - 1, searchModel.PageSize, true);

                model = new PagedListModel<OfferModel>
                {
                    Data = advertisments.Data.Select(s => {
                        var sellAdModel = TypeAdapter.Adapt<BuyAdvertisement, OfferModel>(s);
                        sellAdModel.AmountProcessed = s.AmountBuy.GetValueOrDefault(0);
                        return sellAdModel;
                    }),
                    Total = advertisments.TotalCount
                };
            }
            return Json(model);
        }

        [HttpPost]
        public JsonResult ApproveAdvertisement(int Id, string type)
        {
            bool isSuccess = false;
            try
            {
                if (!string.IsNullOrWhiteSpace(type) && type.Trim().ToLower() == "sell")
                {
                    var sellInfo = _sellAdvertisementBiz.GetById(Id);
                    if (sellInfo != null)
                    {
                        sellInfo.Approved = true;
                        isSuccess = _sellAdvertisementBiz.UpdateSellAdvertisement(sellInfo);
                    } 
                }
                else
                {
                    var buyInfo = _buyAdvertisementBiz.GetBuyAdvertisementById(Id);
                    if (buyInfo != null)
                    {
                        buyInfo.Approved = true;
                        isSuccess = _buyAdvertisementBiz.UpdateBuyAdvertisement(buyInfo);
                    }
                }
            }
            catch { }

            return Json(isSuccess);
        }
    }
}
