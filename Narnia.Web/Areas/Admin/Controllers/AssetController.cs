﻿using FastMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Narnia.Business;
using Narnia.Business.Impl;
using Narnia.Core.Domain;
using Narnia.Web.Areas.Admin.Models.AjaxDataTables;
using Narnia.Web.Areas.Admin.Models.Asset;
using Narnia.Web.Framework.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class AssetController : BaseAdminController
    {
        private readonly ICommonBiz _commonBiz;
        private readonly IAssetTypeBusiness _assetTypeBiz;
        public AssetController(ICommonBiz commonBiz, IAssetTypeBusiness assetTypeBiz)
        {
            _commonBiz = commonBiz;
            _assetTypeBiz = assetTypeBiz;
        }

        public virtual IActionResult Index()
        {
            return RedirectToAction("List");
        }

        public virtual IActionResult List()
        {
            var model = new AssetBalanceSearchModel();

            model.AvailableAssetTypeIds.Add(new SelectListItem { Text = "All", Value = "all" });
            var allAssetTypes = _assetTypeBiz.GetAll();
            foreach (var assetType in allAssetTypes)
            {
                model.AvailableAssetTypeIds.Add(new SelectListItem { Text = assetType.AssetTypeId, Value = assetType.AssetTypeId });
            }
            return View(model);
        }

        [HttpPost]
        public virtual IActionResult List(AssetBalanceSearchModel searchModel)
        {
            PagedListModel<AssetBalanceModel> model;

            if (searchModel.AssetTypeIds != null)
            {
                searchModel.AssetTypeIds.RemoveAll(item => item == null);
            }

            var deposit = _commonBiz.FindAssetBalances(searchModel.UserName,
                        searchModel.AssetTypeIds, searchModel.Page - 1, searchModel.PageSize);

            model = new PagedListModel<AssetBalanceModel>
            {
                Data = deposit.Data.Select(s =>
                {
                    var depositModel = TypeAdapter.Adapt<AssetBalance, AssetBalanceModel>(s);
                    return depositModel;
                }),
                Total = deposit.TotalCount
            };

            return Json(model);
        }
    }
}
