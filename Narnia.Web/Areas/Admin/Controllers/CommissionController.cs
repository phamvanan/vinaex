﻿using FastMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Narnia.Business.Commissions;
using Narnia.Core.Domain.Commissions;
using Narnia.Core.Enums;
using Narnia.Web.Areas.Admin.Models.Commissions;
using Narnia.Web.Framework.Extensions;
using Narnia.Web.Framework.KendoUI;
using Narnia.Web.Framework.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Narnia.Web.Areas.Admin.Controllers
{
    public class CommissionController : BaseAdminController
    {
        private readonly ICommissionService _commissionService;

        public CommissionController(ICommissionService commissionService)
        {
            _commissionService = commissionService;
        }

        #region Commissions

        public virtual IActionResult Index()
        {
            return RedirectToAction("List");
        }

        public virtual IActionResult List(List<int> commissionLevels = null)
        {
            var model = new CommissionSearchModel
            {
                CommissionLevels = commissionLevels
            };
            var levels = _commissionService.GetAllCommissionLevels();

            model.AvailableCommissionLevels.Add(new SelectListItem { Text = "All", Value = "0" });
            foreach (var lvl in levels)
            {
                model.AvailableCommissionLevels.Add(new SelectListItem { Text = lvl.LevelName, Value = lvl.LevelId.ToString() });
            }

            return View(model);
        }

        [HttpPost]
        public virtual IActionResult List(CommissionSearchModel searchModel)
        {
            var commissions = _commissionService.SearchCommissions(searchModel.StartDate, searchModel.EndDate, searchModel.CommissionLevels, searchModel.BeneficiaryUserName, searchModel.Page - 1, searchModel.PageSize, true);

            var model = new PagedListModel<CommissionModel>
            {
                Data = commissions.Data.Select(c =>
                {
                    return new CommissionModel
                    {
                        Id = c.Id,
                        BeneficiaryUserId = c.BeneficiaryUserId,
                        BeneficiaryUserName = c.BeneficiaryUser.UserName,
                        CommAmt = c.CommAmt,
                        CommLevel = c.CommLevel,
                        CommRate = c.CommRate,
                        CreatedDate = c.CreatedDate,
                        //TransactionId = c.TransactionId??0
                    };
                }),
                Total = commissions.TotalCount
            };

            return Json(model);
        }

        #endregion Commissions

        [HttpPost]
        public IActionResult GetHistoryCommissionByUserId(CommissionHistoryByBeneficiaryUserIdSearchModel reqModel)
        {
            var resultData = _commissionService.GetCommissionHistoryByBeneficiaryUserId(reqModel.UserId, reqModel.Page - 1, reqModel.PageSize);

            var model = new PagedListModel<CommissionHistoryByBeneficiaryUserIdResponseModel>
            {
                Data = resultData.Data.Select(c =>
                {
                    return new CommissionHistoryByBeneficiaryUserIdResponseModel
                    {
                        Id = c.Id,
                        RefFromUserName = c.UserName,
                        RefAlias = c.Alias,
                        RefFromChildId = c.RefFromChildId,
                        CommissionAmount = c.CommAmt,
                        CommissionLevel = c.CommLevel,
                        CommissionRate = c.CommRate,
                        CreatedDate = c.CreatedDate,
                        Status = c.Status,
                        UpdateDate = c.UpdateDate,
                        RefId = c.RefId,
                        RefType = c.RefType,
                        AssetTypeId = c.AssetTypeId
                    };
                }),
                Total = resultData.TotalCount
            };

            return Json(model);
        }

        public IActionResult History()
        {
            var model = new CommissionHistoryBySearchModel
            {
            };

            IList<SelectListItem> Transtatus = Enum.GetValues(typeof(CommisionStatusEnum)).Cast<CommisionStatusEnum>().
            Select(x => new SelectListItem { Text = x.ToString(), Value = ((int)x).ToString() }).ToList();
            model.AvailableStatuses.AddRange(Transtatus);

            return View(model);
        }

        [HttpPost]
        public virtual IActionResult History(CommissionHistoryBySearchModel searchModel)
        {
            var commissions = _commissionService.CommissionHistoryBy(searchModel.UserName, searchModel.StartDate, searchModel.EndDate, searchModel.Statuses, searchModel.Page - 1, searchModel.PageSize);

            var model = new PagedListModel<CommissionHistoryByResponseModel>
            {
                Data = commissions.Data.Select(c =>
                {
                    return new CommissionHistoryByResponseModel
                    {
                        Id = c.Id,
                        UserName = c.UserName,
                        Alias = c.Alias,
                        BeneficiaryUserId = c.BeneficiaryUserId,
                        CommissionAmount = c.CommAmt,
                        CommissionLevel = c.CommLevel,
                        CommissionRate = c.CommRate,
                        CreatedDate = c.CreatedDate,
                        Status = c.Status,
                        UpdateDate = c.UpdateDate,
                        RefId = c.RefId,
                        RefType = c.RefType,
                        AssetTypeId = c.AssetTypeId
                    };
                }),
                Total = commissions.TotalCount
            };

            return Json(model);
        }

        #region Commission Levels

        public virtual IActionResult CommissionLevels()
        {
            var model = new PagingSearchModel();
            return View(model);
        }

        [HttpPost]
        public virtual IActionResult CommissionLevels(PagingSearchModel searchModel)
        {
            var allLevels = _commissionService.GetAllCommissionLevels();

            //prepare list model
            var model = new PagedListModel<CommissionLevelModel>
            {
                Data = allLevels.PaginationByRequestModel(searchModel).Select(lvl => { return TypeAdapter.Adapt<CommissionLevel, CommissionLevelModel>(lvl); }),
                Total = allLevels.Count()
            };

            return Json(model);
        }

        [HttpPost]
        public virtual IActionResult CommissionLevelUpdate(CommissionLevelModel model)
        {
            if (model.LevelName != null)
                model.LevelName = model.LevelName.Trim();

            if (!ModelState.IsValid)
            {
                return Json(new DataSourceResult { Errors = ModelState.SerializeErrors() });
            }

            var commissionLevel = _commissionService.GetCommissionLevelById(model.LevelId);

            commissionLevel.LevelName = model.LevelName;
            commissionLevel.CommRate = model.CommRate;

            _commissionService.UpdateCommissionLevel(commissionLevel);

            return new JsonResult(null);
        }

        [HttpPost]
        public virtual IActionResult CommissionLevelAdd(CommissionLevelModel model)
        {
            if (model.LevelName != null)
                model.LevelName = model.LevelName.Trim();

            if (!ModelState.IsValid)
            {
                return Json(new DataSourceResult { Errors = ModelState.SerializeErrors() });
            }

            var res = _commissionService.GetCommissionLevelById(model.LevelId);
            if (res == null)
            {
                var commissionLevel = new CommissionLevel
                {
                    LevelId = model.LevelId,
                    LevelName = model.LevelName,
                    CommRate = model.CommRate
                };

                _commissionService.InsertCommissionLevel(commissionLevel);
            }
            else
            {
                return Json(new DataSourceResult { Errors = string.Format("Commission Level {0} already exists ", model.LevelId) });
            }

            return new JsonResult(null);
        }

        [HttpPost]
        public virtual IActionResult CommissionLevelDelete(int levelId)
        {
            //try to get a locale resource with the specified id
            var commissionLevel = _commissionService.GetCommissionLevelById(levelId)
                ?? throw new ArgumentException("No commission level found with the specified id", nameof(levelId));

            _commissionService.DeleteCommissionLevel(commissionLevel);

            return new JsonResult(null);
        }

        #endregion Commission Levels
    }
}