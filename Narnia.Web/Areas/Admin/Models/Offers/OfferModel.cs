﻿using Narnia.Core.Domain;
using Narnia.Web.Areas.Admin.Models.Account;
using Narnia.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Web.Areas.Admin.Models.Offers
{
    public class OfferModel:BaseEntityModel
    {
        public int UserId { get; set; }
        public string CurrencyId { get; set; }
        public string AssetTypeId { get; set; }
        public int BankId { get; set; }
        public string BankAccountNumber { get; set; }
        public string BankAccountName { get; set; }
        public int CoinExchangeId { get; set; }
        public decimal CoinPrice { get; set; }      // gia ban user nhap
        public decimal MinCoinPrice { get; set; }   // gia ban toi thieu
        public decimal MinCoinNumber { get; set; }
        public decimal MaxCoinNumber { get; set; }
        public int PaymentMethodId { get; set; }
        public int PaymentAllowTime { get; set; }
        public int CountryId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public Boolean Approved { get; set; }
        public decimal AmountProcessed { get; set; }

        public UserAbbrModel AdvertiseUser { get; set; }
        
        public Banks Bank { get; set; }
        public PaymentMethod PayType { get; set; }
        public int? Status { get; set; }
    }
}
