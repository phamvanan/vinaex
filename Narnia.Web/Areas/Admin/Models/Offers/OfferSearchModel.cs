﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Narnia.Web.Framework.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Web.Areas.Admin.Models.Offers
{
    public class OfferSearchModel : BasePagingSearchModel
    {
        public OfferSearchModel()
        {
            Statuses = new List<int>();
            AssetTypeIds = new List<string>();

            AvailableStatuses = new List<SelectListItem>();
            AvailableAssetTypeIds = new List<SelectListItem>();
        }
        /// <summary>
        /// buy or sell
        /// </summary>
        public string Type { get; set; }

        [DisplayName("Start Date")]
        [UIHint("DateNullable")]
        public DateTime? StartDate { get; set; }

        [DisplayName("End Date")]
        [UIHint("DateNullable")]
        public DateTime? EndDate { get; set; }

        [DisplayName("User")]
        public virtual string UserName { get; set; }

        [DisplayName("Status")]
        public List<int> Statuses { get; set; }
        public List<SelectListItem> AvailableStatuses { get; set; }

        [DisplayName("Status")]
        public List<string> AssetTypeIds { get; set; }
        public List<SelectListItem> AvailableAssetTypeIds { get; set; }
    }
}
