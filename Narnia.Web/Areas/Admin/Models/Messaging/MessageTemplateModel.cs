﻿using Narnia.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Web.Areas.Admin.Models.Messaging
{
    public class MessageTemplateModel:BaseEntityModel
    {
        public string AllowedTokens { get; set; }
        public string Name { get; set; }
        [UIHint("RichEditor")]
        public string Body { get; set; }
        public string Subject { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public bool IsActive { get; set; }
    }
}
