﻿using Narnia.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Web.Areas.Admin.Models.Wallets
{
    public class WalletModel:BaseEntityModel
    {
        public long UserId { get; set; }

        public string WalletAddress { get; set; }

        public string WalletPassword { get; set; }

        public decimal Amount { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime UpdatedDate { get; set; }

        public string AssetTypeId { get; set; }

        public string UserName { get; set; }
    }
}
