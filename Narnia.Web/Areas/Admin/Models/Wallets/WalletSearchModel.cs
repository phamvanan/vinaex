﻿
using Microsoft.AspNetCore.Mvc.Rendering;
using Narnia.Core.Domain;
using Narnia.Core.Models;
using Narnia.Web.Framework.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Narnia.Web.Areas.Admin.Models.Wallets
{
    public partial class WalletSearchModel : BasePagingSearchModel
    {
        public WalletSearchModel()
        {
            AssetTypeIds = new List<string>();
            AvailableAssetTypeIds = new List<SelectListItem>();
        }

        [DisplayName("Start Date")]
        [UIHint("DateNullable")]
        public DateTime? StartDate { get; set; }

        [DisplayName("End Date")]
        [UIHint("DateNullable")]
        public DateTime? EndDate { get; set; }

        [DisplayName("Asset Types Levels")]
        public List<string> AssetTypeIds { get; set; }

        public List<SelectListItem> AvailableAssetTypeIds { get; set; }

        [DisplayName("User")]
        public string UserName { get; set; }

        public int? UserId { get; set; }
    }
}
