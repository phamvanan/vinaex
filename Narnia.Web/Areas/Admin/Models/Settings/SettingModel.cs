﻿using Narnia.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Web.Areas.Admin.Models.Settings
{
    public class SettingModel:BaseEntityModel
    {
        [DisplayName("Name")]
        public string Name { get; set; }

        [DisplayName("Value")]
        public string Value { get; set; }

        [DisplayName("Description")]
        public string Description { get; set; }
    }
}
