﻿using Narnia.Web.Framework.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Web.Areas.Admin.Models.Settings
{
    public class SettingSearchModel:BasePagingSearchModel
    {
        [DisplayName("Setting Name")]
        public string SearchSettingName { get; set; }

        [DisplayName("Setting Value")]
        public string SearchSettingValue { get; set; }
    }
}
