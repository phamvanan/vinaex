﻿using Narnia.Web.Framework.Models;

namespace Narnia.Web.Areas.Admin.Models.Localization
{
    public class LocaleResourceSearchModel: BasePagingSearchModel
    {
        public int LanguageId { get; set; }

        public string SearchResourceName { get; set; }

        public string SearchResourceValue { get; set; }

    }
}
