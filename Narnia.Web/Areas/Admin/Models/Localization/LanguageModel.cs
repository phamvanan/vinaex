﻿using Narnia.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Web.Areas.Admin.Models.Localization
{
    public class LanguageModel:BaseModel
    {
        public LanguageModel()
        {
            LocaleResourceSearchModel = new LocaleResourceSearchModel();
        }
        public int Id { get; set; }
        public string Name { get; set; }

        public string LanguageCulture { get; set; }

        public string FlagImageFileName { get; set; }

        public bool Published { get; set; }

        public int DisplayOrder { get; set; }

        public LocaleResourceSearchModel LocaleResourceSearchModel { get; set; }
    }
}
