﻿namespace Narnia.Web.Areas.Admin.Models
{
    public class UserSearchCriteria : BaseSearchViewModel
    {
        public string UserName { get; set; }
        public string Email { get; set; }
    }
}
