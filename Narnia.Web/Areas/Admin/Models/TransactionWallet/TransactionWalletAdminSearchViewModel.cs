﻿using Narnia.Core.Enums;
using Narnia.Core.Helpers;
using Narnia.Web.Framework.Mvc;
using System;
using System.ComponentModel.DataAnnotations;

namespace Narnia.Web.Areas.Admin.Models.TransactionWallet
{
    public class TransactionWalletAdminSearchViewModel: BaseModel
    {
        [Display(Name = "Mã giao dịch")]
        public string TransactionCode { get; set; }
        public int TransactionId { get; set; }
        [Display(Name = "Số tiền")]
        public decimal Amount { get; set; }
        [Display(Name = "Ngày tạo")]
        public DateTime CreatedDate { get; set; }
        [Display(Name = "Ngày cập nhật")]
        public DateTime? UpdatedDate { get; set; }
        [Display(Name = "Người bán")]
        public string UserName { get; set; }
        [Display(Name = "Trạng thái")]
        public int Status { get; set; }
        public string DisplayStatus =>  Enum.GetName(typeof(TransactionStautus),Status);
        [Display(Name = "Loại tiền")]
        public string Unit { get; set; }
        [Display(Name = "Người xác nhận")]
        public string UserConfirm { get; set; }

        public int TransactionType { get; set; }
        public string DisplayTransactionType => ResourceHelper.DisplayForEnumValue((TransactionWalletType)TransactionType);
        [Display(Name = "Ngày xác nhận")]
        public DateTime? ConfirmDate { get; set; }
        [Display(Name = "Ngày upload bằng chứng")]
        public DateTime? EvidenceDate { get; set; }
    }
}