﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Narnia.Web.Framework.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Narnia.Web.Areas.Admin.Models.TransactionWallet
{
    public class TransactionWalletSearchModel : BasePagingSearchModel
    {
        public TransactionWalletSearchModel()
        {
            Statuses = new List<int>();
            AssetTypeIds = new List<string>();
            AvailableAssetTypeIds = new List<SelectListItem>();
            AvailableStatuses = new List<SelectListItem>();
        }

        [DisplayName("Start Date")]
        [UIHint("DateNullable")]
        public DateTime? StartDate { get; set; }

        [DisplayName("End Date")]
        [UIHint("DateNullable")]
        public DateTime? EndDate { get; set; }

        [DisplayName("User")]
        public string UserName { get; set; }

        [DisplayName("Transaction Code")]
        public string TransactionCode { get; set; }

        [DisplayName("Status")]
        public List<int> Statuses { get; set; }

        public List<SelectListItem> AvailableStatuses { get; set; }

        [Display(Name = "Asset Type")]
        public List<string> AssetTypeIds { get; set; }

        public List<SelectListItem> AvailableAssetTypeIds { get; set; }
    }
}