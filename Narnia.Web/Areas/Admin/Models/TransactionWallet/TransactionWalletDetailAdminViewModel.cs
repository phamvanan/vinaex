﻿using Narnia.Core.Enums;
using System;
using System.ComponentModel.DataAnnotations;

namespace Narnia.Web.Areas.Admin.Models.TransactionWallet
{
    public class TransactionWalletDetailAdminViewModel : TransactionWalletAdminSearchViewModel
    {
        public TransactionWalletDetailAdminViewModel()
        {
            ValuesValidToExclude = new int[Enum.GetNames(typeof(TransactionStautus)).Length];
        }
        [Display(Name = "Bằng chứng")]
        public int? EvidenceId { get; set; }

        public string AccountName { get; set; }
        public string AccountNo { get; set; }
        public string Branch { get; set; }
        public string BankName { get; set; }
        public string WalletAddress { get; set; }
        public byte[] Image { get; set; }
        public string EvidenceCode { get; set; }
        public string OrderNo { get; set; }

        public int[] ValuesValidToExclude { get; set; }
        public bool CanChangeStatus =>
            (Status == (int)TransactionStautus.Pending || Status == (int)TransactionStautus.HasEvidence) || (Status == (int)TransactionStautus.HasEvidence && TransactionType == (int)TransactionWalletType.Deposit);

        public bool ShowBankInfo => TransactionType == (int) TransactionWalletType.Withdrawal;

    }
}