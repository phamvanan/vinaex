﻿using Narnia.Web.Areas.Admin.Models.Commissions;
using Narnia.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Web.Areas.Admin.Models.Account
{
    public class UserModel: BaseEntityModel
    {
        public UserModel()
        {
            UserLoginHistorySearchModel = new UserLoginHistorySearchModel();
            UserWithdrawalSearchModel = new UserWithdrawalSearchModel();
            UserDepositSearchModel = new UserDepositSearchModel();
            UserAssetBalanceSearchModel = new UserAssetBalanceSearchModel();
            CommissionHistorySearchModel = new CommissionHistoryByBeneficiaryUserIdSearchModel();
            UplineUsers = new List<UserAbbrModel>();
        }
        public int UserId { get; set; }
        public string Alias { get; set; }
        public string IDNumber { get; set; }

        public string UrlFaceBook { get; set; }
        public string UrlTwitter { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedDateDisplay { get { return CreatedDate.ToString(); } }
        public DateTime UpdatedDate { get; set; }
        public string UpDatedDisplay { get { return UpdatedDate.ToString(); } }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public DateTime LastLoginDate { get; set; }
        [Display(Name = "Avatar")]
        public byte[] Image { get; set; }
        public string ImageDisplay { get; set; }
        public UserAvatarModel UserAvatarModel { get; set; }
        public bool HasReceivedDailyPrice { get; set; }
        public bool HasSendEmailExchangeTransaction { get; set; }
        public bool HasSendEmailNewTransaction { get; set; }
        public bool HasSendSMSNewTransaction { get; set; }
        public bool HasSendSMSTransactionPaid { get; set; }
        public string AssetTypeId { get; set; }
        public string LinkRefer { get; set; }
        public string ReferenceCode { get; set; }
        public bool IsVerify { get; set; }
        public string VerifyDisplay => IsVerify ? "Yes" : "No";
        //public string VerifyDisplay { get; set; }

        public UserLoginHistorySearchModel UserLoginHistorySearchModel { get; set; }
        public UserWithdrawalSearchModel UserWithdrawalSearchModel { get; set; }
        public UserDepositSearchModel UserDepositSearchModel { get; set; }
        public UserAssetBalanceSearchModel UserAssetBalanceSearchModel { get; set; }
        public CommissionHistoryByBeneficiaryUserIdSearchModel CommissionHistorySearchModel { get; set; }

        public List<UserAbbrModel> UplineUsers { get; set; }
    }
}
