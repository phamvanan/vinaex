﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Web.Areas.Admin.Models.Account
{
    public class UserAbbrModel
    {
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Id { get; set; }
    }
}
