﻿using Narnia.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Web.Areas.Admin.Models.Account
{
    public class UserAvatarModel:BaseModel
    {
        public string AvatarUrl { get; set; }
    }
}
