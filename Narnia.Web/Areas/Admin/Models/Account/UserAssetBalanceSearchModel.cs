﻿using Narnia.Web.Framework.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Web.Areas.Admin.Models.Account
{
    public class UserAssetBalanceSearchModel: BasePagingSearchModel
    {
        public int UserId { get; set; }
    }
}
