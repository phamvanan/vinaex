﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Web.Areas.Admin.Models.Account
{
    public enum AdminType
    {
        Admin,
        Merchant
    }
    [Serializable]
    public class AdminUserInfo
    {
        public string Username { get; set; }
        public AdminType Role { get; set; }
    }
}
