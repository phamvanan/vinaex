﻿using Narnia.Web.Framework.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Web.Areas.Admin.Models.Account
{
    public class UserSearchModel: BasePagingSearchModel
    {
        [DisplayName("Start Date")]
        [UIHint("DateNullable")]
        public DateTime? StartDate { get; set; }

        [DisplayName("End Date")]
        [UIHint("DateNullable")]
        public DateTime? EndDate { get; set; }
        public string UserName { get; set; }
        public string PhoneNumber { get; set; }
    }
}
