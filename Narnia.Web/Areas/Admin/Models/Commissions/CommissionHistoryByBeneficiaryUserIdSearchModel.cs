﻿using Narnia.Web.Framework.Models;

namespace Narnia.Web.Areas.Admin.Models.Commissions
{
    public class CommissionHistoryByBeneficiaryUserIdSearchModel : BasePagingSearchModel
    {
        public int UserId { get; set; }
    }
}