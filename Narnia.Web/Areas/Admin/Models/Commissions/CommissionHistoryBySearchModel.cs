﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Narnia.Web.Framework.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Narnia.Web.Areas.Admin.Models.Commissions
{
    public class CommissionHistoryBySearchModel : BasePagingSearchModel
    {
        public CommissionHistoryBySearchModel()
        {
            AvailableStatuses = new List<SelectListItem>();
        }
        [DisplayName("User")]
        public string UserName { get; set; }

        [DisplayName("Start Date")]
        [UIHint("DateNullable")]
        public DateTime? StartDate { get; set; }

        [DisplayName("End Date")]
        [UIHint("DateNullable")]
        public DateTime? EndDate { get; set; }

        [DisplayName("Status")]
        public List<int> Statuses { get; set; }
        public List<SelectListItem> AvailableStatuses { get; set; }
    }
}