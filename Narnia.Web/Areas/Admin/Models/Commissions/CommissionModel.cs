﻿using Narnia.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Web.Areas.Admin.Models.Commissions
{
    public class CommissionModel: BaseEntityModel
    {
        public int TransactionId { get; set; }
        public int BeneficiaryUserId { get; set; }
        public int CommLevel { get; set; }
        public double CommRate { get; set; }
        public decimal CommAmt { get; set; }
        public DateTime CreatedDate { get; set; }
        public string BeneficiaryUserName { get; set; }
    }
}
