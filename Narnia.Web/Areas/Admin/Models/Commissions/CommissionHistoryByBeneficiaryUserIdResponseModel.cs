﻿using Narnia.Web.Framework.Mvc;
using System;

namespace Narnia.Web.Areas.Admin.Models.Commissions
{
    public class CommissionHistoryByBeneficiaryUserIdResponseModel: BaseEntityModel
    {
        public int? RefFromChildId { get; set; }
        public string RefFromUserName { get; set; }
        public string RefAlias { get; set; }
        public int CommissionLevel { get; set; }
        public decimal CommissionRate { get; set; }
        public decimal CommissionAmount { get; set; }
        public DateTime CreatedDate { get; set; }
        public int RefId { get; set; }
        public int RefType { get; set; }
        public int Status { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string AssetTypeId { get; set; }
    }
}