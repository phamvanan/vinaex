﻿using Narnia.Web.Framework.Mvc;
using System;

namespace Narnia.Web.Areas.Admin.Models.Commissions
{
    public class CommissionHistoryByResponseModel: BaseEntityModel
    {
        public int BeneficiaryUserId { get; set; }
        public string Alias { get; set; }
        public string UserName { get; set; }
        public int CommissionLevel { get; set; }
        public decimal CommissionRate { get; set; }
        public decimal CommissionAmount { get; set; }
        public DateTime CreatedDate { get; set; }
        public int RefId { get; set; }
        public int RefType { get; set; }
        public int Status { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string AssetTypeId { get; set; }
    }
}