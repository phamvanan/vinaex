﻿using Narnia.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Web.Areas.Admin.Models.Commissions
{
    public class CommissionLevelModel: BaseModel
    {
        public int LevelId { get; set; }
        public string LevelName { get; set; }
        public double CommRate { get; set; }
    }
}
