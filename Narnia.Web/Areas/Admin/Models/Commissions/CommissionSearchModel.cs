﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Narnia.Web.Framework.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Narnia.Web.Areas.Admin.Models.Commissions
{
    public class CommissionSearchModel : BasePagingSearchModel
    {
        public CommissionSearchModel()
        {
            CommissionLevels = new List<int>();
            AvailableCommissionLevels = new List<SelectListItem>();
        }

        [DisplayName("Start Date")]
        [UIHint("DateNullable")]
        public DateTime? StartDate { get; set; }

        [DisplayName("End Date")]
        [UIHint("DateNullable")]
        public DateTime? EndDate { get; set; }

        [DisplayName("Commission Levels")]
        public IList<int> CommissionLevels { get; set; }

        public IList<SelectListItem> AvailableCommissionLevels { get; set; }

        [DisplayName("Benificiary User")]
        public string BeneficiaryUserName { get; set; }
    }
}