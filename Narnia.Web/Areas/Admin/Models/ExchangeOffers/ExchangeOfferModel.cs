﻿using Narnia.Core.Domain;
using Narnia.Web.Areas.Admin.Models.Account;
using Narnia.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Web.Areas.Admin.Models.ExchangeOffers
{
    public class ExchangeOfferModel : BaseEntityModel
    {
        public string AssetTypeId { get; set; }
        public decimal Amount { get; set; }
        public decimal VNDAmount { get; set; }
        public decimal Fee { get; set; }
        public int Type { get; set; }
        public string UserBankAccountNo { get; set; }
        public string UserBankAccountName { get; set; }
        public int Status { get; set; }
        public string StatusString { get; set; }
        public string ExchangeOfferCode { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }
}
