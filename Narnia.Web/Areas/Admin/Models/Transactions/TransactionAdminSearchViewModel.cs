﻿using Narnia.Core.Enums;
using Narnia.Core.Helpers;
using System;
using System.ComponentModel.DataAnnotations;

namespace Narnia.Web.Models
{
    public class TransactionAdminSearchViewModel
    {
        [Display(Name = "Mã giao dịch")]
        public string TransactionCode { get; set; }
        public int TransactionId { get; set; }
        [Display(Name = "Số tiền")]
        public decimal AmountCoin { get; set; }
        [Display(Name = "Ngày tạo")]
        public DateTime CreatedDate { get; set; }
        [Display(Name = "Ngày cập nhật")]
        public DateTime? UpdatedDate { get; set; }
        [Display(Name = "Người bán")]
        public string UserName { get; set; }
        [Display(Name = "Trạng thái")]
        public int Status { get; set; }
        public string DisplayStatus =>  ResourceHelper.DisplayForEnumValue((TransactionStautus)Status);
        [Display(Name = "Loại tiền")]
        public string AssetTypeId { get; set; }
        //public string DisplayCoinType => ResourceHelper.DisplayForEnumValue((CoinTypeEnum)CoinType);
        [Display(Name = "Thời gian thanh toán")]
        public int PaymentAllowTime { get; set; }
        [Display(Name = "Người xác nhận")]
        public string UserConfirm { get; set; }
    }
}