﻿using Narnia.Core.Enums;
using Narnia.Web.Framework.Mvc;
using System;
using System.ComponentModel.DataAnnotations;

namespace Narnia.Web.Areas.Admin.Models.Transactions
{
    public class TransactionModel : BaseModel
    {
        public TransactionModel()
        {
            ValuesValidToExclude = new int[Enum.GetNames(typeof(TransactionStautus)).Length];
        }

        public int TransactionId { get; set; }
        public int? SellAdvertisementId { get; set; }
        public int? BuyAdvertisementId { get; set; }
        public decimal AmountCoin { get; set; }
        [Display(Name = "AssetType")]
        public string AssetTypeId { get; set; }
        public DateTime CreatedDate { get; set; }
        public int UserId { get; set; }
        public int Status { get; set; }
        public string Description { get; set; }
        public int TransactionType { get; set; }
        public int? ConfirmBy { get; set; }
        [Display(Name = "Evidence")]
        public int? EvidenceId { get; set; }
        public string TransactionCode { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int PaymentType { get; set; }
        public string Buyer { get; set; }
        public string Seller { get; set; }
        public string StatusString => Enum.GetName(typeof(TransactionStautus), Status);
        public string EvidenceCode { get; set; }
        public int[] ValuesValidToExclude { get; set; }
        public bool CanChangeStatus =>
            Status == (int)TransactionStautus.Pending || Status == (int)TransactionStautus.HasEvidence;

        public bool ShowTransferType =>
            Status == (int) TransactionStautus.HasEvidence || Status == (int) TransactionStautus.Success;

        public bool CanChangeTransferType => Status == (int) TransactionStautus.HasEvidence;
        public int TransferType { get; set; }
        public string TransferTypeString => Enum.GetName(typeof(TransferType), TransferType);

    }
}