﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;

namespace Narnia.Web.Models
{
    public class TransactionDetailAdminViewModel : TransactionAdminSearchViewModel
    {
        [Display(Name = "Bằng chứng")]
        public int EvidenceId { get; set; }

        public string EvidenceCode { get; set; }
    }
}