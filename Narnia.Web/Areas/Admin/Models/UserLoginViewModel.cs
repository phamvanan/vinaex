﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Narnia.Web.Areas.Admin.Models
{
    [Serializable]
    public class UserLoginViewModel
    {
        [Required(ErrorMessage = "Vui lòng nhập tên Login")]
        public string Username { get; set; }
        [Required(ErrorMessage = "Vui lòng nhập mật khẩu")]
        public string Password { get; set; }
    }

    public class ChangePassViewModel
    {
        [Required(ErrorMessage = "User không hợp lệ")]
        public string UserName { get; set; }
        //0: property, 1: maxlength, 2: minlength
        [StringLength(100,MinimumLength = 6, ErrorMessage = "Độ dài mật khẩu phải lớn hơn {2} và nhỏ hơn {1}")]
        [Required(ErrorMessage = "Vui lòng nhập mật khẩu mới")]
        [DataType(DataType.Password), Display(Name = "Mật khẩu")]
        public string NewPassWord { get; set; }
        [Required(ErrorMessage = "Vui lòng xác nhận mật khẩu mới"), Compare("NewPassWord",ErrorMessage = "Xác nhận mật khẩu và mật khẩu không khớp")]
        [DataType(DataType.Password), Display(Name = "Xác nhận mật khẩu")]
        public string ConfirmNewPassWord { get; set; }
    }
}
