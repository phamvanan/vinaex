﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Narnia.Core.Models;
using Narnia.Web.Framework.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Web.Areas.Admin.Models.Asset
{
    public class AssetBalanceSearchModel: BasePagingSearchModel
    {
        [DisplayName("User Name")]
        public string UserName { get; set; }

        [DisplayName("Asset Types")]
        public List<string> AssetTypeIds { get; set; }
        public List<SelectListItem> AvailableAssetTypeIds { get; set; }

        public AssetBalanceSearchModel()
        {
            AvailableAssetTypeIds = new List<SelectListItem>();
        }
    }
}
