﻿using Narnia.Web.Areas.Admin.Models.Account;
using Narnia.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Web.Areas.Admin.Models.Asset
{
    public class AssetBalanceModel: BaseEntityModel
    {
        public int UserId { get; set; }
        public string AssetTypeId { get; set; }
        public decimal Balance { get; set; }
        public decimal FrozenBalance { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime UpdatedOn { get; set; }
        public UserAbbrModel User { get; set; }
    }
}
