﻿using Narnia.Core.Domain.Payments;
using Narnia.Web.Areas.Admin.Models.Account;
using Narnia.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Web.Areas.Admin.Models.Deposits
{
    public class DepositModel : BaseEntityModel
    {     
        public decimal Amount { get; set; }
        public string AssetTypeId { get; set; }
        public int UserId { get; set; }
        public DateTime CreatedDate { get; set; }

        public DateTime UpdatedDate { get; set; }

        public DepositStatus Status { get; set; }
        public string Description { get; set; }
       
        public UserAbbrModel User { get; set; }
        public string StatusString => Enum.GetName(typeof(DepositStatus), Status);
    }
}
