﻿using Narnia.Web.Areas.Admin.Models.Account;
using Narnia.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Web.Areas.Admin.Models.Payments
{
    public class GatewayAccountModel: BaseEntityModel
    {
        public int UserId { get; set; }
        public int GatewayId { get; set; }
        public string AccName { get; set; }
        public DateTime CreatedDate { get; set; }
        public string Description { get; set; }
        public bool IsSystemAccount { get; set; }
        public UserAbbrModel User { get; set; }
    }
}
