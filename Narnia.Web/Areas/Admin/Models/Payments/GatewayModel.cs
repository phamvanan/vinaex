﻿using Narnia.Web.Areas.Admin.Models.Account;
using Narnia.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Web.Areas.Admin.Models.Payments
{
    public class GatewayModel : BaseEntityModel
    {
        public string GatewaySystemName { get; set; }
        public string GatewayFriendlyName { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
    }
}
