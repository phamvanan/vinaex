﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Narnia.Core.Models;
using Narnia.Web.Framework.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Web.Areas.Admin.Models.Payments
{
   
    public class BankTransferSearchModel : BasePagingSearchModel
    {
        [DisplayName("User")]
        public string UserName { get; set; }
        
        [DisplayName("Start Date")]
        [UIHint("DateNullable")]
        public DateTime? StartDate { get; set; }

        [DisplayName("End Date")]
        [UIHint("DateNullable")]
        public DateTime? EndDate { get; set; }

        [DisplayName("Billing Date")]
        [UIHint("DateNullable")]
        public DateTime? BillingDate { get; set; }

        [DisplayName("Status")]
        public List<int> Statuses { get; set; }
        public List<SelectListItem> AvailableStatuses { get; set; }

        [DisplayName("Asset Types")]
        public List<string> AssetTypeIds { get; set; }
        public List<SelectListItem> AvailableAssetTypeIds { get; set; }

        public BankTransferSearchModel()
        {
            AvailableAssetTypeIds = new List<SelectListItem>();
            AvailableStatuses = new List<SelectListItem>();
        }
    }
}
