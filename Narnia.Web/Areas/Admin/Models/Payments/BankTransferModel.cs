﻿using Narnia.Core.Domain;
using Narnia.Web.Areas.Admin.Models.Account;
using Narnia.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Web.Areas.Admin.Models.Payments
{
    public class BankTransferModel: BaseEntityModel
    {
        public int UserId { get; set; }
        public int DepositId { get; set; }
        public string OrderNo { get; set; }
        public string BankName { get; set; }
        public string AccountName { get; set; }
        public string AccountNumber { get; set; }
        public string Comments { get; set; }
        public decimal Amount { get; set; }
        public decimal Fee { get; set; }
        public string AssetTypeId { get; set; }
        public DateTime BillingDate { get; set; }
        public BankTransferStatus Status { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime UpdatedOn { get; set; }
        public AssetType AssetType { get; set; }
        public UserAbbrModel User { get; set; }
        public int? EvidencePictureId { get; set; }
        [DisplayName("Evidence Picture")]
        public string EvidencePictureUrl { get; set; }
        [DisplayName("Status")]
        public string StatusString => Enum.GetName(typeof(BankTransferStatus), Status);
    }
}
