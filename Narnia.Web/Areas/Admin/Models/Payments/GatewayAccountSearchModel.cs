﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Narnia.Core.Models;
using Narnia.Web.Framework.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Web.Areas.Admin.Models.Payments
{
   
    public class GatewayAccountSearchModel : BasePagingSearchModel
    {
        [DisplayName("User")]
        public string UserName { get; set; }
        
        [DisplayName("Start Date")]
        [UIHint("DateNullable")]
        public DateTime? StartDate { get; set; }

        [DisplayName("End Date")]
        [UIHint("DateNullable")]
        public DateTime? EndDate { get; set; }

        [DisplayName("Account Type")]
        public int AccountType { get; set; }

        public List<SelectListItem> AvailableAccountTypes { get; set; }
        public GatewayAccountSearchModel()
        {
            AvailableAccountTypes = new List<SelectListItem>();

        }
    }
    public enum GatewayAccountType
    {
        Normal = 0,
        System = 1
    }
}
