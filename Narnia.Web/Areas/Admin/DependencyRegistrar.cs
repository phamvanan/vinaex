﻿using System;
using System.Collections.Generic;
using DryIoc;
using Narnia.Business.Messaging;
using Narnia.Core.Infrastructure;
using Narnia.Web.Framework.Menu;

namespace Mercedes.Admin
{
    public class DependencyRegistrar : DryIocModule
    {
        protected override void Load(IRegistrator builder)
        {
            builder.Register<IEmailAccountBiz, EmailAccountBiz>(Reuse.Transient);
            //builder.RegisterDelegate<Func<IList<string>, bool>>(c => {
            //    return roles => { return true; };
            //}, serviceKey: "funcHasRole",reuse:Reuse.Singleton);

            //builder.Register<XmlSiteMap>(Made.Of(() => new XmlSiteMap(Arg.Index<string>(0), Arg.Of<Func<IList<string>, bool>>("funcHasRole")),
            //    request1 => "~/Areas/Admin/sitemap.config"), serviceKey: "adminsitemap");

        }
    }
}