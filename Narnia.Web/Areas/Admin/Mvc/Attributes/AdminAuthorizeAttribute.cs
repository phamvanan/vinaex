﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;
using Narnia.Core.Infrastructure;
using Narnia.Web.Areas.Admin.Models.Account;
using System;
using System.Linq;
using Narnia.Web.Extensions;

namespace Narnia.Web.Areas.Admin.Mvc.Attributes
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class, Inherited = true, AllowMultiple = true)]
    public class AdminAuthorizeAttribute : ActionFilterAttribute, IAuthorizationFilter
    {
        public AdminAuthorizeAttribute()
        {
            _roles = new[] { AdminType.Admin };
        }
        public AdminAuthorizeAttribute(params AdminType[] roles)
        {
            _roles = roles;
        }
        private readonly AdminType[] _roles = null;

        private void HandleUnauthorizedRequest(AuthorizationFilterContext filterContext)
        {
            filterContext.Result = new UnauthorizedResult();
        }

        public void OnAuthorization(AuthorizationFilterContext filterContext)
        {
            if (filterContext == null)
                throw new ArgumentNullException(nameof(filterContext));

            bool isAdmin = false;
            var httpContext = EngineContext.Current.Resolve<IHttpContextAccessor>().HttpContext;
            if (httpContext == null || httpContext.Session == null)
            {
                filterContext.Result = new JsonResult(new { Data = "Access denied!!!" });
                return;
            }

            var adminUser = httpContext.Session.GetObject<AdminUserInfo>("AccAdmin");
            isAdmin = adminUser != null;

            if (!isAdmin)
            {

                if (filterContext.HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    var viewResult = new JsonResult((new { IsSuccess = "Unauthorized", description = "Sorry, you do not have the required permission to perform this action." }));
                    filterContext.Result = viewResult;
                }
                else
                {
                    var routeValue = new RouteValueDictionary(new { action = "Login", controller = "Account", area = "Admin" });
                    filterContext.Result = new RedirectToRouteResult(routeValue);
                }
            }
            else
            {
                //Check for roles
                if (_roles != null && _roles.Length > 0)
                {
                    if (!_roles.Contains(adminUser.Role))
                    {
                        filterContext.Result = new JsonResult(new { Data = "Access denied!!!" });
                    }
                }
            }
        }
    }
}
