﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Routing;
using Narnia.Business.Transactions;
using Narnia.Core;
using Narnia.Core.Infrastructure;
using Narnia.Web.Areas.Payments.Models;
using Narnia.Web.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Web.Areas.Payments.Components
{
    [ViewComponent(Name = "PerfectMoney")]
    public class PerfectMoneyViewComponent : ViewComponent
    {
        public async Task<IViewComponentResult> InvokeAsync(ProcessPaymentRequest processPaymentRequest)
        {
            var workContext = EngineContext.Current.Resolve<IWorkContext>();
            
            var model = new PerfectMoneyModel();
            model.PaymentId = processPaymentRequest.GetCustomValue<int>("GatewayTransferId");
            model.Amount = Math.Round(processPaymentRequest.GetCustomValue<decimal>("AmountInUSD"));
            model.AssetTypeId = processPaymentRequest.AssetTypeId;
            model.Fee = processPaymentRequest.Fee;
            model.PayeeAccount = processPaymentRequest.GetCustomValue<string>("PayeeAccount");
            model.PayeeName = processPaymentRequest.GetCustomValue<string>("PayeeName");
           
            model.Unit = processPaymentRequest.GetCustomValue<string>("Unit");
            model.UserId = workContext.CurrentUser == null ? 10 : workContext.CurrentUser.Id;

            model.StatusUrl = null;
            model.SuccessUrl = Url.RouteUrl("Payments.PerfectMoney.Success", new { area = AreaNames.Payments }, Request.Scheme);
            model.CancelUrl = Url.RouteUrl("Payments.PerfectMoney.Cancel", new {area=AreaNames.Payments},Request.Scheme);
            model.StatusUrl = Url.RouteUrl("Payments.PerfectMoney.StatusChanged", new { area = AreaNames.Payments }, Request.Scheme);
            
            return View("~/Areas/Payments/Views/Components/PerfectMoney/Default.cshtml",model);
        }
    }
}
