﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Routing;
using Narnia.Business;
using Narnia.Business.Transactions;
using Narnia.Core;
using Narnia.Web.Areas.Payments.Models;
using Narnia.Web.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Web.Areas.Payments.Components
{
    [ViewComponent(Name = "Deposit.Eth")]
    public class EthViewComponent : ViewComponent
    {
        private readonly IWorkContext _workContext;
        private readonly ICoinWalletBusiness _coinWalletBusiness;

        public EthViewComponent(IWorkContext workContext, ICoinWalletBusiness coinWalletBusiness)
        {
            _workContext = workContext;
            _coinWalletBusiness = coinWalletBusiness;
        }
        public async Task<IViewComponentResult> InvokeAsync(ProcessPaymentRequest processPaymentRequest)
        {
            var model = new EthDepositModel();
            var wallet = _coinWalletBusiness.GetCurrentCoinWalletByUserId(_workContext.CurrentUser.Id, "ETH");
            model.WalletAddress = wallet == null ? string.Empty : wallet.WalletAddress;
            return View("~/Areas/Payments/Views/Components/Eth/Default.cshtml",model);
        }
    }
}
