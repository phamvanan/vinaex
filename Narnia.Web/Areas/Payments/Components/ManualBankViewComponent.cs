﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Routing;
using Narnia.Business;
using Narnia.Business.Transactions;
using Narnia.Core;
using Narnia.Web.Areas.Payments.Models;
using Narnia.Web.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Web.Areas.Payments.Components
{
    [ViewComponent(Name = "Deposit.ManualBank")]
    public class ManualBankViewComponent : ViewComponent
    {
        private readonly IWorkContext _workContext;
        private readonly IBankAccountBiz _bankAccBiz;

        public ManualBankViewComponent(IWorkContext workContext, IBankAccountBiz bankAccBiz)
        {
            _workContext = workContext;
            _bankAccBiz = bankAccBiz;
        }
        public async Task<IViewComponentResult> InvokeAsync(ProcessPaymentRequest processPaymentRequest)
        {
            var model = new ManualBankDepositModel();

            model.UserId = _workContext.CurrentUser.Id;
            model.SystemBankAccounts = _bankAccBiz.GetAvailableSystemBankAccounts();
            model.AssetTypeId = "VND";
            return View("~/Areas/Payments/Views/Components/ManualBank/Default.cshtml", model);
        }
    }
}
