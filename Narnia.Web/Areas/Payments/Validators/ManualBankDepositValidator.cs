﻿using FluentValidation;
using Narnia.Business.Localization;
using Narnia.Web.Areas.Payments.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Web.Areas.Payments.Validators
{
    public class ManualBankDepositValidator:AbstractValidator<ManualBankDepositModel>
    {
        public ManualBankDepositValidator(ILocalizationService localizationService)
        {
            RuleFor(x => x.BillingDate).NotEmpty().WithMessage(localizationService.GetResource("Deposit.Fields.BillingDate.Required"));
            RuleFor(x => x.Amount).NotEmpty().WithMessage(localizationService.GetResource("Deposit.Fields.Amount.Required"));
            RuleFor(x => x.OrderNo).NotEmpty().WithMessage(localizationService.GetResource("Deposit.Fields.OrderNo.Required"));
            RuleFor(x => x.AssetTypeId).NotEmpty().WithMessage(localizationService.GetResource("Deposit.Fields.AssetTypeId.Required"));
            RuleFor(x => x.BankName).NotEmpty().WithMessage(localizationService.GetResource("Deposit.Fields.BankName.Required"));
            RuleFor(x => x.AccountName).NotEmpty().WithMessage(localizationService.GetResource("Deposit.Fields.AccountName.Required"));
            RuleFor(x => x.AccountNumber).NotEmpty().WithMessage(localizationService.GetResource("Deposit.Fields.AccountNumber.Required"));
            RuleFor(x => x.EvidencePictureId).NotEmpty().WithMessage(localizationService.GetResource("Deposit.Fields.EvidencePictureId.Required"));
        }
    }
}

