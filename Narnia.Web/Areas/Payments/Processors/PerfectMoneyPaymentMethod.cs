﻿
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;
using Narnia.Business;
using Narnia.Business.Transactions;
using Narnia.Core.Domain;
using Narnia.Core.Domain.Payments;
using Narnia.Web.Areas.Payments.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;

namespace Narnia.Web.Areas.Payments.Processors
{
    public class PerfectMoneyPaymentMethod: BasePaymentMethod
    {
        private readonly IPaymentBiz _paymentBiz;
        private readonly ICoinSymbolBiz _coinSymbolBiz;

        private PerfectMoneyPaymentSettings _paymentSettings = new PerfectMoneyPaymentSettings();
        public override int GatewayId
        {
            get
            {
                return 2;
            }
        }
        public override PaymentMethodType PaymentMethodType => PaymentMethodType.Standard;

        public override bool SkipPaymentInfo
        {
            get { return false; }
        }

        public override string PaymentMethodDescription
        {
            get { return "Perfect Money Payment gateway"; }
        }

        public PerfectMoneyPaymentMethod()
        {
         
        }
        public PerfectMoneyPaymentMethod(IPaymentBiz paymentBiz, ICoinSymbolBiz coinSymbolBiz)
        {
            _paymentBiz = paymentBiz;
            _coinSymbolBiz = coinSymbolBiz;
        }

        public override ProcessPaymentResult ProcessPayment(ProcessPaymentRequest processPaymentRequest)
        {
            ProcessPaymentResult result = new ProcessPaymentResult();

            int depositId = processPaymentRequest.GetCustomValue<int>("DepositId");
            var systemAccounts = _paymentBiz.GetActiveSystemGatewayAccounts(GatewayId);
            if(systemAccounts != null && systemAccounts.Count > 0)
            {
                string pmUnit = "";
                GatewayAccount payeeAccount = null;
                if (processPaymentRequest.AssetTypeId == "USDT")
                {
                    pmUnit = "USD";
                    payeeAccount = systemAccounts.Where(acc => acc.AccName.StartsWith('U')).FirstOrDefault();
                }
                else if (processPaymentRequest.AssetTypeId == "BTC")
                {
                    //pmUnit = "BTC";
                    //payeeAccount = systemAccounts.Where(acc => acc.AccName.StartsWith('B')).FirstOrDefault();
                    pmUnit = "USD";
                    payeeAccount = systemAccounts.Where(acc => acc.AccName.StartsWith('U')).FirstOrDefault();
                }
                else
                {
                    result.AddError("Payment unit is not valid!!!");
                    return result;
                }
                if(payeeAccount == null)
                {
                    result.AddError("Payee account not found!!!");
                    return result;
                }
               
                processPaymentRequest.CustomValues["PayeeAccount"] = payeeAccount.AccName;
                processPaymentRequest.CustomValues["PayeeName"] = "VINAX Co.";
                processPaymentRequest.CustomValues["Unit"] = processPaymentRequest.AssetTypeId;
                processPaymentRequest.CustomValues["Unit"] = pmUnit;

                string fromAcc = "";
                var coinSymbol = _coinSymbolBiz.GetSymbolByName(processPaymentRequest.AssetTypeId);
                if(coinSymbol == null)
                {
                    result.AddError("No coin symbol found!!!");
                    return result;
                }
                var currency = _coinSymbolBiz.GetCurrencyByCode(coinSymbol.AssetIdQuote);
                if (coinSymbol == null)
                {
                    result.AddError("No currency found!!!");
                    return result;
                }
                var amount = processPaymentRequest.Amount * coinSymbol.AvgPrice / currency.Rate;//Amount in USD
                var gatewayTransfer = _paymentBiz.GatewayDepositPre(GatewayId, depositId, fromAcc, payeeAccount.AccName, amount, processPaymentRequest.Fee,
                    pmUnit, processPaymentRequest.Description);

                if (gatewayTransfer != null)//save successfully
                {
                    processPaymentRequest.CustomValues["GatewayTransferId"] = gatewayTransfer.Id;
                    processPaymentRequest.CustomValues["AmountInUSD"] = amount;
                }
                else
                {
                    result.AddError("Canot save payment info!!!");
                    return result;
                }
            }
            else
            {
                result.AddError("System gateway account not found!!!");
                return result;
            }
            
            return result;
        }

        public override IList<string> ValidatePaymentForm(IFormCollection form)
        {
            if (form == null)
                throw new ArgumentException(nameof(form));

            //try to get errors
            if (form.TryGetValue("Errors", out StringValues errorsString) && !StringValues.IsNullOrEmpty(errorsString))
                return new[] { errorsString.ToString() }.ToList();

            return new List<string>();
        }

        public override ProcessPaymentRequest GetPaymentInfo(IFormCollection form)
        {
            if (form == null)
                throw new ArgumentException(nameof(form));

            var paymentRequest = new ProcessPaymentRequest();

            if (form.TryGetValue("AssetTypeId", out StringValues assetType) && !StringValues.IsNullOrEmpty(assetType))
                paymentRequest.AssetTypeId = assetType;

            if (form.TryGetValue("Amount", out StringValues amount) && !StringValues.IsNullOrEmpty(amount) && decimal.TryParse(amount, out decimal amt))
                paymentRequest.Amount = amt;
            return paymentRequest;
        }

        public override string GetPublicViewComponentName()
        {
            return "PerfectMoney";
        }


        #region Perfect Money API

        private string CalcHash(string paymentId, string payeeAcc, string paymentAmt, string paymentUnit, string paymentBatchNum, string payerAccount,
            string passPhrase, string timestampGMT)
        {
            //"PAYMENT_ID:PAYEE_ACCOUNT:PAYMENT_AMOUNT:PAYMENT_UNITS:PAYMENT_BATCH_NUM:PAYER_ACCOUNT:AlternateMerchantPassphraseHash:TIMESTAMPGMT"
            string data = string.Format("{0}:{1}:{2}:{3}:{4}:{5}:{6}:{7}",
                paymentId ?? "NULL", payeeAcc, paymentAmt, paymentUnit, paymentBatchNum, payerAccount, passPhrase, timestampGMT);

            MD5 md5 = MD5.Create();
            byte[] content = Encoding.UTF8.GetBytes(data);
            byte[] requestContentHash = md5.ComputeHash(content);

            string result = "";
            for (int i = 0; i < requestContentHash.Length; i++)
            {
                result += requestContentHash[i].ToString("x2");
            }
            return result;
        }


        #region Base Perfect Money API query methods 
        protected Dictionary<string, string> ParsePerfectMoneyResponse(string s)
        {
            if (s == null) return null;

            Regex regEx = new Regex("<input name='(.*)' type='hidden' value='(.*)'>");
            MatchCollection matches = regEx.Matches(s);
            Dictionary<string, string> results = new Dictionary<string, string>();
            foreach (Match match in matches)
            {
                results.Add(match.Groups[1].Value, match.Groups[2].Value);
            }
            return results;
        }

        protected string FetchPage(string url)
        {
            WebClient webClient = new WebClient();
            string result = null; ;
            try
            {
                result = webClient.DownloadString(url);
            }
            catch (WebException ex)
            {
                return null;
            }
            return result;
        }

        protected string FetchPerfectMoneyPageWithQuery(string method, string query)
        {
            return FetchPage(
                String.Format("https://perfectmoney.is/acct/{0}.asp?{1}",
                    method,
                    query));
        }

        protected string FetchPerfectMoneyPage(string method, params string[] args)
        {
            string query = "";

            if (args.Length % 2 != 0) throw new ArgumentException();

            for (int i = 0; i < args.Length; i += 2)
            {
                query = query + "&" + args[i] + "=" + args[i + 1];
            }

            return FetchPerfectMoneyPageWithQuery(method, query.Substring(1));
        }
        #endregion

        /// <summary>
        /// Get a response from Perfect Money and parse it searching for
        /// <input name='...' type='hidden' value='...'>. Result is returned as a
        /// hash.
        /// </summary>
        /// <param name="method">Perfect Money API method</param>
        /// <param name="args">Argument pairs: title, value, title, value ...</param>
        /// <returns>A hash  (Dictionary<string, string>) containing key-value data from Perfect Money response</returns>
        protected Dictionary<string, string> FetchPerfectMoneyPageParameters(string method, params string[] args)
        {
            return ParsePerfectMoneyResponse(FetchPerfectMoneyPage(method, args));
        }

        #region Perfect Money API direct queries
        public Dictionary<string, string> QueryBalance(string accountID, string passPhrase)
        {
            return FetchPerfectMoneyPageParameters("balance",
                    "AccountID", accountID,
                    "PassPhrase", passPhrase);
        }

        public Dictionary<string, string> EvoucherPurchase(string accountID, string passPhrase, string payerAccount, double amount)
        {
            return FetchPerfectMoneyPageParameters("ev_create",
                    "AccountID", accountID,
                    "PassPhrase", passPhrase,
                    "Payer_Account", payerAccount,
                    "Amount", XmlConvert.ToString(amount));
        }

        public Dictionary<string, string> Transfer(string accountID, string passPhrase,
            string payerAccount, string payeeAccount, double amount, int payIn, int paymentId)
        {
            return FetchPerfectMoneyPageParameters("confirm",
                "AccountID", accountID,
                "PassPhrase", passPhrase,
                "Payer_Account", payerAccount,
                "Payee_Account", payeeAccount,
                "Amount", XmlConvert.ToString(amount),
                "PAY_IN", payIn.ToString(),
                "PAYMENT_ID", paymentId.ToString());
        }
        #endregion

        #region Perfect money data list get methods
        /// <summary>
        /// Parses a table returned by Perfect Money server
        /// </summary>
        /// <param name="s">Full page text</param>
        /// <returns>A list of hashes which are lines of a table returned by Perfect Money</returns>
        protected List<Dictionary<string, string>> ParsePerfectMoneyList(string s)
        {
            string[] lines = s.Split(new char[] { '\r', '\n' });
            if (lines.Length < 2) return null;
            string[] fields = lines[0].Split(new char[] { ',' });

            List<Dictionary<string, string>> result = new List<Dictionary<string, string>>();
            Dictionary<string, string> line;
            string[] values;

            for (int y = 1; y < lines.Length; y++)
            {
                values = lines[y].Split(new char[] { ',' });
                if (values.Length != fields.Length) continue;

                line = new Dictionary<string, string>();
                for (int x = 1; x < fields.Length; x++)
                {
                    line.Add(fields[x], values[x]);
                }
                result.Add(line);
            }

            return result;
        }

        /// <summary>
        /// Get a response from Perfect Money, parse it and check if it is a
        /// CSV table.
        /// </summary>
        /// <param name="method">Perfect Money API method</param>
        /// <param name="args">Argument pairs: title, value, title, value ...</param>
        /// <returns>A list of hashes which are table lines</returns>
        protected List<Dictionary<string, string>> FetchPerfectMoneyPageList(string method, params string[] args)
        {
            return ParsePerfectMoneyList(FetchPerfectMoneyPage(method, args));
        }
        #endregion

        public List<Dictionary<string, string>> GetEvouchersCreatedListing(string accountId, string passPhrase)
        {
            return FetchPerfectMoneyPageList("evcsv",
                "AccountID", accountId,
                "PassPhrase", passPhrase);
        }

        public List<Dictionary<string, string>> GetAccountHistory(string accountId, string passPhrase, DateTime start, DateTime end)
        {
            return FetchPerfectMoneyPageList("historycsv",
                "startday", start.Day.ToString(),
                "startmonth", start.Month.ToString(),
                "startyear", start.Year.ToString(),
                "endday", end.Day.ToString(),
                "endmonth", end.Month.ToString(),
                "endyear", end.Year.ToString(),
                "AccountID", accountId,
                "PassPhrase", passPhrase);
        }

        #endregion

    }
}