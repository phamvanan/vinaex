﻿
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;
using Narnia.Business.Transactions;
using Narnia.Web.Areas.Payments.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;

namespace Narnia.Web.Areas.Payments.Processors
{
    public class EthPaymentMethod: BasePaymentMethod
    {
        private EthPaymentSettings _paymentSettings = new EthPaymentSettings();

        public override PaymentMethodType PaymentMethodType => PaymentMethodType.Standard;

        public override bool SkipPaymentInfo
        {
            get { return true; }
        }

        public override string PaymentMethodDescription
        {
            get { return "ETH Payment gateway"; }
        }
        public EthPaymentMethod()
        {

        }
        public override string GetPublicViewComponentName()
        {
            return "Deposit.Eth";
        }
    }
}