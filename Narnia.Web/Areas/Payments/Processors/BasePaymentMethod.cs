﻿using Microsoft.AspNetCore.Http;
using Narnia.Business.Transactions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Web.Areas.Payments.Processors
{
    public abstract class BasePaymentMethod : IPaymentMethod
    {
        public virtual PaymentMethodType PaymentMethodType => PaymentMethodType.Standard;

        public virtual bool SkipPaymentInfo
        {
            get { return true; }
        }

        public virtual string PaymentMethodDescription
        {
            get { return "Payment gateway"; }
        }

        public virtual int GatewayId
        {
            get; private set;
        }

        public BasePaymentMethod()
        {

        }

        public virtual string GetPublicViewComponentName()
        {
            return "";
        }

        public virtual ProcessPaymentResult ProcessPayment(ProcessPaymentRequest processPaymentRequest)
        {
            return new ProcessPaymentResult();
        }

        public virtual IList<string> ValidatePaymentForm(IFormCollection form)
        {
            return new List<string>();
        }

        public virtual ProcessPaymentRequest GetPaymentInfo(IFormCollection form)
        {
            return new ProcessPaymentRequest();
        }
    }
}
