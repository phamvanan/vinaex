﻿
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;
using Narnia.Business.Transactions;
using Narnia.Core;
using Narnia.Core.Domain.Payments;
using Narnia.Web.Areas.Payments.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;

namespace Narnia.Web.Areas.Payments.Processors
{
    public class ManualBankPaymentMethod : BasePaymentMethod
    {
        private readonly IPaymentBiz _paymentBiz;
        private ManualBankPaymentSettings _paymentSettings = new ManualBankPaymentSettings();

        public override PaymentMethodType PaymentMethodType => PaymentMethodType.Standard;

        public override bool SkipPaymentInfo
        {
            get { return true; }
        }

        public override string PaymentMethodDescription
        {
            get { return "Manual Bank Payment gateway"; }
        }
        public ManualBankPaymentMethod(IPaymentBiz paymentBiz, IWorkContext workContext)
        {
            _paymentBiz = paymentBiz;
        }


        public override ProcessPaymentResult ProcessPayment(ProcessPaymentRequest processPaymentRequest)
        {
            ProcessPaymentResult result = new ProcessPaymentResult();
            //var deposit = _paymentBiz.DepositPre(processPaymentRequest.UserId, processPaymentRequest.Amount, processPaymentRequest.Fee, processPaymentRequest.AssetType, processPaymentRequest.Description);

           
            //if(deposit == null)
            //{
            //    result.AddError("Cannot deposit!!!");
            //    return result;
            //}

            try
            {
                int.TryParse(processPaymentRequest.GetCustomValue<string>("EvidencePictureId"), out int evidencePictureID);
                string orderNo = processPaymentRequest.GetCustomValue<string>("OrderNo");
                string bankName = processPaymentRequest.GetCustomValue<string>("BankName");
                string accountName = processPaymentRequest.GetCustomValue<string>("AccountName");
                string accountNumber = processPaymentRequest.GetCustomValue<string>("AccountNumber");
                DateTime billingDate = DateTime.Parse(processPaymentRequest.GetCustomValue<string>("BillingDate"));
                int depositId = processPaymentRequest.GetCustomValue<int>("DepositId");
                var bankTransfer = _paymentBiz.AddBankDepositInfo(processPaymentRequest.UserId, depositId, processPaymentRequest.Amount,
                    processPaymentRequest.Fee, processPaymentRequest.AssetTypeId, orderNo,
                    bankName,
                    accountName,
                    accountNumber,
                    billingDate,
                    evidencePictureID,
                    processPaymentRequest.Description
                    );
            }
            catch (Exception ex)
            {
                result.AddError("Error. Cannot deposit!!!");
                return result;
            }

            return result;
        }

        public override IList<string> ValidatePaymentForm(IFormCollection form)
        {
            if (form == null)
                throw new ArgumentException(nameof(form));

            //try to get errors
            if (form.TryGetValue("Errors", out StringValues errorsString) && !StringValues.IsNullOrEmpty(errorsString))
                return new[] { errorsString.ToString() }.ToList();

            return new List<string>();
        }

        public override ProcessPaymentRequest GetPaymentInfo(IFormCollection form)
        {
            if (form == null)
                throw new ArgumentException(nameof(form));

            var paymentRequest = new ProcessPaymentRequest();

            //pass custom values to payment method
            if (form.TryGetValue("AssetTypeId", out StringValues assetTypeId) && !StringValues.IsNullOrEmpty(assetTypeId))
                paymentRequest.AssetTypeId = assetTypeId.ToString();

            if (form.TryGetValue("Amount", out StringValues amount) && !StringValues.IsNullOrEmpty(amount) && decimal.TryParse(amount, out decimal amt))
                paymentRequest.Amount = amt;

            if (form.TryGetValue("Fee", out StringValues sfee) && !StringValues.IsNullOrEmpty(sfee) && decimal.TryParse(sfee, out decimal fee))
                paymentRequest.Fee = fee;

            if (form.TryGetValue("OrderNo", out StringValues orderNo) && !StringValues.IsNullOrEmpty(orderNo))
                paymentRequest.CustomValues.Add("OrderNo", orderNo.ToString());

            if (form.TryGetValue("BankName", out StringValues bankName) && !StringValues.IsNullOrEmpty(bankName))
                paymentRequest.CustomValues.Add("BankName", bankName.ToString());

            if (form.TryGetValue("AccountName", out StringValues accountName) && !StringValues.IsNullOrEmpty(accountName))
                paymentRequest.CustomValues.Add("AccountName", accountName.ToString());

            if (form.TryGetValue("AccountNumber", out StringValues accountNo) && !StringValues.IsNullOrEmpty(accountNo))
                paymentRequest.CustomValues.Add("AccountNumber", accountNo.ToString());

            if (form.TryGetValue("BillingDate", out StringValues billingDate) && !StringValues.IsNullOrEmpty(billingDate))
                paymentRequest.CustomValues.Add("BillingDate", billingDate.ToString());

            if (form.TryGetValue("EvidencePictureId", out StringValues EvidencePictureId) && !StringValues.IsNullOrEmpty(EvidencePictureId))
                paymentRequest.CustomValues.Add("EvidencePictureId", EvidencePictureId.ToString());
            return paymentRequest;
        }

        public override string GetPublicViewComponentName()
        {
            return "Deposit.ManualBank";
        }
    }
}