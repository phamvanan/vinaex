﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Routing;
using Narnia.Web.Framework;
using Narnia.Web.Framework.Mvc.Routing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Web.Areas.Payments.Routes
{
    public class PerfectMoneyRouteProvider : IRouteProvider
    {
        public void RegisterRoutes(IRouteBuilder routeBuilder)
        {
            routeBuilder.MapRoute("Payments.PerfectMoney.IPNHandler", "",
                 new { controller = "PerfectMoneyPayment", action = "IPNHandler" });

            routeBuilder.MapAreaRoute("Payments.PerfectMoney.Success", AreaNames.Payments, "pm/success",
                 new { controller = "PerfectMoneyPayment", action = "OnSuccess" });

            routeBuilder.MapAreaRoute("Payments.PerfectMoney.Cancel", AreaNames.Payments, "pm/cancelled",
                 new { controller = "PerfectMoneyPayment", action = "OnCancel" });

            routeBuilder.MapAreaRoute("Payments.PerfectMoney.StatusChanged", AreaNames.Payments, "pm/statuschanged",
                 new { controller = "PerfectMoneyPayment", action = "OnStatusChanged" });

        }

        public int Priority
        {
            get { return -1; }
        }
    }
}
