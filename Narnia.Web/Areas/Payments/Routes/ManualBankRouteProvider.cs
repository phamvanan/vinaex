﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Routing;
using Narnia.Web.Framework;
using Narnia.Web.Framework.Mvc.Routing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Web.Areas.Payments.Routes
{
    public class ManualBankRouteProvider : IRouteProvider
    {
        public void RegisterRoutes(IRouteBuilder routeBuilder)
        {
            routeBuilder.MapRoute("Payments.ManualBank.IPNHandler", "",
                 new { controller = "ManualBankPayment", action = "IPNHandler" });

        }

        public int Priority
        {
            get { return -1; }
        }
    }
}
