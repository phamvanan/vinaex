﻿using Microsoft.AspNetCore.Mvc;
using Narnia.Business.Transactions;
using Narnia.Core;
using Narnia.Web.Areas.Payments.Models;
using Narnia.Web.Areas.Payments.Processors;
using Narnia.Web.Framework;
using Narnia.Web.Framework.Mvc.Controlles;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Narnia.Web.Areas.Payments.Controllers
{
    [Area(AreaNames.Payments)]
    public class ManualBankPaymentController : BasePaymentController
    {
        private readonly IWorkContext _workContext;
        private readonly IPaymentBiz _paymentBiz;
        private readonly ManualBankPaymentMethod _processor;
        public ManualBankPaymentController(ManualBankPaymentMethod processor, IWorkContext workContext)
        {
            _processor = processor;
            _workContext = workContext;
        }
        [HttpPost]
        public IActionResult ProcessDeposit(ManualBankDepositModel model)
        {
            if (_workContext.CurrentUser == null || _workContext.CurrentUser.Id < 0)
            {
                return Content("Please login first");
            }
            if (ModelState.IsValid)
            {
                var paymentInfo = _processor.GetPaymentInfo(Request.Form);
                paymentInfo.UserId = _workContext.CurrentUser.Id;
                var result = _processor.ProcessPayment(paymentInfo);
                if (result.Success)
                    return Content("Deposit completed");
                else
                    return Content("Deposit error");
            }
            else
            {
                return Content("Invalid data");
            }
        }

        public IActionResult OnSuccess()
        {
            return View();
        }
        public IActionResult OnCancel()
        {
            return View();
        }
    }
}
