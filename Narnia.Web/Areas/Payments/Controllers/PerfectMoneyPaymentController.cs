﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Narnia.Business.Transactions;
using Narnia.Core.Infrastructure;
using Narnia.Web.Areas.Payments.Models;
using Narnia.Web.Areas.Payments.Processors;
using Narnia.Web.Framework;
using Narnia.Web.Framework.Mvc.Controlles;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

namespace Narnia.Web.Areas.Payments.Controllers
{
    [Area(AreaNames.Payments)]
    public class PerfectMoneyPaymentController: BasePaymentController
    {
        private readonly IPaymentBiz _paymentBiz;
        private readonly PerfectMoneyPaymentMethod _processor;
        public PerfectMoneyPaymentController(IPaymentBiz paymentBiz, PerfectMoneyPaymentMethod processor)
        {
            _paymentBiz = paymentBiz;
            _processor = processor;
        }

        public IActionResult OnSuccess()
        {
            byte[] parameters;
            using (var stream = new MemoryStream())
            {
                this.Request.Body.CopyTo(stream);
                parameters = stream.ToArray();
            }
            var strRequest = Encoding.ASCII.GetString(parameters);

            var data = HttpUtility.ParseQueryString(strRequest);
            string payeeAccount = data["PAYEE_ACCOUNT"];
            string payerAccount = data["PAYER_ACCOUNT"];
            string paymentAmount = data["PAYMENT_AMOUNT"];
            string paymentUnit = data["PAYMENT_UNITS"];
            string paymentId = data["PAYMENT_ID"];
            string memo = data["SUGGESTED_MEMO"];
            string batchNum = data["PAYMENT_BATCH_NUM"];
            string assetType = data["AssetTypeId"];
            string userId = data["UserId"];

            var response = _paymentBiz.CompleteGatewayDeposit(int.Parse(paymentId), int.Parse(userId), payerAccount);
            var model = new DepositCompleteModel();
            if (response.HasError)
            {
                ModelState.AddModelError("Error_1", response.Message);
                return Content(response.Message);
            }
            return Content("Deposit success");
            //return View(model);
        }
       
        public IActionResult OnStatusChanged()
        {
            byte[] parameters;
            using (var stream = new MemoryStream())
            {
                this.Request.Body.CopyTo(stream);
                parameters = stream.ToArray();
            }
            var strRequest = Encoding.ASCII.GetString(parameters);

            return View();
        }
        public IActionResult OnCancel()
        {
            PerfectMoneyPaymentMethod processor = new PerfectMoneyPaymentMethod();

            byte[] parameters;
            using (var stream = new MemoryStream())
            {
                this.Request.Body.CopyTo(stream);
                parameters = stream.ToArray();
            }
            var strRequest = Encoding.ASCII.GetString(parameters);

            var data = HttpUtility.ParseQueryString(strRequest);
            string payeeAccount = data["PAYEE_ACCOUNT"];
            string paymentAmount = data["PAYMENT_AMOUNT"];
            string paymentUnit = data["PAYMENT_UNITS"];
            string paymentId = data["PAYMENT_ID"];
            string memo = data["SUGGESTED_MEMO"];
            string batchNum = data["PAYMENT_BATCH_NUM"];
            string assetType = data["AssetTypeId"];
            string userId = data["UserId"];

            var response = _paymentBiz.CancelGatewayDeposit(int.Parse(paymentId));
            var model = new DepositCompleteModel();
            if (response.HasError)
            {
                ModelState.AddModelError("Error_1", response.Message);
                return Content(response.Message);
            }
            return Content("Payment cancelled completed");
            // return View(model);
        }
    }
}
