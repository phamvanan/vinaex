﻿using Microsoft.AspNetCore.Mvc;
using Narnia.Web.Areas.Payments.Processors;
using Narnia.Web.Framework;
using Narnia.Web.Framework.Mvc.Controlles;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Narnia.Web.Areas.Payments.Controllers
{
    [Area(AreaNames.Payments)]
    public class EthPaymentController : BasePaymentController
    {
        private readonly EthPaymentMethod _processor;
        public EthPaymentController(EthPaymentMethod processor)

        {
            _processor = processor;
        }

        public IActionResult OnSuccess()
        {
            return View();
        }
        public IActionResult OnCancel()
        {
            return View();
        }
    }
}
