﻿using Narnia.Core.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Web.Areas.Payments.Settings
{
    public class PerfectMoneyPaymentSettings: ISettings
    {
        public string PassPhrase { get; set; }
        public string PayeeName { get; set; }
        public string PayeeAccount_USD { get; set; }
        public string PayeeAccount_BTC { get; set; }
    }
}
