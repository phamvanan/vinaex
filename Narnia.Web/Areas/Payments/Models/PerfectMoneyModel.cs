﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Web.Areas.Payments.Models
{
    public class PerfectMoneyModel
    {
        /// <summary>
        /// Identifier for this payment. You may enter any word or text
        /// </summary>
        /// 
        public int UserId { get; set; }
        public int PaymentId { get; set; }
        public string Unit { get; set; }
        public decimal Amount { get; set; }
        public decimal Fee { get; set; }
        public string FromAccount { get; set; }
        public string ToAccount { get; set; }
        public string AssetTypeId { get; set; }
        public string PayeeAccount { get; set; }
        /// <summary>
        /// This is seen by the user who makes a payment
        /// </summary>
        public string PayeeName { get; set; }
        /// <summary>
        /// This is the URL (that can be used) to which merchant will address after a successful payment. 
        /// You can enter mailto:user@server.com, to direct all your payments to the indicated email.
        /// Ex:mailto:trinhthaituyen2003@yahoo.com
        /// </summary>
        public string StatusUrl { get; set; }
        /// <summary>
        /// This is the URL where user will be redirected after a successful payment
        /// </summary>
        public string SuccessUrl { get; set; }
        /// <summary>
        /// GET / POST / LINK
        /// </summary>
        public string PaymentUrlMethod { get; set; }
        /// <summary>
        /// This is the URL where user will be redirected after an unsuccessful payment attempt.
        /// </summary>
        public string CancelUrl { get; set; }
        /// <summary>
        /// GET / POST / LINK
        /// </summary>
        public string NoPaymentUrlMethod { get; set; }
    }
}
