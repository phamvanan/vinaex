﻿using Narnia.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Web.Areas.Payments.Models
{
    public class EthDepositModel:BaseModel
    {
        public int UserId { get; set; }
        public string WalletAddress { get; set; }
    }
}
