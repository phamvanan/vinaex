﻿using FluentValidation.Attributes;
using Narnia.Core.Domain;
using Narnia.Web.Areas.Payments.Validators;
using Narnia.Web.Framework.Mvc;
using Narnia.Web.Framework.Mvc.Validators;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Web.Areas.Payments.Models
{
    [Validator(typeof(ManualBankDepositValidator))]
    public class ManualBankDepositModel : BaseModel
    {
        public int UserId { get; set; }
        public List<BankAccount> SystemBankAccounts { get; set; }
        public decimal Amount { get; set; }
        public string OrderNo { get; set; }
        public string AssetTypeId { get; set; }
        [UIHint("Date")]
        [DisplayName("BillingDate")]
        public DateTime BillingDate { get; set; }
        public string BankName { get; set; }
        public string AccountName { get; set; }
        public string AccountNumber { get; set; }
        [UIHint("Picture")]
        [DisplayName("Picture")]
        public int EvidencePictureId { get; set; }
    }
}
