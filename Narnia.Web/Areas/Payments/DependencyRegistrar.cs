﻿using DryIoc;
using Narnia.Business.Transactions;
using Narnia.Core.Infrastructure;
using Narnia.Web.Areas.Payments.Processors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Web.Areas.Payments
{
    public class DependencyRegistrar : DryIocModule
    {
        protected override void Load(IRegistrator builder)
        {
            builder.Register<ManualBankPaymentMethod, ManualBankPaymentMethod>(Reuse.InWebRequest);
            builder.Register<EthPaymentMethod, EthPaymentMethod>(Reuse.InWebRequest);
            builder.Register<PerfectMoneyPaymentMethod, PerfectMoneyPaymentMethod>(Reuse.InWebRequest);
            builder.Register<IPaymentMethodFactory, PaymentMethodFactory>(Reuse.Singleton);
        }
    }
}
