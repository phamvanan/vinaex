﻿using Microsoft.Extensions.Primitives;
using Narnia.Business.Transactions;
using Narnia.Core.Infrastructure;
using Narnia.Web.Areas.Payments.Processors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Web.Areas.Payments
{
    public class PaymentMethodFactory : IPaymentMethodFactory
    {
        public PaymentMethodFactory()
        {
            AvailablePaymentMethods = new List<string>  { PaymentMethodNames.PerfectMoney};
        }
        public IList<string> AvailablePaymentMethods
        {
            get;private set;
        }

        public IPaymentMethod GetPaymentMethod(string gatewaySystemName)
        {
            switch (gatewaySystemName)
            {
                case PaymentMethodNames.PerfectMoney:
                    return EngineContext.Current.Resolve<PerfectMoneyPaymentMethod>();
            }
            return null;
        }

        public IPaymentMethod GetPaymentMethodsByAssetType(string assetType)
        {
            switch (assetType)
            {
                case "ETH":
                    return EngineContext.Current.Resolve<EthPaymentMethod>();
                case "VND":
                    return EngineContext.Current.Resolve<ManualBankPaymentMethod>();
                case "USD":
                case "BTC":
                case "USDT":
                    return EngineContext.Current.Resolve<PerfectMoneyPaymentMethod>();
            }
            return null;
        }
    }

    public class PaymentMethodNames
    {
        public const string PerfectMoney = "PerfectMoney";
    }
}
