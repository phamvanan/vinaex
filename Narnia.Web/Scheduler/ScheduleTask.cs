﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Threading.Tasks;

namespace Narnia.Web.Scheduler
{
    public class ScheduleTask : ScheduledProcessor
    {
        public ScheduleTask(IServiceScopeFactory serviceScopeFactory) : base(serviceScopeFactory)
        {
        }

        //https://thinkrethink.net/2018/05/31/run-scheduled-background-tasks-in-asp-net-core/
        //https://en.wikipedia.org/wiki/Cron
        protected override string Schedule => "*/1 * * * *";

        public override Task ProcessInScope(IServiceProvider serviceProvider)
        {
            Console.WriteLine($"Processing ScheduleTask starts here {DateTime.Now}");
            return Task.CompletedTask;
        }
    }
}