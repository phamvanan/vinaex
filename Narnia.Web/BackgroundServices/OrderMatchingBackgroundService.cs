﻿using Microsoft.Extensions.DependencyInjection;
using Narnia.Business.Impl;
using Narnia.Core.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Web.BackgroundServices
{
    public class OrderMatchingBackgroundService:BackgroundService
    {
        public OrderMatchingBackgroundService() : base()
        {
        }
        protected override Task Process()
        {
            IUserBusiness userBusiness = EngineContext.Current.Resolve<IUserBusiness>();

            Console.WriteLine("Processing starts here");

            return Task.CompletedTask;
        }
    }
}
