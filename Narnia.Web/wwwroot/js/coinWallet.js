﻿$(document).ready(function() {
    $("form[name='frmWithDraw']").validate({
        rules: {
            CoinWalletAddress: {
                required: true
            },
            WithdrawAmount: {
                required: true
            }
        },
        // Specify validation error messages
        messages: {
            CoinWalletAddress: "Please provide coin wallet address",
            WithdrawAmount: "Please provide a amount"
        },
        // Make sure the form is submitted to the destination defined
        // in the "action" attribute of the form when valid
        submitHandler: function (form) {
            form.submit();
        }
    });
});