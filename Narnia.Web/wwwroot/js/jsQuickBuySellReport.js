﻿
/*
 *  1: buy, 2: sell
 */
function QuickBuySellReport(type,coinNumber) {
    NarniaReport.call(this, {
        rptItemContainer: "#buy-bitcoin-panel",
        tmplRptItem: "#rpt-buy-bitcoin-item-tmpl"
    });

    if (type == 1)
        this.AjaxReportUrl = window.applicationBaseUrl + "/quick/buy-report";
    else
        this.AjaxReportUrl = window.applicationBaseUrl + "/quick/sell-report";

    this.AjaxParameters = { "CoinNumber": coinNumber };

}

QuickBuySellReport.prototype = new NarniaReport();
QuickBuySellReport.prototype.constructor = QuickBuySellReport;
QuickBuySellReport.prototype.parent = NarniaReport.prototype;