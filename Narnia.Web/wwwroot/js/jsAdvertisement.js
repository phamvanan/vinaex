﻿
function BuySellReport(type, coinTypeId, showOfflineUser, options) {
    var selectors = {
        pagingPanel: "#pagingPanel",
        rptItemContainer: "#body-report",
        tmplRptItem: "#rpt-item-tmpl",
        btnQuery: "#btnQuery"
    };
    $.extend(selectors, options);

    NarniaReport.call(this, selectors);

    this.AjaxReportUrl = window.applicationBaseUrl + "/Home/LoadAdvertisment";
    this.AjaxParameters = { "Type": type, "CoinTypeId": coinTypeId, "ShowOfflineUser": showOfflineUser};
}

BuySellReport.prototype = new NarniaReport();
BuySellReport.prototype.constructor = BuySellReport;
BuySellReport.prototype.parent = NarniaReport.prototype;


function BuySellReportByUserId(type, coinTypeId, options) {
    var selectors = {
        pagingPanel: "#pagingPanel",
        rptItemContainer: "#body-report",
        tmplRptItem: "#rpt-item-tmpl",
        btnQuery: "#btnQuery"
    };
    $.extend(selectors, options);

    NarniaReport.call(this, selectors);

    this.AjaxReportUrl = window.applicationBaseUrl + "/Account/LoadAdvertismentByUserId";
    this.AjaxParameters = { "Type": type, "CoinTypeId": coinTypeId};

}

BuySellReportByUserId.prototype = new NarniaReport();
BuySellReportByUserId.prototype.constructor = BuySellReport;
BuySellReportByUserId.prototype.parent = NarniaReport.prototype;