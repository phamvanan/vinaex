﻿function CommonHelper() {
    this.formatCurrency = function (amount, decimalPlaces) { return amount; }
}
//var CommonHelper = new CommonHelper();

var commonJs = (function () {

    var displayMessage = function (message, msgType) {
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "positionClass": "toast-top-right",
            "onClick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "8000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };
        toastr[msgType](message);
    };

    var showNoticeMessage = function () {
        if ($('#NotiSuccess').val()) {
            displayMessage($('#NotiSuccess').val(), 'success');
        }
        if ($('#NotiInfo').val()) {
            displayMessage($('#NotiInfo').val(), 'info');
        }
        if ($('#NotiWarning').val()) {
            displayMessage($('#NotiWarning').val(), 'warning');
        }
        if ($('#NotiError').val()) {
            displayMessage($('#NotiError').val(), 'error');
        }
    }

    // allow only number to be typed
    var allowOnlyNumberInput = function (evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }

    var allowNumberAndDot = function(event, input) {
        return (/^[0-9]*\.?[0-9]*$/).test($(input).val() + event.key);
    };

    var error = function (result, status, message) {
        console.log(result.status, status, message);
    }

    return {
        showNoticeMessage: showNoticeMessage,
        displayMessage: displayMessage,
        allowOnlyNumberInput: allowOnlyNumberInput,
        allowNumberAndDot: allowNumberAndDot,
        error: error
    }
}());
