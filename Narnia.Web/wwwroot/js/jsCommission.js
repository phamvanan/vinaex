﻿function CommissionReport(UserId, options) {
    var selectors = {
        pagingPanel: "#pagingPanel",
        rptItemContainer: "#body-report",
        tmplRptItem: "#rpt-item-tmpl",
        btnQuery: "#btnQuery"
    };
    $.extend(selectors, options);

    NarniaReport.call(this, selectors);

    this.AjaxReportUrl = window.applicationBaseUrl + "/Account/LoadCommissionPaging";
    this.AjaxParameters = { "UserId": UserId };
}

CommissionReport.prototype = new NarniaReport();
CommissionReport.prototype.constructor = CommissionReport;
CommissionReport.prototype.parent = NarniaReport.prototype;