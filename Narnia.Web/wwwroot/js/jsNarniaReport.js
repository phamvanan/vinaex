﻿function NarniaReport(selectors) {
    var defaultSelectors = {
        pagingPanel: "#pagingPanel",
        rptItemContainer: "#body-report",
        tmplRptItem: "#rpt-item-tmpl",
        btnQuery: "#btnQuery"
    };
    $.extend(defaultSelectors, selectors);
    this.selectors = defaultSelectors;

    this.AjaxReportUrl = '';
    this.AjaxType = 'POST';
    this.AjaxParameters = {};
    this.AjaxDataType = "json";
    this.AjaxContentType = "application/json; charset=utf-8";

    this.NoDataFoundData = {
        TotalPages: 0,
        data: []
    };
    this.helpers = {
        formatCurrency: function (amount) {
            return accounting.formatMoney(amount, "", 5, ",", ".");
        },
    };
    this.PaginationSettings = { PageSize: 5 };
}
NarniaReport.prototype.createAjaxParametersFunc = null;

NarniaReport.prototype.generatePaging = function (p, tp) {
    if (tp <= 1) {
        $(this.selectors.pagingPanel).css("display", "none");
        return;
    }
    var me = this;
    $(this.selectors.pagingPanel).pagination({
        pages: tp,
        currentPage: p,
        cssStyle: 'dark-theme',
        prevText: '<i class="fa fa-caret-left"></i>',
        nextText: '<i class="fa fa-caret-right"></i>',
        displayedPages: 3,
        edges: 1,
        onPageClick: function (pageNumber, event) {
            me.loadData(pageNumber);
        }
    });
    $(this.selectors.pagingPanel).css("display", "");
}

NarniaReport.prototype.init = function () {
    var me = this;
    $(me.selectors.btnQuery).unbind('click');
    $(me.selectors.btnQuery).click(function () {
        me.getData(0);
    });

    // Fill default data.
    var htmlTable = $(me.selectors.tmplRptItem).render(this.NoDataFoundData, this.helpers);
    $(me.selectors.rptItemContainer).html(htmlTable);

}

NarniaReport.prototype.showReport = function (data) {
    var htmlTable = $(this.selectors.tmplRptItem).render(data,
        this.helpers);
    $(this.selectors.rptItemContainer).html(htmlTable);
    this.generatePaging(data.pageIndex + 1, data.totalPages);
    if (this.showReportCompleted != null && typeof this.showReportCompleted == "function") {
        this.showReportCompleted(data);
    }
};

NarniaReport.prototype.getAjaxParams = function (pageNo,pageSize) {
    var ajaxParams = null;
    if (this.createAjaxParametersFunc != null && typeof this.createAjaxParametersFunc == "function") {
        ajaxParams = this.createAjaxParametersFunc();
    } else {
        ajaxParams = this.AjaxParameters;
    }
    if (ajaxParams == null)
        ajaxParams = {};
    ajaxParams.PageNo = pageNo;
    ajaxParams.PageSize = pageSize;

    var obj = {
        context: this,
        async: true,
        url: this.AjaxReportUrl,
        type: this.AjaxType,
        data: JSON.stringify(ajaxParams),
        dataType: "json",
        contentType: this.AjaxContentType,
        success: function (data) { this.onLoadDataSuccess(data); },
        error: function () { this.onLoadDataError(); }
    };

    return obj;
};

NarniaReport.prototype.showNoDataTemplate = function () {
    //this.showReport(this.NoDataFoundData);
};

NarniaReport.prototype.dataLoading = function () {
};
NarniaReport.prototype.loadData = function (page) {
    if (page === undefined) {
        page = 1;
    }

    this.showNoDataTemplate();
    var ajaxParam = this.getAjaxParams(page, this.PaginationSettings.PageSize);
    this.dataLoading();
    $.ajax(ajaxParam);
};

NarniaReport.prototype.onLoadDataCompletedCallback = function (data) {

};
NarniaReport.prototype.onLoadDataSuccess = function (data) {
    this.showReport(data);
    this.onLoadDataCompletedCallback(data);
};

NarniaReport.prototype.onLoadDataError = function () {
};