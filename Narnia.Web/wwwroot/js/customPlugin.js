//Jacob.

(function ($) {

    $.fn.addRequiredRule = function (mes) {
        this.rules("add", {
            required: true,
            messages: {
                required: mes
            }
        });
    };

    $.fn.selectAll = function (selectors, defaultChecked) {
        defaultChecked = (defaultChecked == undefined || defaultChecked == null) ? false : true;
        var children = $(selectors);
        this.prop("checked", defaultChecked);
        children.prop("checked", defaultChecked);

        this.click(function () {
            children.prop('checked', this.checked);
        });
        var me = this;
        children.click(function () {
            var numOfCheckedItems = 0;
            $('input:checkbox:gt(0)').each(function () {
                if (this.checked) {
                    numOfCheckedItems++;
                }
            });
            var prevVal = me.prop("checked");
            switch (numOfCheckedItems) {
                case 0:
                    me.prop("checked", false);
                    me.prop("indeterminate", false);
                    break;
                case children.length:
                    me.prop("checked", true);
                    me.prop("indeterminate", false);
                    break;
                default:
                    me.prop("indeterminate", true);
                    break;
            }
        });
        return this;
    };

    $.fn.forceNumeric = function () {
        return this.each(function () {
            $(this).keydown(function (e) {
                var key = e.which || e.keyCode;
                if (!e.shiftKey && !e.altKey && !e.ctrlKey &&
                    // numbers   
                    key >= 48 && key <= 57 ||
                    // Numeric keypad
                    key >= 96 && key <= 105 ||
                    // comma, period and minus, . on keypad
                    key === 190 || key === 188 || key === 109 || key === 110 ||
                    // Backspace and Tab and Enter
                    key === 8 || key === 9 || key === 13 ||
                    // Home and End
                    key === 35 || key === 36 ||
                    // left and right arrows
                    key === 37 || key === 39 ||
                    // Del and Ins
                    key === 46 || key === 45)
                    return true;

                return false;
            }).bind("blur", function () {
                if ($(this).val().trim().length !== 0) {
                    var val = parseFloat($(this).val());
                    if (isNaN(val)) {
                        $(this).val("");
                    } else {
                        $(this).val(val);
                    }
                }
            });

        });
    };

}(jQuery));