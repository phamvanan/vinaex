$.validator.addMethod("greaterthan", function (value, element, params) {
    var minValue = parseFloat($(params).val());
    return value > minValue;
});
$.validator.unobtrusive.adapters.add("greaterthan", ["otherpropertyname"], function (options) {
    options.rules["greaterthan"] = "#" + options.params.otherpropertyname;
    options.messages["greaterthan"] = options.message;
});

$.validator.addMethod("greaterthanequal", function (value, element, params) {
    var minValue = parseFloat($(params).val());
    return value >= minValue;
});
$.validator.unobtrusive.adapters.add("greaterthanequal", ["otherpropertyname"], function (options) {
    options.rules["greaterthanequal"] = "#" + options.params.otherpropertyname;
    options.messages["greaterthanequal"] = options.message;
});


$.validator.addMethod("lessthanequal", function (value, element, params) {
    var maxValue = parseFloat($(params).val());
    return value <= maxValue;
});
$.validator.unobtrusive.adapters.add("lessthanequal", ["otherpropertyname"], function (options) {
    options.rules["lessthanequal"] = "#" + options.params.otherpropertyname;
    options.messages["lessthanequal"] = options.message;
});

$.validator.addMethod("notequal", function (value, element, params) {
    var valueSecond = parseInt($(params).val());
    return value !== valueSecond;
});
$.validator.unobtrusive.adapters.add("notequal", ["otherpropertyname"], function (options) {
    options.rules["notequal"] = "#" + options.params.otherpropertyname;
    options.messages["notequal"] = options.message;
});


$.validator.addMethod("currencygreaterthanequal", function (value, element, params) {
    var minValue = parseFloat($(params).val());
    return value >= minValue;
});
$.validator.unobtrusive.adapters.add("currencygreaterthanequal", ["otherpropertyname"], function (options) {
    options.rules["currencygreaterthanequal"] = "#" + options.params.otherpropertyname;
    options.messages["currencygreaterthanequal"] = function () {
        var otherValue = parseFloat($("#" + options.params.otherpropertyname).val());
        return options.message.format(otherValue != isNaN ? formatCurrencyValue(otherValue) : "");
    };
});

$.validator.addMethod("currencylessthanequal", function (value, element, params) {
    var maxValue = parseFloat($(params).val());
    return value <= maxValue;
});
$.validator.unobtrusive.adapters.add("currencylessthanequal", ["otherpropertyname"], function (options) {
    options.rules["currencylessthanequal"] = "#" + options.params.otherpropertyname;
    options.messages["currencylessthanequal"] = function () {
        var otherValue = parseFloat($("#" + options.params.otherpropertyname).val());
        return options.message.format(otherValue != isNaN ? formatCurrencyValue(otherValue) : "");
    };
});
