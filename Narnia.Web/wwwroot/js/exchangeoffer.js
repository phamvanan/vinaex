﻿//$(document).ready(function () {
//    $("div.bhoechie-tab-menu>div.list-group.list-sell>a").click(function (e) {
//        e.preventDefault();
//        $(this).parents('.bhoechie-tab-menu').find('a').removeClass("active");
//        $(this).siblings('a.active').removeClass("active");
//        $(this).addClass("active");
//        var index = $(this).index();
//        $(".bhoechie-tab-content-buy").css("display", "none");
//        $("div.bhoechie-tab>div.bhoechie-tab-content-sell").removeClass("active");
//        $("div.bhoechie-tab>div.bhoechie-tab-content-sell").eq(index).addClass("active").removeClass("not-active");;
//        $("div.bhoechie-tab>div.bhoechie-tab-content-buy").removeClass("active")
//        $(".bhoechie-tab-content-buy.not-active").css("display", "none");
//    });
//    $("div.bhoechie-tab-menu>div.list-group.list-buy>a").click(function (e) {
//        e.preventDefault();
//        $(this).parents('.bhoechie-tab-menu').find('a').removeClass("active");
//        $(this).siblings('a.active').removeClass("active");
//        $(this).addClass("active");
//        var index = $(this).index();
//        $("div.bhoechie-tab>div.bhoechie-tab-content-sell").removeClass("active");
//        $("div.bhoechie-tab>div.bhoechie-tab-content-buy").removeClass("active").addClass("not-active");
//        $("div.bhoechie-tab>div.bhoechie-tab-content-buy").eq(index).addClass("active").removeClass("not-active");
//        $(".bhoechie-tab-content-buy.active").css("display", "block");
//        $(".bhoechie-tab-content-buy.not-active").css("display", "none");
//    });
//    $("div.bhoechie-tab-menu>div.list-group.list-trade>a").click(function (e) {
//        e.preventDefault();
//        $(this).parents('.bhoechie-tab-menu').find('a').removeClass("active");
//        $(this).siblings('a.active').removeClass("active");
//        $(this).addClass("active");
//        var index = $(this).index();
//        $(".bhoechie-tab-content-buy").css("display", "none");
//        $("div.bhoechie-tab>div.bhoechie-tab-content-trade").removeClass("active");
//        $("div.bhoechie-tab>div.bhoechie-tab-content-trade").eq(index).addClass("active").removeClass("not-active");;
//        $("div.bhoechie-tab>div.bhoechie-tab-content-buy").removeClass("active")
//        $(".bhoechie-tab-content-buy.not-active").css("display", "none");
//    });
//});
$(document).ready(function () {
    $("form[name='frm-exchange-offer-sell']").validate({
        rules: {
            UserBankAccountNo: {
                required: true
            },
            UserBankAccountName: {
                required: true
            },
            Phone: {
                required: true,
                rangelength: [10,11]
            }
        },
        // Specify validation error messages
        messages: {
            UserBankAccountNo: "Vui lòng nhập số tài khoản",
            UserBankAccountName: "Vui lòng nhập tên tài khoản",
            Phone: "Vui lòng nhập số điện thoại"
        },
        // Make sure the form is submitted to the destination defined
        // in the "action" attribute of the form when valid
        submitHandler: function (form) {
            form.submit();
        }
    });
});

var exchaneOfferJs = (function () {
    var functionName = function() {
        console.log('hello offer js');
    };


    return {
        functionName: functionName
    };
}());