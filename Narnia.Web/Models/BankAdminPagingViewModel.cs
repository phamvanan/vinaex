﻿using System;

namespace Narnia.Web.Models
{
    public class BankAdminPagingViewModel
    {
        public int id { get; set; }
        public string BankName { get; set; }
        public string BankCode { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}