﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Web.Models
{
    public class QuickBuySellRequest
    {
        public int AdId { get; set; }
        public decimal coinNumber { get; set; }
        public string walletAddress { get; set; }
    }
}
