﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Web.Models
{
    public class RatingViewModel
    {
        public int Id { get; set; }
        public string Content { get; set; }
        /// <summary>
        /// people's rating
        /// </summary>
        public string Alias { get; set; }
    }
}
