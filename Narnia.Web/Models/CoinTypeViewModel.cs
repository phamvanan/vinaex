﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Web.Models
{
    public class CoinTypeViewModel
    {
        public int Id { get; set; }
        public string CoinCode { get; set; }
        public string CoinName { get; set; }
    }
}
