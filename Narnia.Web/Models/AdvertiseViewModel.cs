﻿using Narnia.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Web.Models
{
    public class AdvertiseViewModel
    {
        public long Id { get; set; }       
        public decimal CoinPrice { get; set; }
        public decimal MinCoinPrice { get; set; }
        public decimal MaxCoinPrice { get; set; }
        public double MinCoinNumber { get; set; }
        public double MaxCoinNumber { get; set; }
        public int PaymentAllowTime { get; set; }
        public DateTime CreatedDate { get; set; }
        public bool Approved { get; set; }
        public string CurrencyId { get; set; }
        public string AssetTypeId { get; set; }

        public User AdvertiseUser { get; set; }
        //public CoinSymbol CoinType { get; set; }
        public Currency CurrencyType { get; set; }
        public Banks Bank { get; set; }
        public PaymentMethod PayType { get; set; }
        public decimal PriceDisplay
        {
            get
            {
                if (CoinPrice > 0)
                    return CoinPrice;

                return CurrencyType.Rate;
            }
        }
    }
}
