﻿using System.Collections.Generic;

namespace Narnia.Web.Models
{
    public class DepositVNDViewModel
    {
        public DepositVNDViewModel()
        {
            banks = new List<BankAccountViewModel>();
        }

        public int BankId { get; set; }
        public decimal DepositAmount { get; set; }
        public List<BankAccountViewModel> banks { get; set; }
    }
}