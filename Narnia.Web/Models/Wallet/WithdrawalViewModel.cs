﻿using System.Collections.Generic;

namespace Narnia.Web.Models
{
    public class WithdrawalViewModel
    {
        public WithdrawalViewModel()
        {
            banks = new List<BankAccountViewModel>();
        }

        public decimal BalanceVND { get; set; }
        public int BankId { get; set; }
        public decimal WithdrawalAmount { get; set; }
        public List<BankAccountViewModel> banks { get; set; }
    }
}