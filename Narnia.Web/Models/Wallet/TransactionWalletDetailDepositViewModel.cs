﻿using Narnia.Core.Enums;
using Narnia.Core.Helpers;
using System;

namespace Narnia.Web.Models
{
    public class TransactionWalletDetailVNDViewModel
    {
        public int TransactionId { get; set; }
        public decimal Amount { get; set; }
        public string TransactionCode { get; set; }
        public string Branch { get; set; }
        public string BankName { get; set; }
        public string AccountNo { get; set; }
        public string AccountName { get; set; }
        public int Status { get; set; }
        public string StatusDisplay => ResourceHelper.DisplayForEnumValue((TransactionStautus)Status);
        public int? EvidenceId { get; set; }
        public int UserId { get; set; }
        public int TransactionType { get; set; }
        public DateTime? ConfirmDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }
}