﻿using System;
using Narnia.Core.Enums;
using Narnia.Core.Helpers;

namespace Narnia.Web.Models.Wallet
{
    public class TransactionWalletPagingViewModel
    {
        public string TransactionId { get; set; }
        public string TransactionCode { get; set; }
        public DateTime CreatedDate { get; set; }
        public decimal Amount { get; set; }
        public int Status { get; set; }
        public string StatusDisplay=> ResourceHelper.DisplayForEnumValue((TransactionStautus)Status);
    }
}