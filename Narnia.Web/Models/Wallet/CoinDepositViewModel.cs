﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Web.Models.Wallet
{
    public class CoinDepositViewModel
    {
        public CoinDepositViewModel()
        {
        }
        public string CoinType { get; set; }
        public string WalletAddress { get; set; }
    }
}
