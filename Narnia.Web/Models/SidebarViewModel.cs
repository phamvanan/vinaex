﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Web.Models
{
    public class SidebarViewModel
    {
        public SidebarViewModel()
        {
            Symbols = new List<SymbolViewModel>();
        }
        public string CurrentSymbol { get; set; }
        public List<SymbolViewModel> Symbols { get; set; }

        public bool IsAuthenticated { get; set; }
    }
    public class SymbolViewModel
    {
        public int Id { get; set; }
        public string Symbol { get; set; }
        public string Name { get; set; }
    }
}
