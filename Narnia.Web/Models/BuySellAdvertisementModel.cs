﻿using MicroOrm.Pocos.SqlGenerator.Attributes;
using System;

namespace Narnia.Web.Models
{
    public class BuySellAdvertisementModel
    {
        public Boolean IsSale { get; set; }
        public int UserId { get; set; }
        public string CurrencyId { get; set; }
        public string AssetTypeId { get; set; }
        public int BankId { get; set; }
        public string BankAccountNumber { get; set; }
        public string BankAccountName { get; set; }
        public int CoinExchangeId { get; set; }
        public decimal CoinPrice { get; set; }
        public decimal MinCoinPrice { get; set; }
        public decimal MaxCoinPrice { get; set; }
        public double MinCoinNumber { get; set; }
        public double MaxCoinNumber { get; set; }
        public int PaymentMethodId { get; set; }
        public int PaymentAllowTime { get; set; }
        public int CountryId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }

        [NonStored()]
        public Byte[] RowVersionNo { get; set; }

        public Boolean Approved { get; set; }
    }
}