﻿using Narnia.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Web.Models.Messages
{
    public class MessageTemplateModel:BaseEntityModel
    {
        public MessageTemplateModel()
        {
        }


        public string AllowedTokens { get; set; }

        public string Name { get; set; }

        public string Subject { get; set; }

        public string Body { get; set; }

        public bool IsActive { get; set; }
    }

}