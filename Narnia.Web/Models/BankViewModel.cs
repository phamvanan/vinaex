﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Web.Models
{
    public class BankViewModel
    {
        public int Id { get; set; }
        public string BankName { get; set; }
        public string BankCode { get; set; }
        public string WebsiteUrl { get; set; }
    }
}
