﻿namespace Narnia.Web.Models.Paging
{
    public class LoadAdvertismentRequest : PagedRequest
    {
        public int Type { get; set; }   // 1: ban, 2 mua
        public string AssetTypeId { get; set; }
        public bool ShowOfflineUser { get; set; }   //true: show both user online and offline
    }
}