﻿namespace Narnia.Web.Models.Paging
{
    public class TransactionWalletVNDHistoryRequest : PagedRequest
    {
        public int TransactionType { get; set; }
    }
}