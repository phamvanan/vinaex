﻿using Narnia.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Narnia.Web.Models.AccountViewModels
{
    public class ProfileViewModel:BaseModel
    {
        public ProfileViewModel()
        {
            AssetBalances = new List<AssetBalanceViewModel>();
        }
        public int UserId { get; set; }
        public string Alias { get; set; }
        public string IDNumber { get; set; }
        public string UrlFaceBook { get; set; }
        public string UrlTwitter { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedDateDisplay { get { return CreatedDate.ToString(); } }
        public DateTime UpdatedDate { get; set; }
        public string UpDatedDisplay { get { return UpdatedDate.ToString(); } }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public DateTime LastLoginDate { get; set; }
        [Display(Name = "Avatar")]
        public byte[] Image { get; set; }
        public string ImageDisplay
        {
            get
            {
                if (Image == null || Image.Length == 0)
                {
                    return string.Empty;
                }
                return $"data:{ContentType};base64,{Convert.ToBase64String(Image)}";
            }
        }
        public bool HasReceivedDailyPrice { get; set; }
        public bool HasSendEmailExchangeTransaction { get; set; }
        public bool HasSendEmailNewTransaction { get; set; }
        public bool HasSendSMSNewTransaction { get; set; }
        public bool HasSendSMSTransactionPaid { get; set; }
        public string AssetTypeId { get; set; }
        public IList<AssetBalanceViewModel> AssetBalances { get; set; }
        public string ReferralLink { get; set; }
        public string ReferenceCode { get; set; }
        public string ContentType { get; set; }
        public bool CanShowBalance { get; set; } = false;
        public bool CanShowCommission { get; set; } = false;
        public string IPAddress { get; set; }
    }
}
