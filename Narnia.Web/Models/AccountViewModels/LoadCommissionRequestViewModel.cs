﻿using Narnia.Web.Models.Paging;

namespace Narnia.Web.Models.AccountViewModels
{
    public class LoadCommissionRequestViewModel : PagedRequest
    {
        public int UserId { get; set; }
    }
}