﻿using System.ComponentModel.DataAnnotations;

namespace Narnia.Web.Models.AccountViewModels
{
    public class UserViewModel
    {
        public int Id { get; set; }
        //[Required()]
        public string UserName { get; set; }
        [DataType(DataType.EmailAddress, ErrorMessage = "E-mail is not valid")]
        [Required()]
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        [DataType(DataType.PhoneNumber, ErrorMessage = "Phone is not valid")]
        public string Phone { get; set; }
        //[Required()]
        public string PassWord { get; set; }
        //[Required()]
        public string ConfirmPassWord { get; set; }
        public string ReferenceCode { get; set; }

    }
}
