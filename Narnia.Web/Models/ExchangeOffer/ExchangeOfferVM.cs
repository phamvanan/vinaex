﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Narnia.Core;
using Narnia.Core.Enums;
using Narnia.Web.Framework.Mvc;

namespace Narnia.Web.Models.ExchangeOffer
{
    public class ExchangeOfferVM : BaseEntityModel
    {
        public ExchangeOfferVM()
        {
            this.AssetTypeId = string.Empty;
            this.AssetAmountReserves = string.Empty;
            this.BankAccount = new List<BankAccountViewModel>();
        }

        [Required(ErrorMessage = "Loại tiền không hợp lệ")]
        public string AssetTypeId { get; set; }

        public string AssetTypeName
        {
            get
            {
                if (this.AssetTypeId.Equals(AssetTypeEnum.PM.ToString()))
                {
                    return AssetTypeEnum.PM.GetDisplayName();
                }

                return AssetTypeId;
            }
        }

        [Required(ErrorMessage = "Số lượng không hơp lệ")]
        [Range(1, double.MaxValue, ErrorMessage = "Số lượng không hơp lệ")]
        public decimal Amount { get; set; }
        [Required(ErrorMessage = "Số tiền không hợp lệ")]
        public decimal VNDAmount { get; set; }

        public string VNDAmountDisplay => VNDAmount.ToString("N");
        public decimal Fee { get; set; }
        [Required(ErrorMessage = "loai giao dich khong hop le")]
        public int Type { get; set; }

        public string TypeDisplay
        {
            get {
                if (Type == (int)ExchangeOfferType.Buy)
                {
                    return "mua";
                }
                if (Type == (int)ExchangeOfferType.Sell)
                {
                    return "bán";
                }

                return "trao đổi";
            }
        }

        public string UserBankAccountNo { get; set; }
        public string UserBankAccountName { get; set; }
        public int Status { get; set; }

        public string StatusDisplay
        {
            get
            {
                if (Status == (int)ExchangeOfferStatus.Pending)
                {
                    return "Pending";
                }

                if (Status == (int)ExchangeOfferStatus.Complete)
                {
                    return "Complete";
                }

                return "Cancel";
            }
        }
        public string ExchangeOfferCode { get; set; }
        public DateTime CreatedDate { get; set; }

        public string CreatedDateDisplay => CreatedDate.ToString("G");
        public string WalletAddress { get; set; }
        public string Phone { get; set; }
        public string PerfectMoneyUSDAccount { get; set; }
        public decimal VNDReserves { get; set; }
        public string VNDReservesDisplay => VNDReserves.ToString("N");
        public string AssetAmountReserves { get; set; }
        public List<BankAccountViewModel> BankAccount { get; set; }
    }
}
