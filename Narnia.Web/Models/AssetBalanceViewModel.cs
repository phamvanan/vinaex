﻿using Narnia.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Web.Models
{
    public class AssetBalanceViewModel:BaseModel
    {
        public int UserId { get; set; }
        public string AssetType { get; set; }
        public decimal Balance { get; set; }
        public decimal FrozenBalance { get; set; }
    }
}
