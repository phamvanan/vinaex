﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Web.Models
{
    public class WithdrawHistoryViewModel
    {
        public int Id { get; set; }
        public string TransactionHash { get; set; }
        public decimal WithdrawAmount { get; set; }
        public int Status { get; set; }
        public decimal BalanceChange { get; set; }
        public string WalletAddress { get; set; }
        public DateTime WithdrawDate { get; set; }

        public string StatusDisplay {
            get {
                switch (Status)
                {
                    case 0:
                        return "Cancel";
                    case 1:
                        return "Success";

                    default:
                        return "Pending";
                }
            }
        }
    }
}
