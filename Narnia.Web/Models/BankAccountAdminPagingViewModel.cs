﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Web.Models
{
    public class BankAccountAdminPagingViewModel
    {
        public string BankName { get; set; }
        public string AccountName { get; set; }
        public string AccountNo { get; set; }
        public string Branch { get; set; }
        public DateTime CreatedDate { get; set; }
        public bool IsActived { get; set; }
        public int Id { get; set; }
    }
}
