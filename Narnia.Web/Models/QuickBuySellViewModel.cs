﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Web.Models
{
    public class QuickBuySellViewModel
    {       
        public string CoinName { get; set; }
        public string AssetTypeId { get; set; }
        public string CurrencyName { get; set; }        
        public decimal ExchangeRate { get; set; }
    }
}
