﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Web.Models.BankAccounts
{
    public class BankDepositInfo
    {
        public BankDepositInfo() { }
        public int SystemBank { get; set; }
        public decimal Amount { get; set; }
        public string OrderNo { get; set; }       
    }
}
