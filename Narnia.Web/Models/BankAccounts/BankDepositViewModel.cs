﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Web.Models.BankAccounts
{
    public class BankDepositViewModel
    {
        public BankDepositViewModel() { }

        public List<SelectListItem> SystemBankList { get; set; }
        public List<BankAccountViewModel> SystemBankAccounts { get; set; }
        public decimal Amount { get; set; }
        public string OrderNo { get; set; }
        public DateTime BillingDate { get; set; }
    }
}
