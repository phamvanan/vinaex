﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Web.Models.BankAccounts
{
    public class BankAccountViewModel
    {
        public int Id { get; set; }
        public string AccountName { get; set; }
        public string AccountNo { get; set; }
        public string Branch { get; set; }
        public BankViewModel Bank { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
