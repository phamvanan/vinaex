﻿using Narnia.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Web.Models.Common
{
    public class LanguageModel:BaseEntityModel
    {
        public string Name { get; set; }

        public string FlagImageFileName { get; set; }
    }
}
