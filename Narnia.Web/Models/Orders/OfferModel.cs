﻿using Narnia.Core.Domain2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Web.Models.Orders
{
    public class OfferModel
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int SymbolId { get; set; }
        public TradeSide Side { get; set; }
        public OfferStatus Status { get; set; }
        public decimal Quantity { get; set; }
        public decimal Price { get; set; }
        public decimal? MaxPrice { get; set; }
        public decimal? MinPrice { get; set; }
        public int CountryId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public DateTime? ExpiredDate { get; set; }
        public bool Approved { get; set; }
        public decimal ProcessedQuantity { get; set; }
        public decimal AvailableQuantity
        {
            get
            {
                return this.Quantity - this.ProcessedQuantity;
            }
        }
    }
}
