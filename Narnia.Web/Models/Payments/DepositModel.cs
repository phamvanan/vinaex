﻿using Narnia.Business.Transactions;
using Narnia.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Web.Models.Payments
{
    public class DepositModel:BaseModel
    {
        [Required(ErrorMessage = "Bạn phải nhập tổng số tiền muốn nạp")]
        [Range(0.0, Double.MaxValue, ErrorMessage = "Dữ liệu không hợp lệ")]
        public decimal Amount { get; set; }
        public string AssetTypeId { get; set; }
        public decimal Fee { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string Description { get; set; }
        public string GatewayComponentName { get; set; }
        public ProcessPaymentRequest PaymentRequestInfo { get; set; }
    }
}
