﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Narnia.Core;
using Narnia.Core.Enums;

namespace Narnia.Web.Models
{
    public class CoinSymbolViewModel
    {
        public int Id { get; set; }
        public string SymbolId { get; set; }
        public string ExchangeId { get; set; }
        public string SymbolType { get; set; }
        public string AssetIdBase { get; set; }
        public string AssetTypeName
        {
            get
            {
                if (this.AssetIdBase.Equals(AssetTypeEnum.PM.ToString()))
                {
                    return AssetTypeEnum.PM.GetDisplayName();
                }

                return AssetIdBase;
            }
        }

        public string AssetIdQuote { get; set; }
        public decimal AvgPrice { get; set; }
        public decimal BuyPrice { get; set; }
        public decimal SellPrice { get; set; }
        public string Value
        {
            get
            {
                return $"{AssetIdBase}/{AssetIdQuote}";
            }
        }
    }
}
