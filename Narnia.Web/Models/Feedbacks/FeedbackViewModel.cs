﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Web.Models.Feedbacks
{
    public class FeedbackViewModel
    {
        public string Content { get; set; }
        public int CreatedBy { get; set; }
        public UserAbbrViewModel CreatedUser { get; set; }
        public DateTime CreatedDate { get; set; }
        public int UserId { get; set; }
        public UserAbbrViewModel ReceivedUser { get; set; }
    }
}
