﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Web.Models
{
    public class DrawViewModel
    {
        public int Id { get; set; }
        public string DrawNo { get; set; }
        public DateTime CreatedDate { get; set; }
        public char? Result { get; set; }
    }
}
