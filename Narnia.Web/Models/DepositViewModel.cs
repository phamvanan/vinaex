﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Web.Models
{
    public class DepositViewModel
    {
        public int Id { get; set; }
        public string TransactionHash { get; set; }
        public decimal Amount { get; set; }
        public string FromAddress { get; set; }
        public string ToAddress { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
