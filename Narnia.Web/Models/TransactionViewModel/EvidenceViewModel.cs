﻿namespace Narnia.Web.Models
{
    public class EvidenceViewModel
    {
        public long Id { get; set; }

        public string FileName { get; set; }

        public byte[] Image { get; set; }
        public string Transcode { get; set; }
    }
}