﻿using System;

namespace Narnia.Web.Models
{
    public class InfoAdvertisementDetailViewModel
    {
        public int TransactionId { get; set; }
        public decimal AmountCoin { get; set; }
        public string AssetTypeId { get; set; }
        public string UserName { get; set; }
        public string WalletAddress { get; set; }
        public DateTime CreatedDate { get; set; }
        public string PaymentMethodName { get; set; }
        public string BankName { get; set; }
        public string BankAccountNameAds { get; set; }
        public string BankAccountNumberAds { get; set; }
        public int? EvidenceId { get; set; }
        public int PaymentAllowTime { get; set; }
        public string TransactionCode { get; set; }
        public bool IsSellTransaction { get; set; }

        public string Alias { get; set; }
        public  int Status { get; set; }
        public byte[] Image { get; set; }
        public int UserIdAds { get; set; }
        public int UserIdTran { get; set; }
        public bool TransIsValid { get; set; } = true;
        public bool IsAllowUploadEvidence { get; set; }
        public  string EvidenceCode { get; set; }
        public  int PaymentType { get; set; }
        public bool IsAllowViewEvidence { get; set; }
        public bool IsAllowConfirm { get; set; }
    }
}