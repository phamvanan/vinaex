﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Web.Models
{
    public class CreateTransactionViewModel
    {
        //[DataType(DataType.EmailAddress, ErrorMessage = "E-mail is not valid")]
        //[Required()]

        public long SellAdvertisementId { get; set; }
        public decimal MaxAmountCoin { get; set; }

        [Required()]
        public decimal AmountCoin { get; set; }
        public int CoinType { get; set; }
        public string Description { get; set; }
        public int TransactionType { get; set; }
        public int UserId { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}
