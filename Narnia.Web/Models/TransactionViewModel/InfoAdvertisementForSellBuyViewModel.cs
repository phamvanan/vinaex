﻿using System;

namespace Narnia.Web.Models
{
    public class InfoAdvertisementForSellBuyViewModel
    {
        public int Id { get; set; }
        public decimal MaxCoinNumber { get; set; }
        public decimal MinCoinNumber { get; set; }
        public string UserName { get; set; }
        public string PaymentMethodName { get; set; }
        public string CountryName { get; set; }
        public int PaymentAllowTime { get; set; }
        public string BankName { get; set; }
        public decimal AvgPrice { get; set; }
        public decimal Rate { get; set; }
        public decimal ToVND => Math.Round(MaxCoinNumber * AvgPrice, 0);
        public decimal TransactionAmount { get; set; }
        public string AssetIdBase { get; set; }
        public string CurrencyName { get; set; }
        public bool IsActived { get; set; }
        public string WalletAddressUser { get; set; }
        public  string Alias { get; set; }
        public bool CanTransaction { get; set; }
        public int UserId { get; set; }
    }
}