﻿using System;

namespace Narnia.Web.Models
{
    public class TransactionHistoryViewModel
    {
        public int TransactionId { get; set; }
        public string TransactionCode { get; set; }
        public decimal AmountCoin { get; set; }
        public string AssetTypeId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? EvidenceId { get; set; }
        public string Description { get; set; }
        public int PaymentAllowTime { get; set; }
        public int UserAdsId { get; set; }
        public bool IsSelAds { get; set; }
        public int AdsId { get; set; }
        public string Alias { get; set; }
        public string UserName { get; set; }
        public DateTime DeadlineConfirm => CreatedDate.AddMinutes(PaymentAllowTime);
        public string CreatedDateDisplay => CreatedDate.ToString("G");
    }
}
