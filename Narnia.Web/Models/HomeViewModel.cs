﻿using Narnia.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Web.Models
{
    public class HomeViewModel
    {
        public int UserId { get; set; }
        public string AssetTypeId { get; set; }
        public string CurrencyId { get; set; }
        public string BuyPrice { get; set; }
        public string SellPrice { get; set; }
        public string ReferralLink { get; set; }
        public decimal ReferralAmount { get; set; }
    }
}
