﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Routing;
using Narnia.Web.Framework.Mvc.Routing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Web
{
    public class RouteProvider : IRouteProvider
    {
        public int Priority
        {
            get
            {
                return 0;
            }
        }

        public void RegisterRoutes(IRouteBuilder routeBuilder)
        {
            routeBuilder.MapRoute("Deposit", "deposit/{assettype}",
                 new { controller = "Payment", action = "Deposit",assettype="ETH" });

            routeBuilder.MapRoute("ExchangeOffer.PM.OnSuccess", "exchange/pm/success",
                new { controller = "ExchangeOffer", action = "Onsuccess" });
            routeBuilder.MapRoute("ExchangeOffer.PM.OnError", "exchange/pm/error",
                new { controller = "ExchangeOffer", action = "OnError" });
            routeBuilder.MapRoute("ExchangeOffer.PM.OnCancel", "exchange/pm/cancel",
                new { controller = "ExchangeOffer", action = "OnCancel" });
        }
    }
}
