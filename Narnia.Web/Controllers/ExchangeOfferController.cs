﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Web;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Narnia.Business;
using Narnia.Business.Impl;
using Narnia.Business.Transactions;
using Narnia.Core;
using Narnia.Core.Domain;
using Narnia.Core.Domain.Payments;
using Narnia.Core.Enums;
using Narnia.Web.Areas.Payments.Models;
using Narnia.Web.Models.ExchangeOffer;
using System.Text;
using FastMapper;
using Narnia.Business.CoinServices;
using Narnia.Core.Helpers;
using Narnia.Web.Areas.Admin.Models.AjaxDataTables;
using Narnia.Web.Models;
using Newtonsoft.Json;

namespace Narnia.Web.Controllers
{
    //[Authorize]
    public class ExchangeOfferController : BaseController
    {
        private readonly IExchangeOfferBusiness _exchangeOfferBusiness;
        private readonly IWorkContext _workContext;
        private readonly ICoinSymbolBiz _coinSymbolBiz;
        private readonly IPaymentBiz _paymentBiz;
        private readonly IAssetTypeBusiness _assetTypeBusiness;
        private readonly ISiteSetting _siteSetting;
        private readonly IBankAccountBiz _bankAccount;
        private readonly CoinServiceFactory _coinServiceFactory;
        private readonly ICoinAccountSystemBusiness _accountSystemBusiness;

        public ExchangeOfferController(IExchangeOfferBusiness exchangeOfferBusiness,
                                        IWorkContext workContext,
                                        ICoinSymbolBiz coinSymbolBiz,
                                        IPaymentBiz paymentBiz,
                                        IAssetTypeBusiness assetTypeBusiness,
                                        ISiteSetting siteSetting,
                                        IBankAccountBiz bankAccount,
                                        CoinServiceFactory coinServiceFactory,
                                        ICoinAccountSystemBusiness accountSystemBusiness)
        {
            _exchangeOfferBusiness = exchangeOfferBusiness;
            _workContext = workContext;
            _coinSymbolBiz = coinSymbolBiz;
            _paymentBiz = paymentBiz;
            _assetTypeBusiness = assetTypeBusiness;
            _siteSetting = siteSetting;
            _bankAccount = bankAccount;
            _coinServiceFactory = coinServiceFactory;
            _accountSystemBusiness = accountSystemBusiness;
        }

        public IActionResult Index()
        {
            //var model = new ExchangeOfferVM()
            //{
            //    Type = (int) ExchangeOfferType.Buy,
            //    AssetTypeId = Enum.GetName(typeof(AssetTypeEnum), AssetTypeEnum.ETH)
            //};

            return RedirectToAction("Buy", "ExchangeOffer", new { assetTypeId = Enum.GetName(typeof(AssetTypeEnum), AssetTypeEnum.ETH) });
        }

        [HttpGet]
        public IActionResult Buy(string assetTypeId)
        {
            if (string.IsNullOrEmpty(assetTypeId))
            {
                TempData["error"] = "Loại tiền không hợp lệ";

                return RedirectToAction("Index");
            }
            var assetType = _assetTypeBusiness.GetById(assetTypeId);
            if (assetType == null)
            {
                TempData["error"] = "Loại tiền không hợp lệ";

                return RedirectToAction("Index");
            }

            var model = new ExchangeOfferVM()
            {
                AssetTypeId = assetTypeId,
                Type = (int)ExchangeOfferType.Buy
            };

            switch (assetTypeId)
            {
                case "ETH":
                    model.AssetAmountReserves = _siteSetting.ETHReserves.ToString("N");
                    break;

                case "BTC":
                    model.AssetAmountReserves = _siteSetting.BTCReserves.ToString("N");
                    break;

                case "USDT":
                    model.AssetAmountReserves = _siteSetting.USDTReserves.ToString("N");
                    break;

                case "PM":
                    model.AssetAmountReserves = _siteSetting.PMReserves.ToString("N");
                    break;
            }

            return View(model);
        }

        [HttpPost]
        public IActionResult Buy(ExchangeOfferVM exchange)
        {
            if (!ModelState.IsValid)
            {
                TempData["error"] = GetModelStateError(ModelState);
                return View();
            }

            var model = new ExchangeOfferVM
            {
                Id = exchange.Id,
                Amount = exchange.Amount,
                Type = (int)ExchangeOfferType.Buy,
                AssetTypeId = exchange.AssetTypeId,
                Fee = exchange.Fee,
                Status = (int)ExchangeOfferStatus.Pending,
                UserBankAccountName = exchange.UserBankAccountName,
                UserBankAccountNo = exchange.UserBankAccountNo,
                WalletAddress = exchange.WalletAddress,
                ExchangeOfferCode = $"EB{DateTime.Now.ToUnixTimestamp()}",
            };

            TempData["ExchangeOffer"] = JsonConvert.SerializeObject(model);

            return View("ExchangeConfirm", model);
        }

        [HttpGet]
        public IActionResult Sell(string assetTypeId)
        {
            if (string.IsNullOrEmpty(assetTypeId))
            {
                TempData["error"] = "Loại tiền không hợp lệ";

                return RedirectToAction("Index");
            }
            var asseType = _assetTypeBusiness.GetById(assetTypeId);
            if (asseType == null)
            {
                TempData["error"] = "Loại tiền không hợp lệ";

                return RedirectToAction("Index");
            }

            var model = new ExchangeOfferVM()
            {
                AssetTypeId = assetTypeId,
                Type = (int)ExchangeOfferType.Sell,
                VNDReserves = _siteSetting.VNDReserves
            };
            //todo: neu ban(user bán cho vinaex) thi lay địa chỉ quý cua admin show lên cho người dùng biết
            //todo: bang cach call service coi de tao quy

            return View(model);
        }

        [HttpPost]
        public IActionResult Sell(ExchangeOfferVM exchange)
        {
            if (!ModelState.IsValid)
            {
                TempData["error"] = GetModelStateError(ModelState);
                return View();
            }

            var model = new ExchangeOfferVM
            {
                Id = exchange.Id,
                Amount = exchange.Amount,
                Type = (int)ExchangeOfferType.Sell,
                AssetTypeId = exchange.AssetTypeId,
                Fee = exchange.Fee,
                Status = (int)ExchangeOfferStatus.Pending,
                UserBankAccountName = exchange.UserBankAccountName,
                UserBankAccountNo = exchange.UserBankAccountNo,
                WalletAddress = exchange.WalletAddress,
                ExchangeOfferCode = $"ES{DateTime.Now.ToUnixTimestamp()}"
            };

            TempData["ExchangeOffer"] = JsonConvert.SerializeObject(model);

            return View("ExchangeConfirm", model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="amount"></param>
        /// <param name="coinType"></param>
        /// <returns></returns>
        public JsonResult GetVNDPrice(decimal amount = 0, string coinType = "")
        {
            if (amount == 0 || string.IsNullOrEmpty(coinType))
            {
                return Json(0);
            }

            CoinSymbol coinSymbol;
            try
            {
                coinSymbol = _coinSymbolBiz.GetSymbolByName(coinType);
                if (coinSymbol == null)
                {
                    return Json(0);
                }
            }
            catch (Exception ex)
            {
                return Json(0);
            }

            return Json((coinSymbol.AvgPrice * amount).ToString("N"));
        }

        [HttpGet]
        public IActionResult ExchangeConfirm()
        {
            var model = new ExchangeOfferVM();
            if (TempData["ExchangeOffer"] != null)
            {
                model = JsonConvert.DeserializeObject<ExchangeOfferVM>(TempData["ExchangeOffer"].ToString());
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult ExchangeConfirm(ExchangeOfferVM exchange)
        {
            if (!ModelState.IsValid)
            {
                TempData["error"] = this.GetModelStateError(ModelState);
                if (exchange.Type == (int)ExchangeOfferType.Sell)
                {
                    return RedirectToAction("Sell", "ExchangeOffer", new { assetTypeId = exchange.AssetTypeId });
                }
                return RedirectToAction("Buy", "ExchangeOffer", new { assetTypeId = exchange.AssetTypeId });
            }
            var coinSymbol = _coinSymbolBiz.GetSymbolByName(exchange.AssetTypeId);
            if (coinSymbol == null)
            {
                TempData["error"] = "Loại tiền không tồn tại trong hệ thống";
                return View("Index");
            }

            if (exchange.Type == (int)ExchangeOfferType.Buy)
            {
                exchange.VNDAmount = exchange.Amount * coinSymbol.BuyPrice;
                exchange.ExchangeOfferCode = $"EB{DateTime.Now.ToUnixTimestamp()}";
                exchange.BankAccount =
                    TypeAdapter.Adapt<List<BankAccountViewModel>>(_bankAccount.GetAvailableSystemBankAccounts());
            }

            if (exchange.Type == (int)ExchangeOfferType.Sell)
            {
                exchange.VNDAmount = exchange.Amount * coinSymbol.SellPrice;
                exchange.ExchangeOfferCode = $"ES{DateTime.Now.ToUnixTimestamp()}";
                //todo: uypc: cho nay khong goi tao quy tu coi service ma lay tu bang CoinAccountSystem ra
                //var coinService = _coinServiceFactory.GetService(exchange.AssetTypeId);
                //#if !DEBUG
                //    exchange.WalletAddress = coinService.CreateWallet($"{_workContext.CurrentUser.UserName}@#11%");
                //#endif
                //#if DEBUG
                //    exchange.WalletAddress = "xxxxxxxxxxxxxx ban dang o che do debug";
                //#endif
                exchange.WalletAddress = _accountSystemBusiness.GetByAssetTypeId(exchange.AssetTypeId)?.WalletAddress;
            }

            // if khong phai la perfect money thi insert luc hien thi ra trang confirm
            if (!string.Equals(exchange.AssetTypeId, Enum.GetName(typeof(AssetTypeEnum), AssetTypeEnum.PM)))
            {
                var model = new ExchangeOffer
                {
                    Id = exchange.Id,
                    Amount = exchange.Amount,
                    Type = exchange.Type,
                    AssetTypeId = exchange.AssetTypeId,
                    Fee = exchange.Fee,
                    Status = (int)ExchangeOfferStatus.Pending,
                    UserBankAccountName = exchange.UserBankAccountName,
                    UserBankAccountNo = exchange.UserBankAccountNo,
                    ExchangeOfferCode = exchange.ExchangeOfferCode,
                    VNDAmount = exchange.VNDAmount,
                    WalletAddress = exchange.WalletAddress,
                    Phone = exchange.Phone
                };

                try
                {
                    var result = _exchangeOfferBusiness.AddNew(model);
                    if (result.HasError)
                    {
                        TempData["error"] = result.Message;
                        return View();
                    }

                    ExchangeOfferVM vm = FastMapper.TypeAdapter.Adapt<ExchangeOfferVM>(result.Data);
                    vm.BankAccount = exchange.BankAccount;

                    TempData["ExchangeOffer"] = JsonConvert.SerializeObject(vm);

                    return View(vm);
                }
                catch (Exception ex)
                {
                    TempData["error"] = CommonConst.ExchangeOfferErrorBuy;
                }
            }

            TempData["ExchangeOffer"] = JsonConvert.SerializeObject(exchange);

            return View(exchange);
        }

        public IActionResult Payment(ExchangeOfferVM exchange)
        {
            if (!ModelState.IsValid)
            {
                TempData["error"] = this.GetModelStateError(ModelState);
                return View("ExchangeConfirm", exchange);
            }

            var perfectMoneyModel = new PerfectMoneyModel
            {
                //UserId = _workContext.CurrentUser.Id,
                AssetTypeId = exchange.AssetTypeId,
                PayeeName = exchange.UserBankAccountName,
                PayeeAccount = exchange.UserBankAccountNo,
                SuccessUrl = Url.RouteUrl("ExchangeOffer.PM.OnSuccess", null, Request.Scheme),
                CancelUrl = Url.RouteUrl("ExchangeOffer.PM.OnCancel", null, Request.Scheme),
                //StatusUrl = Url.RouteUrl("ExchangeOffer.PM.OnCancel", Request.Scheme),
                Unit = "USD",
            };

            var systemAccounts = _paymentBiz.GetActiveSystemGatewayAccounts(2);
            if (systemAccounts != null && systemAccounts.Count > 0)
            {
                GatewayAccount payeeAccount = null;
                payeeAccount = systemAccounts.FirstOrDefault(acc => acc.AccName.StartsWith('U'));

                perfectMoneyModel.PayeeAccount = payeeAccount?.AccName;
                perfectMoneyModel.PayeeName = "VINAX Co.";

                var coinSymbol = _coinSymbolBiz.GetSymbolByName(exchange.AssetTypeId);
                if (coinSymbol == null)
                {
                    TempData["error"] = "Loại tiền không hợp lệ";
                    return View("ExchangeConfirm", exchange);
                }

                perfectMoneyModel.Amount = exchange.Amount;
            }

            var model = new ExchangeOffer
            {
                Id = exchange.Id,
                Amount = exchange.Amount,
                Type = exchange.Type,
                AssetTypeId = exchange.AssetTypeId,
                Fee = exchange.Fee,
                Status = (int)ExchangeOfferStatus.Pending,
                UserBankAccountName = exchange.UserBankAccountName,
                UserBankAccountNo = exchange.UserBankAccountNo,
                ExchangeOfferCode = exchange.ExchangeOfferCode,
                Phone = exchange.Phone
            };

            if (string.Equals(exchange.AssetTypeId, Enum.GetName(typeof(AssetTypeEnum), AssetTypeEnum.PM)))
            {
                try
                {
                    var result = _exchangeOfferBusiness.AddNew(model);
                    if (result.HasError)
                    {
                        TempData["error"] = result.Message;

                        TempData["ExchangeOffer"] = JsonConvert.SerializeObject(exchange);
                        return View("ExchangeConfirm");
                    }
                    perfectMoneyModel.PaymentId = result.Data.Id;

                    ExchangeOfferVM vm = FastMapper.TypeAdapter.Adapt<ExchangeOfferVM>(result.Data);
                    TempData["ExchangeOffer"] = JsonConvert.SerializeObject(vm);

                    return View(perfectMoneyModel);
                }
                catch (Exception ex)
                {
                    TempData["error"] = CommonConst.ExchangeOfferErrorBuy;
                }
            }

            return View(perfectMoneyModel);
        }

        [HttpGet]
        public IActionResult Exchange(string assetTypeId)
        {
            if (string.IsNullOrEmpty(assetTypeId))
            {
                TempData["error"] = "Loại tiền không hợp lệ";

                return RedirectToAction("Index");
            }
            var asseType = _assetTypeBusiness.GetById(assetTypeId);
            if (asseType == null)
            {
                TempData["error"] = "Loại tiền không hợp lệ";

                return RedirectToAction("Index");
            }

            var model = new ExchangeOfferVM()
            {
                AssetTypeId = assetTypeId,
                Type = (int)ExchangeOfferType.Sell
            };

            return View(model);
        }

        public IActionResult History()
        {
            return View();
        }

        [HttpPost]
        public virtual IActionResult Search(string code = "")
        {
            var offers = _exchangeOfferBusiness.SearchExchangeOffers(code: code, pageIndex: 0, pageSize: 10);
            var data = TypeAdapter.Adapt<IList<ExchangeOffer>, IList<ExchangeOfferVM>>(offers.Data.ToList());
            return Json(new DataTableAjaxReturnModel<ExchangeOfferVM>()
            {
                recordsFiltered = offers.TotalCount,
                recordsTotal = offers.TotalCount,
                data = data
            });
        }

        [AllowAnonymous]
        public IActionResult OnSuccess()
        {
            //kong hieu ExchangeOfferId
            var data = GetDataFromPM();
            int.TryParse(data["PAYMENT_ID"], out var exchangeOfferId);
            var updateResult = _exchangeOfferBusiness.UpdateStatus(exchangeOfferId, (int)ExchangeOfferStatus.Complete);

            TempData["success"] = "ban da giao dich thanh cong!";

            return RedirectToAction("Index");
        }

        [AllowAnonymous]
        public IActionResult OnError()
        {
            var data = GetDataFromPM();
            string payeeAccount = data["PAYEE_ACCOUNT"];
            string paymentAmount = data["PAYMENT_AMOUNT"];
            string paymentUnit = data["PAYMENT_UNITS"];
            string paymentId = data["PAYMENT_ID"];
            string memo = data["SUGGESTED_MEMO"];
            string batchNum = data["PAYMENT_BATCH_NUM"];
            string assetType = data["AssetTypeId"];
            string userId = data["UserId"];

            int.TryParse(data["ExchangeOfferId"], out var exchangeOfferId);
            var updateResult = _exchangeOfferBusiness.UpdateStatus(exchangeOfferId, (int)ExchangeOfferStatus.Complete);

            return RedirectToAction("Index");
        }

        [AllowAnonymous]
        public IActionResult OnCancel()
        {
            var data = GetDataFromPM();
            string payeeAccount = data["PAYEE_ACCOUNT"];
            string paymentAmount = data["PAYMENT_AMOUNT"];
            string paymentUnit = data["PAYMENT_UNITS"];
            string paymentId = data["PAYMENT_ID"];
            string memo = data["SUGGESTED_MEMO"];
            string batchNum = data["PAYMENT_BATCH_NUM"];
            string assetType = data["AssetTypeId"];
            string userId = data["UserId"];

            int.TryParse(data["PAYMENT_ID"], out var exchangeOfferId);
            var updateResult = _exchangeOfferBusiness.UpdateStatus(exchangeOfferId, (int)ExchangeOfferStatus.Complete);

            TempData["success"] = "ban da giao dich thanh cong!";

            return RedirectToAction("Index");
        }

        [AllowAnonymous]
        private NameValueCollection GetDataFromPM()
        {
            byte[] parameters;
            using (var stream = new MemoryStream())
            {
                this.Request.Body.CopyTo(stream);
                parameters = stream.ToArray();
            }
            var strRequest = Encoding.ASCII.GetString(parameters);

            var data = HttpUtility.ParseQueryString(strRequest);

            return data;
        }
    }
}