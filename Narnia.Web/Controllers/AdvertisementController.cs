﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Narnia.Business;
using Narnia.Core;
using Narnia.Core.Domain;
using Narnia.Core.Infrastructure;
using Narnia.Core.Models;
using Narnia.Web.Areas.Admin.Models.AjaxDataTables;

namespace Narnia.Web.Controllers
{
    public class AdvertisementController : Controller
    {
        private readonly User _currentUser = null;
        private readonly ISellAdvertisementBiz _sellAdvertisement;
        private readonly IBuyAdvertisementBiz _buyAdvertisement;
        private readonly IWorkContext _workContext;
        public AdvertisementController(ISellAdvertisementBiz sellAdvertisement,
                                        IBuyAdvertisementBiz buyAdvertisement,
                                        IWorkContext workContext)
        {
            this._buyAdvertisement = buyAdvertisement;
            this._sellAdvertisement = sellAdvertisement;
            _workContext = workContext;
            _currentUser = EngineContext.Current.Resolve<IWorkContext>().CurrentUser;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult SellAdvertisement(DataTableAjaxPostModel model, int type)
        {
            var advertisment = _sellAdvertisement.GetSellAdvertisementByUserId(model.length, model.start, _currentUser.Id, out var totalRecords
                , true, _workContext.WorkingSymbol.AssetIdBase).ToList();

            return Json(new DataTableAjaxReturnModel<BuySellViewModel>()
            {
                draw = model.draw,
                recordsFiltered = totalRecords,
                recordsTotal = totalRecords,
                data = advertisment
            });
        }

        [HttpPost]
        public JsonResult BuyAdvertisement(DataTableAjaxPostModel model, int type)
        {
            var advertisment = _buyAdvertisement.GetBuyAdvertisementByUserId(model.length, model.start, _currentUser.Id, out var totalRecords
                , true, _workContext.WorkingSymbol.AssetIdBase).ToList();

            return Json(new DataTableAjaxReturnModel<BuySellViewModel>()
            {
                draw = model.draw,
                recordsFiltered = totalRecords,
                recordsTotal = totalRecords,
                data = advertisment
            });
        }

        [HttpPost]
        public JsonResult MyAdvertisement(DataTableAjaxPostModel model, int type = 1)
        {
            int totalRecords;
            IList<BuySellViewModel> advertisment = null;
            if (type == 1)
            {
                advertisment = _sellAdvertisement.GetSellAdvertisementByUserId(model.length, model.start, _currentUser.Id, out totalRecords
                    , true, _workContext.WorkingSymbol.AssetIdBase).ToList();
            }
            else
            {
                advertisment = _buyAdvertisement.GetBuyAdvertisementByUserId(model.length, model.start, _currentUser.Id, out totalRecords
                    , true, _workContext.WorkingSymbol.AssetIdBase).ToList();
            }

            return Json(new DataTableAjaxReturnModel<BuySellViewModel>()
            {
                draw = model.draw,
                recordsFiltered = totalRecords,
                recordsTotal = totalRecords,
                data = advertisment
            });
        }
    }
}