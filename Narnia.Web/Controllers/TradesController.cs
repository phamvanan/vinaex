﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FastMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Narnia.Business;
using Narnia.Core;
using Narnia.Core.Domain;
using Narnia.Core.Enums;
using Narnia.Core.Infrastructure;
using Narnia.Core.Models;
using Narnia.Web.Areas.Admin.Models.AjaxDataTables;
using Narnia.Web.Models;

namespace Narnia.Web.Controllers
{
    [Authorize]
    [Route("trades")]
    public class TradesController : Controller
    {
        private readonly User _currentUser;
        private readonly ITransactionBiz _transactionBiz;
        public TradesController(ITransactionBiz transactionBiz)
        {
            _transactionBiz = transactionBiz;
            _currentUser = EngineContext.Current.Resolve<IWorkContext>().CurrentUser;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [Route("trade-history")]
        public JsonResult TransactionHistory(DataTableAjaxPostModel model, int type)
        {
            var userId = _currentUser?.Id ?? 0;

            //var searchModel = new TransactionHistorySearchModel()
            //{
            //    UserId = userId,
            //    AssetTypeId = _workContext.WorkingSymbol.AssetIdBase,
            //    TransactionType = (TransactionWalletType)type,
            //    Skip = model.start,
            //    Take = model.length
            //};
            var listData = _transactionBiz.GetTransactionHistoryForUser(userId, (TransactionStatusClient)type, model.start, model.length);
            var transactionViewModel = TypeAdapter.Adapt<IList<TransactionHistoryUserModel>, IList<TransactionHistoryViewModel>>(listData.Data.ToList());
            return Json(new DataTableAjaxReturnModel<TransactionHistoryViewModel>()
            {
                draw = model.draw,
                recordsFiltered = listData.TotalCount,
                recordsTotal = listData.TotalCount,
                data = transactionViewModel
            });
        }

        [Route("active")]
        public async Task<IActionResult> Active()
        {
            return ViewComponent("PendingTrade", null);
        }

        [Route("closed")]
        public async Task<IActionResult> Closed()
        {
            return ViewComponent("ClosedTrade", null);
        }

        [Route("completed")]
        public async Task<IActionResult> Completed()
        {
            return ViewComponent("CompletedTrade", null);
        }
    }
}