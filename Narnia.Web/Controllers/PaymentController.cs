﻿using Microsoft.AspNetCore.Mvc;
using Narnia.Business.Transactions;
using Narnia.Core;
using Narnia.Web.Framework.Mvc.Controlles;
using Narnia.Web.Models.Payments;
using Microsoft.AspNetCore.Authorization;

namespace Narnia.Web.Controllers
{
    [Authorize]
    public class PaymentController:BaseController
    {
        private readonly IPaymentBiz _paymentBiz;
        private readonly IWorkContext _workContext;
        public PaymentController(IPaymentBiz paymentBiz, IWorkContext workContext)
        {
            _paymentBiz = paymentBiz;
            _workContext = workContext;
        }
        public IActionResult Deposit(string assettype)
        {
            DepositModel model = new DepositModel();
            model.AssetTypeId = assettype;
            if(string.IsNullOrEmpty(assettype))
            {
                return Content("");
            }
            var paymentMethod = _paymentBiz.GetPaymentMethodByAssetType(assettype);
            if(paymentMethod != null)
            {
                if (paymentMethod.SkipPaymentInfo)
                {
                    model.GatewayComponentName = paymentMethod.GetPublicViewComponentName();
                }
                else
                {

                }
                return View(model);
            }
            else
            {
                return Content("No payment method");
            }
        }
        [HttpPost]
        public IActionResult Deposit(DepositModel model)
        {
            if (string.IsNullOrEmpty(model.AssetTypeId))
            {
                return View(model);
            }

            //Preproccess deposit:
            decimal fee = 0;

            var userId = _workContext.CurrentUser.Id;
            var depositPre = _paymentBiz.DepositPre(userId, model.Amount,fee,model.AssetTypeId, model.Description);
            if(depositPre != null)
            {
                var paymentMethod = _paymentBiz.GetPaymentMethodByAssetType(model.AssetTypeId);
                if (paymentMethod != null)
                {
                    if (!paymentMethod.SkipPaymentInfo)
                    {
                        model.GatewayComponentName = paymentMethod.GetPublicViewComponentName();
                    }

                    model.PaymentRequestInfo = paymentMethod.GetPaymentInfo(Request.Form);
                    model.PaymentRequestInfo.CustomValues.Add("DepositId", depositPre.Id);
                    model.PaymentRequestInfo.UserId = userId;
                    var result = paymentMethod.ProcessPayment(model.PaymentRequestInfo);

                    if (result.Success)
                    {
                        return View(model);
                    }
                    else
                    {
                        int i = 0;
                        foreach(var error in result.Errors)
                        {
                            i++;
                            ModelState.AddModelError($"Error_{i}", error);
                        }
                    }
                }
            }
            else
            {
                ModelState.AddModelError("Error_1", "Cannot save deposit info. Please contact administrator");
            }
            
            return View(model);
        }
    }
}
