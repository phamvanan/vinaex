﻿using System;
using FastMapper;
using Microsoft.AspNetCore.Mvc;
using Narnia.Business;
using Narnia.Core.Domain2;
using Narnia.Web.Models.Orders;

namespace Narnia.Web.Controllers
{
    public class OfferController: Controller
    {
        private readonly IOrderService _orderService;

        public OfferController(IOrderService orderService)
        {
            _orderService = orderService;
        }
        [HttpGet]
        public ActionResult NewOffer(string id)
        {
            //var assetIdBase = _workContext.WorkingSymbol.AssetIdBase;
            var model = new EditOfferModel();

            //var allSymbol = _walletBusiness.GetAllSymbols(true);
            //var allSymbolView = TypeAdapter.Adapt<IList<CoinSymbol>, List<CoinSymbolViewModel>>(allSymbol);
            //model.CurrentCoinSymbol = allSymbolView.Where(symbol => symbol.AssetIdBase == assetIdBase).FirstOrDefault();

            //var allCurrency = _walletBusiness.GetAllCurrencies();
            //model.AvailableCurrencies = TypeAdapter.Adapt<IList<Currency>, List<CurrencyViewModel>>(allCurrency);

            //model.AvailableBanks = TypeAdapter.Adapt<IList<Banks>, List<BankViewModel>>(_walletBusiness.GetAllBanks());
           
            return View(model);
        }

        [HttpPost]
        public JsonResult AddOffer(EditOfferModel model)
        {
            Random rand = new Random();
            var offer = new Offer();
            offer.Price = rand.Next(14, 23); // model.Price;
            offer.Quantity = rand.Next(2, 5);// model.Quantity;
            offer.CreatedDate = DateTime.Now;

            var response = _orderService.CreateOffer(offer);
            return null;
        }
    }
}
