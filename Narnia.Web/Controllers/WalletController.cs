﻿using FastMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Narnia.Business;
using Narnia.Business.Impl;
using Narnia.Core;
using Narnia.Core.Domain;
using Narnia.Core.Enums;
using Narnia.Core.Helpers;
using Narnia.Core.Infrastructure;
using Narnia.Core.Models;
using Narnia.Web.Areas.Admin.Models.AjaxDataTables;
using Narnia.Web.Models;
using Narnia.Web.Models.Wallet;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace Narnia.Web.Controllers
{
    [Authorize]
    [Route("wallet")]
    public class WalletController : Controller
    {
        private readonly User _currentUser;
        private readonly IBankAccountBiz _bankAccountBiz;
        private readonly IUserBusiness _userBusiness;
        private readonly ICoinWalletBusiness _coinWalletBusiness;
        private readonly ITransactionWalletBiz _transactionWalletBiz;
        private readonly IWithdrawBusiness _withdrawBusiness;
        private readonly IWorkContext _workContext;
        private int _userIdDefault = 0;

        public WalletController(IBankAccountBiz bankAccountBiz,
                                IUserBusiness userBusiness,
                                ICoinWalletBusiness coinWalletBusiness,
                                ITransactionWalletBiz transactionWalletBiz,
                                IWorkContext workContext,
                                IWithdrawBusiness withdrawBusiness)
        {
            _currentUser = EngineContext.Current.Resolve<IWorkContext>().CurrentUser;
            _bankAccountBiz = bankAccountBiz;
            _userBusiness = userBusiness;
            _coinWalletBusiness = coinWalletBusiness;
            _transactionWalletBiz = transactionWalletBiz;
            _workContext = workContext;
            _withdrawBusiness = withdrawBusiness;
        }

        public IActionResult Index()
        {
            return RedirectToAction("Withdrawal");
        }

        #region coin

        //[AllowAnonymous()]
        [HttpGet("coin/deposit")]
        public IActionResult Deposit()
        {
            var model = new CoinDepositViewModel();
            var userId = _workContext.CurrentUser.Id;
            var coinType = _workContext.WorkingSymbol.AssetIdBase;

            var currentWallet = _coinWalletBusiness.GetCurrentCoinWalletByUserId(userId, coinType);

            model.CoinType = coinType;
            model.WalletAddress = currentWallet.WalletAddress;

            return View(model);
        }

        [HttpGet("coin/withdrawal")]
        public IActionResult Withdrawal()
        {
            try
            {
                return View();
            }
            catch (Exception)
            {
                return View();
            }
        }

        [HttpPost]
        [Route("coin/withdrawal")]
        public IActionResult Withdrawal(WithdrawViewModel model)
        {
            try
            {
                model.CoinCode = _workContext.WorkingSymbol.AssetIdBase;
                model.UserId = _currentUser.Id;
                var result = _withdrawBusiness.Withdraw(model);
                if (result.HasError)
                {
                    TempData["error"] = result.Message;
                }
                else
                {
                    TempData["success"] = "Bạn đã rút tiền thành công";
                }
            }
            catch (Exception ex)
            {
                TempData["error"] = CommonConst.HasErrPleaseContactAdmin;
            }

            return View();
        }

        [HttpPost]
        [Route("TransactionHistory")]
        public JsonResult TransactionHistory(DataTableAjaxPostModel model, int type)
        {
            var userId = _currentUser?.Id ?? 0;

            var searchModel = new TransactionHistorySearchModel()
            {
                UserId = userId,
                AssetTypeId = _workContext.WorkingSymbol.AssetIdBase,
                TransactionType = (TransactionWalletType)type,
                Skip = model.start,
                Take = model.length
            };
            var listData = _transactionWalletBiz.GetTransactionWalletPaging(searchModel, out var totalRecords);

            return Json(new DataTableAjaxReturnModel<TransactionWalletPagingModel>()
            {
                draw = model.draw,
                recordsFiltered = totalRecords,
                recordsTotal = totalRecords,
                data = listData
            });
        }

        #endregion coin

        #region VND

        [HttpGet("vnd/deposit")]
        public IActionResult DepositVND()
        {
            var userId = _currentUser?.Id ?? _userIdDefault;
            var userData = _userBusiness.GetUserById(userId);
            if (userData == null || !userData.IsVerify)
            {
                return RedirectToAction("Verify", "Account");
            }
            var bankAccountAdmins = _bankAccountBiz.GetListAccountAdmin();
            if (bankAccountAdmins == null || bankAccountAdmins.Count == 0)
                TempData["error"] = CommonConst.HasErrPleaseContactAdmin;
            var model = new DepositVNDViewModel
            {
                banks = TypeAdapter.Adapt<List<BankAccountModel>, List<BankAccountViewModel>>(bankAccountAdmins),
            };
            return View(model);
        }

        [HttpPost("vnd/deposit")]
        public IActionResult DepositVNDPost(DepositVNDViewModel model)
        {
            var userId = _currentUser?.Id ?? _userIdDefault;//TODO:
            var userData = _userBusiness.GetUserById(userId);
            if (userData == null || !userData.IsVerify)
            {
                return RedirectToAction("Verify", "Account");
            }
            if (model.BankId == 0)
            {
                TempData["error"] = CommonConst.BankidEmptyForWithdrawal;
                return View("DepositVND", model);
            }

            var transactionWallet = new TransactionWallet
            {
                Amount = model.DepositAmount,
                UserId = userId,
                CreatedDate = DateTime.Now,
                BankAccountId = model.BankId,
                TransactionType = (int)TransactionWalletType.Deposit
            };
            var result = _transactionWalletBiz.AddTransactionVND(transactionWallet);
            if (result.HasError)
            {
                TempData["error"] = result.Message;
                return View("DepositVND", model);
            }
            TempData["success"] = CommonConst.TransactionWithDepositSuccess;
            return RedirectToAction("DepositVND");
        }

        [HttpGet("vnd/detail/{transcode}")]
        public IActionResult DetailDepositVND(string transcode)
        {
            var userId = _currentUser?.Id ?? _userIdDefault;
            var userData = _userBusiness.GetUserById(userId);
            if (userData == null || !userData.IsVerify)
            {
                return RedirectToAction("Verify", "Account");
            }
            var result = _transactionWalletBiz.GetDetailVNDByTrancodeUserId(userId, transcode);
            if (result.HasError)
            {
                TempData["error"] = result.Message;
                return RedirectToAction("DepositVND");
            }
            var model =
                TypeAdapter.Adapt<TransactionWalletDetailVNDModel, TransactionWalletDetailVNDViewModel>(
                    result.Data);
            return View(model);
        }

        [HttpPost("UploadEvidence")]
        public async Task<IActionResult> UploadEvidence(EvidenceViewModel model, List<IFormFile> Image)
        {
            if (Image != null && Image.Count > 0 && Image[0].Length > 0)
            {
                var evidenceEntity = new Evidence();
                var userId = _currentUser?.Id ?? _userIdDefault;
                using (var stream = new MemoryStream())
                {
                    await Image[0].CopyToAsync(stream);
                    evidenceEntity.Image = stream.ToArray();
                    evidenceEntity.FileName = Image[0].FileName;
                    evidenceEntity.ContentType = Image[0].ContentType;
                    evidenceEntity.CreatedDate = DateTime.Now;
                }

                var isImage = UploadHelper.IsImage(evidenceEntity.ContentType, evidenceEntity.FileName);
                if (!isImage)
                {
                    TempData["error"] = "Bằng chứng không hợp lệ.";
                    return RedirectToAction("Detail", new { transcode = model.Transcode });
                }

                var result = _transactionWalletBiz.UploadEvidenceDepositVND(evidenceEntity, model.Transcode, userId);
                if (result.HasError == false)
                {
                    TempData["success"] = CommonConst.UpdateEvidenceSuccess;
                }
                else
                {
                    TempData["error"] = result.Message;
                }
            }
            return RedirectToAction("DetailDepositVND", new { transcode = model.Transcode });
        }

        [HttpPost("vnd/cancel")]
        public IActionResult CancelTransactionVND(string transcode)
        {
            var userId = _currentUser?.Id ?? _userIdDefault;
            if (string.IsNullOrEmpty(transcode))
            {
                TempData["error"] = CommonConst.HasErrWhenCancelTransaction;
                return RedirectToAction("DetailDepositVND", new { transcode = transcode });
            }
            var result = _transactionWalletBiz.CancelTransactionVND(transcode, userId);
            if (result.HasError)
            {
                TempData["error"] = result.Message;
            }
            else
            {
                TempData["success"] = CommonConst.CancelTransactionSuccess;
            }
            return RedirectToAction("DetailDepositVND", new { transcode = transcode });
        }

        [HttpGet("vnd/withdrawal")]
        public IActionResult WithdrawalVND()
        {
            var userId = _currentUser?.Id ?? _userIdDefault;
            var userData = _userBusiness.GetUserById(userId);
            if (userData == null || !userData.IsVerify)
            {
                return RedirectToAction("Verify", "Account");
            }
            var balanceAmount = _transactionWalletBiz.GetBalanceVNDByUserId(userId);
            if (balanceAmount == 0)
            {
                TempData["error"] = CommonConst.NotEnoughMoneyForWithdrawal;
                return View();
            }
            var model = new WithdrawalViewModel();
            var listBanks = _bankAccountBiz.GetListByUserId(userId);
            model.banks = TypeAdapter.Adapt<List<BankAccountModel>, List<BankAccountViewModel>>(listBanks);
            model.BalanceVND = balanceAmount;

            return View(model);
        }

        [HttpPost("vnd/withdrawal")]
        public IActionResult WithdrawalVNDPost(WithdrawalViewModel model)
        {
            var userId = _currentUser?.Id ?? _userIdDefault;
            var userData = _userBusiness.GetUserById(userId);
            if (userData == null || !userData.IsVerify)
            {
                return RedirectToAction("Verify", "Account");
            }
            if (model.BankId == 0)
            {
                TempData["error"] = CommonConst.BankidEmptyForWithdrawal;
                return View("WithdrawalVND", model);
            }
            var transactionWallet = new TransactionWallet
            {
                Amount = model.WithdrawalAmount,
                UserId = userId,
                CreatedDate = DateTime.Now,
                BankAccountId = model.BankId,
                TransactionType = (int)TransactionWalletType.Withdrawal
            };
            var result = _transactionWalletBiz.AddTransactionVND(transactionWallet);
            if (result.HasError)
            {
                TempData["error"] = result.Message;
                return View("WithdrawalVND", model);
            }
            TempData["success"] = CommonConst.TransactionWithDrawalSuccess;
            return RedirectToAction("WithdrawalVND");
        }

        [HttpGet("vnd/createaccount")]
        public IActionResult CreateAccountVND()
        {
            var model = new BankAccountViewModel();
            return View(model);
        }

        [HttpPost("vnd/createaccount")]
        public IActionResult CreateAccountVNDPost(BankAccountViewModel model)
        {
            var userId = _currentUser?.Id ?? _userIdDefault;
            var userData = _userBusiness.GetUserById(userId);
            if (userData == null || !userData.IsVerify)
            {
                return RedirectToAction("Verify", "Account");
            }
            var entity = new BankAccount();
            entity = TypeAdapter.Adapt<BankAccountViewModel, BankAccount>(model);
            entity.UserId = userId;
            var result = _bankAccountBiz.CreateBankAccount(entity);
            if (result.HasError)
            {
                TempData["error"] = result.Message;
            }
            else
            {
                TempData["success"] = CommonConst.AddBankAccountSuccess;
            }
            return RedirectToAction("WithdrawalVND");
        }

        [HttpGet("vnd/verifytrade")]
        public IActionResult VerifyTransactionVND()
        {
            return View();
        }

        #endregion VND
    }
}