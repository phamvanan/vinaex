﻿using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Narnia.Business;
using Narnia.Core;
using Narnia.Core.Models;
using Narnia.Web.Models;

namespace Narnia.Web.Controllers
{
    [Authorize]
    [Route("quick")]
    public class QuickBuySellBitcoinController : Controller
    {
        private readonly ISellAdvertisementBiz _sellAdvertisement;
        private readonly IBuyAdvertisementBiz _buyAdvertisement;
        private readonly IWorkContext _workContext;
        private readonly string _currentAssetTypeId;
        private readonly string _currentCurrencyId;

        public QuickBuySellBitcoinController(ISellAdvertisementBiz sellAdvertisement,
                                            IBuyAdvertisementBiz buyAdvertisement,
                                            IWorkContext workContext)
        {
            _sellAdvertisement = sellAdvertisement;
            _buyAdvertisement = buyAdvertisement;
            _workContext = workContext;
            _currentAssetTypeId = workContext.WorkingSymbol.AssetIdBase;
            _currentCurrencyId = workContext.WorkingSymbol.AssetIdQuote;
        }


        public IActionResult Index()
        {
            return View();
        }

        [Route("buy-bitcoin")]
        public IActionResult BuyBitcoin()
        {
            var currentCoinSymbol = _workContext.WorkingSymbol;
            var model = new QuickBuySellViewModel()
            {
                CoinName = currentCoinSymbol.AssetIdBase,
                CurrencyName = _workContext.WorkingSymbol.AssetIdQuote,
                ExchangeRate = currentCoinSymbol.BuyPrice
            };

            return View(model);
        }

        [Route("sell-bitcoin")]
        public IActionResult SellBitcoin()
        {
            var currentCoinSymbol = _workContext.WorkingSymbol;
            var model = new QuickBuySellViewModel()
            {
                CoinName = currentCoinSymbol.AssetIdBase,
                CurrencyName = currentCoinSymbol.AssetIdQuote,
                ExchangeRate = currentCoinSymbol.SellPrice
            };

            return View(model);
        }

        [Produces("application/json")]
        [HttpPost("buy-report")]
        public PagedList<InfoQuickBuySellAdvertisement> LoadQuickBuyReport([FromBody]QuickAdvertisementRequest request)
        {
            int totalResult = 10;
            var buyAd = _sellAdvertisement.LoadQuickSellAdvertisement(_currentAssetTypeId, _currentCurrencyId, request.CoinNumber);
            if (buyAd != null)
                totalResult = buyAd.Count();

            return new PagedList<InfoQuickBuySellAdvertisement>(buyAd, 0, 10, totalResult);
        }

        [Produces("application/json")]
        [HttpPost("sell-report")]
        public PagedList<InfoQuickBuySellAdvertisement> LoadQuickSellReport([FromBody]QuickAdvertisementRequest request)
        {
            int totalResult = 10;
            var _sellAd = _buyAdvertisement.LoadQuickBuyAdvertisement(_currentAssetTypeId, _currentCurrencyId, request.CoinNumber);
            if (_sellAd != null)
                totalResult = _sellAd.Count();

            return new PagedList<InfoQuickBuySellAdvertisement>(_sellAd, 0, 10, totalResult);
        }
    }

    public class QuickAdvertisementRequest : Models.Paging.PagedRequest
    {
        public decimal CoinNumber { get; set; }   //the number of coin
    }
}