﻿using Microsoft.AspNetCore.Mvc;

namespace Narnia.Web.Controllers
{
    [Route("policy")]
    public class PolicyController : Controller
    {
        public IActionResult Index()
        {
            return RedirectToAction("TermsOfService");
        }

        [Route("terms-of-service")]
        [HttpGet]
        public IActionResult TermsOfService()
        {
            return View();
        }
        [Route("privacy-policy")]
        public IActionResult PrivacyPolicy()
        {
            return View();
        }

        [Route("trading-policy")]
        public IActionResult TradingPolicy()
        {
            return View();
        }
    }
}