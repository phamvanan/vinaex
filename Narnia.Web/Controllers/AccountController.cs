﻿using FastMapper;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Narnia.Business;
using Narnia.Business.Commissions;
using Narnia.Business.Impl;
using Narnia.Business.Messaging;
using Narnia.Business.Multimedia;
using Narnia.Core;
using Narnia.Core.Domain;
using Narnia.Core.Domain.Multimedia;
using Narnia.Core.Enums;
using Narnia.Core.Helpers;
using Narnia.Core.Infrastructure;
using Narnia.Core.Models;
using Narnia.Web.Areas.Admin.Models.AjaxDataTables;
using Narnia.Web.Framework.Mvc.Attributes;
using Narnia.Web.Models;
using Narnia.Web.Models.AccountViewModels;
using Narnia.Web.Models.Paging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Narnia.Web.Controllers
{
    [Authorize]
    [Route("[controller]/[action]")]
    public class AccountController : Controller
    {
        private readonly IUserBusiness _userBusiness;
        private readonly ISellAdvertisementBiz _sellAdvertisement;
        private readonly IBuyAdvertisementBiz _buyAdvertisement;
        private readonly IAssetTypeBusiness _assetTypeBusiness;
        private readonly IWorkContext _workContext;
        private readonly ICommissionService _commissionService;
        private readonly User _currentUser;
        private readonly ITransactionBiz _transaction;
        private readonly IPictureService _pictureService;

        public AccountController(IUserBusiness userBusiness,
                                ITransactionBiz transactionBiz,
                                ISellAdvertisementBiz sellAdvertisement,
                                IBuyAdvertisementBiz buyAdvertisement,
                                IAssetTypeBusiness assetTypeBusiness,
                                IPictureService pictureService,
                                IWorkContext workContext,
                                ICommissionService commissionService)
        {
            _userBusiness = userBusiness;
            _sellAdvertisement = sellAdvertisement;
            _buyAdvertisement = buyAdvertisement;
            _assetTypeBusiness = assetTypeBusiness;
            _workContext = workContext;
            this._commissionService = commissionService;
            _transaction = transactionBiz;
            _pictureService = pictureService;
            _currentUser = EngineContext.Current.Resolve<IWorkContext>().CurrentUser;
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Login(string refer = "")
        {
            if (_currentUser != null)
            {
                return RedirectToAction("Index", "Home");
            }

            return View(new LoginViewModel() { ReferenceCode = refer });
        }

        [HttpPost]
        public async Task<IActionResult> LogOut()
        {
            try
            {
                await HttpContext.SignOutAsync();

                return RedirectToAction("Login");
            }
            catch (Exception ex)
            {
                // ignored
            }

            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public IActionResult Login(LoginViewModel userViewModel)
        {
            if (_currentUser != null)
            {
                return RedirectToAction("Index", "Home");
            }

            // Need to validate
            if (!ModelState.IsValid)
            {
                TempData["error"] = "invalid email";
                return View(userViewModel);
            }
            try
            {
                var user = new User
                {
                    Email = userViewModel.Email,
                    UserName = userViewModel.Email,
                    ReferenceCode = userViewModel.ReferenceCode,
                    ActivationKey = SecurityHelper.Encrypt(Guid.NewGuid().ToString("N") + "/" + DateTime.Now.ToUnixTimestamp().ToString())
                };

                var result = _userBusiness.Process<User>(() => _userBusiness.AddNewUser(user));
                if (result.HasError == false)
                {
                    var activationLink = Url.Action("Activate", "Account", new { activeCode = user.ActivationKey }, this.Request.Scheme);

                    Hangfire.BackgroundJob.Enqueue(()
                        => EngineContext.Current.Resolve<EmailJob>().SendActivationEmail(user, activationLink)
                    );
                    return RedirectToAction("WaitingActive", new { email = user.Email });
                }

                TempData["error"] = result.Message;

                return View(userViewModel);
            }
            catch (Exception ex)
            {
                TempData["error"] = "System error.";

                return View(userViewModel);
            }
        }

        [AllowAnonymous]
        public IActionResult WaitingActive(string email)
        {
            if (_currentUser != null)
            {
                return RedirectToAction("Index", "Home");
            }
            ViewBag.Email = email;

            return View();
        }

        [AllowAnonymous]
        public async Task<IActionResult> Activate(string activeCode = null)
        {
            try
            {
                var result = _userBusiness.Process<User>(() => _userBusiness.Activate(activeCode));
                if (result.HasError == false)
                {
                    //Use cookie authentication:
                    var identity = new ClaimsIdentity(CookieAuthenticationDefaults.AuthenticationScheme);
                    identity.AddClaim(new Claim(ClaimTypes.Name, result.Data.UserName));
                    identity.AddClaim(new Claim(ClaimTypes.Sid, result.Data.Id.ToString()));
                    identity.AddClaim(new Claim("referalCode", result.Data.ReferenceCode));
                    identity.AddClaim(new Claim("UserAlias", result.Data.Alias));
                    await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(identity));

                    return RedirectToAction("Index", "Home");
                }

                return RedirectToAction("Login", "Account", new { hasError = true });
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index", "Home");
            }
        }

        [HttpGet]
        public IActionResult Profile(int userId)
        {
            try
            {
                userId = userId == 0 ? _workContext.CurrentUser.Id : userId;
                ProfileViewModel model = new ProfileViewModel();
                model.CanShowBalance = _workContext.CurrentUser.Id == userId;
                model.CanShowCommission = _workContext.CurrentUser.Id == userId;

                string assetTypeId = _workContext.WorkingSymbol.AssetIdBase;

                
                var responseData = _userBusiness.GetUserFullInforByUserId(userId);
                if (responseData.HasError == true)
                {
                    return RedirectToAction("Index", "Home");
                }


                model.CreatedDate = responseData.Data.CreatedDate;
                model.LastLoginDate = responseData.Data.LoginDate;
                model.IPAddress = responseData.Data.IPAddress;
                var availableBalances = responseData.Data.AssetBalances;
                if (model.CanShowBalance == true)
                {
                    var supportedAssetTypes = _assetTypeBusiness.GetAll();
                    foreach (var item in supportedAssetTypes)
                    {
                        var vm = new AssetBalanceViewModel();
                        vm.UserId = _workContext.CurrentUser.Id;
                        vm.AssetType = item.AssetTypeId;
                        foreach (var ab in availableBalances)
                        {
                            if (ab.AssetTypeId == item.AssetTypeId)//Found
                            {
                                vm.Balance = ab.Balance;
                                vm.FrozenBalance = ab.FrozenBalance;
                                break;
                            }
                        }
                        model.AssetBalances.Add(vm);
                    }
                }

                model.UserId = _workContext.CurrentUser.Id;
                model.UserName = _currentUser.Email;
                model.AssetTypeId = assetTypeId;
                model.ReferralLink = HttpContext.Request.Host.Value + "/Account/Login?refer=" + _workContext.CurrentUser.ReferenceCode;

                return View(model);
            }
            catch (Exception ex)
            {
                TempData["error"] = CommonConst.HasErrorWhenGetUserInfor;
                return RedirectToAction("Index", "Home");
            }
        }

        [HttpPost]
        public PagedList<CommissionHistoryViewModel> LoadCommissionPaging([FromBody]LoadCommissionRequestViewModel reqModel)
        {
            var data = new List<CommissionHistoryViewModel>();
            if (reqModel.UserId != _workContext.CurrentUser.Id)
            {
                return new PagedList<CommissionHistoryViewModel>(data, reqModel.PageNo - 1, reqModel.PageSize, 0);
            }
            var commissionResponse = _commissionService.GetCommissionHistoryByBeneficiaryUserId(reqModel.UserId, reqModel.PageNo-1, reqModel.PageSize);
            data = TypeAdapter.Adapt<List<CommissionHistoryByBeneficiaryUserIdModel>, List<CommissionHistoryViewModel>>(commissionResponse.Data.ToList());

            return new PagedList<CommissionHistoryViewModel>(data, reqModel.PageNo - 1, reqModel.PageSize, commissionResponse.TotalCount);
        }

        [HttpGet]
        public IActionResult Verify()
        {
            return View(new EvidenceViewModel());
        }

        [HttpPost]
        public async Task<IActionResult> Verify(EvidenceViewModel model, List<IFormFile> Image)
        {
            try
            {
                if (Image != null && Image.Count > 0 && Image[0].Length > 0)
                {
                    var userInfor = new UserInfor();
                    decimal fileSize = 0;
                    using (var stream = new MemoryStream())
                    {
                        await Image[0].CopyToAsync(stream);
                        userInfor.Image = stream.ToArray();
                        userInfor.UserId = _currentUser != null ? _currentUser.Id : 0;
                        userInfor.ContentType = Image[0].ContentType;
                        fileSize = stream.Length / 1024;
                    }

                    const long maxByte = 5;
                    if (fileSize / 1024 > maxByte)
                    {
                        TempData["error"] = $"Ảnh bạn đã upload lớn hơn {maxByte} bytes.";
                        return View();
                    }

                    var result = _userBusiness.UpdateImage(userInfor);
                    if (result.HasError == false)
                    {
                        TempData["success"] = "Ảnh bạn đã upload thành công, admin sẽ kiểm duyệt trong thời gian sớm nhất.";
                        return View();
                    }

                    TempData["error"] = result.Message;
                }
                else
                {
                    TempData["error"] = "Vui lòng chọn ảnh để upload bằng chứng!";
                }

                return View();
            }
            catch (Exception ex)
            {
                return View();
            }
        }

        [HttpPost]
        public PagedList<BuySellViewModel> LoadAdvertismentByUserId([FromBody]LoadAdvertismentRequest request)
        {
            int totalResult = 0;
            List<BuySellViewModel> advertisment = null;

            if (request.Type == 1)
            {
                advertisment = _sellAdvertisement.GetSellAdvertisementByUserId(request.PageNo, request.PageSize, _currentUser.Id, out totalResult
                    , true, request.AssetTypeId).ToList();
            }
            else
            {
                advertisment = _buyAdvertisement.GetBuyAdvertisementByUserId(request.PageNo, request.PageSize, _currentUser.Id, out totalResult
                    , true, request.AssetTypeId).ToList();
            }

            if (totalResult == -1)
                totalResult = request.TotalItems;

            return new PagedList<BuySellViewModel>(advertisment, request.PageNo - 1, request.PageSize, totalResult);
        }

        [HttpGet]
        public IActionResult DashBoard()
        {
            return View();
        }

        [HttpPost]
        public JsonResult OpenningTransaction(DataTableAjaxPostModel model, int type)
        {
            int totalRecords = 0;
            IList<TransactionHistoryModel> transactions = null;
            if (type == (int)TransactionStatusMode.Open)
            {
                transactions = _transaction.GetTransactionHistoryByType(_currentUser.Id, TransactionStatusMode.Open, model.length, model.start, out totalRecords);
            }
            else
            {
                transactions = _transaction.GetTransactionHistoryByType(_currentUser.Id, TransactionStatusMode.Close, model.length, model.start, out totalRecords);
            }

            return Json(new DataTableAjaxReturnModel<TransactionHistoryModel>()
            {
                draw = model.draw,
                recordsFiltered = totalRecords,
                recordsTotal = totalRecords,
                data = transactions
            });
        }

        #region My account / Avatar

        public virtual IActionResult Avatar()
        {
            var model = new UserAvatarModel();
            //  model.AvatarUrl = _pictureService.GetPictureUrl()
            return View(model);
        }

        [HttpPost, ActionName("Avatar")]
        [FormValueRequired("upload-avatar")]
        public virtual IActionResult UploadAvatar(UserAvatarModel model, IFormFile uploadedFile)
        {
            var curUser = _workContext.CurrentUser;

            if (ModelState.IsValid)
            {
                try
                {
                    Picture userAvatar = null;
                    if (curUser != null && curUser.AvatarPictureId.HasValue && curUser.AvatarPictureId.Value > 0)
                    {
                        userAvatar = _pictureService.GetPictureById(curUser.AvatarPictureId.Value);
                    }

                    if (uploadedFile != null && !string.IsNullOrEmpty(uploadedFile.FileName))
                    {
                        byte[] customerPictureBinary = null;
                        using (var fileStream = uploadedFile.OpenReadStream())
                        {
                            using (var ms = new MemoryStream())
                            {
                                fileStream.CopyTo(ms);
                                customerPictureBinary = ms.ToArray();
                            }
                        }
                        if (userAvatar != null)
                            userAvatar = _pictureService.UpdatePicture(userAvatar.Id, customerPictureBinary, uploadedFile.ContentType, null);
                        else
                        {
                            userAvatar = _pictureService.InsertPicture(customerPictureBinary, uploadedFile.ContentType, null);
                            if (userAvatar != null)
                            {
                                _userBusiness.UpdateAvatarIdForUser(userAvatar.Id, curUser.Id);
                            }
                        }
                    }

                    var userAvatarId = 0;
                    if (userAvatar != null)
                        userAvatarId = userAvatar.Id;

                    model.AvatarUrl = _pictureService.GetPictureUrl(userAvatarId);
                    return View(model);
                }
                catch (Exception exc)
                {
                    ModelState.AddModelError("", exc.Message);
                }
            }
            if (curUser.AvatarPictureId.HasValue && curUser.AvatarPictureId.Value > 0)
                model.AvatarUrl = _pictureService.GetPictureUrl(curUser.AvatarPictureId.Value);
            return View(model);
        }

        [HttpPost, ActionName("Avatar")]
        [FormValueRequired("remove-avatar")]
        public virtual IActionResult RemoveAvatar(UserAvatarModel model)
        {
            var curUser = _workContext.CurrentUser;
            if (curUser != null && curUser.AvatarPictureId.HasValue && curUser.AvatarPictureId.Value > 0)
            {
                var userAvatar = _pictureService.GetPictureById(curUser.AvatarPictureId.Value);
                if (userAvatar != null)
                    _pictureService.DeletePicture(userAvatar);
                _userBusiness.UpdateAvatarIdForUser(0, curUser.Id);
            }

            return RedirectToAction("Avatar");
        }

        #endregion My account / Avatar
    }
}