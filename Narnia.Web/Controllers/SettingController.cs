﻿using FastMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Narnia.Business.Impl;
using Narnia.Core;
using Narnia.Core.Domain;
using Narnia.Core.Infrastructure;
using Narnia.Core.Models;
using System.Collections.Generic;

namespace Narnia.Web.Controllers
{
    [Authorize]
    [Route("settings")]
    public class SettingController : Controller
    {
        private readonly IUserBusiness _userBusiness;
        private readonly UserInforModel _userInforModel;
        private readonly User _user;
        public SettingController(IUserBusiness userBusiness)
        {
            _userBusiness = userBusiness;
            _user = EngineContext.Current.Resolve<IWorkContext>().CurrentUser;
            _userInforModel = _userBusiness.GetUserInfor(_user.Id);
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet("notification")]
        public IActionResult Notification()
        {
            return View(_userInforModel);
        }

        [HttpGet("profile")]
        public IActionResult Profile()
        {
            return View(_userInforModel);
        }

        [HttpGet("history-login")]
        public IActionResult HistoryLogin()
        {
            try
            {
                var loginHistory = _userBusiness.GetHistoryLoginByUserId(_user.Id);
                var model = TypeAdapter.Adapt<List<UserLoginHistoryModel>>(loginHistory);
                return View(model);
            }
            catch (System.Exception ex)
            {
                return View(new List<UserLoginHistoryModel>());
            }
        }

        [HttpGet("close-account")]
        public IActionResult CloseAccount()
        {
            return View();
        }
    }
}