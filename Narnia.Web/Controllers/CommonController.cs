﻿using Microsoft.AspNetCore.Mvc;
using Narnia.Business;
using Narnia.Business.Localization;
using Narnia.Core;

namespace Narnia.Web.Controllers
{
    public class CommonController : Controller
    {
        private readonly ILanguageService _languageService;
        private readonly ICoinSymbolBiz _coinSymbolService;
        private readonly IWorkContext _workContext;
        public CommonController(ILanguageService languageService,
                                IWorkContext workContext,
                                ICoinSymbolBiz coinSymbolService)
        {
            _languageService = languageService;
            _workContext = workContext;
            _coinSymbolService = coinSymbolService;
        }

        public virtual IActionResult PageNotFound()
        {
            Response.StatusCode = 404;
            Response.ContentType = "text/html";

            return View();
        }

        public virtual IActionResult SetLanguage(int langId, string returnUrl = "")
        {
            var language = _languageService.GetLanguageById(langId);
            if (!language?.Published ?? false)
                language = _workContext.WorkingLanguage;

            if (string.IsNullOrEmpty(returnUrl))
                returnUrl = Url.Action("Index", "Home");

            //prevent open redirection attack
            if (!Url.IsLocalUrl(returnUrl))
                returnUrl = Url.Action("Index", "Home");

            _workContext.WorkingLanguage = language;

            return Redirect(returnUrl);
        }
        
        public virtual IActionResult SetSymbol(string symbolId, string returnUrl = "")
        {
            var symbol = _coinSymbolService.GetSymbolByName(symbolId);
            if (symbol != null)
                _workContext.WorkingSymbol = symbol;


            if (string.IsNullOrEmpty(returnUrl))
                returnUrl = Url.Action("Index", "Home");

            //prevent open redirection attack
            if (!Url.IsLocalUrl(returnUrl))
                returnUrl = Url.Action("Index", "Home");

            return Redirect(returnUrl);
        }
    }
}
