﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Narnia.Web.Controllers
{
    public class BaseController : Controller
    {

        public static IEnumerable<String> GetErrors(ModelStateDictionary modelState)
        {
            return modelState.Values.SelectMany(v => v.Errors)
                .Select(v => v.ErrorMessage + " " + v.Exception).ToList();
        }

        public string GetModelStateError(ModelStateDictionary modelState)
        {
            String messages = String.Join(Environment.NewLine, modelState.Values.SelectMany(v => v.Errors)
                .Select(v => v.ErrorMessage + " " + v.Exception));

            return messages;
        }
    }
}