﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using FastMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Narnia.Business;
using Narnia.Core.Domain;
using Narnia.Core.Enums;
using Narnia.Web.Models;
using System.Drawing;
using Microsoft.AspNetCore.Authorization;
using Narnia.Business.Impl;
using Narnia.Core.Models;
using Narnia.Core.Infrastructure;
using Narnia.Core;
using System.Drawing.Imaging;
using Narnia.Core.Helpers;

namespace Narnia.Web.Controllers
{
    [Authorize]
    [Route("offers")]
    public class TransactionController : Controller
    {
        private readonly ISellAdvertisementBiz _sellAdvertisementBiz;
        private readonly ITransactionBiz _transactionBiz;
        private readonly IBuyAdvertisementBiz _buyAdvertisementBiz;
        private readonly IUserBusiness _userBusiness;
        private readonly IEvidenceBiz _evidenceBiz;
        private readonly User _currentUser;
        private int _userIdDefault = 0;

        public TransactionController(ISellAdvertisementBiz sellAdvertisementBiz,
                                    ITransactionBiz transactionBiz,
                                    IBuyAdvertisementBiz buyAdvertisementBiz,
                                    IUserBusiness userBusiness,
                                    IEvidenceBiz evidenceBiz)
        {
            _sellAdvertisementBiz = sellAdvertisementBiz;
            _transactionBiz = transactionBiz;
            _buyAdvertisementBiz = buyAdvertisementBiz;
            _userBusiness = userBusiness;
            _currentUser = EngineContext.Current.Resolve<IWorkContext>().CurrentUser;
            _evidenceBiz = evidenceBiz;
        }
        public IActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Bán EHT cho giao dịch BuyAdvertisement
        /// </summary>
        /// <param name="buyAdId"></param>
        /// <returns></returns>
        [HttpGet("sell/{buyAdId:int}")]
        public IActionResult InfoSellAdvertisement(int buyAdId)
        {
            int userId = _currentUser?.Id ?? _userIdDefault;
            var result = _buyAdvertisementBiz.GetInfoBuyAdvertisement(buyAdId);
            if (result.HasError || result.Data == null)
            {
                return View(new InfoAdvertisementForSellBuyViewModel());
            }

            var model = TypeAdapter.Adapt<InfoAdvertisementForSellBuyModel, InfoAdvertisementForSellBuyViewModel>(result.Data);
            //if (string.IsNullOrEmpty(result.Data.WalletAddressAds))
            //{
            //    TempData["error"] = "Người mua chưa cung cấp ví nhận tiền.";
            //    model.IsActived = false;
            //};
            model.TransactionAmount = model.MaxCoinNumber;
            model.CanTransaction = result.Data.UserId != userId;
            return View(model);
        }


        /// <summary>
        /// Quick sell
        /// </summary>
        /// <param name="buyAdId"></param>
        /// <returns></returns>
        [Route("quicksell/{adId:int}/{coinNumber:decimal}")]
        public IActionResult InfoSellAdvertisement(int adId, decimal coinNumber)
        {
            QuickBuySellRequest info = new QuickBuySellRequest()
            {
                AdId = adId,
                coinNumber = coinNumber,
                walletAddress = ""
            };

            var result = _buyAdvertisementBiz.GetInfoBuyAdvertisement(info.AdId);
            if (result.HasError || result.Data == null)
            {
                return View("InfoSellAdvertisement", new InfoAdvertisementForSellBuyViewModel());
            }
            result.Data.MaxCoinNumber = info.coinNumber;
            var model = TypeAdapter.Adapt<InfoAdvertisementForSellBuyModel, InfoAdvertisementForSellBuyViewModel>(result.Data);
            //model.ToUSD = info.coinNumber * model.AvgPrice; //model.ToUSD = model.MaxCoinNumber * model.AvgPrice;
            //model.ToVND = model.ToUSD * model.Rate;
            model.TransactionAmount = info.coinNumber;
            model.WalletAddressUser = info.walletAddress;
            return View("InfoSellAdvertisement", model);
        }

        [Route("quicksell/{adId:int}/{coinNumber:decimal}/{walletAddress}")]
        public IActionResult InfoSellAdvertisement(int adId, decimal coinNumber, string walletAddress = null)
        {
            QuickBuySellRequest info = new QuickBuySellRequest()
            {
                AdId = adId,
                coinNumber = coinNumber,
                walletAddress = walletAddress
            };

            var result = _buyAdvertisementBiz.GetInfoBuyAdvertisement(info.AdId);
            if (result.HasError || result.Data == null)
            {
                return View("InfoSellAdvertisement", new InfoAdvertisementForSellBuyViewModel());
            }
            result.Data.MaxCoinNumber = info.coinNumber;
            var model = TypeAdapter.Adapt<InfoAdvertisementForSellBuyModel, InfoAdvertisementForSellBuyViewModel>(result.Data);
            //model.ToUSD = info.coinNumber * model.AvgPrice; //model.ToUSD = model.MaxCoinNumber * model.AvgPrice;
            //model.ToVND = model.ToUSD * model.Rate;
            model.TransactionAmount = info.coinNumber;
            model.WalletAddressUser = info.walletAddress;
            return View("InfoSellAdvertisement", model);
        }

        /// <summary>
        /// Tạo giao dịch mua (BuyAdvertisement)
        /// </summary>
        /// <param name="transaction"></param>
        /// <returns></returns>
        [HttpPost("createtranforsell")]
        public IActionResult CreatTransactionSell(InfoAdvertisementForSellBuyViewModel model)
        {
            if (model != null)
            {
                var _transaction = new Transaction();
                try
                {
                    _transaction.BuyAdvertisementId = model.Id;
                    _transaction.CreatedDate = DateTime.Now;
                    _transaction.Status = (int)TransactionStautus.Pending;
                    _transaction.TransactionType = (int)TransactionType.Deposit;
                    _transaction.AmountCoin = model.TransactionAmount;
                    _transaction.UpdatedDate = (DateTime?)null;
                    _transaction.UserId = _currentUser?.Id ?? _userIdDefault;
                    var result = _transactionBiz.AddTransactionForBuy(_transaction);
                    if (result.HasError)
                    {
                        TempData["error"] = result.Message;
                        return RedirectToAction("InfoSellAdvertisement", new { buyAdId = model.Id });
                    }
                    return RedirectToAction("Detail", new { transcode = result.Data.TransactionCode });
                }
                catch (Exception ex)
                {

                }
            }
            return View("InfoSellAdvertisement", new InfoAdvertisementForSellBuyViewModel());
        }

        /// <summary>
        /// SellAdvertisementId
        /// </summary>
        /// <param name="sellAdid"></param>
        /// <returns></returns>
        [HttpGet("buy/{sellAdid:int}")]
        public IActionResult InfoBuyAdvertisement(int sellAdid)
        {
            int userId = _currentUser?.Id ?? _userIdDefault;

            var result = _sellAdvertisementBiz.GetInfoSellAdvertisement(sellAdid, userId);
            if (result.HasError || result.Data == null)
            {
                return View(new InfoAdvertisementForSellBuyViewModel());
            }
            var model = TypeAdapter.Adapt<InfoAdvertisementForSellBuyModel, InfoAdvertisementForSellBuyViewModel>(result.Data);
            model.CanTransaction = result.Data.UserId != userId;
            //model.ToUSD = model.MaxCoinNumber * model.AvgPrice;
            //model.ToVND = model.ToUSD * model.Rate;
            model.TransactionAmount = model.MaxCoinNumber;
            return View(model);
        }

        private InfoAdvertisementForSellBuyViewModel GetSellInfo(int sellAdid, decimal coinNumber, string walletAddress)
        {
            //QuickBuySellRequest info = new QuickBuySellRequest()
            //{
            //    AdId = sellAdid,
            //    coinNumber = coinNumber,
            //    walletAddress = ""
            //};
            int userId = _currentUser?.Id ?? _userIdDefault;
            //var curUser = EngineContext.Current.Resolve<IWorkContext>().CurrentUser;
            //if (curUser != null)
            //    userId = curUser.Id;

            var result = _sellAdvertisementBiz.GetInfoSellAdvertisement(sellAdid, userId);
            if (result.HasError || result.Data == null)
            {
                return new InfoAdvertisementForSellBuyViewModel();
            }
            var model = TypeAdapter.Adapt<InfoAdvertisementForSellBuyModel, InfoAdvertisementForSellBuyViewModel>(result.Data);
            //model.ToUSD = coinNumber * model.AvgPrice; //model.MaxCoinNumber * model.AvgPrice;
            //model.ToVND = model.ToUSD * model.Rate;
            model.TransactionAmount = coinNumber;
            model.WalletAddressUser = walletAddress;

            return model;
        }

        [Route("quickbuy/{sellAdid:int}/{coinNumber:decimal}")]
        public IActionResult InfoBuyAdvertisement(int sellAdid, decimal coinNumber)
        {
            var model = GetSellInfo(sellAdid, coinNumber, string.Empty);
            return View(model);
        }

        [Route("quickbuy/{sellAdid:int}/{coinNumber:decimal}/{walletAddress}")]
        public IActionResult InfoBuyAdvertisement(int sellAdid, decimal coinNumber, string walletAddress = null)
        {
            var model = GetSellInfo(sellAdid, coinNumber, walletAddress);
            return View(model);
        }


        /// <summary>
        /// Tạo giao dịch bán  SellAdvertisementId
        /// </summary>
        /// <param name="transaction"></param>
        /// <returns></returns>
        /// TODO:
        [HttpPost("createtranforbuy")]
        public IActionResult CreatTransactionBuy(InfoAdvertisementForSellBuyViewModel model)
        {
            var userId = _currentUser?.Id ?? _userIdDefault;
            var userData = _userBusiness.GetUserById(userId);
            if (userData == null || !userData.IsVerify)
            {
                return RedirectToAction("Verify", "Account");
            }
            if (model != null)
            {
                var _transaction = new Transaction();
                try
                {
                    _transaction.SellAdvertisementId = model.Id;
                    _transaction.CreatedDate = DateTime.Now;
                    _transaction.Status = (int)TransactionStautus.Pending;
                    _transaction.TransactionType = (int)TransactionType.Deposit;
                    _transaction.AmountCoin = model.TransactionAmount;
                    _transaction.UpdatedDate = (DateTime?)null;
                    _transaction.UserId = userId;
                    var result = _transactionBiz.AddTransactionForSell(_transaction);
                    if (result.HasError)
                    {
                        TempData["error"] = result.Message;
                        return RedirectToAction("InfoBuyAdvertisement", new { sellAdid = model.Id });
                    }
                    TempData["success"] = "Tạo giao dịch thành công.";
                    return RedirectToAction("Detail", new { transcode = result.Data.TransactionCode });
                }
                catch (Exception ex)
                {

                }
                return View("InfoBuyAdvertisement", model);
            }
            return View("InfoBuyAdvertisement", new InfoAdvertisementForSellBuyViewModel());
        }


        /// <summary>
        /// Cập nhật bằng chứng chuyển tiền
        /// </summary>
        /// <param name="model"></param>
        /// <param name="Image"></param>
        /// <returns></returns>
        [HttpPost("UploadEvidence")]
        public async Task<IActionResult> UploadEvidence(EvidenceViewModel model, List<IFormFile> Image)
        {
            if (Image != null && Image.Count > 0 && Image[0].Length > 0)
            {
                var evidenceEntity = new Evidence();
                var userId = _currentUser?.Id ?? _userIdDefault;
                using (var stream = new MemoryStream())
                {
                    await Image[0].CopyToAsync(stream);
                    evidenceEntity.Image = stream.ToArray();
                    evidenceEntity.FileName = Image[0].FileName;
                    evidenceEntity.ContentType = Image[0].ContentType;
                    evidenceEntity.CreatedDate = DateTime.Now;
                }
                var isImage = UploadHelper.IsImage(evidenceEntity.ContentType, evidenceEntity.FileName);
                if (!isImage)
                {
                    TempData["error"] = "Bằng chứng không hợp lệ.";
                    return RedirectToAction("Detail", new { transcode = model.Transcode });
                }

                var result = _transactionBiz.CreateEvidencce(evidenceEntity, model.Transcode, userId);
                if (result.HasError == false)
                {
                    TempData["success"] = "Cập nhật bằng chứng thành công";
                    return RedirectToAction("Detail", new { transcode = model.Transcode });
                }
                TempData["error"] = result.Message;
            }
            TempData["error"] = "Chưa chọn bằng chứng để cập nhật.";
            return RedirectToAction("Detail", new { transcode = model.Transcode });
        }

        /// <summary>
        /// Người mua hủy giao dịch
        /// </summary>
        /// <param name="transId"></param>
        /// <returns></returns>
        //[HttpPost("CancelTrade")]
        ////[HttpPost("buyercancel/{transId:int}")]
        //public IActionResult CancelTrade(int transId)
        //{
        //    var userId = _currentUser?.Id ?? _userIdDefault;
        //    var result = _transactionBiz.BuyerCancel(transId, userId);
        //    if (result.HasError)
        //    {
        //        TempData["error"] = result.Message;
        //    }
        //    else
        //    {
        //        TempData["success"] = "Huỷ giao dịch thành công";
        //    }
        //    return RedirectToAction("Detail", new { transid = transId });
        //}

        [HttpGet("getfile/{code}")]
        public FileStreamResult GetFile(string code)
        {
            var result = _evidenceBiz.GetByCode(code);
            if (result == null)
            {
                return null;
            }
            //byte[] imgBytes = result.Image;

            Stream stream = new MemoryStream(result.Image);
            return new FileStreamResult(stream, result.ContentType);
        }

        [HttpGet("getfilethumbnail/{code}/{width}/{height}")]
        public FileStreamResult GetFileThumbnail(string code, int width, int height)
        {
            var result = _evidenceBiz.GetByCode(code);
            if (result == null)
            {
                return null;
            }
            byte[] imgBytes = getThumbNail(result.Image, width, height);

            Stream stream = new MemoryStream(imgBytes);
            return new FileStreamResult(stream, result.ContentType);
        }

        private byte[] getThumbNail(byte[] data, int width, int height)
        {
            using (var file = new MemoryStream(data))
            {
                //int width = 200 * multi;
                using (var image = Image.FromStream(file, true, true)) /* Creates Image from specified data stream */
                {
                    int X = image.Width;
                    int Y = image.Height;
                    //int height = (int)((width * Y) / X);
                    using (var thumb = image.GetThumbnailImage(width, height, () => false, IntPtr.Zero))
                    {
                        var jpgInfo = ImageCodecInfo.GetImageEncoders()
                            .Where(codecInfo => codecInfo.MimeType == "image/png").First();
                        using (var encParams = new EncoderParameters(1))
                        {
                            using (var samllfile = new MemoryStream())
                            {
                                long quality = 100;
                                encParams.Param[0] = new EncoderParameter(Encoder.Quality, quality);
                                thumb.Save(samllfile, jpgInfo, encParams);
                                return samllfile.ToArray();
                            }
                        };
                    };
                };
            };
        }

        [HttpGet("download/{code}")]
        public async Task<IActionResult> Download(string code)
        {
            if (string.IsNullOrEmpty(code))
                return Content("filename not present");

            var result = _evidenceBiz.GetByCode(code);
            if (result == null)
            {
                return Content("filename not present");
            }

            byte[] imgBytes = result.Image;

            Stream stream = new MemoryStream(result.Image);

            var memory = new MemoryStream();
            memory.Position = 0;
            return File(memory, result.ContentType, result.FileName);
        }

        [HttpGet("detail/{transcode}")]
        public IActionResult Detail([FromRoute] string transcode)
        {
            int userId = _currentUser?.Id ?? _userIdDefault;
            var userData = _userBusiness.GetUserById(userId);

            var result = _transactionBiz.GetDetailTransByCode(transcode);
            if (result.HasError || result.Data == null || (result.Data.UserIdAds != userId && result.Data.UserIdTran != userId))
            {
                //User không có quyền xem chi tiết giao dịch
                return View(new InfoAdvertisementDetailViewModel { TransIsValid = false });
            }
            var model = TypeAdapter.Adapt<InfoAdvertisementDetailModel, InfoAdvertisementDetailViewModel>(result.Data);
            if ((model.IsSellTransaction && userId == model.UserIdTran) || (!model.IsSellTransaction && userId == model.UserIdAds))
            {
                //quảng cáo bán và user hiện tại là người mua ( tạo giao dịch)
                //quảng cáo mua và user hiện tại là người mua ( tạo quảng cáo mua)
                model.IsAllowUploadEvidence = true;
            }
            if (((model.IsSellTransaction && userId == model.UserIdAds) ||
                 (!model.IsSellTransaction && userId == model.UserIdTran)) &&
                (model.Status == (int)TransactionStautus.Pending ||
                 model.Status == (int)TransactionStautus.HasEvidence))
            {
                //quảng cáo bán và user hiện tại là người bán ( người tạo quảng cáo)
                //quảng cáo mua và user hiện tại là người bán (người tạo giao dịch)
                //giao dịch đang là mới  hoặc có bằng chứng
                model.IsAllowConfirm = true;
            }

            model.IsAllowViewEvidence = userId == model.UserIdTran || userId == model.UserIdAds;
            return View(model);
        }

        [HttpPost("confirm/{transcode}")]
        public IActionResult Confirm(string transcode)
        {
            var userId = _currentUser?.Id ?? _userIdDefault;
            var result = _transactionBiz.SellerConfirm(userId, transcode);
            if (result.HasError)
            {
                TempData["error"] = result.Message;
            }
            else
            {
                TempData["success"] = "Xác thực giao dịch thành công.";
            }
            return RedirectToAction("Detail", new { transcode = transcode });
        }

        [AllowAnonymous]
        [HttpGet]
        public JsonResult ExchangePrice(decimal price, decimal avgPrice, decimal minCoin, decimal maxCoin, string currencyName)
        {
            //string USD = $"≈ {(price * avgPrice).ToString("G29")} USD";
            decimal VNDDecimal = Math.Round(price * avgPrice);
            string VND = $"{VNDDecimal.ToString("##,###")} VNĐ";

            if (price > maxCoin)
            {
                return Json(
                    new { valid = false, messErr = $"Lượng {currencyName} tối đa là  :{maxCoin.ToString("G29")} {currencyName}", VND });
            }
            if (price < minCoin)
            {
                return Json(
                    new { valid = false, messErr = $"Lượng {currencyName} tối thiểu là  :{maxCoin.ToString("G29")} {currencyName}", VND });
            }
            return Json(new { valid = true, VND });
        }
    }
}