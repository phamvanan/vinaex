﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using FastMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Narnia.Business;
using Narnia.Business.Impl;
using Narnia.Core;
using Narnia.Core.Domain;
using Narnia.Core.Enums;
using Narnia.Core.Infrastructure;
using Narnia.Core.Models;
using Narnia.Web.Models.BankAccounts;

namespace Narnia.Web.Controllers
{
    [Authorize]
    [Route("bank-account")]
    public class BankAccountController : Controller
    {
        private readonly User _currentUser;
        private readonly IBankAccountBiz _bankAccountBiz;
        private readonly ITransactionWalletBiz _transactionWalletBiz;
        private readonly ITransactionBiz _transactionBiz;
        public BankAccountController(IBankAccountBiz bankAccountBiz,
                                    IUserBusiness userBusiness,
                                    ITransactionWalletBiz transactionWalletBiz,
                                    ITransactionBiz transactionBiz)
        {
            _currentUser = EngineContext.Current.Resolve<IWorkContext>().CurrentUser;
            _bankAccountBiz = bankAccountBiz;
            _transactionWalletBiz = transactionWalletBiz;
            _transactionBiz = transactionBiz;
        }
        public IActionResult Index()
        {
            return View();
        }

        [Route("deposit")]
        public IActionResult Deposit()
        {
            var model = new BankDepositViewModel();
            var bankAccounts = _bankAccountBiz.GetListAccountAdmin();
            model.SystemBankAccounts = new List<BankAccountViewModel>();
            model.SystemBankList = new List<SelectListItem>();
            model.SystemBankList.Add(new SelectListItem() { Value = "0", Text = "Chọn Ngân Hàng" });
            foreach (var bankAcc in bankAccounts)
            {
                var ba = TypeAdapter.Adapt<BankAccountModel, BankAccountViewModel>(bankAcc);
                ba.Bank = new Models.BankViewModel();
                ba.Bank.Id = bankAcc.BankId;
                ba.Bank.BankName = bankAcc.BankName;

                model.SystemBankAccounts.Add(ba);

                model.SystemBankList.Add(new SelectListItem() { Value = bankAcc.BankId.ToString(), Text = bankAcc.BankName });
            }

            return View(model);
        }

        [HttpPost("postdeposit")]
        public async Task<JsonResult> DepositVNDPost(BankDepositInfo model, IFormFile imgEvidence)
        {
            var userId = _currentUser?.Id ?? 0;
            int evidenceId = 0;
            if (imgEvidence != null && imgEvidence.Length > 0)
            {
                var evidenceEntity = new Evidence();
                using (var stream = new MemoryStream())
                {
                    await imgEvidence.CopyToAsync(stream);
                    evidenceEntity.Image = stream.ToArray();
                    evidenceEntity.FileName = imgEvidence.FileName;
                    evidenceEntity.ContentType = imgEvidence.ContentType;
                    evidenceEntity.CreatedDate = DateTime.Now;
                }

                var resEvidence = _transactionBiz.CreateDepositEvidence(evidenceEntity, out var outEvidenceId);
                if (resEvidence.HasError == false)
                    evidenceId = outEvidenceId;
            }

            if (model.SystemBank == 0)
            {
                TempData["error"] = CommonConst.BankidEmptyForWithdrawal;
                return Json(false);
            }

            var transactionWallet = new TransactionWallet
            {
                Amount = model.Amount,
                UserId = userId,
                CreatedDate = DateTime.Now,
                BankAccountId = model.SystemBank,
                OrderNo = model.OrderNo,
                TransactionType = (int)TransactionWalletType.Deposit
            };

            var result = _transactionWalletBiz.AddTransactionVND(transactionWallet);
            if (result.HasError)
            {
                TempData["error"] = result.Message;
                return Json(false);
            }

            TempData["success"] = CommonConst.TransactionWithDepositSuccess;
            return Json(true);
        }
    }
}