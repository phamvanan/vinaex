﻿using FastMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Narnia.Business;
using Narnia.Core;
using Narnia.Core.Domain;
using Narnia.Core.Models;
using Narnia.Web.Models;
using Narnia.Web.Models.Paging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Narnia.Web.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly ISellAdvertisementBiz _sellAdvertisement;
        private readonly IBuyAdvertisementBiz _buyAdvertisement;
        private readonly ISiteSetting _siteSetting;
        private readonly ICoinWalletBusiness _walletBusiness;
        private readonly IWorkContext _workContext;
        private readonly User _currentUser;
        private readonly string _currentAssetTypeId;
        private readonly string _currentCurrencyId;

        private int _userIdDefault = 0;

        public HomeController(ISellAdvertisementBiz sellAdvertisement,
                            IBuyAdvertisementBiz buyAdvertisement,
                            ISiteSetting siteSetting,
                            ICoinWalletBusiness walletBusiness,
                            IWorkContext workContext
            )
        {
            _sellAdvertisement = sellAdvertisement;
            _buyAdvertisement = buyAdvertisement;
            _siteSetting = siteSetting;
            _walletBusiness = walletBusiness;
            _workContext = workContext;
            _currentUser = _workContext.CurrentUser;
            _currentAssetTypeId = _workContext.WorkingSymbol.AssetIdBase;
            _currentCurrencyId = _workContext.WorkingSymbol.AssetIdQuote;
        }

        public IActionResult Index()
        {
            var coinSymbol = _walletBusiness.GetCoinSymbolByCode(_currentAssetTypeId);
            int userId = _currentUser?.Id ?? _userIdDefault;
            var model = new HomeViewModel()
            {
                UserId = userId,
                AssetTypeId = _currentAssetTypeId,
                CurrencyId = _currentCurrencyId,
                BuyPrice = string.Format("{0:n} {1}", coinSymbol.BuyPrice, _currentCurrencyId),
                SellPrice = string.Format("{0:n} {1}", coinSymbol.SellPrice, _currentCurrencyId),
                ReferralLink = HttpContext.Request.Host.Value + "/Account/Login?refer=" + _currentUser?.ReferenceCode,
                ReferralAmount = _siteSetting.ReferralAmount
            };

            return View(model);
        }

        [HttpPost]
        public PagedList<BuySellViewModel> LoadAdvertisment([FromBody]LoadAdvertismentRequest request)
        {
            if (string.IsNullOrEmpty(request.AssetTypeId))
                request.AssetTypeId = _currentAssetTypeId;

            int totalResult = 0, totalFilteredResult = 0;
            List<BuySellViewModel> advertisment = null;

            if (request.Type == 1)
            {
                advertisment = _sellAdvertisement.SearchSellAdvertisement(request.PageNo, request.PageSize, out totalResult, out totalFilteredResult
                    , true, request.ShowOfflineUser, request.AssetTypeId).ToList();
            }
            else
            {
                advertisment = _buyAdvertisement.SearchBuyAdvertisement(request.PageNo, request.PageSize, out totalResult, out totalFilteredResult
                    , true, request.ShowOfflineUser, request.AssetTypeId).ToList();
            }

            if (totalResult == -1)
                totalResult = request.TotalItems;

            return new PagedList<BuySellViewModel>(advertisment, request.PageNo - 1, request.PageSize, totalResult);
        }

        [Authorize]
        public ActionResult BuySellAdvertisement(string id)
        {
            var allSymbol = _walletBusiness.GetAllSymbols(true);
            var allSymbolView = TypeAdapter.Adapt<IList<CoinSymbol>, List<CoinSymbolViewModel>>(allSymbol);

            CoinSymbolViewModel currentSymbol = allSymbolView.FirstOrDefault(symbol => symbol.AssetIdBase == id);
            var allCurrency = _walletBusiness.GetAllCurrencies();
            var allCurrencyView = TypeAdapter.Adapt<IList<Currency>, List<CurrencyViewModel>>(allCurrency);
            var currentCurrency = allCurrencyView.FirstOrDefault(currency => currency.CurrencyCode == _currentCurrencyId);
            var allBanks = TypeAdapter.Adapt<IList<Banks>, List<BankViewModel>>(_walletBusiness.GetAllBanks());

            ViewBag.CurrencyId = _currentCurrencyId;
            ViewBag.CurrentSymbol = currentSymbol;
            ViewBag.AllCurrency = allCurrencyView;
            ViewBag.ListBanks = allBanks;

            double minCoinNumber = 0.0500, maxCoinNumber = 10.0000;

            switch (id)
            {
                case "BTC":
                    minCoinNumber = _siteSetting.MinNumberOfBTC;
                    maxCoinNumber = _siteSetting.MaxNumberOfBTC;
                    break;

                case "ETH":
                    minCoinNumber = _siteSetting.MinNumberOfETH;
                    maxCoinNumber = _siteSetting.MaxNumberOfETH;
                    break;

                case "BCH":
                    minCoinNumber = _siteSetting.MinNumberOfBCH;
                    maxCoinNumber = _siteSetting.MaxNumberOfBCH;
                    break;

                case "USDT":
                    minCoinNumber = _siteSetting.MinNumberOfUSDT;
                    maxCoinNumber = _siteSetting.MaxNumberOfUSDT;
                    break;
            }

            var model = new BuySellAdvertisementModel()
            {
                MinCoinNumber = minCoinNumber,
                MaxCoinNumber = maxCoinNumber,
                PaymentAllowTime = _siteSetting.PaymentWaitingTime,
                CoinPrice = currentCurrency?.Rate ?? 0
            };

            return View(model);
        }

        [Authorize]
        [HttpPost]
        public JsonResult AddBuyAdvertisement(BuySellAdvertisementModel advertise)
        {
            // response: true = success, false = Not success
            advertise.UserId = _currentUser?.Id ?? _userIdDefault;

            //if (_currentUser == null || !_currentUser.IsVerify)
            //    return new ResponseData<bool>().Error(CommonConst.UserIdInvalid);

            var responseOk = new ResponseData<bool>();
            if (advertise.IsSale)
            {
                var _adSale = TypeAdapter.Adapt<BuySellAdvertisementModel, SellAdvertisement>(advertise);
                responseOk = _sellAdvertisement.CreateSellAdvertisement(_adSale);
            }
            else
            {
                advertise.CreatedDate = advertise.UpdatedDate = DateTime.Now;
                var _adBuy = TypeAdapter.Adapt<BuySellAdvertisementModel, BuyAdvertisement>(advertise);

                responseOk = _buyAdvertisement.AddBuyAdvertisement(_adBuy);
            }

            if (responseOk.HasError)
            {
                //TempData["error"] = responseOk.Message;
                return Json(new { error = true, message = responseOk.Message });
            }

            TempData["success"] = CommonConst.CreateAdsSuccess;
            return Json(new { error = false });
        }

        public IActionResult About()
        {
            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Recruitment()
        {
            return View();
        }

        public IActionResult SecuredTransactions()
        {
            return View();
        }

        public IActionResult Media()
        {
            return View();
        }

        public IActionResult PolicyAndConditions()
        {
            return View();
        }

        public IActionResult FAQ()
        {
            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public IActionResult TestPayment()
        {
            return View();
        }
    }
}