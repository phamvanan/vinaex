﻿using Narnia.Core;
using Narnia.Core.Domain;
using Narnia.Core.Domain.Commissions;
using Narnia.Core.Enums;
using Narnia.Core.Models;
using Narnia.Data.Base;
using Narnia.Data.Repository.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Narnia.Business.Commissions
{
    public class CommissionService : ICommissionService
    {
        private readonly IDbContextScopeFactory _dbContextScopeFactory;
        private readonly IAggregateRepository _aggregateRepository;
        private readonly IUserRepository _userRepository;
        private readonly IAssetBalanceRepository _assetBalanceRepository;
        private readonly ICommissionRepository _commissionRepository;
        private readonly ISellAdvertisementRepository _sellAdvertisementRepository;
        private readonly IBuyAdvertisementRepository _buyAdvertisementRepository;
        private readonly ITransactionRepository _transactionRepository;
        private readonly ITransactionWalletRepository _transactionWalletRepository;

        public CommissionService(IDbContextScopeFactory dbContextScopeFactory, IAggregateRepository aggregateRepository, IUserRepository userRepository,
            IAssetBalanceRepository assetBalanceRepository, ICommissionRepository commissionRepository,
            ISellAdvertisementRepository sellAdvertisementRepository,
            IBuyAdvertisementRepository buyAdvertisementRepository
            , ITransactionRepository transactionRepository
            , ITransactionWalletRepository transactionWalletRepository)
        {
            _dbContextScopeFactory = dbContextScopeFactory;
            _aggregateRepository = aggregateRepository;
            _userRepository = userRepository;
            _assetBalanceRepository = assetBalanceRepository;
            this._commissionRepository = commissionRepository;
            this._sellAdvertisementRepository = sellAdvertisementRepository;
            this._buyAdvertisementRepository = buyAdvertisementRepository;
            this._transactionRepository = transactionRepository;
            this._transactionWalletRepository = transactionWalletRepository;
        }

        #region Commissions

        public IPagedList<Commission> SearchCommissions(DateTime? createdFromUtc = null, DateTime? createdToUtc = null,
          IList<int> commLevels = null, string beneficiaryUserName = "",
          int pageIndex = 0, int pageSize = int.MaxValue, bool countTotal = true)
        {
            if (commLevels != null && commLevels.Contains(0))
                commLevels = null;

            int totalResultsCount = 0, filteredResultsCount = 0;
            var commissions = _aggregateRepository.FindCommissions(pageIndex * pageSize, pageSize, out totalResultsCount, out filteredResultsCount, countTotal, createdFromUtc, createdToUtc,
                commLevels, beneficiaryUserName);
            return new PagedList<Commission>(commissions, pageIndex, pageSize, totalResultsCount);
        }

        #endregion Commissions

        #region Commission Levels

        public IList<CommissionLevel> GetAllCommissionLevels()
        {
            using (var scope = _dbContextScopeFactory.Create())
            {
                return scope.Repository<CommissionLevel>().GetAll().ToList();
            }
        }

        public CommissionLevel GetCommissionLevelById(int levelId)
        {
            using (var scope = _dbContextScopeFactory.Create())
            {
                return scope.Repository<CommissionLevel>().GetByObject(new { LevelId = levelId });
            }
        }

        public void UpdateCommissionLevel(CommissionLevel commissionLevel)
        {
            using (var scope = _dbContextScopeFactory.Create())
            {
                scope.Repository<CommissionLevel>().Update(commissionLevel);
            }
        }

        public void DeleteCommissionLevel(CommissionLevel commissionLevel)
        {
            using (var scope = _dbContextScopeFactory.Create())
            {
                scope.Repository<CommissionLevel>().Delete(new { LevelId = commissionLevel.LevelId });
            }
        }

        public void InsertCommissionLevel(CommissionLevel commissionLevel)
        {
            using (var scope = _dbContextScopeFactory.Create())
            {
                scope.Repository<CommissionLevel>().Insert(commissionLevel);
            }
        }

        /// <summary>
        /// Trừ amountFee vào tài khoản user
        /// </summary>
        /// <param name="scope"></param>
        /// <param name="userId"></param>
        /// <param name="assetTypeId"></param>
        /// <param name="amountCoin"></param>
        /// <param name="amountFee"></param>
        /// <param name="sellAdsId"></param>
        /// <param name="buyAdsId"></param>
        /// <param name="transactionId"></param>
        /// <returns></returns>
        public bool InsertCommissionForUplines(IDbContextScope scope, AssetBalance balanceUser, int userId, string assetTypeId, decimal amountCoin, decimal amountFee, int refId, RefType reftype)
        {
            //commission
            var allUplines = _userRepository.GetAllUplineUsers(userId);
            //var totalCommissionForUplines = (decimal)0;
            var amountFeeForUpline = amountFee;
            bool updateOk = false;

            balanceUser.Balance -= amountFee;
            balanceUser.UpdatedOn = DateTime.Now;
            updateOk = scope.Repository<AssetBalance>().Update(balanceUser);
            if (updateOk == false)
            {
                scope.Rollback();
                return false;
            }

            if (allUplines != null && allUplines.Count > 0)
            {
                var allCommissionLevels = scope.Repository<CommissionLevel>().GetAll().ToList();
                if (allCommissionLevels != null && allCommissionLevels.Count > 0)
                {
                    foreach (var uplineCurent in allUplines)
                    {
                        //get upline current
                        var commissLevelCurrent = allCommissionLevels.FirstOrDefault(x => x.LevelId == uplineCurent.Generation);
                        if (commissLevelCurrent != null)
                        {
                            //get commiss level current
                            var amountFeeCurrent = amountFeeForUpline * (decimal)commissLevelCurrent.CommRate;

                            //get userId child
                            var userIdCurrent = uplineCurent.Generation == 1
                                ? userId
                                : allUplines.FirstOrDefault(x => x.UplineId == uplineCurent.Id).Id;

                            //insert commission user level parent

                            updateOk = scope.Repository<Commission>().Insert(new Commission
                            {
                                RefId = refId,
                                RefType = (int)reftype,
                                BeneficiaryUserId = uplineCurent.Id,
                                CommLevel = commissLevelCurrent.LevelId,
                                CommRate = commissLevelCurrent.CommRate,
                                CommAmt = amountFeeCurrent,
                                UserId = userIdCurrent,
                                CreatedDate = DateTime.Now,
                                Status = (int)CommisionStatusEnum.NotPaid,
                                RefFromChildId = uplineCurent.RefFromChildId,
                                AssetTypeId=assetTypeId
                            });
                            if (updateOk == false)
                            {
                                scope.Rollback();
                                return false;
                            }

                            //update balance user level parent
                            //var balanceCurrent =
                            //    _assetBalanceRepository.GetByUserIdAssetType(uplineCurent.Id, assetTypeId);
                            //if (balanceCurrent == null)
                            //{
                            //    scope.Rollback();
                            //    return false;
                            //}
                            //balanceCurrent.Balance += amountFeeCurrent;
                            //balanceCurrent.UpdatedOn = DateTime.Now;
                            //updateOk = scope.Repository<AssetBalance>().Update(balanceCurrent);

                            //set amoutFee for upline next
                            amountFeeForUpline = amountFeeCurrent;
                            //totalCommissionForUplines += amountFeeCurrent;
                        }
                    }
                }
            }

            //Insert AdsFee
            var adsFeeEntity = new AdvertisementFee
            {
                AssetTypeId = assetTypeId,
                Status = (int)AdvertisementFeeStatus.Actived,
                UserId = userId,
                Amount = amountCoin,
                CreatedDate = DateTime.Now,
                Fee = amountFee,
                RefId = refId,
                RefType = (int)reftype,
                UpdatedDate = DateTime.Now,
            };
            updateOk = scope.Repository<AdvertisementFee>().Insert(adsFeeEntity);
            if (updateOk == false)
            {
                scope.Rollback();
                return false;
            }

            //add commission for admin
            var amountFeeAdmin = amountFee; /* - totalCommissionForUplines;*/
            var commissionEntity = new Commission
            {
                RefId = refId,
                RefType = (int)reftype,
                BeneficiaryUserId = CommonConst.UserIdAdmin,
                //CommLevel = commissLevelCurrent.LevelId,
                //CommRate = commissLevelCurrent.CommRate,
                CommAmt = amountFeeAdmin,
                UserId = userId,
                CreatedDate = DateTime.Now,
                Status = (int)CommisionStatusEnum.Paid,
                AssetTypeId=assetTypeId
            };
            updateOk = scope.Repository<Commission>().Insert(commissionEntity);
            if (updateOk == false)
            {
                scope.Rollback();
                return false;
            }

            //add balance admin
            var balanceAdmin = _assetBalanceRepository.GetByUserIdAssetType(CommonConst.UserIdAdmin, assetTypeId);
            if (balanceAdmin == null)
            {
                scope.Rollback();
                return false;
            }
            balanceAdmin.Balance += amountFeeAdmin;
            balanceAdmin.UpdatedOn = DateTime.Now;
            updateOk = scope.Repository<AssetBalance>().Update(balanceAdmin);
            if (updateOk == false)
            {
                scope.Rollback();
                return false;
            }
            return true;
        }

        #endregion Commission Levels

  
        public IPagedList<CommissionHistoryByBeneficiaryUserIdModel> GetCommissionHistoryByBeneficiaryUserId(int userId, int pageIndex, int pageSize)
        {
            var totalRecords = 0;
            var skip = pageIndex * pageSize;
            var take = pageSize;
            var data= _commissionRepository.GetCommissionHistoryByBeneficiaryUserId(userId, skip, take, out totalRecords);
            return new PagedList<CommissionHistoryByBeneficiaryUserIdModel>(data, pageIndex, pageSize, totalRecords);
        }
        
        public IPagedList<CommissionHistoryByModel> CommissionHistoryBy(string userName = "", DateTime? startDate = null, DateTime? endDate = null, IList<int> statuses = null, int pageIndex = 0, int pageSize = int.MaxValue)
        {
            var totalRecords = 0;
            var skip = pageIndex * pageSize;
            var take = pageSize;

            var status = (bool?)null;
            if(statuses != null && statuses.Count == 1)
            {
                status = statuses[0] == 1;
            }
            var data = _commissionRepository.CommissionHistoryBy(userName,startDate,endDate,status, skip, take, out totalRecords);
            return new PagedList<CommissionHistoryByModel>(data, pageIndex, pageSize, totalRecords);
        }
    }
}