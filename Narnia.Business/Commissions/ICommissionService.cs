﻿using Narnia.Core;
using Narnia.Core.Domain;
using Narnia.Core.Domain.Commissions;
using Narnia.Core.Enums;
using Narnia.Core.Models;
using Narnia.Data.Base;
using System;
using System.Collections.Generic;

namespace Narnia.Business.Commissions
{
    public interface ICommissionService
    {
        IList<CommissionLevel> GetAllCommissionLevels();

        CommissionLevel GetCommissionLevelById(int levelId);

        void UpdateCommissionLevel(CommissionLevel commissionLevel);

        void DeleteCommissionLevel(CommissionLevel commissionLevel);

        void InsertCommissionLevel(CommissionLevel commissionLevel);

        IPagedList<Commission> SearchCommissions(DateTime? createdFromUtc = null, DateTime? createdToUtc = null,
          IList<int> commLevels = null, string beneficiaryUserName = "",
          int pageIndex = 0, int pageSize = int.MaxValue, bool countTotal = true);

        bool InsertCommissionForUplines(IDbContextScope scope, AssetBalance balanceUser, int userId, string assetTypeId, decimal amountCoin, decimal amountFee, int refId, RefType reftype);
        IPagedList<CommissionHistoryByBeneficiaryUserIdModel> GetCommissionHistoryByBeneficiaryUserId(int userId, int pageIndex, int pageSize);
        IPagedList<CommissionHistoryByModel> CommissionHistoryBy(string userName="", DateTime? startDate = null, DateTime? endDate=null, IList<int> statuses=null, int pageIndex=0, int pageSize=int.MaxValue);
    }
}