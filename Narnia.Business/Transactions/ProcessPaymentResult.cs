﻿using Narnia.Core.Domain.Payments;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Narnia.Business.Transactions
{
    public partial class ProcessPaymentResult
    {
        public ProcessPaymentResult()
        {
            this.Errors = new List<string>();
        }

        public bool Success
        {
            get { return (!Errors.Any()); }
        }

        public void AddError(string error)
        {
            Errors.Add(error);
        }

        public IList<string> Errors { get; set; }

        public PaymentStatus PaymentStatus { get; set; } = PaymentStatus.Pending;
    }
}
