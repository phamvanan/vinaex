﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Business.Transactions
{
    public interface IPaymentMethodFactory
    {
        IPaymentMethod GetPaymentMethod(string gatewaySystemName);
        IPaymentMethod GetPaymentMethodsByAssetType(string assetType);
        IList<string> AvailablePaymentMethods { get;} 
    }
}
