﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Business.Transactions
{
    public class PostProcessPaymentRequest
    {
        public PostProcessPaymentRequest()
        {
            this.CustomValues = new Dictionary<string, object>();
        }
        public T GetCustomValue<T>(string key)
        {
            if (string.IsNullOrEmpty(key) || !this.CustomValues.ContainsKey(key))
                return default(T);
            return (T)this.CustomValues[key];
        }
        public Dictionary<string, object> CustomValues { get; set; }
    }
}
