﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Business.Transactions
{
    public enum PaymentMethodType
    {
        /// <summary>
        /// All payment information is entered on the site
        /// </summary>
        Standard = 1,
        /// <summary>
        /// A customer is redirected to a third-party site in order to complete the payment
        /// </summary>
        Redirection = 2,
        /// <summary>
        /// Button
        /// </summary>
        Button = 3,
    }
}
