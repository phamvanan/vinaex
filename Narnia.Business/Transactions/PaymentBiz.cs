﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Narnia.Core;
using Narnia.Core.Domain;
using Narnia.Core.Domain.Payments;
using Narnia.Data.Base;
using Narnia.Data.Base.Repositories;
using Narnia.Data.Repository.Contracts;


namespace Narnia.Business.Transactions
{
    public class PaymentBiz : IPaymentBiz
    {
        private readonly IPaymentMethodFactory _paymentMethodFactory;
        private readonly IAggregateRepository _aggregateRepository;
        private readonly IGenericRepository<GatewayAccount> _gatewayAccountRepository;
        private readonly IGenericRepository<PaymentGateway> _gatewayRepository;
        private readonly IGenericRepository<GatewayTransfer> _gatewayTransferRepository;
        private readonly IGenericRepository<BankTransfer> _bankTransferRepository;
        private readonly IDepositRepository _depositRepository;
        private readonly IDbContextScopeFactory _dbContextScopeFactory;
        public PaymentBiz(IPaymentMethodFactory paymentMethodFactory, IAggregateRepository aggregateRepository, IGenericRepository<GatewayAccount> gatewayAccountRepository, IGenericRepository<PaymentGateway> gatewayRepository,
            IGenericRepository<GatewayTransfer> gatewayTransferRepository, IDepositRepository depositRepository, IGenericRepository<BankTransfer> bankTransferRepository, IDbContextScopeFactory dbContextScopeFactory)
        {
            _paymentMethodFactory = paymentMethodFactory;
            _aggregateRepository = aggregateRepository;
            _gatewayAccountRepository = gatewayAccountRepository;
            _gatewayRepository = gatewayRepository;
            _gatewayTransferRepository = gatewayTransferRepository;
            _depositRepository = depositRepository;
            _bankTransferRepository = bankTransferRepository;
            _dbContextScopeFactory = dbContextScopeFactory;
        }
        public PaymentGateway GetGatewayById(int gatewayId)
        {
            return _gatewayRepository.GetById(gatewayId);
        }
        public bool AddGateway(PaymentGateway gateway)
        {
            return _gatewayRepository.Insert(gateway);
        }
        public bool UpdateGateway(PaymentGateway gateway)
        {
            return _gatewayRepository.Update(gateway);
        }
        public bool DeleteGateway(PaymentGateway gateway)
        {
            return _gatewayRepository.Delete(gateway);
        }
        public IList<PaymentGateway> GetAllGateways()
        {
            return _gatewayRepository.GetAll().ToList();
        }


        public IPaymentMethod GetPaymentMethodByAssetType(string assetType)
        {
            return _paymentMethodFactory.GetPaymentMethodsByAssetType(assetType);
        }

        public bool RemoveGateway(int gatewayId)
        {
            throw new NotImplementedException();
        }

        public GatewayAccount GetGatewayAccountById(int gatewayAccountId)
        {
            return _gatewayAccountRepository.GetById(gatewayAccountId);
        }
        public bool AddGatewayAccount(GatewayAccount gatewayAccount)
        {
            return _gatewayAccountRepository.Insert(gatewayAccount);
        }
        public bool UpdateGatewayAccount(GatewayAccount gatewayAccount)
        {
            return _gatewayAccountRepository.Update(gatewayAccount);
        }
        public bool DeleteGatewayAccount(GatewayAccount gatewayAccount)
        {
            return _gatewayAccountRepository.Delete(new { gatewayAccount.Id });
        }
        public IList<GatewayAccount> GetActiveSystemGatewayAccounts(int gatewayId)
        {
            return _aggregateRepository.GetActiveSystemGatewayAccounts(gatewayId);
        }
        public IPagedList<GatewayAccount> SearchGatewayAccounts(DateTime? createdFromUtc = null, DateTime? createdToUtc = null, string userName = "", bool isSystemAccount = false,
          int pageIndex = 0, int pageSize = int.MaxValue, bool countTotal = true)
        {
            int totalResultsCount = 0, filteredResultsCount = 0;
            var transactions = _aggregateRepository.FindGatewayAccounts(pageIndex * pageSize, pageSize, out totalResultsCount, out filteredResultsCount, countTotal, createdFromUtc, createdToUtc,
                userName,isSystemAccount);
            return new PagedList<GatewayAccount>(transactions, pageIndex, pageSize, totalResultsCount);
        }
        public Deposit DepositPre(int userId, decimal amount, decimal fee, string assetType, string description)
        {
            var deposit = new Deposit();
            deposit.UserId = userId;
            deposit.Amount = amount;
            deposit.Fee = fee;
            deposit.AssetTypeId = assetType;
            deposit.Description = description;
            deposit.Status = DepositStatus.Pending;
            deposit.CreatedDate = DateTime.Now;

            if (_depositRepository.Insert(deposit))
                return deposit;
            return null;
        }
        public GatewayTransfer GatewayDepositPre(int gatewayId, int depositId, string fromAccount, string toAccount, decimal amount, decimal fee, string assetType, string description)
        {
            var transfer = new GatewayTransfer();

            transfer.GatewayId = gatewayId;
            transfer.DepositId = depositId;
            transfer.GatewayAccFrom = fromAccount;
            transfer.GatewayAccTo = toAccount;
            transfer.Amount = amount;
            transfer.Fee = fee;
            transfer.AssetTypeId = assetType;
            transfer.Description = description;
            transfer.Status = GatewayTransferStatus.Pending;
            transfer.CreatedDate = DateTime.Now;

            if (_gatewayTransferRepository.Insert(transfer))
                return transfer;
            return null;
        }
        public ResponseData CancelGatewayDeposit(int gatewayDepositId)
        {
            using (var scope = _dbContextScopeFactory.Create())
            {
                try
                {
                    var gatewayTransfer = scope.Repository<GatewayTransfer>().GetById(gatewayDepositId);
                    if (gatewayTransfer == null)
                    {
                        return new ResponseData().Error("PaymentID not found!");
                    }
                    if(gatewayTransfer.Status != GatewayTransferStatus.Pending)
                    {
                        return new ResponseData().Error("Invalid status!");
                    }
                    gatewayTransfer.Status = GatewayTransferStatus.Rejected;
                    scope.Repository<GatewayTransfer>().Update(gatewayTransfer);
                    var deposit = scope.Repository<Deposit>().GetById(gatewayTransfer.DepositId);
                    deposit.Status = DepositStatus.Rejected;
                    scope.Repository<Deposit>().Update(deposit);
                }
                catch (Exception ex)
                {
                    scope.Rollback();
                }
            }
            return new ResponseData();
        }

        public ResponseData CompleteGatewayDeposit(int gatewayDepositId, int userId, string fromAccount)
        {
            using (var scope = _dbContextScopeFactory.Create())
            {
                try
                {
                    var gatewayTransfer = scope.Repository<GatewayTransfer>().GetById(gatewayDepositId);
                    if (gatewayTransfer == null)
                    {
                        return new ResponseData().Error("PaymentID not found!");
                    }
                    if (gatewayTransfer.Status != GatewayTransferStatus.Pending)
                    {
                        return new ResponseData().Error("Invalid status!");
                    }
                    gatewayTransfer.GatewayAccFrom = fromAccount;
                    gatewayTransfer.Status = GatewayTransferStatus.Success;
                    scope.Repository<GatewayTransfer>().Update(gatewayTransfer);

                    var deposit = scope.Repository<Deposit>().GetById(gatewayTransfer.DepositId);
                    deposit.Status = DepositStatus.Success;
                    scope.Repository<Deposit>().Update(deposit);
                    //Top up balance
                    var balance = scope.Repository<AssetBalance>().GetWhere(new { deposit.UserId, deposit.AssetTypeId }).FirstOrDefault();
                    if(balance != null)
                    {
                        balance.Balance = balance.Balance + deposit.Amount;
                        scope.Repository<AssetBalance>().Update(balance);
                    }
                    else
                    {
                        balance = new AssetBalance();
                        balance.Balance = deposit.Amount;
                        balance.UserId = userId;
                        balance.AssetTypeId = gatewayTransfer.AssetTypeId;
                        balance.CreatedOn = DateTime.Now;
                        balance.FrozenBalance = 0;
                        balance.UpdatedOn = balance.CreatedOn;

                        scope.Repository<AssetBalance>().Insert(balance);
                    }
                }
                catch (Exception ex)
                {
                    scope.Rollback();
                }
            }
            return new ResponseData();
        }

        public IPagedList<Deposit> SearchDeposits(DateTime? createdFromUtc = null, DateTime? createdToUtc = null, string userName = "", IList<string> assetTypeIds = null, IList<int> statuses = null,
          int pageIndex = 0, int pageSize = int.MaxValue, bool countTotal = true)
        {
            if (assetTypeIds != null && assetTypeIds.Contains("all"))
                assetTypeIds = null;

            if (statuses != null && statuses.Contains(0))
                statuses = null;


            int totalResultsCount = 0, filteredResultsCount = 0;
            var deposits = _depositRepository.SearchDeposits(pageIndex * pageSize, pageSize, out totalResultsCount, out filteredResultsCount, countTotal, createdFromUtc, createdToUtc,
                assetTypeIds, statuses, userName);
            return new PagedList<Deposit>(deposits, pageIndex, pageSize, totalResultsCount);
        }

        #region Bank Transfer
        public BankTransfer AddBankDepositInfo(int userId, int depositId, decimal amount, decimal fee, string assetType, string orderNo, string userBankName, string userAccountName, string userAccountNo,
          DateTime billingDate, int evidencePictureId, string comments)
        {
            var transfer = new BankTransfer();
            transfer.UserId = userId;
            transfer.DepositId = depositId;
            transfer.OrderNo = orderNo;
            transfer.BankName = userBankName;
            transfer.AccountName = userAccountName;
            transfer.AccountNumber = userAccountNo;
            transfer.Amount = amount;
            transfer.AssetTypeId = assetType;
            transfer.Comments = comments;
            transfer.Status = BankTransferStatus.Pending;
            transfer.BillingDate = billingDate;
            transfer.CreatedOn = DateTime.Now;
            transfer.UpdatedOn = DateTime.Now;
            transfer.EvidencePictureId = evidencePictureId;
            if (_bankTransferRepository.Insert(transfer))
                return transfer;
            return null;
        }
        public ResponseData CancelBankDeposit(int bankDepositId)
        {
            using (var scope = _dbContextScopeFactory.Create())
            {
                try
                {
                    var transfer = scope.Repository<BankTransfer>().GetById(bankDepositId);
                    if (transfer == null)
                    {
                        return new ResponseData().Error("PaymentID not found!");
                    }
                    if (transfer.Status != BankTransferStatus.Pending)
                    {
                        return new ResponseData().Error("Invalid status!");
                    }
                    transfer.Status = BankTransferStatus.Rejected;
                    scope.Repository<BankTransfer>().Update(transfer);
                    var deposit = scope.Repository<Deposit>().GetById(transfer.DepositId);
                    deposit.Status = DepositStatus.Rejected;
                    scope.Repository<Deposit>().Update(deposit);
                }
                catch (Exception ex)
                {
                    scope.Rollback();
                }
            }
            return new ResponseData();
        }

        public ResponseData CompleteBankDeposit(int bankDepositId)
        {
            using (var scope = _dbContextScopeFactory.Create())
            {
                try
                {
                    var transfer = scope.Repository<BankTransfer>().GetById(bankDepositId);
                    if (transfer == null)
                    {
                        return new ResponseData().Error("PaymentID not found!");
                    }
                    if (transfer.Status != BankTransferStatus.Pending)
                    {
                        return new ResponseData().Error("Invalid status!");
                    }
                    transfer.Status = BankTransferStatus.Success;
                    scope.Repository<BankTransfer>().Update(transfer);

                    var deposit = scope.Repository<Deposit>().GetById(transfer.DepositId);
                    deposit.Status = DepositStatus.Success;
                    scope.Repository<Deposit>().Update(deposit);
                    //Top up balance
                    var balance = scope.Repository<AssetBalance>().GetWhere(new { deposit.UserId, deposit.AssetTypeId }).FirstOrDefault();
                    if (balance != null)
                    {
                        balance.Balance = balance.Balance + deposit.Amount;
                        scope.Repository<AssetBalance>().Update(balance);
                    }
                }
                catch (Exception ex)
                {
                    scope.Rollback();
                }
            }
            return new ResponseData();
        }

        public IPagedList<BankTransfer> SearchBankTransfers(DateTime? createdFromUtc = null, DateTime? createdToUtc = null, string userName = "", DateTime? billingDate = null, IList<string> assetTypeIds = null, IList<int> statuses = null,
          int pageIndex = 0, int pageSize = int.MaxValue, bool countTotal = true)
        {
            if (assetTypeIds != null && assetTypeIds.Contains("all"))
                assetTypeIds = null;

            if (statuses != null && statuses.Contains(0))
                statuses = null;


            int totalResultsCount = 0, filteredResultsCount = 0;
            var transfers = _depositRepository.SearchBankTransfers(pageIndex * pageSize, pageSize, out totalResultsCount, out filteredResultsCount, countTotal, createdFromUtc, createdToUtc,
                assetTypeIds, statuses, userName,billingDate);
            return new PagedList<BankTransfer>(transfers, pageIndex, pageSize, totalResultsCount);
        }

        public BankTransfer GetBankTransferById(int bankTransferId)
        {
            return _bankTransferRepository.GetById(bankTransferId);
        }
        #endregion
    }
}
