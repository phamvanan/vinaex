﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Business.Transactions
{
    [Serializable]
    public partial class ProcessPaymentRequest
    {
        public ProcessPaymentRequest()
        {
            this.CustomValues = new Dictionary<string, object>();
        }

        public int UserId { get; set; }
        public string GatewaySystemName { get; set; }

        public decimal Amount { get; set; }
        public decimal Fee { get; set; }
        public string AssetTypeId { get; set; }
        public string Description { get; set; }
        
        public Dictionary<string, object> CustomValues { get; set; }
        public T GetCustomValue<T>(string key)
        {
            if (string.IsNullOrEmpty(key) || !this.CustomValues.ContainsKey(key))
                return default(T);
            return (T)this.CustomValues[key];
        }
    }
}
