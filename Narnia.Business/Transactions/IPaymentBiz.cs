﻿using Narnia.Core;
using Narnia.Core.Domain;
using Narnia.Core.Domain.Payments;
using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Business.Transactions
{
    public interface IPaymentBiz
    {
        PaymentGateway GetGatewayById(int gatewayId);
        bool AddGateway(PaymentGateway gateway);
        bool UpdateGateway(PaymentGateway gateway);
        bool DeleteGateway(PaymentGateway gateway);
        IPaymentMethod GetPaymentMethodByAssetType(string assetType);
        IList<PaymentGateway> GetAllGateways();


        GatewayAccount GetGatewayAccountById(int gatewayAccountId);
        bool AddGatewayAccount(GatewayAccount gatewayAccount);
        bool UpdateGatewayAccount(GatewayAccount gatewayAccount);
        bool DeleteGatewayAccount(GatewayAccount gatewayAccount);

        IList<GatewayAccount> GetActiveSystemGatewayAccounts(int gatewayId);
        IPagedList<GatewayAccount> SearchGatewayAccounts(DateTime? createdFromUtc = null, DateTime? createdToUtc = null, string userName = "", bool isSystemAccount = false,
          int pageIndex = 0, int pageSize = int.MaxValue, bool countTotal = true);

        
        Deposit DepositPre(int userId, decimal amount, decimal fee, string assetType, string description);
        GatewayTransfer GatewayDepositPre(int gatewayId,int depositId, string fromAccount, string toAccount, decimal amount, decimal fee, string assetType, string description);
        ResponseData CancelGatewayDeposit(int gatewayDepositId);
        ResponseData CompleteGatewayDeposit(int gatewayDepositId, int userId, string fromAccount);

        IPagedList<Deposit> SearchDeposits(DateTime? createdFromUtc = null, DateTime? createdToUtc = null, string userName = "", IList<string> assetTypeIds = null, IList<int> statuses = null, 
          int pageIndex = 0, int pageSize = int.MaxValue, bool countTotal = true);

        #region Bank Transfer
        BankTransfer AddBankDepositInfo(int userId, int depositId, decimal amount, decimal fee, string assetType, string orderNo, string userBankName, string userAccountName, string userAccountNo,
           DateTime billingDate, int evidencePictureId, string comments);
        ResponseData CancelBankDeposit(int bankDepositId);
        ResponseData CompleteBankDeposit(int bankDepositId);

        IPagedList<BankTransfer> SearchBankTransfers(DateTime? createdFromUtc = null, DateTime? createdToUtc = null, string userName = "", DateTime? billingDate = null, IList<string> assetTypeIds = null, IList<int> statuses = null,
          int pageIndex = 0, int pageSize = int.MaxValue, bool countTotal = true);

        BankTransfer GetBankTransferById(int bankTransferId);
        #endregion
    }
}
