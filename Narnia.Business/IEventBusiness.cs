﻿using Narnia.Core.Domain;

namespace Narnia.Business
{
    public interface IEventBusiness
    {
        int AddNewEvent(int drawId, char type, decimal amount);
    }
}
