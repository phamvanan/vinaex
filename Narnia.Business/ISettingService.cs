﻿using Narnia.Core;
using Narnia.Core.Domain;
using System.Collections.Generic;

namespace Narnia.Business
{
    public interface ISettingService
    {
        IList<Setting> LoadAllSettings();
        IPagedList<Setting> SearchSettings(string name = "", string value = "",
          int pageIndex = 0, int pageSize = int.MaxValue, bool countTotal = true);

        int AddNewSetting(Setting setting);
        bool Update(Setting setting);
        Setting GetSettingById(int id);
        string GetSettingByKey(string key, string defaultValue = "");
        bool Delete(int id);
    }
}
