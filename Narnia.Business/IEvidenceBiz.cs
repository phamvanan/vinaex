﻿using Narnia.Core.Domain;

namespace Narnia.Business
{
    public interface IEvidenceBiz
    {
        Evidence GetByCode(string code);

        Evidence GetById(int Id);
    }
}