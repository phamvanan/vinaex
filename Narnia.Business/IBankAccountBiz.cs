﻿using Narnia.Core;
using Narnia.Core.Domain;
using Narnia.Core.Models;
using System.Collections.Generic;

namespace Narnia.Business
{
    public interface IBankAccountBiz
    {
        List<BankAccount> GetAvailableSystemBankAccounts();
        List<BankAccountModel> GetListByUserId(int userId);
        ResponseData CreateBankAccount(BankAccount model);
        List<BankAccountModel> GetListAccountAdmin();
        List<BankAccountAdminPagingModel> GetBankAccountAdminPagings(int pageIndex, int pageSize, string orderBy, out int totalRecords);
        ResponseData CreateBankAccountAdmin(BankAccount model);
        ResponseData UpdateBankAccountAdmin(BankAccount model);
        BankAccount GetById(int id);
    }
}