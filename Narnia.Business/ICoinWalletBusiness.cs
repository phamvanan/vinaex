﻿using Narnia.Core;
using Narnia.Core.Domain;
using System;
using System.Collections.Generic;

namespace Narnia.Business
{
    public interface ICoinWalletBusiness
    {
        List<CoinSymbol> GetAllSymbols(bool? activeSymbolOnly);

        CoinSymbol GetCoinSymbolByCode(string coincode);

        List<Currency> GetAllCurrencies();

        Currency GetCurrencyByCode(string currencyCode);

        List<Banks> GetAllBanks();
        CoinWallet GetCurrentCoinWalletByUserId(int userId, string coinCode);

        PagedList<CoinWallet> FindWallets(DateTime? createdFromUtc = null, DateTime? createdToUtc = null, IList<string> assetTypeIds = null, string userName = "",int? userId = null,
            int pageIndex = 0, int pageSize = int.MaxValue, bool countTotal = true);

    }
}
