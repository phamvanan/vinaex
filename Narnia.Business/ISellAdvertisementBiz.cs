﻿using Narnia.Core.Domain;
using Narnia.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;
using Narnia.Core;

namespace Narnia.Business
{
    public interface ISellAdvertisementBiz
    {
        bool AddSellAdvertisement(SellAdvertisement sellAdvertisement);

        ResponseData<bool> CreateSellAdvertisement(SellAdvertisement sellInfo);

        IEnumerable<BuySellViewModel> SearchSellAdvertisement(int pageIndex, int pageSize, out int totalResultsCount, out int filteredResultsCount, bool countTotal = true
            , bool isShowOfflineUser = false, string assetTypeId = null);
        SellAdvertisement GetById(int SellAdtId);
        ResponseData<InfoAdvertisementForSellBuyModel> GetInfoSellAdvertisement(int SellAdId,int userId);
        bool UpdateSellAdvertisement(SellAdvertisement sellAdvertisement);

        IEnumerable<SellAdvertisement> LoadSellAdvertisement(int pageIndex, int pageSize, out int totalResultsCount, out int filteredResultsCount, bool countTotal = true, string sort = null, BuySellAdvertiseSearchCriteria criteria = null);
        SellAdvertisement LoadSellAdvertisementDetail(int buyId);

        IEnumerable<InfoQuickBuySellAdvertisement> LoadQuickSellAdvertisement(string assetTypeId, string currencyType, decimal coinNumber);
        ResponseData<IList<SellAdvertisement>> GetSellInforByUserId(int userId);
        IEnumerable<BuySellViewModel> GetSellAdvertisementByUserId(int pageIndex, int pageSize, int userId, out int totalResultsCount, bool countTotal = true, string assetTypeId = null);
        IPagedList<SellAdvertisement> SearchAdvertisments(DateTime? createdFromUtc = null, DateTime? createdToUtc = null,
            IList<int> statuses = null, IList<string> assetTypeIds = null, string userName = "", int pageIndex = 0, int pageSize = int.MaxValue, bool countTotal = true);
    }
}
