﻿using Narnia.Core;
using Narnia.Core.Domain;
using Narnia.Core.Models;
using System;
using System.Collections.Generic;

namespace Narnia.Business
{
    public interface IBuyAdvertisementBiz
    {
        ResponseData<bool> AddBuyAdvertisement(BuyAdvertisement buyAdvertisement);

        bool UpdateBuyAdvertisement(BuyAdvertisement buyAdvertisement);

        BuyAdvertisement GetBuyAdvertisementById(int buyId);

        IEnumerable<BuySellViewModel> SearchBuyAdvertisement(int pageIndex, int pageSize, out int totalResultsCount, out int filteredResultsCount, bool countTotal = true
            , bool isShowOfflineUser = false, string assetTypeId = null);

        ResponseData<InfoAdvertisementForSellBuyModel> GetInfoBuyAdvertisement(int buyAdId);

        IEnumerable<BuyAdvertisement> LoadBuyAdvertisement(int pageIndex, int pageSize, out int totalResultsCount, out int filteredResultsCount, bool countTotal = true, string sort = null, BuySellAdvertiseSearchCriteria criteria = null);

        BuyAdvertisement LoadBuyAdvertisementDetail(int buyId);

        IEnumerable<InfoQuickBuySellAdvertisement> LoadQuickBuyAdvertisement(string assetTypeId, string currencyType, decimal coinNumber);

        ResponseData<IList<BuyAdvertisement>> GetSellInforByUserId(int userId);

        IEnumerable<BuySellViewModel> GetBuyAdvertisementByUserId(int pageIndex, int pageSize, int userId, out int totalResultsCount, bool countTotal = true, string assetTypeId = null);
        IPagedList<BuyAdvertisement> SearchAdvertisments(DateTime? createdFromUtc = null, DateTime? createdToUtc = null,
            IList<int> statuses = null, IList<string> assetTypeIds = null, string userName = "", int pageIndex = 0, int pageSize = int.MaxValue, bool countTotal = true);
    }
}