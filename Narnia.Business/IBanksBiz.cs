﻿using Narnia.Core;
using Narnia.Core.Domain;
using Narnia.Core.Models;
using System.Collections.Generic;

namespace Narnia.Business
{
    public interface IBanksBiz
    {
        List<Banks> GetList();

        List<BankAdminPagingModel> GetBankAdminPagings(int pageIndex, int pageSize, string orderBy,
            out int totalRecords);

        ResponseData CreateBank(Banks model);
        ResponseData UpdateBank(Banks model);
        ResponseData DeleteBank(Banks model);
        Banks GetById(int Id);
    }
}