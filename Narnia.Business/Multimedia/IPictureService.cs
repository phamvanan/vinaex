﻿using Narnia.Core;
using Narnia.Core.Domain.Multimedia;
using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Business.Multimedia
{
    public partial interface IPictureService
    {
        byte[] LoadPictureBinary(Picture picture);

        string GetDefaultPictureUrl(int targetSize = 0,
            PictureType defaultPictureType = PictureType.Entity);

        string GetPictureUrl(int pictureId,
            int targetSize = 0,
            bool showDefaultPicture = true,
            PictureType defaultPictureType = PictureType.Entity);

        string GetPictureUrl(Picture picture,
            int targetSize = 0,
            bool showDefaultPicture = true,
            PictureType defaultPictureType = PictureType.Entity);

        string GetThumbLocalPath(Picture picture, int targetSize = 0, bool showDefaultPicture = true);

        
        Picture GetPictureById(int pictureId);

        void DeletePicture(Picture picture);

        IPagedList<Picture> GetPictures(int pageIndex = 0, int pageSize = int.MaxValue);

        Picture InsertPicture(byte[] pictureBinary, string mimeType,
            string altAttribute = null, string titleAttribute = null, bool validateBinary = true);

        Picture UpdatePicture(int pictureId, byte[] pictureBinary, string mimeType, string altAttribute = null, string titleAttribute = null, bool isNew = true, bool validateBinary = true);

        byte[] ValidatePicture(byte[] pictureBinary, string mimeType);

        Picture GetUserPicture(int userId);
    }
}
