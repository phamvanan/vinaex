﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Business.Multimedia
{
    public enum ResizeType
    {
        /// <summary>
        /// Longest side
        /// </summary>
        LongestSide,
        /// <summary>
        /// Width
        /// </summary>
        Width,
        /// <summary>
        /// Height
        /// </summary>
        Height
    }
}
