﻿using Microsoft.AspNetCore.Http;
using Narnia.Core;
using Narnia.Core.Domain;
using Narnia.Core.Enums;
using Narnia.Core.Models;
using System;
using System.Collections.Generic;

namespace Narnia.Business
{
    public interface ITransactionWalletBiz
    {
        decimal GetBalanceVNDByUserId(int userId);
        ResponseData AddTransactionVND(TransactionWallet entity);

        List<TransactionWalletPagingModel> GetTransactionWalletPaging(TransactionHistorySearchModel searchModel, out int totalRecords);

        ResponseData<TransactionWalletDetailVNDModel> GetDetailVNDByTrancodeUserId(int userId, string transcode);
        ResponseData UploadEvidenceDepositVND(Evidence evidence, string transcode, int userId);
        ResponseData CancelTransactionVND(string transcode, int userId);

        IPagedList<TransactionWalletAdminSearchModel> SearchTransactions(DateTime? createdFromUtc = null,
            DateTime? createdToUtc = null, string transactionCode = null,
            IList<int> statuses = null, IList<string> assetTypeIds = null, string userName = "", int pageIndex = 0,
            int pageSize = int.MaxValue);

        ResponseData<TransactionWallletDetailAdminModel> GetDetailAdmin(int transactionId);
        int[] GetTransactionStatusValid(int transactionStatus, int transactionWalletType,string assetTypeId);
        ResponseData AdminConfirm(TransactionWallet input, List<IFormFile> Image);
    }
}