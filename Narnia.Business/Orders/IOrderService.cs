﻿using Narnia.Core;
using Narnia.Core.Domain2;
using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Business
{
    public interface IOrderService
    {
        ResponseData<bool> CreateOffer(Offer offer);
    }
}
