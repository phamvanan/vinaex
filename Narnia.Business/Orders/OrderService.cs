﻿using Narnia.Core;
using Narnia.Core.Domain2;
using Narnia.Core.Infrastructure;
using Narnia.Data.Base;
using Narnia.Data.Base.Repositories;
using Narnia.Exchange.ClientProxy;
using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Business
{
    public class OrderService : IOrderService
    {
        private readonly IGenericRepository<Offer> _offerRepository;
        private readonly IDbContextScopeFactory _dbContextScopeFactory;

        public OrderService(IGenericRepository<Offer> offerRepository, IDbContextScopeFactory dbContextScopeFactory)
        {
            _offerRepository = offerRepository;
            _dbContextScopeFactory = dbContextScopeFactory;
        }
        public ResponseData<bool> CreateOffer(Offer offer)
        {
            bool bOk = _offerRepository.Insert(offer);
            if(bOk)
            {
                EngineContext.Current.Resolve<IClient>().ProcessOffer(offer.Id);

                return new ResponseData<bool>();
            }
            else
            {
                return new ResponseData<bool>() { HasError = true};
            }
        }
    }
}
