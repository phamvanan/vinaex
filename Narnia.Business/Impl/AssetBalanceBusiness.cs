﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Narnia.Core.Domain;
using Narnia.Data.Repository.Contracts;

namespace Narnia.Business.Impl
{
    public interface IAssetBalanceBusiness
    {
        AssetBalance GetByUserIdAssetType(int userID, string assetTypeID);
        IList<AssetBalance> GetAssetBalancesByUserId(int userId);
    }
    public class AssetBalanceBusiness : IAssetBalanceBusiness
    {
        private readonly IAssetBalanceRepository _assetBalanceRepository;
        public AssetBalanceBusiness(IAssetBalanceRepository assetBalanceRepository)
        {
            _assetBalanceRepository = assetBalanceRepository;
        }

        public AssetBalance GetByUserIdAssetType(int userID, string assetTypeID)
        {
            return _assetBalanceRepository.GetByUserIdAssetType(userID, assetTypeID);
        }

        public IList<AssetBalance> GetAssetBalancesByUserId(int userId)
        {
            return _assetBalanceRepository.GetWhere(new {UserId = userId}).ToList();
        }
    }
}
