﻿using Narnia.Core.Domain;
using Narnia.Data.Repository.Contracts;

namespace Narnia.Business.Impl
{
    public class EvidenceBiz : IEvidenceBiz
    {
        private readonly IEvidenceRepository _evidenceRepository;

        public EvidenceBiz(IEvidenceRepository evidenceRepository)
        {
            _evidenceRepository = evidenceRepository;
        }

        public Evidence GetByCode(string code)
        {
            return _evidenceRepository.GetbyCode(code);
        }

        public Evidence GetById(int Id)
        {
            return _evidenceRepository.GetbyId(Id);
        }
    }
}