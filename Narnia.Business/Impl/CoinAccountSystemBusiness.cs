﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Narnia.Core.Domain;
using Narnia.Data.Base.Repositories;

namespace Narnia.Business.Impl
{
    public interface ICoinAccountSystemBusiness
    {
        IList<CoinAccountSystem> GetAll();
        CoinAccountSystem GetByAssetTypeId(string assetTypeId);
    }

    public class CoinAccountSystemBusiness : ICoinAccountSystemBusiness
    {
        private readonly IGenericRepository<CoinAccountSystem> _genericRepository;
        public CoinAccountSystemBusiness(IGenericRepository<CoinAccountSystem> genericRepository)
        {
            _genericRepository = genericRepository;
        }

        public IList<CoinAccountSystem> GetAll()
        {
            return _genericRepository.GetAll().ToList();
        }

        public CoinAccountSystem GetByAssetTypeId(string assetTypeId)
        {
            if (string.IsNullOrEmpty(assetTypeId))
            {
                return null;
            }

            return _genericRepository.GetByObject(new {AssetTypeId = assetTypeId.Trim(), IsActive = true});
        }
    }
}
