﻿using Narnia.Business.CoinServices;
using Narnia.Business.Commissions;
using Narnia.Core;
using Narnia.Core.Domain;
using Narnia.Core.Enums;
using Narnia.Core.Models;
using Narnia.Data.Base;
using Narnia.Data.Repository.Contracts;
using System;

namespace Narnia.Business.Impl
{
    public interface IWithdrawBusiness
    {
        ResponseData Withdraw(WithdrawViewModel model);
    }

    public class WithdrawBusiness : IWithdrawBusiness
    {
        private readonly ISiteSetting _siteSetting;
        private readonly IEthService _ethService;
        private readonly IDbContextScopeFactory _dbContextScopeFactory;
        private readonly ICoinWalletRepository _coinWalletRepository;
        private readonly IAssetBalanceBusiness _assetBalanceBusiness;
        private readonly ICommissionService _commissionService;

        private decimal _feePercentageDefault = (decimal)0.5;

        public WithdrawBusiness(ISiteSetting siteSetting,
                                IEthService ethService,
                                IDbContextScopeFactory dbContextScopeFactory,
                                ICoinWalletRepository coinWalletRepository,
                                IAssetBalanceBusiness assetBalanceBusiness,
                                ICommissionService commissionService)
        {
            _siteSetting = siteSetting;
            _ethService = ethService;
            _dbContextScopeFactory = dbContextScopeFactory;
            _coinWalletRepository = coinWalletRepository;
            _assetBalanceBusiness = assetBalanceBusiness;
            _commissionService = commissionService;
        }

        public ResponseData Withdraw(WithdrawViewModel model)
        {
            if (string.IsNullOrEmpty(model.CoinWalletAddress))
            {
                return new ResponseData().Error(CommonConst.InvalidCoinWalletAddress);
            }

            if (model.WithdrawAmount == 0)
            {
                return new ResponseData().Error(CommonConst.InvalidWithdrawAmount);
            }

            if (model.UserId == 0)
            {
                return new ResponseData().Error(CommonConst.UserIdInvalid);
            }

            var assetBalance = _assetBalanceBusiness.GetByUserIdAssetType(model.UserId, model.CoinCode);
            if (assetBalance == null)
            {
                return new ResponseData().Error(CommonConst.NotFoundData);
            }

            //feePercent
            var feePercentage = _siteSetting.WithdrawalFee;
            if (feePercentage == 0)
            {
                feePercentage = _feePercentageDefault;
            }
            var amountFee = model.WithdrawAmount * feePercentage;
            if (model.WithdrawAmount > assetBalance.Balance - assetBalance.FrozenBalance - amountFee)
            {
                return new ResponseData().Error(CommonConst.NotEnoughBalance);
            }

            // withdraw eth
            var coinWallet = _coinWalletRepository.GetByUserIdType(model.UserId, model.CoinCode); //HungLN edit: _coinWalletBusiness.GetCurrentCoinWalletByUserId(model.UserId, model.CoinCode);
            if (coinWallet == null)
            {
                return new ResponseData().Error(CommonConst.NotFoundData);
            }
            var resultDraw = _ethService.TransferAndWaitForPolling(coinWallet.WalletAddress, coinWallet.WalletPassword, model.CoinWalletAddress, model.WithdrawAmount);

            if (resultDraw == null)
            {
                return new ResponseData().Error();
            }
            // create transactionwallet
            var transactionwallet = new TransactionWallet()
            {
                Amount = model.WithdrawAmount,
                AssetTypeId = assetBalance.AssetTypeId,
                ConfirmBy = model.UserId,
                ConfirmDate = DateTime.Now,
                Status = (int)TransactionStautus.Success,
                TransactionCode = $"WE{DateTime.Now.ToString("yyMMddHHmmssff")}",
                UserId = model.UserId,
                CreatedDate = DateTime.Now,
                UpdatedDate = DateTime.Now,
                TransactionType = (int)TransactionWalletType.Withdrawal
            };
            int newid = 0;
            bool updateOk = false;
            using (var scope = _dbContextScopeFactory.Create())
            {
                try
                {
                    scope.Repository<TransactionWallet>().Insert(transactionwallet, out newid);
                    if (newid == 0)
                    {
                        return new ResponseData().Error(CommonConst.HasErrorWhenAdminConfirmTransaction);
                    }

                    //commission

                    updateOk = _commissionService.InsertCommissionForUplines(scope, assetBalance, transactionwallet.UserId,
                        transactionwallet.AssetTypeId, transactionwallet.Amount, amountFee, newid, RefType.TransactionWallet);

                    if (updateOk == false)
                    {
                        scope.Rollback();
                        return new ResponseData().Error(CommonConst.HasErrorWhenAdminConfirmTransaction);
                    }

                    //update balance
                    assetBalance.Balance = assetBalance.Balance - transactionwallet.Amount - amountFee;
                    assetBalance.UpdatedOn = DateTime.Now;
                    updateOk = scope.Repository<AssetBalance>().Update(assetBalance);

                    if (updateOk == false)
                    {
                        scope.Rollback();
                        return new ResponseData().Error(CommonConst.HasErrorWhenAdminConfirmTransaction);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    scope.Rollback();
                    return new ResponseData().Error(CommonConst.HasErrorWhenAdminConfirmTransaction);
                }
            }

            return new ResponseData().Success();
        }
    }
}