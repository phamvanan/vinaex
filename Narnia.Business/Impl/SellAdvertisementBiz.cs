﻿using Narnia.Core;
using Narnia.Core.Domain;
using Narnia.Core.Enums;
using Narnia.Core.Models;
using Narnia.Data.Base;
using Narnia.Data.Repository.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using Narnia.Business.Commissions;
using Dapper;
using System.Data;

namespace Narnia.Business.Impl
{
    public class SellAdvertisementBiz : ISellAdvertisementBiz
    {
        private readonly ISellAdvertisementRepository _sellAdvertisement;
        private readonly IDbContextScopeFactory _dbContextScopeFactory;
        private readonly ISiteSetting _siteSetting;
        private readonly IAssetBalanceRepository _assetBalanceRepository;
        private readonly ICommissionService _commissionService;
        private readonly IUserRepository _userRepository;

        private decimal _feePercentageDefault = (decimal)0.5;

        public SellAdvertisementBiz(ISellAdvertisementRepository sellAdvertisement, IDbContextScopeFactory dbContextScopeFactory,
            ISiteSetting siteSetting, IAssetBalanceRepository assetBalanceRepository,
            IUserRepository userRepository,
            ICommissionRepository commissionRepository,
            ICommissionService commissionService)
        {
            _sellAdvertisement = sellAdvertisement;
            _dbContextScopeFactory = dbContextScopeFactory;
            _siteSetting = siteSetting;
            _assetBalanceRepository = assetBalanceRepository;
            _commissionService = commissionService;
            _userRepository = userRepository;
        }

        public bool AddSellAdvertisement(SellAdvertisement sellAdvertisement)
        {
            if (sellAdvertisement != null)
            {
                int newid = 0;
                using (var scope = _dbContextScopeFactory.Create())
                {
                    var result = scope.Repository<SellAdvertisement>().Insert(sellAdvertisement, out newid);
                }

                return newid > 0;
            }

            return false;
        }

        public bool UpdateSellAdvertisement(SellAdvertisement sellAdvertisement)
        {
            bool result = false;
            if (sellAdvertisement != null)
            {
                using (var scope = _dbContextScopeFactory.Create())
                {
                    result = scope.Repository<SellAdvertisement>().Update(sellAdvertisement);
                }
            }

            return result;
        }

        public ResponseData<bool> CreateSellAdvertisement(SellAdvertisement sellInfo)
        {
            //var repository = EngineContext.Current.Resolve<IAggregateRepository>();
            //return repository.CreateSellAdvertisement(sellInfo, feePercentage);

            if (sellInfo.UserId == 0 || sellInfo.MinCoinNumber < 0 || sellInfo.MaxCoinNumber <= 0 || string.IsNullOrEmpty(sellInfo.BankAccountNumber)|| string.IsNullOrEmpty(sellInfo.BankAccountName))
            {
                return new ResponseData<bool>().Error(CommonConst.InvalidInfo);
            }

            var feePercentage = _siteSetting.FeePercentage;
            if (feePercentage == 0)
            {
                feePercentage = _feePercentageDefault;
            }
            var amountFee = feePercentage * sellInfo.MaxCoinNumber;

            //check balance
            var balanceUser = _assetBalanceRepository.GetByUserIdAssetType(sellInfo.UserId, sellInfo.AssetTypeId);
            if (balanceUser == null ||
                balanceUser.Balance - (balanceUser.FrozenBalance + amountFee + sellInfo.MaxCoinNumber) < 0)
            {
                return new ResponseData<bool>().Error(CommonConst.NotEnoughBalanceForSell);
            }

            bool updateOk = false;
            int newId = 0;
            using (var scope = _dbContextScopeFactory.Create())
            {
                //insert sellAds
                var sellAdsEntity = new SellAdvertisement
                {
                    AssetTypeId = sellInfo.AssetTypeId,
                    Status = (int)AdvertisementStatus.New,
                    UserId = sellInfo.UserId,
                    AmountSell = 0,
                    Approved = false,
                    CurrencyId = sellInfo.CurrencyId,
                    BankId = sellInfo.BankId,
                    BankAccountName = sellInfo.BankAccountName,
                    BankAccountNumber = sellInfo.BankAccountNumber,
                    CoinExchangeId = sellInfo.CoinExchangeId,
                    CoinPrice = sellInfo.CoinPrice,
                    MinCoinPrice = sellInfo.MinCoinPrice,
                    MaxCoinNumber = sellInfo.MaxCoinNumber,
                    PaymentMethodId = sellInfo.PaymentMethodId,
                    PaymentAllowTime = sellInfo.PaymentAllowTime,
                    CountryId = sellInfo.CountryId,
                    CreatedDate = DateTime.Now,
                    UpdatedDate = DateTime.Now,
                    IsDeleted = false
                };

                updateOk = scope.Repository<SellAdvertisement>().Insert(sellAdsEntity, out newId);
                if (updateOk == false || newId == 0)
                {
                    return new ResponseData<bool>().Error(CommonConst.HasErrPleaseContactAdmin);
                }

                //frozenBalance
                balanceUser.FrozenBalance += sellAdsEntity.MaxCoinNumber;
                updateOk = scope.Repository<AssetBalance>().Update(balanceUser);
                if (updateOk == false)
                {
                    scope.Rollback();
                    return new ResponseData<bool>().Error(CommonConst.HasErrPleaseContactAdmin);
                }

                //commission
                updateOk = _commissionService.InsertCommissionForUplines(scope, balanceUser,sellAdsEntity.UserId,
                    sellAdsEntity.AssetTypeId, sellAdsEntity.MaxCoinNumber, amountFee, newId,RefType.SellAds);
                if (updateOk == false)
                {
                    return new ResponseData<bool>().Error(CommonConst.HasErrPleaseContactAdmin);
                }

                return new ResponseData<bool>().Success(true);
            }
        }

        public IEnumerable<BuySellViewModel> SearchSellAdvertisement(int pageIndex, int pageSize, out int totalResultsCount, out int filteredResultsCount, bool countTotal = true
            , bool isShowOfflineUser = false, string assetTypeId = null)
        {
            return _sellAdvertisement.SearchSellAdvertisement(pageIndex, pageSize, out totalResultsCount, out filteredResultsCount, countTotal, isShowOfflineUser, assetTypeId);
        }

        public SellAdvertisement GetById(int SellAdtId)
        {
            if (SellAdtId == 0)
                return null;

            return _sellAdvertisement.GetByObject(new { Id = SellAdtId });
        }

        public ResponseData<InfoAdvertisementForSellBuyModel> GetInfoSellAdvertisement(int SellAdId, int userId)
        {
            if (SellAdId == 0)
                throw new Exception("Invalid Data");

            var data = _sellAdvertisement.GetInfoSellAdvertisement(SellAdId, userId);
            return new ResponseData<InfoAdvertisementForSellBuyModel>().Success(data);
        }

        public IEnumerable<SellAdvertisement> LoadSellAdvertisement(int pageIndex, int pageSize, out int totalResultsCount, out int filteredResultsCount, bool countTotal = true, string sort = null, BuySellAdvertiseSearchCriteria criteria = null)
        {
            return _sellAdvertisement.LoadSellAdvertisement(pageIndex, pageSize, out totalResultsCount, out filteredResultsCount, countTotal, sort, criteria);
        }

        public SellAdvertisement LoadSellAdvertisementDetail(int SellAdId)
        {
            return _sellAdvertisement.LoadSellAdvertisementDetail(SellAdId);
        }

        public IEnumerable<InfoQuickBuySellAdvertisement> LoadQuickSellAdvertisement(string assetTypeId, string currencyType, decimal coinNumber)
        {
            return _sellAdvertisement.LoadQuickSellAdvertisement(assetTypeId, currencyType, coinNumber);
        }

        public ResponseData<IList<SellAdvertisement>> GetSellInforByUserId(int userId)
        {
            if (userId == 0)
            {
                return new ResponseData<IList<SellAdvertisement>>().Error();
            }

            var data = _sellAdvertisement.GetWhere(new { UserId = userId });

            return new ResponseData<IList<SellAdvertisement>>().Success(data?.ToList());
        }

        public IEnumerable<BuySellViewModel> GetSellAdvertisementByUserId(int pageIndex, int pageSize, int userId, out int totalResultsCount, bool countTotal = true, string assetTypeId = null)
        {
            return _sellAdvertisement.GetSellAdvertisementByUserId(pageIndex, pageSize, userId, out totalResultsCount, countTotal, assetTypeId);
        }

        public IPagedList<SellAdvertisement> SearchAdvertisments(DateTime? createdFromUtc = null, DateTime? createdToUtc = null, 
     IList<int> statuses = null, IList<string> assetTypeIds = null, string userName = "", int pageIndex = 0, int pageSize = int.MaxValue, bool countTotal = true)
        {
            if (statuses != null && statuses.Contains(0))
                statuses = null;
            if (assetTypeIds != null && (assetTypeIds.Contains("") || assetTypeIds.Contains(null)))
                assetTypeIds = null;

            int totalResultsCount = 0, filteredResultsCount = 0;
            var advertisements = _sellAdvertisement.SearchAdvertisments(pageIndex * pageSize, pageSize, out totalResultsCount, out filteredResultsCount, countTotal, createdFromUtc, createdToUtc,
                statuses, assetTypeIds, userName);
            return new PagedList<SellAdvertisement>(advertisements, pageIndex, pageSize, totalResultsCount);
        }

    }
}