﻿using Narnia.Core.Domain;
using Narnia.Data.Repository.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Business.Impl
{
    public interface IAdvertisementFeeBusiness
    {
        AdvertisementFee GetBySellAdtId(int sellAdtId);
        AdvertisementFee GetByBuyAdtId(int buyAdtId);
        AdvertisementFee GetByBuyTransactionId(int transactionId);

        AdvertisementFee GetAmountFreezeByUserIdCoinType(int userId, string assetTypeId);
    }

    public class AdvertisementFeeBusiness : IAdvertisementFeeBusiness
    {
        private readonly IAdvertisementFeeRepository _advertisementFeeRepository;
        public AdvertisementFeeBusiness(IAdvertisementFeeRepository advertisementFeeRepository)
        {
            _advertisementFeeRepository = advertisementFeeRepository;
        }

        public AdvertisementFee GetBySellAdtId(int sellAdtId)
        {
            if (sellAdtId <= 0) return null;

            return _advertisementFeeRepository.GetBySellAdtId(sellAdtId);
        }

        public AdvertisementFee GetByBuyAdtId(int buyAdtId)
        {
            if (buyAdtId <= 0) return null;

            return _advertisementFeeRepository.GetByBuyAdtId(buyAdtId);
        }

        public AdvertisementFee GetByBuyTransactionId(int transactionId)
        {
            if (transactionId <= 0) return null;

            return _advertisementFeeRepository.GetByBuyTransactionId(transactionId);
        }

        public AdvertisementFee GetAmountFreezeByUserIdCoinType(int userId, string assetTypeId)
        {
            return _advertisementFeeRepository.GetAmountFreezeByUserIdCoinType(userId, assetTypeId);
        }
    }
}
