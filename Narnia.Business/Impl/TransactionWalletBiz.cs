﻿using Microsoft.AspNetCore.Http;
using Narnia.Business.Commissions;
using Narnia.Core;
using Narnia.Core.Domain;
using Narnia.Core.Enums;
using Narnia.Core.Helpers;
using Narnia.Core.Models;
using Narnia.Data.Base;
using Narnia.Data.Repository.Contracts;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Narnia.Business.Impl
{
    public class TransactionWalletBiz : ITransactionWalletBiz
    {
        private readonly ITransactionWalletRepository _transactionWalletRepository;
        private readonly ICoinSymbolRepository _coinSymbolRepository;
        private readonly ICoinWalletRepository _coinWalletRepository;
        private readonly IAdvertisementFeeRepository _advertisementFeeRepository;
        private readonly IDbContextScopeFactory _dbContextScopeFactory;
        private readonly IAssetBalanceBusiness _assetBalanceBusiness;
        private readonly ISiteSetting _siteSetting;
        private readonly ICommissionService _commissionService;
        private readonly IUserRepository _userRepository;

        private string _assetTypeVND = Enum.GetName(typeof(AssetTypeEnum), AssetTypeEnum.VND);
        private decimal _feePercentageDefault = (decimal)0.5;

        public TransactionWalletBiz(ITransactionWalletRepository transactionWalletRepository,
            ICoinSymbolRepository coinSymbolRepository,
            ICoinWalletRepository coinWalletRepository,
            IAdvertisementFeeRepository advertisementFeeRepository,
            IDbContextScopeFactory dbContextScopeFactory,
            IAssetBalanceBusiness assetBalanceBusiness,
            ISiteSetting siteSetting,
            ICommissionService commissionService,
            IUserRepository userRepository)
        {
            _transactionWalletRepository = transactionWalletRepository;
            _coinSymbolRepository = coinSymbolRepository;
            _coinWalletRepository = coinWalletRepository;
            _advertisementFeeRepository = advertisementFeeRepository;
            _dbContextScopeFactory = dbContextScopeFactory;
            _assetBalanceBusiness = assetBalanceBusiness;
            _siteSetting = siteSetting;
            _commissionService = commissionService;
            _userRepository = userRepository;
        }

        public decimal GetBalanceVNDByUserId(int userId)
        {
            if (userId == 0) return 0;

            var assetBalance = _assetBalanceBusiness.GetByUserIdAssetType(userId, _assetTypeVND);
            if (assetBalance == null || assetBalance.Id == 0)
            {
                return 0;
            }

            var amountFreeze = _advertisementFeeRepository.GetAmountFreezeByUserIdCoinType(userId, _assetTypeVND);
            var balanceAmount = assetBalance.Balance - (amountFreeze?.Amount ?? 0) - assetBalance.FrozenBalance;

            return balanceAmount;
        }

        public ResponseData AddTransactionVND(TransactionWallet entity)
        {
            if (entity.UserId == 0 || entity.Amount == 0)
                return new ResponseData().Error("Giao dịch không hợp lệ");

            //var coinType = _coinSymbolRepository.GetbyAssetIdBase(CoinTypeEnum.VND.ToString());
            //if (coinType == null) return new ResponseData().Error("Giao dịch không hợp lệ");
            bool isTransactionWithDrawal = false;
            string prefixTransactionCode = "DV";
            if (entity.TransactionType == (int)TransactionWalletType.Withdrawal)
            {
                isTransactionWithDrawal = true;
                prefixTransactionCode = "WV";
            }

            if (isTransactionWithDrawal)
            {
                var balanceAmount = GetBalanceVNDByUserId(entity.UserId);
                if (entity.Amount - balanceAmount > 0) return new ResponseData().Error("Không đủ tiền để thực hiện giao dịch rút");
            }

            entity.Status = (int)TransactionStautus.Pending;
            entity.AssetTypeId = _assetTypeVND;
            entity.TransactionCode = $"{prefixTransactionCode}{DateTime.Now.ToString("yyMMddHHmmssff")}";
            entity.UpdatedDate = (DateTime?)null;
            entity.ConfirmDate = (DateTime?)null;
            int newid = 0;
            var updateOk = false;
            using (var scope = _dbContextScopeFactory.Create())
            {
                updateOk = scope.Repository<TransactionWallet>().Insert(entity, out newid);
                if (updateOk == false || newid == 0) return new ResponseData().Error("Giao dịch thất bại");

                if (isTransactionWithDrawal)
                {
                    //feePercent
                    var feePercentage = _siteSetting.WithdrawalFee;
                    if (feePercentage == 0)
                    {
                        feePercentage = _feePercentageDefault;
                    }
                    var amountFee = entity.Amount * feePercentage;

                    var assetBalanceVND = _assetBalanceBusiness.GetByUserIdAssetType(entity.UserId, _assetTypeVND);
                    if (assetBalanceVND == null || assetBalanceVND.Id == 0)
                    {
                        scope.Rollback();
                        return new ResponseData().Error("Giao dịch thất bại");
                    }

                    if (assetBalanceVND.Balance - assetBalanceVND.FrozenBalance - amountFee < 0)
                    {
                        scope.Rollback();
                        return new ResponseData().Error(CommonConst.NotEnoughMoneyForWithdrawal);
                    }

                    assetBalanceVND.FrozenBalance += entity.Amount;
                    assetBalanceVND.UpdatedOn = DateTime.Now;
                    updateOk = scope.Repository<AssetBalance>().Update(assetBalanceVND);
                    if (updateOk == false)
                    {
                        scope.Rollback();
                        return new ResponseData().Error("Giao dịch thất bại");
                    }
                    //set commission withdraw
                    updateOk = _commissionService.InsertCommissionForUplines(scope, assetBalanceVND, entity.UserId,
                        entity.AssetTypeId, entity.Amount, amountFee, newid, RefType.TransactionWallet);

                    if (updateOk == false)
                    {
                        scope.Rollback();
                        return new ResponseData().Error("Giao dịch thất bại");
                    }
                }
                return new ResponseData().Success();
            }
        }

        public List<TransactionWalletPagingModel> GetTransactionWalletPaging(TransactionHistorySearchModel searchModel, out int totalRecords)
        {
            var result = _transactionWalletRepository.GetTransactionWalletPaging(searchModel, out totalRecords);
            return result;
        }

        public ResponseData<TransactionWalletDetailVNDModel> GetDetailVNDByTrancodeUserId(int userId, string transcode)
        {
            var data = _transactionWalletRepository.GetDetailVNDByTrancodeUserId(transcode);
            if (data == null || data.UserId != userId)
            {
                return new ResponseData<TransactionWalletDetailVNDModel>().Error(CommonConst.NotFoundData);
            }
            return new ResponseData<TransactionWalletDetailVNDModel>().Success(data);
        }

        public ResponseData UploadEvidenceDepositVND(Evidence evidence, string transcode, int userId)
        {
            if (string.IsNullOrEmpty(transcode) || userId <= 0 || evidence == null)
            {
                return new ResponseData().Error(CommonConst.HasErrorWhenAddNewEvidenceTransaction);
            }

            using (var scope = _dbContextScopeFactory.Create())
            {
                var updateOk = false;
                var transactionWalletEntity = _transactionWalletRepository.GetByTranscode(transcode);
                if (transactionWalletEntity == null || transactionWalletEntity.UserId != userId)
                {
                    return new ResponseData().Error(CommonConst.NotFoundData);
                }

                if (transactionWalletEntity.Status != (int)TransactionStautus.Pending ||
                    transactionWalletEntity.TransactionType != (int)TransactionWalletType.Deposit)
                {
                    return new ResponseData().Error(CommonConst.NotPermissionToUpdateEvidence);
                }
                evidence.Code = Guid.NewGuid().ToString("N");
                updateOk = scope.Repository<Evidence>().Insert(evidence, out int evidenceNewId);

                if (updateOk == false || evidenceNewId <= 0)
                {
                    return new ResponseData().Error(CommonConst.HasErrorWhenAddNewEvidenceTransaction);
                }
                transactionWalletEntity.EvidenceId = evidenceNewId;
                transactionWalletEntity.UpdatedDate = DateTime.Now;
                transactionWalletEntity.EvidenceDate = DateTime.Now;
                transactionWalletEntity.Status = (int)TransactionStautus.HasEvidence;

                updateOk = scope.Repository<TransactionWallet>().Update(transactionWalletEntity);
                if (updateOk == false)
                {
                    scope.Rollback();
                    return new ResponseData().Error(CommonConst.HasErrorWhenAddNewEvidenceTransaction);
                }

                ////SendEmail
                //var userBuy = _userRepository.GetById(transactionEntity.UserId);
                //Task.Run(() =>
                //{
                //    EngineContext.Current.Resolve<EmailJob>()
                //        .SendTransaction(userBuy, transactionEntity, SendEmailType.TransactionUploadEvidence);
                //});

                return new ResponseData().Success();
            }
        }

        private ResponseData UploadEvidenceWithdrawVND(IDbContextScope scope, Evidence evidence, TransactionWallet transactionWalletEntity)
        {
            var updateOk = false;
            if (transactionWalletEntity == null)
            {
                return new ResponseData().Error(CommonConst.NotFoundData);
            }

            if (transactionWalletEntity.Status != (int)TransactionStautus.Pending ||
                transactionWalletEntity.TransactionType != (int)TransactionWalletType.Withdrawal)
            {
                return new ResponseData().Error(CommonConst.NotPermissionToUpdateEvidence);
            }
            evidence.Code = Guid.NewGuid().ToString("N");
            updateOk = scope.Repository<Evidence>().Insert(evidence, out int evidenceNewId);

            if (updateOk == false || evidenceNewId <= 0)
            {
                return new ResponseData().Error(CommonConst.HasErrorWhenAddNewEvidenceTransaction);
            }
            transactionWalletEntity.EvidenceId = evidenceNewId;
            transactionWalletEntity.UpdatedDate = DateTime.Now;
            transactionWalletEntity.EvidenceDate = DateTime.Now;
            transactionWalletEntity.Status = (int)TransactionStautus.Success;
            transactionWalletEntity.ConfirmDate = DateTime.Now;

            updateOk = scope.Repository<TransactionWallet>().Update(transactionWalletEntity);
            if (updateOk == false)
            {
                scope.Rollback();
                return new ResponseData().Error(CommonConst.HasErrorWhenAddNewEvidenceTransaction);
            }

            //update FrozenBalance user
            var assetBalanceVND =
                    _assetBalanceBusiness.GetByUserIdAssetType(transactionWalletEntity.UserId, _assetTypeVND);
            if (assetBalanceVND == null || assetBalanceVND.Id == 0)
            {
                scope.Rollback();
                return new ResponseData().Error(CommonConst.HasErrWhenCancelTransaction);
            }
            assetBalanceVND.FrozenBalance -= transactionWalletEntity.Amount;
            assetBalanceVND.Balance -= transactionWalletEntity.Amount;
            if (assetBalanceVND.FrozenBalance < 0 || assetBalanceVND.Balance < 0)
            {
                scope.Rollback();
                return new ResponseData().Error(CommonConst.HasErrWhenCancelTransaction);
            }

            assetBalanceVND.UpdatedOn = DateTime.Now;
            updateOk = scope.Repository<AssetBalance>().Update(assetBalanceVND);
            if (updateOk == false)
            {
                scope.Rollback();
                return new ResponseData().Error(CommonConst.HasErrWhenCancelTransaction);
            }

            //Trừ tiền VND trong tài khoản admin
            var assetBalanceAdminVND =
                _assetBalanceBusiness.GetByUserIdAssetType(CommonConst.UserIdAdmin, _assetTypeVND);
            if (assetBalanceAdminVND == null || assetBalanceAdminVND.Id == 0)
            {
                scope.Rollback();
                return new ResponseData().Error(CommonConst.HasErrWhenCancelTransaction);
            }
            assetBalanceAdminVND.Balance = -transactionWalletEntity.Amount;
            assetBalanceAdminVND.UpdatedOn = DateTime.Now;
            updateOk = scope.Repository<AssetBalance>().Update(assetBalanceVND);
            if (updateOk == false)
            {
                scope.Rollback();
                return new ResponseData().Error(CommonConst.HasErrWhenCancelTransaction);
            }

            ////SendEmail
            //var userBuy = _userRepository.GetById(transactionEntity.UserId);
            //Task.Run(() =>
            //{
            //    EngineContext.Current.Resolve<EmailJob>()
            //        .SendTransaction(userBuy, transactionEntity, SendEmailType.TransactionUploadEvidence);
            //});

            return new ResponseData().Success();
        }

        public ResponseData CancelTransactionVND(string transcode, int userId)
        {
            if (string.IsNullOrEmpty(transcode) || userId <= 0)
            {
                return new ResponseData().Error(CommonConst.HasErrWhenCancelTransaction);
            }
            using (var scope = _dbContextScopeFactory.Create())
            {
                var updateOk = false;
                var transactionWalletEntity = _transactionWalletRepository.GetByTranscode(transcode);
                if (transactionWalletEntity == null || transactionWalletEntity.UserId != userId)
                {
                    return new ResponseData().Error(CommonConst.NotFoundData);
                }

                if ((transactionWalletEntity.TransactionType == (int)TransactionWalletType.Withdrawal &&
                     transactionWalletEntity.Status != (int)TransactionStautus.Pending)
                    || (transactionWalletEntity.TransactionType == (int)TransactionWalletType.Deposit &&
                        transactionWalletEntity.Status != (int)TransactionStautus.Pending &&
                         transactionWalletEntity.Status != (int)TransactionStautus.HasEvidence))
                {
                    return new ResponseData().Error(CommonConst.NotPermissionToCancelTranaction);
                }

                transactionWalletEntity.Status = (int)TransactionStautus.Cancel;
                transactionWalletEntity.UpdatedDate = DateTime.Now;
                updateOk = scope.Repository<TransactionWallet>().Update(transactionWalletEntity);

                if (updateOk == false)
                {
                    return new ResponseData().Error(CommonConst.HasErrWhenCancelTransaction);
                }
                if (transactionWalletEntity.TransactionType == (int)TransactionWalletType.Withdrawal)
                {
                    var assetBalanceVND =
                        _assetBalanceBusiness.GetByUserIdAssetType(transactionWalletEntity.UserId, _assetTypeVND);
                    if (assetBalanceVND == null || assetBalanceVND.Id == 0)
                    {
                        scope.Rollback();
                        return new ResponseData().Error(CommonConst.HasErrWhenCancelTransaction);
                    }
                    assetBalanceVND.FrozenBalance -= transactionWalletEntity.Amount;
                    if (assetBalanceVND.FrozenBalance < 0)
                    {
                        scope.Rollback();
                        return new ResponseData().Error(CommonConst.HasErrWhenCancelTransaction);
                    }

                    assetBalanceVND.UpdatedOn = DateTime.Now;
                    updateOk = scope.Repository<AssetBalance>().Update(assetBalanceVND);
                    if (updateOk == false)
                    {
                        scope.Rollback();
                        return new ResponseData().Error(CommonConst.HasErrWhenCancelTransaction);
                    }

                    //var ads = _advertisementFeeRepository.GetByUserIdAssetTypeTranId(transactionWalletEntity.UserId,
                    //    transactionWalletEntity.AssetTypeId, transactionWalletEntity.TransactionId);
                    //if (ads != null)
                    //{
                    //    ads.Status = (int)AdvertisementFeeStatus.Close;
                    //    ads.UpdatedDate = DateTime.Now;
                    //    updateOk = scope.Repository<AdvertisementFee>().Update(ads);
                    //    if (updateOk == false)
                    //    {
                    //        scope.Rollback();
                    //        return new ResponseData().Error(CommonConst.HasErrWhenCancelTransaction);
                    //    }
                    //}
                }
                return new ResponseData().Success();
            }
        }

        public ResponseData<TransactionWallletDetailAdminModel> GetDetailAdmin(int transactionId)
        {
            var result = _transactionWalletRepository.GetDetailAdmin(transactionId);
            if (result == null || result.TransactionId == 0)
            {
                return new ResponseData<TransactionWallletDetailAdminModel>().Error();
            }
            return new ResponseData<TransactionWallletDetailAdminModel>().Success(result);
        }

        private ResponseData ConfirmTransactoinDepositVND(IDbContextScope scope, TransactionWallet transactionEntity)
        {
            transactionEntity.Status = (int)TransactionStautus.Success;
            var updateOK = scope.Repository<TransactionWallet>().Update(transactionEntity);
            if (updateOK == false)
            {
                return new ResponseData().Error(CommonConst.HasErrWhenConfirmTransaction);
            }

            //update balance
            var assetBalanceVND =
                _assetBalanceBusiness.GetByUserIdAssetType(transactionEntity.UserId, _assetTypeVND);
            var assetBalanceAdminVND =
                _assetBalanceBusiness.GetByUserIdAssetType(CommonConst.UserIdAdmin, _assetTypeVND);
            if (assetBalanceVND == null || assetBalanceVND.Id == 0 || assetBalanceAdminVND == null ||
                assetBalanceAdminVND.Id == 0)
            {
                scope.Rollback();
                return new ResponseData().Error(CommonConst.HasErrWhenConfirmTransaction);
            }

            //user
            assetBalanceVND.Balance += transactionEntity.Amount;
            assetBalanceVND.UpdatedOn = DateTime.Now;
            updateOK = scope.Repository<AssetBalance>().Update(assetBalanceVND);

            if (updateOK == false)
            {
                scope.Rollback();
                return new ResponseData().Error(CommonConst.HasErrWhenConfirmTransaction);
            }

            //admin
            assetBalanceAdminVND.Balance += transactionEntity.Amount;
            assetBalanceAdminVND.UpdatedOn = DateTime.Now;
            updateOK = scope.Repository<AssetBalance>().Update(assetBalanceAdminVND);

            if (updateOK == false)
            {
                scope.Rollback();
                return new ResponseData().Error(CommonConst.HasErrWhenConfirmTransaction);
            }

            return new ResponseData().Success();
        }

        public IPagedList<TransactionWalletAdminSearchModel> SearchTransactions(DateTime? createdFromUtc = null, DateTime? createdToUtc = null, string transactionCode = null,
            IList<int> statuses = null, IList<string> assetTypeIds = null, string userName = "", int pageIndex = 0, int pageSize = int.MaxValue)
        {
            if (statuses != null && statuses.Contains(0))
                statuses = null;
            if (assetTypeIds != null && (assetTypeIds.Contains("") || assetTypeIds.Contains(null)))
                assetTypeIds = null;

            int totalResultsCount = 0;
            var transactions = _transactionWalletRepository.SearchTransactions(pageIndex * pageSize, pageSize, out totalResultsCount, createdFromUtc, createdToUtc,
                transactionCode, statuses, assetTypeIds, userName);
            return new PagedList<TransactionWalletAdminSearchModel>(transactions, pageIndex, pageSize, totalResultsCount);
        }

        public int[] GetTransactionStatusValid(int transactionStatus, int transactionWalletType, string assetTypeId)
        {
            var result = new int[Enum.GetNames(typeof(TransactionStautus)).Length];
            if (assetTypeId == _assetTypeVND)
            {
                switch (transactionStatus)
                {
                    case (int)TransactionStautus.Pending:
                        if (transactionWalletType == (int)TransactionWalletType.Deposit)
                        {
                            result[0] = (int)TransactionStautus.Reject;
                        }
                        else
                        {
                            result[0] = (int)TransactionStautus.Success;
                        }
                        break;

                    case (int)TransactionStautus.HasEvidence:
                        if (transactionWalletType == (int)TransactionWalletType.Deposit)
                        {
                            result[0] = (int)TransactionStautus.Reject;
                            result[1] = (int)TransactionStautus.Success;
                        }
                        break;
                }
            }
            result = result.Where(x => x != 0).ToArray();
            return result;
        }

        public ResponseData AdminConfirm(TransactionWallet entityInput, List<IFormFile> Image)
        {
            var confirmBy = entityInput.ConfirmBy ?? 0;
            if (entityInput == null || confirmBy == 0 || entityInput.TransactionId == 0)
            {
                return new ResponseData().Error(CommonConst.HasErrorWhenSellerConfirmTransaction);
            }
            var transactionWalletEntity = _transactionWalletRepository.GetByTranId(entityInput.TransactionId);
            if (transactionWalletEntity == null)
            {
                return new ResponseData().Error(CommonConst.HasErrorWhenSellerConfirmTransaction);
            }
            transactionWalletEntity.ConfirmBy = entityInput.ConfirmBy;
            transactionWalletEntity.ConfirmDate = DateTime.Now;

            if (transactionWalletEntity.AssetTypeId != _assetTypeVND)
            {
                //ETH
                return new ResponseData();
            }
            var statusValid = GetTransactionStatusValid(transactionWalletEntity.Status, transactionWalletEntity.TransactionType, transactionWalletEntity.AssetTypeId);
            if (!statusValid.Contains(entityInput.Status))
            {
                return new ResponseData().Error(CommonConst.HasErrorWhenSellerConfirmTransaction);
            }

            var updateOk = false;
            //truong hop: yeu cau rut tien -> success

            using (var scope = _dbContextScopeFactory.Create())
            {
                switch (transactionWalletEntity.TransactionType)
                {
                    case (int)TransactionType.Withdraw:
                        if (entityInput.Status == (int)TransactionStautus.Success && Image != null && Image.Count > 0 &&
                            Image[0].Length > 0)
                        {
                            var evidenceEntity = new Evidence();
                            using (var stream = new MemoryStream())
                            {
                                Image[0].CopyTo(stream);
                                evidenceEntity.Image = stream.ToArray();
                                evidenceEntity.FileName = Image[0].FileName;
                                evidenceEntity.ContentType = Image[0].ContentType;
                                evidenceEntity.CreatedDate = DateTime.Now;
                            }

                            var isImage = UploadHelper.IsImage(evidenceEntity.ContentType, evidenceEntity.FileName);
                            if (!isImage)
                            {
                                return new ResponseData().Error(CommonConst.HasErrorWhenSellerConfirmTransaction);
                            }
                            var result = UploadEvidenceWithdrawVND(scope, evidenceEntity, transactionWalletEntity);
                            return result;
                        }
                        break;

                    case (int)TransactionType.Deposit:
                        if (entityInput.Status == (int)TransactionStautus.Reject)
                        {
                            transactionWalletEntity.Status = (int)TransactionStautus.Reject;

                            updateOk = scope.Repository<TransactionWallet>().Update(transactionWalletEntity);
                            if (updateOk == false)
                            {
                                return new ResponseData().Error(CommonConst.HasErrorWhenSellerConfirmTransaction);
                            }
                            return new ResponseData().Success();
                        }
                        if (entityInput.Status == (int)TransactionStautus.Success)
                        {
                            var result = ConfirmTransactoinDepositVND(scope, transactionWalletEntity);
                            return result;
                        }
                        break;
                }
            }
            return new ResponseData().Success();
        }
    }
}