﻿using Microsoft.AspNetCore.Http;
using Narnia.Core;
using Narnia.Core.Domain;
using Narnia.Core.Helpers;
using Narnia.Core.Infrastructure;
using Narnia.Data.Base;
using Narnia.Data.Base.Repositories;
using System;
using System.Collections.Generic;
using Narnia.Core.Models;
using Narnia.Data.Repository.Contracts;
using System.Linq;
using Narnia.Business.CoinServices;

namespace Narnia.Business.Impl
{
    public interface IUserBusiness : IBaseBusiness
    {
        ResponseData<User> AddNewUser(User user);
        ResponseData<User> Login(string userName);
        ResponseData<User> Activate(string active);
        IEnumerable<User> GetAll();
        User GetUserById(int id);
        List<User> GetUplineUsers(int userId);
        UserInforModel GetUserInfor(int userId);
        IList<UserInforModel> FindUsers(SearchUserModel searchUser, out int totalRecords);
        bool UpdateVerify(int userId);
        IList<UserLoginHistory> GetHistoryLoginByUserId(int userId);
        ResponseData<UserInforModel> GetUserInforByUserName(string userName);
        ResponseData<UserInfor> UpdateImage(UserInfor userInfor);
        ResponseData UpdateAvatarIdForUser(int avatarId, int userId);
        User GetUplineUser(int userId);
        LinkedList<User> GetAllUplineUsers(int userId);
        IPagedList<User> FindUsers(DateTime? createdFromUtc = null, DateTime? createdToUtc = null, string userName = "",
           string phoneNumber = "", int pageIndex = 0, int pageSize = int.MaxValue, bool countTotal = true);

        ResponseData<User> LoginAdmin(string userName, string passWord);
        ResponseData<User> ChangePassword(User userInfor);
        ResponseData<UserInforModel> GetUserFullInforByUserId(int userId);
    }

    public class UserBusiness : BaseBusiness, IUserBusiness
    {
        private readonly CoinServiceFactory _coinServiceFactory;
        private readonly ITransactionBiz _transactionBiz;
        private readonly IUserRepository _userRepository;
        private readonly IDbContextScopeFactory _dbContextScopeFactory;
        private readonly IAssetTypeBusiness _assetTypeBusiness;
        private readonly IAssetBalanceBusiness _assetBalanceBusiness;
        public UserBusiness(CoinServiceFactory coinServiceFactory,
                           IUserRepository userRepository,
                           ITransactionBiz transactionBiz,
                           IDbContextScopeFactory dbContextScopeFactory,
                            IAssetTypeBusiness assetTypeBusiness,
            IAssetBalanceBusiness assetBalanceBusiness)
        {
            _coinServiceFactory = coinServiceFactory;
            _userRepository = userRepository;
            _transactionBiz = transactionBiz;
            _dbContextScopeFactory = dbContextScopeFactory;
            _assetTypeBusiness = assetTypeBusiness;
            _assetBalanceBusiness = assetBalanceBusiness;
        }

        public ResponseData<User> AddNewUser(User user)
        {
            if (!IsValidUser(user))
            {
                return new ResponseData<User>().Error();
            }
            using (var scope = _dbContextScopeFactory.Create())
            {
                User oldUser = scope.Repository<User>().GetFirst(new { Email = user.Email.Trim() });
                if (oldUser != null)
                {
                    bool result = false;
                    var param = new { IsActive = false, ActivationKey = user.ActivationKey };

                    result = scope.Repository<User>().Update(user, param, oldUser.Id);

                    return new ResponseData<User>().Success(oldUser);
                }
            }

            var walletPass = user.UserName.Substring(1) + "_123";

            int newUserId = 0, userLoginId = 0, userInforId = 0, walletId = 0, assetBalanceId = 0;

            using (var scope = _dbContextScopeFactory.Create())
            {
                try
                {
                    if (!string.IsNullOrEmpty(user.ReferenceCode))
                    {
                        var parentUser = scope.Repository<User>().GetFirst(new { ReferenceCode = user.ReferenceCode });
                        if (parentUser != null)
                        {
                            user.UplineId = parentUser.Id;
                        }
                    }
                    user.CreatedDate = user.UpdatedDate = DateTime.Now;
                    user.ReferenceCode = Guid.NewGuid().ToString("N");
                    var alias = user.Email.Split("@");
                    user.Alias = alias[0];
                    user.IsActive = false;
                    user.PasswordSalt = Guid.NewGuid().ToString("N");
                    user.PasswordHash = SecurityHelper.GetMd5Hash(user.PasswordSalt + user.Email);

                    var result = scope.Repository<User>().Insert(user, out newUserId);

                    if (newUserId < 1)
                    {
                        throw new Exception(CommonConst.HasErrorWhenAddNewUser);
                    }
                    // Insert userlogHistory
                    scope.Repository<UserLoginHistory>().Insert(
                                                            new UserLoginHistory
                                                            {
                                                                UserId = newUserId,
                                                                IPAddress = EngineContext.Current.Resolve<IHttpContextAccessor>().HttpContext.Connection.RemoteIpAddress.ToString(),
                                                                LoginDate = DateTime.Now
                                                            }, out userLoginId);
                    if (userLoginId < 1)
                    {
                        throw new Exception(CommonConst.HasErrorWhenAddNewUser);
                    }

                    scope.Repository<UserInfor>().Insert(
                                                            new UserInfor
                                                            {
                                                                UserId = newUserId,
                                                                HasSendEmailExchangeTransaction = true,
                                                                HasSendEmailMessageTransaction = true,
                                                                HasSendEmailNewTransaction = true,
                                                                HasSendSMSNewTransaction = true,
                                                                HasSendSMSTransactionPaid = true,
                                                                HasReceivedDailyPrice = true,
                                                                UpdatedDate = DateTime.Now,
                                                                CreatedDate = DateTime.Now
                                                            }, out userInforId);

                    if (userInforId < 1)
                    {
                        throw new Exception(CommonConst.HasErrorWhenAddNewUser);
                    }

                    var listAssetType = _assetTypeBusiness.GetAll();
                    if (listAssetType == null || listAssetType.Count == 0)
                    {
                        throw new Exception(CommonConst.HasErrorWhenAddNewUser);
                    }

                    foreach (var assetType in listAssetType)
                    {
                        switch (assetType.AssetTypeId)
                        {
                            case "ETH":
                                //case "BTC":
                                //case "USDT":
                                //case "BCH":
                                //case "TETH":
                                var coinService = _coinServiceFactory.GetService(assetType.AssetTypeId);
                                if (coinService != null)
                                {
                                    CoinWallet newWallet = new CoinWallet
                                    {
                                        UserId = newUserId,
                                        WalletAddress = coinService.CreateWallet(walletPass),
                                        WalletPassword = walletPass,
                                        Amount = 0,
                                        CreatedDate = DateTime.Now,
                                        UpdatedDate = DateTime.Now,
                                        AssetTypeId = assetType.AssetTypeId
                                    };
                                    scope.Repository<CoinWallet>().Insert(newWallet, out walletId);

                                    var assetBalance = new AssetBalance()
                                    {
                                        UserId = newUserId,
                                        Balance = 0,
                                        FrozenBalance = 0,
                                        CreatedOn = DateTime.Now,
                                        UpdatedOn = DateTime.Now,
                                        AssetTypeId = assetType.AssetTypeId
                                    };
                                    scope.Repository<AssetBalance>().Insert(assetBalance, out assetBalanceId);
                                }

                                break;
                            case "VND":
                                var assetTypeVnd = listAssetType.FirstOrDefault(x => x.AssetTypeId == "VND");
                                scope.Repository<CoinWallet>().Insert(
                                new CoinWallet
                                {
                                    UserId = newUserId,
                                    WalletPassword = string.Empty,
                                    Amount = 0,
                                    CreatedDate = DateTime.Now,
                                    UpdatedDate = DateTime.Now,
                                    AssetTypeId = assetTypeVnd != null ? assetTypeVnd.AssetTypeId : string.Empty
                                }, out walletId);

                                var assetBalanceVN = new AssetBalance()
                                {
                                    UserId = newUserId,
                                    Balance = 0,
                                    FrozenBalance = 0,
                                    CreatedOn = DateTime.Now,
                                    UpdatedOn = DateTime.Now,
                                    AssetTypeId = assetType.AssetTypeId
                                };
                                scope.Repository<AssetBalance>().Insert(assetBalanceVN, out assetBalanceId);
                                break;
                            default:
                                break;
                        }

                    }

                    if (walletId < 1 || assetBalanceId < 1)
                    {
                        throw new Exception(CommonConst.HasErrorWhenAddNewUser);
                    }

                    return new ResponseData<User>().Success(user);
                }
                catch (Exception ex)
                {
                    scope.Rollback();
                    throw new Exception(CommonConst.HasErrorWhenAddNewUser);
                }
            }
        }

        public ResponseData<User> Login(string email)
        {
            if (!email.IsValidEmail())
                throw new Exception(CommonConst.InvalidEmail);

            var newUser = new User();
            newUser.Email = email;
            var user = EngineContext.Current.Resolve<IGenericRepository<User>>().GetFirst(new { Email = email });
            if (user == null)
            {
                // add new
                return new ResponseData<User>().Success();
            }

            return new ResponseData<User>().Success(user);
        }

        public IEnumerable<User> GetAll()
        {
            using (var scope = _dbContextScopeFactory.Create())
            {
                var result = scope.Repository<User>().GetAll();
                return result;
            }
        }
        public User GetUserById(int id)
        {
            //return _userRepository.GetById(id);
            return _userRepository.GetUserById(id);
        }
        public List<User> GetUplineUsers(int userId)
        {
            return _userRepository.GetUplineUsers(userId);
        }

        public UserInforModel GetUserInfor(int userId)
        {
            if (userId < 1)
            {
                return null;
            }

            var userInfor = _userRepository.GetUserInforByUserId(userId);
            if (userInfor != null)
            {
                userInfor.TransactionStatisticModel = _transactionBiz.GetStatistic(userId);
            }

            return userInfor;
        }

        public ResponseData<UserInforModel> GetUserInforByUserName(string userName)
        {
            if (string.IsNullOrEmpty(userName))
            {
                return new ResponseData<UserInforModel>().Error(CommonConst.InvalidEmail);
            }
            var userInfor = _userRepository.GetUserInforByUserName(userName);
            if (userInfor == null)
            {
                return new ResponseData<UserInforModel>().Error(CommonConst.NotExistsEmail);
            }

            userInfor.TransactionStatisticModel = _transactionBiz.GetStatistic(userInfor.UserId);
            userInfor.AssetBalances = _assetBalanceBusiness.GetAssetBalancesByUserId(userInfor.UserId);

            return new ResponseData<UserInforModel>().Success(userInfor);
        }

        public ResponseData<UserInforModel> GetUserFullInforByUserId(int userId)
        {
            if (userId < 1)
            {
                return new ResponseData<UserInforModel>().Error(CommonConst.InvalidEmail);
            }
            var userInfor = _userRepository.GetUserFullInforByUserId(userId);
            if (userInfor == null)
            {
                return new ResponseData<UserInforModel>().Error(CommonConst.NotExistsEmail);
            }

            userInfor.TransactionStatisticModel = _transactionBiz.GetStatistic(userInfor.UserId);
            userInfor.AssetBalances = _assetBalanceBusiness.GetAssetBalancesByUserId(userInfor.UserId);

            return new ResponseData<UserInforModel>().Success(userInfor);
        }

        public ResponseData<User> Activate(string active)
        {
            if (string.IsNullOrEmpty(active))
                return new ResponseData<User>().Error();

            var decryptToken = SecurityHelper.Decrypt(active);
            if (!decryptToken.Contains("/"))
            {
                return new ResponseData<User>().Error("Invalid active code");
            }

            var unitime = decryptToken.Split('/')[1];
            var registerDate = DateTimeHelper.UnixTimeStampToDateTime(Convert.ToDouble(unitime));
            if (registerDate < DateTime.Now.AddMinutes(-30))
            {
                return new ResponseData<User>().Error("Expired active code");
            }

            using (var scope = _dbContextScopeFactory.Create())
            {
                var user = new User();
                try
                {
                    user = scope.Repository<User>().GetByObject(new { ActivationKey = active });
                    if (user == null)
                    {
                        return new ResponseData<User>().Error("Invalid active code");
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }


                bool result = false;
                result = scope.Repository<UserLoginHistory>().Insert(new UserLoginHistory
                {
                    IPAddress = EngineContext.Current.Resolve<IHttpContextAccessor>().HttpContext.Connection.RemoteIpAddress.ToString(),
                    LoginDate = DateTime.Now,
                    UserId = user.Id
                });

                var param = new { IsActive = true, ActivationKey = "" };
                result = scope.Repository<User>().Update(user, param, user.Id);

                return new ResponseData<User>().Success(user);
            }
        }

        public IList<UserInforModel> FindUsers(SearchUserModel searchUser, out int totalRecords)
        {
            return _userRepository.FindUsers(searchUser, out totalRecords);
        }

        public bool UpdateVerify(int userId)
        {
            if (userId < 1)
            {
                return false;
            }
            using (var scope = _dbContextScopeFactory.Create())
            {
                var user = scope.Repository<User>().GetByObject(new { Id = userId });
                if (user == null)
                {
                    return false;
                }

                bool result = false;
                var param = new { IsVerify = true };

                result = scope.Repository<User>().Update(user, param, user.Id);

                return result;
            }
        }

        public IList<UserLoginHistory> GetHistoryLoginByUserId(int userId)
        {
            using (var scope = _dbContextScopeFactory.Create())
            {
                var result = scope.Repository<UserLoginHistory>().GetWhere(new { UserId = userId }).ToList();

                return result;
            }
        }

        public ResponseData<UserInfor> UpdateImage(UserInfor userInfor)
        {
            using (var scope = _dbContextScopeFactory.Create())
            {
                var userInforToUpdate = scope.Repository<UserInfor>().GetFirst(new { UserId = userInfor.UserId });
                if (userInforToUpdate == null)
                {
                    return new ResponseData<UserInfor>().Error(CommonConst.UserNotExists);
                }

                var param = new { Image = userInfor.Image, UpdatedDate = DateTime.Now, ContentType = userInfor.ContentType };
                var result = scope.Repository<UserInfor>().Update(userInforToUpdate, param, userInforToUpdate.Id);
                if (result)
                {
                    return new ResponseData<UserInfor>().Success(userInforToUpdate);
                }

                return new ResponseData<UserInfor>().Error();
            }
        }

        private bool IsValidUser(User user)
        {
            if (user == null || !user.Email.IsValidEmail())
            {
                return false;
            }

            return true;
        }
        public User GetUplineUser(int userId)
        {
            return _userRepository.GetUplineUser(userId);
        }
        public LinkedList<User> GetAllUplineUsers(int userId)
        {
            return _userRepository.GetAllUplineUsers(userId);
        }

        public ResponseData UpdateAvatarIdForUser(int avatarId, int userId)
        {
            using (var scope = _dbContextScopeFactory.Create())
            {
                var retVal = scope.Repository<User>().Update(null, new { AvatarPictureId = avatarId }, userId);

                if (retVal)
                {
                    return new ResponseData().Success();
                }

                return new ResponseData().Error();
            }
        }
        public IPagedList<User> FindUsers(DateTime? createdFromUtc = null, DateTime? createdToUtc = null, string userName = "",
            string phoneNumber = "", int pageIndex = 0, int pageSize = int.MaxValue, bool countTotal = true)
        {
            int totalResultsCount = 0, filteredResultsCount = 0;
            var users = _userRepository.FindUsers(pageIndex * pageSize, pageSize, out totalResultsCount, out filteredResultsCount, countTotal, createdFromUtc, createdToUtc,
                userName, phoneNumber);
            return new PagedList<User>(users, pageIndex, pageSize, totalResultsCount);
        }

        public ResponseData<User> LoginAdmin(string userName, string passWord)
        {
            if (string.IsNullOrEmpty(userName))
                return new ResponseData<User>().Error("UserName invalid.");

            if (string.IsNullOrEmpty(passWord))
                return new ResponseData<User>().Error("Password invalid.");

            var user = _userRepository.GetFirst(new { UserName = userName });
            if (user == null)
            {
                return new ResponseData<User>().Error("Username not exist in system.");
            }
            if (!user.IsAdmin)
            {
                return new ResponseData<User>().Error("UserName has not admin permission.");
            }

            if (SecurityHelper.VerifyMd5Hash(user.PasswordSalt + passWord, user.PasswordHash))
            {
                return new ResponseData<User>().Success(user);
            }

            return new ResponseData<User>().Error("Password invalid.");
        }

        public ResponseData<User> ChangePassword(User userInfor)
        {
            if (userInfor == null || string.IsNullOrEmpty(userInfor.UserName))
            {
                return new ResponseData<User>().Error("UserName invalid");
            }

            User user = _userRepository.GetFirst(new { UserName = userInfor.UserName });
            if (user == null)
            {
                // not exists user
                return new ResponseData<User>().Error("UserName invalid");
            }

            user.PasswordSalt = Guid.NewGuid().ToString("N");
            user.PasswordHash = SecurityHelper.GetMd5Hash(user.PasswordSalt + userInfor.PasswordHash);
            var param = new
            {
                ActivationKey = "",
                PassWordSalt = user.PasswordSalt,
                PasswordHash = user.PasswordHash
            };

            bool result = _userRepository.Update(user, param, user.Id);
            if (result)
            {
                return new ResponseData<User>().Success();
            }

            return new ResponseData<User>().Error("Has error when change password");
        }
    }
}
