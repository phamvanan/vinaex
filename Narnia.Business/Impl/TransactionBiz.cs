﻿using Narnia.Business.Commissions;
using Narnia.Business.Messaging;
using Narnia.Core;
using Narnia.Core.Domain;
using Narnia.Core.Enums;
using Narnia.Core.Infrastructure;
using Narnia.Core.Models;
using Narnia.Data.Base;
using Narnia.Data.Repository.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Business
{
    public class TransactionBiz : ITransactionBiz
    {
        private readonly ITransactionRepository _transactionRepository;
        private readonly ISellAdvertisementRepository _sellAdvertisementRepository;
        private readonly IBuyAdvertisementRepository _buyAdvertisementRepository;
        private readonly ISiteSetting _siteSetting;
        private readonly IDbContextScopeFactory _dbContextScopeFactory;
        private readonly IUserRepository _userRepository;
        private readonly IAssetBalanceRepository _assetBalanceRepository;
        private readonly ICoinSymbolRepository _coinSymbolRepository;
        private readonly ICommissionService _commissionService;

        private string _assetTypeVND = Enum.GetName(typeof(AssetTypeEnum), AssetTypeEnum.VND);
        private decimal _feePercentageDefault = (decimal)0.5;

        public TransactionBiz(ITransactionRepository transactionRepository,
                            ISellAdvertisementRepository sellAdvertisementRepository,
                            IBuyAdvertisementRepository buyAdvertisementRepository,
                            ISiteSetting siteSetting,
                            IDbContextScopeFactory dbContextScopeFactory,
                            IUserRepository userRepository,
                            IAssetBalanceRepository assetBalanceRepository,
                            ICoinSymbolRepository coinSymbolRepository,
            ICommissionService commissionService)
        {
            _transactionRepository = transactionRepository;
            _sellAdvertisementRepository = sellAdvertisementRepository;
            _buyAdvertisementRepository = buyAdvertisementRepository;
            _siteSetting = siteSetting;
            _dbContextScopeFactory = dbContextScopeFactory;
            _userRepository = userRepository;
            _assetBalanceRepository = assetBalanceRepository;
            _coinSymbolRepository = coinSymbolRepository;
            _commissionService = commissionService;
        }

        public IList<Transaction> GetAllTransactions()
        {
            return _transactionRepository.GetAll().ToList();
        }

        /// <summary>
        /// Transaction for SellAds
        /// Giao dịch bán Bitcon,ETH....
        /// </summary>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public ResponseData<Transaction> AddTransactionForSell(Transaction transaction)
        {
            if (transaction == null || transaction.AmountCoin <= 0 || transaction.UserId < 1 ||
                transaction.SellAdvertisementId < 1 || transaction.AmountCoin <= 0)
                return new ResponseData<Transaction>().Error("Thông tin giao dịch không hợp lệ.");

            int newid = 0;
            var updateOk = false;
            using (var scope = _dbContextScopeFactory.Create())
            {
                try
                {
                    var sellAds = _sellAdvertisementRepository.GetActiveById(transaction.SellAdvertisementId ?? 0);
                    if (sellAds == null || sellAds.Id == 0)
                    {
                        return new ResponseData<Transaction>().Error("Thông tin quảng cáo không tồn tại.");
                    }

                    if (sellAds.UserId == transaction.UserId)
                    {
                        return new ResponseData<Transaction>().Error("Bạn không thể tạo giao dịch trên quảng cáo của mình.");
                    }

                    if (transaction.AmountCoin < sellAds.MinCoinNumber)
                    {
                        return new ResponseData<Transaction>().Error("Thấp hơn lượng giới hạn");
                    }

                    if (transaction.AmountCoin > (sellAds.MaxCoinNumber - sellAds.AmountSell))
                    {
                        return new ResponseData<Transaction>().Error("Cao hơn lượng giới hạn cho phép");
                    }

                    transaction.AssetTypeId = sellAds.AssetTypeId;
                    transaction.PaymentType = (int)PaymentType.Transfer;
                    transaction.Status = (int)TransactionStautus.Pending;
                    transaction.TransactionCode = $"S{DateTime.Now.ToString("yyMMddHHmmssff")}";
                    transaction.TransferType = (int)TransferType.User;

                    //get balance VND of Buyer
                    var assetBalanceVNDBuyer = _assetBalanceRepository.GetByUserIdAssetType(transaction.UserId, _assetTypeVND);
                    if (assetBalanceVNDBuyer == null || assetBalanceVNDBuyer.Id == 0)
                    {
                        return new ResponseData<Transaction>().Error(CommonConst.HasErrorWhenCreateTransaction);
                    }
                    var coinSymbolCurrent = _coinSymbolRepository.GetbyAssetIdBase(transaction.AssetTypeId);
                    if (coinSymbolCurrent == null || coinSymbolCurrent.Id == 0)
                    {
                        return new ResponseData<Transaction>().Error(CommonConst.HasErrorWhenCreateTransaction);
                    }

                    //update AmountSell for SellAds
                    sellAds.AmountSell += transaction.AmountCoin;
                    updateOk = scope.Repository<SellAdvertisement>().Update(sellAds);
                    if (updateOk == false)
                    {
                        scope.Rollback();
                        return new ResponseData<Transaction>().Error(CommonConst.HasErrorWhenCreateTransaction);
                    }

                    var emailType = SendEmailType.NewTransactionForAdsSell;

                    //check balanceVND >= value of AmountCoin( base VND)
                    var amountCoinToVND = Math.Round(transaction.AmountCoin * coinSymbolCurrent.AvgPrice, 0);//quy đổi giá trị tiền ETH,BTC ... về VND
                    if (assetBalanceVNDBuyer.Balance - assetBalanceVNDBuyer.FrozenBalance >= amountCoinToVND)
                    {
                        //giao dich nhanh

                        assetBalanceVNDBuyer.Balance -= amountCoinToVND;
                        assetBalanceVNDBuyer.UpdatedOn = DateTime.Now;
                        updateOk = scope.Repository<AssetBalance>().Update(assetBalanceVNDBuyer);
                        if (updateOk == false)
                        {
                            return new ResponseData<Transaction>().Error(CommonConst.HasErrorWhenCreateTransaction);
                        }

                        //get balance VND of seller
                        var assetBalanceVNDSeller =
                            _assetBalanceRepository.GetByUserIdAssetType(sellAds.UserId, _assetTypeVND);
                        if (assetBalanceVNDBuyer == null || assetBalanceVNDBuyer.Id == 0)
                        {
                            scope.Rollback();
                            return new ResponseData<Transaction>().Error(CommonConst.HasErrorWhenCreateTransaction);
                        }
                        assetBalanceVNDSeller.Balance += amountCoinToVND;
                        assetBalanceVNDSeller.UpdatedOn = DateTime.Now;
                        updateOk = scope.Repository<AssetBalance>().Update(assetBalanceVNDSeller);
                        if (updateOk == false)
                        {
                            scope.Rollback();
                            return new ResponseData<Transaction>().Error(CommonConst.HasErrorWhenCreateTransaction);
                        }
                        transaction.PaymentType = (int)PaymentType.Inside;
                        transaction.Status = (int)TransactionStautus.Success;
                        transaction.ConfirmBy = transaction.UserId;
                        transaction.UpdatedDate = DateTime.Now;
                        updateOk = scope.Repository<Transaction>().Insert(transaction, out newid);

                        //insert transaction complete
                        if (updateOk == false || newid == 0)
                        {
                            scope.Rollback();
                            return new ResponseData<Transaction>().Error(CommonConst.HasErrorWhenCreateTransaction);
                        }

                        //feePercent
                        var feePercentage = _siteSetting.FeePercentage;
                        if (feePercentage == 0)
                        {
                            feePercentage = _feePercentageDefault;
                        }

                        var amountFee = transaction.AmountCoin * feePercentage;

                        //update balance ETH,BTC ... transaction complete
                        var assetBalanceCoinSeller =
                            _assetBalanceRepository.GetByUserIdAssetType(sellAds.UserId, transaction.AssetTypeId);
                        var assetBalanceCoinBuyer =
                            _assetBalanceRepository.GetByUserIdAssetType(transaction.UserId, transaction.AssetTypeId);
                        if (assetBalanceCoinSeller == null || assetBalanceCoinSeller.Id == 0 ||
                            assetBalanceCoinBuyer == null || assetBalanceCoinBuyer.Id == 0)
                        {
                            scope.Rollback();
                            return new ResponseData<Transaction>().Error(CommonConst.HasErrorWhenCreateTransaction);
                        }

                        //add amountCoin into balanceBuyer
                        assetBalanceCoinBuyer.Balance += transaction.AmountCoin;
                        assetBalanceCoinBuyer.UpdatedOn = DateTime.Now;
                        updateOk = scope.Repository<AssetBalance>().Update(assetBalanceCoinBuyer);

                        if (updateOk == false)
                        {
                            scope.Rollback();
                            return new ResponseData<Transaction>().Error(CommonConst.HasErrorWhenCreateTransaction);
                        }

                        //subtract amountCoin into BalanceSeller
                        assetBalanceCoinSeller.Balance -= transaction.AmountCoin;
                        assetBalanceCoinSeller.FrozenBalance -= transaction.AmountCoin;
                        assetBalanceCoinSeller.UpdatedOn = DateTime.Now;
                        updateOk = scope.Repository<AssetBalance>().Update(assetBalanceCoinSeller);
                        if (updateOk == false)
                        {
                            scope.Rollback();
                            return new ResponseData<Transaction>().Error(CommonConst.HasErrorWhenCreateTransaction);
                        }

                        //set commission Buyer
                        updateOk = _commissionService.InsertCommissionForUplines(scope, assetBalanceCoinBuyer, transaction.UserId,
                            transaction.AssetTypeId, transaction.AmountCoin, amountFee, newid, RefType.Transaction);

                        if (updateOk == false)
                        {
                            scope.Rollback();
                            return new ResponseData<Transaction>().Error(CommonConst.HasErrorWhenCreateTransaction);
                        }
                        emailType = SendEmailType.TransactionConfirm;
                    }
                    else
                    {
                        //giao dịch bình thường
                        updateOk = scope.Repository<Transaction>().Insert(transaction, out newid);
                        if (updateOk == false || newid == 0)
                        {
                            scope.Rollback();
                            return new ResponseData<Transaction>().Error(CommonConst.HasErrorWhenCreateTransaction);
                        }
                    }

                    //SendEmail
                    var userBuy = scope.Repository<User>().GetById(transaction.UserId);
                    var userSell = scope.Repository<User>().GetById(sellAds.UserId);
                    Task.Run(() =>
                    {
                        EngineContext.Current.Resolve<EmailJob>()
                            .SendTransaction(userBuy, transaction, emailType);
                        EngineContext.Current.Resolve<EmailJob>()
                            .SendTransaction(userSell, transaction, emailType);
                    });
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    scope.Rollback();
                    return new ResponseData<Transaction>().Error(CommonConst.HasErrorWhenCreateTransaction);
                }
            }
            return new ResponseData<Transaction>().Success(transaction);
        }

        /// <summary>
        /// Transaction for buyAds
        /// Giao dịch mua Bitcon,ETH....
        /// Giao này này luôn luôn là giao dịch tức thì
        /// </summary>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public ResponseData<Transaction> AddTransactionForBuy(Transaction transaction)
        {
            var result = new ResponseData<Transaction>();
            if (transaction == null || transaction.AmountCoin <= 0 || transaction.UserId < 1 ||
                transaction.BuyAdvertisementId < 1)
            {
                return result.Error("Thông tin giao dịch không hợp lệ.");
            }

            int newid = 0;
            bool updateOK = false;
            using (var scope = _dbContextScopeFactory.Create())
            {
                var buyAds = _buyAdvertisementRepository.GetByObject(new { Id = transaction.BuyAdvertisementId });
                if (buyAds == null || buyAds.Id == 0)
                {
                    return result.Error("Thông tin quảng cáo không tồn tại.");
                }

                if (buyAds.UserId == transaction.UserId)
                {
                    return new ResponseData<Transaction>().Error("Bạn không thể tạo giao dịch trên quảng cáo của mình.");
                }

                transaction.AssetTypeId = buyAds.AssetTypeId;
                transaction.PaymentType = (int)PaymentType.Inside;
                transaction.TransactionCode = $"B{DateTime.Now.ToString("yyMMddHHmmssff")}";
                transaction.Status = (int)TransactionStautus.Success;
                transaction.UpdatedDate = DateTime.Now;
                transaction.ConfirmBy = transaction.UserId;

                if (transaction.AmountCoin < buyAds.MinCoinNumber)
                {
                    return result.Error("Thấp hơn lượng giới hạn");
                }
                if (transaction.AmountCoin > buyAds.MaxCoinNumber)
                {
                    return result.Error("Cao hơn lượng giới hạn");
                }
                if (transaction.AmountCoin > (buyAds.MaxCoinNumber - buyAds.AmountBuy))
                {
                    return result.Error("Cao hơn lượng giới hạn cho phép");
                }

                //get balanceVND buyer
                var assetBalanceVNDBuyer =
                    _assetBalanceRepository.GetByUserIdAssetType(buyAds.UserId, _assetTypeVND);
                if (assetBalanceVNDBuyer == null || assetBalanceVNDBuyer.Id == 0)
                {
                    scope.Rollback();
                    return result.Error(CommonConst.HasErrorWhenCreateTransaction);
                }

                var coinSymbolCurrent = _coinSymbolRepository.GetbyAssetIdBase(transaction.AssetTypeId);
                if (coinSymbolCurrent == null || coinSymbolCurrent.Id == 0)
                {
                    return new ResponseData<Transaction>().Error(CommonConst.HasErrorWhenCreateTransaction);
                }

                //check balanceVND >= value of AmountCoin( base VND)
                var amountCoinToVND = Math.Round(transaction.AmountCoin * coinSymbolCurrent.AvgPrice, 0);//quy đổi giá trị tiền ETH,BTC ... về VND
                if (assetBalanceVNDBuyer.Balance - assetBalanceVNDBuyer.FrozenBalance - amountCoinToVND < 0)
                {
                    return new ResponseData<Transaction>().Error(CommonConst.NotEnoughBalanceVNDForBuy);
                }

                var assetBalanceSellerVND =
                    _assetBalanceRepository.GetByUserIdAssetType(transaction.UserId, _assetTypeVND);
                if (assetBalanceSellerVND == null || assetBalanceSellerVND.Id == 0)
                {
                    return new ResponseData<Transaction>().Error(CommonConst.HasErrorWhenCreateTransaction);
                }

                //update balanceVND buyer
                assetBalanceVNDBuyer.Balance -= amountCoinToVND;
                assetBalanceVNDBuyer.UpdatedOn = DateTime.Now;
                updateOK = scope.Repository<AssetBalance>().Update(assetBalanceVNDBuyer);
                if (updateOK == false)
                {
                    return new ResponseData<Transaction>().Error(CommonConst.HasErrorWhenCreateTransaction);
                }

                //update balanceVND seller
                assetBalanceSellerVND.Balance += amountCoinToVND;
                assetBalanceSellerVND.UpdatedOn = DateTime.Now;
                updateOK = scope.Repository<AssetBalance>().Update(assetBalanceSellerVND);
                if (updateOK == false)
                {
                    scope.Rollback();
                    return new ResponseData<Transaction>().Error(CommonConst.HasErrorWhenCreateTransaction);
                }

                //feePercent
                var feePercentage = _siteSetting.FeePercentage;
                if (feePercentage == 0)
                {
                    feePercentage = _feePercentageDefault;
                }

                var amountFee = transaction.AmountCoin * feePercentage;
                //var amountCoinAfterfee = transaction.AmountCoin - amountFee/*chargefee*/;

                //balanceCoin Seller
                var balanceCoinSeller =
                    _assetBalanceRepository.GetByUserIdAssetType(transaction.UserId, transaction.AssetTypeId);
                if (balanceCoinSeller == null || balanceCoinSeller.Id == 0)
                {
                    scope.Rollback();
                    return new ResponseData<Transaction>().Error(CommonConst.HasErrorWhenCreateTransaction);
                }

                if (balanceCoinSeller.Balance - balanceCoinSeller.FrozenBalance - transaction.AmountCoin - amountFee/*chargefee*/ < 0)
                {
                    scope.Rollback();
                    return new ResponseData<Transaction>().Error(CommonConst.NotEnoughBalanceCoinForTransaction);
                }

                //Insert transaction
                updateOK = scope.Repository<Transaction>().Insert(transaction, out newid);
                if (updateOK == false || newid == 0)
                {
                    scope.Rollback();
                    return new ResponseData<Transaction>().Error(CommonConst.HasErrorWhenCreateTransaction);
                }

                //balanceCoin Buyer
                var balanceCoinBuyer =
                    _assetBalanceRepository.GetByUserIdAssetType(buyAds.UserId, transaction.AssetTypeId);
                if (balanceCoinBuyer == null || balanceCoinBuyer.Id == 0)
                {
                    scope.Rollback();
                    return new ResponseData<Transaction>().Error(CommonConst.HasErrorWhenCreateTransaction);
                }

                //update balanceCoin Seller
                balanceCoinSeller.Balance -= transaction.AmountCoin;
                balanceCoinSeller.UpdatedOn = DateTime.Now;
                updateOK = scope.Repository<AssetBalance>().Update(balanceCoinSeller);

                //update balanceCoin buyer
                balanceCoinBuyer.Balance += transaction.AmountCoin;
                balanceCoinBuyer.UpdatedOn = DateTime.Now;
                updateOK = scope.Repository<AssetBalance>().Update(balanceCoinBuyer);

                //set commission buyer
                updateOK = _commissionService.InsertCommissionForUplines(scope, balanceCoinBuyer, buyAds.UserId, transaction.AssetTypeId, transaction.AmountCoin,
                    amountFee, (int)buyAds.Id, RefType.BuyAds);
                if (updateOK == false)
                {
                    scope.Rollback();
                    return new ResponseData<Transaction>().Error(CommonConst.HasErrorWhenCreateTransaction);
                }

                //set commission seller
                updateOK = _commissionService.InsertCommissionForUplines(scope, balanceCoinSeller, transaction.UserId, transaction.AssetTypeId, transaction.AmountCoin, amountFee, newid, RefType.Transaction);
                if (updateOK == false)
                {
                    scope.Rollback();
                    return new ResponseData<Transaction>().Error(CommonConst.HasErrorWhenCreateTransaction);
                }

                //update amountBuy buyAds
                buyAds.AmountBuy += transaction.AmountCoin;
                updateOK = scope.Repository<BuyAdvertisement>().Update(buyAds);
                if (updateOK == false)
                {
                    scope.Rollback();
                    return new ResponseData<Transaction>().Error(CommonConst.HasErrorWhenCreateTransaction);
                }

                //SendEmail
                var userBuy = scope.Repository<User>().GetById(buyAds.UserId);
                var userSeller = scope.Repository<User>().GetById(transaction.UserId);
                Task.Run(() =>
                {
                    EngineContext.Current.Resolve<EmailJob>()
                        .SendTransaction(userBuy, transaction, SendEmailType.NewTransactionForAdsBuy);

                    EngineContext.Current.Resolve<EmailJob>()
                        .SendTransaction(userSeller, transaction, SendEmailType.NewTransactionForAdsBuy);
                });
            }
            return new ResponseData<Transaction>().Success(transaction);
        }

        public ResponseData<Transaction> GetTransactionForEvidence(int transId, int userId)
        {
            if (transId <= 0 || userId <= 0)
            {
                return new ResponseData<Transaction>().Error();
            }
            using (var scope = _dbContextScopeFactory.Create())
            {
                var transactionEntity = scope.Repository<Transaction>().GetFirst(new { TransactionId = transId, UserId = userId, Status = (int)TransactionStautus.Pending });
                if (transactionEntity == null)
                {
                    return new ResponseData<Transaction>().Error("Không có quyền xem giao dịch");
                }
                return new ResponseData<Transaction>().Success(transactionEntity);
            }
        }

        public ResponseData<Transaction> CreateEvidencce(Evidence evidence, string transcode, int userId)
        {
            ResponseData<Transaction> result = new ResponseData<Transaction>();
            if (string.IsNullOrEmpty(transcode) || userId <= 0)
            {
                return result.Error(CommonConst.HasErrorWhenAddNewEvidenceTransaction);
            }
            if (evidence == null)
            {
                return result.Error(CommonConst.HasErrorWhenAddNewEvidenceTransaction);
            }
            using (var scope = _dbContextScopeFactory.Create())
            {
                var transactionEntity = scope.Repository<Transaction>().GetFirst(new { TransactionCode = transcode, UserId = userId, Status = (int)TransactionStautus.Pending });
                if (transactionEntity == null)
                {
                    return result.Error(CommonConst.HasErrorWhenAddNewEvidenceTransaction);
                }

                var userAllowUploadEvidence = transactionEntity.UserId;
                //quảng cáo bán => người có quyền upload bằng chứng là người tạo transaction ( người mua eth)
                if (transactionEntity.BuyAdvertisementId.HasValue)
                {
                    //quảng cáo mua
                    //người có quyền upload bằng chứng là người đăng tin mua eth
                    var adsBuy = _buyAdvertisementRepository.GetByObject(new { Id = transactionEntity.BuyAdvertisementId });
                    if (adsBuy == null)
                    {
                        return result.Error(CommonConst.NotPermissionToUpdateEvidence);
                    }
                    userAllowUploadEvidence = adsBuy.UserId;
                }

                if (userAllowUploadEvidence != userId)
                {
                    return result.Error(CommonConst.NotPermissionToUpdateEvidence);
                }
                evidence.Code = Guid.NewGuid().ToString("N");
                scope.Repository<Evidence>().Insert(evidence, out int evidenceNewId);

                if (evidenceNewId <= 0)
                {
                    return result.Error(CommonConst.HasErrorWhenAddNewEvidenceTransaction);
                }
                transactionEntity.EvidenceId = evidenceNewId;
                transactionEntity.UpdatedDate = DateTime.Now;
                transactionEntity.Status = (int)TransactionStautus.HasEvidence;
                var updateOk = scope.Repository<Transaction>().Update(transactionEntity);
                if (updateOk == false)
                {
                    scope.Rollback();
                    return result.Error(CommonConst.HasErrorWhenAddNewEvidenceTransaction);
                }

                //SendEmail
                var userBuy = _userRepository.GetById(transactionEntity.UserId);
                Task.Run(() =>
                {
                    EngineContext.Current.Resolve<EmailJob>()
                        .SendTransaction(userBuy, transactionEntity, SendEmailType.TransactionUploadEvidence);
                });

                result = new ResponseData<Transaction>().Success(transactionEntity);
            }
            return result;
        }

        public ResponseData<Transaction> CreateDepositEvidence(Evidence evidence, out int evidenceId)
        {
            evidenceId = 0;
            ResponseData<Transaction> result = new ResponseData<Transaction>();

            using (var scope = _dbContextScopeFactory.Create())
            {
                evidence.Code = Guid.NewGuid().ToString("N");
                scope.Repository<Evidence>().Insert(evidence, out int evidenceNewId);

                if (evidenceNewId <= 0)
                    return result.Error(CommonConst.HasErrorWhenAddNewEvidenceTransaction);

                evidenceId = evidenceNewId;
                result = new ResponseData<Transaction>().Success();
            }
            return result;
        }

        //TODO:
        public ResponseData<Transaction> BuyerCancel(int transId, int buyerId)
        {
            ResponseData<Transaction> result = new ResponseData<Transaction>();
            if (transId <= 0 || buyerId <= 0)
            {
                return result.Error(CommonConst.HasErrorWhenBuyerCancelTransaction);
            }
            using (var scope = _dbContextScopeFactory.Create())
            {
                var transactionEntity = scope.Repository<Transaction>().GetFirst(new { TransactionId = transId, UserId = buyerId, Status = (int)TransactionStautus.Pending });
                if (transactionEntity == null)
                {
                    return result.Error(CommonConst.HasErrorWhenBuyerCancelTransaction);
                }

                if (transactionEntity.UserId != buyerId)
                {
                    return result.Error(CommonConst.HasErrorWhenBuyerCancelTransaction);
                }

                var mode = transactionEntity.BuyAdvertisementId.HasValue ? TransactionMode.Buy : TransactionMode.Sell;
                var UpdateOk = false;
                switch (mode)
                {
                    case TransactionMode.Buy:
                        var BuyAds = _buyAdvertisementRepository.GetByObject(new { Id = transactionEntity.BuyAdvertisementId.Value });
                        if (BuyAds == null)
                        {
                            return result.Error(CommonConst.HasErrorWhenBuyerCancelTransaction);
                        }
                        BuyAds.AmountBuy -= transactionEntity.AmountCoin;
                        UpdateOk = scope.Repository<BuyAdvertisement>().Update(BuyAds);
                        if (!UpdateOk)
                        {
                            return result.Error(CommonConst.HasErrorWhenBuyerCancelTransaction);
                        }

                        //assetbalanceBuyer

                        //var adsFee = _advertisementFeeRepository.GetByBuyTransactionId(transactionEntity.TransactionId);
                        //if (adsFee == null)
                        //{
                        //    return result.Error(CommonConst.HasErrorWhenBuyerCancelTransaction);
                        //}
                        //adsFee.Status = (int)AdvertisementFeeStatus.Close;
                        //UpdateOk = scope.Repository<AdvertisementFee>().Update(adsFee);
                        //if (!UpdateOk)
                        //{
                        //    scope.Rollback();
                        //    return result.Error(CommonConst.HasErrorWhenBuyerCancelTransaction);
                        //}
                        break;

                    case TransactionMode.Sell:
                        var sellAds =
                            _sellAdvertisementRepository.GetActiveById(transactionEntity.SellAdvertisementId ?? 0);
                        if (sellAds == null)
                        {
                            return result.Error(CommonConst.HasErrorWhenBuyerCancelTransaction);
                        }
                        sellAds.AmountSell -= transactionEntity.AmountCoin;
                        UpdateOk = scope.Repository<SellAdvertisement>().Update(sellAds);
                        if (!UpdateOk)
                        {
                            return result.Error(CommonConst.HasErrorWhenBuyerCancelTransaction);
                        }
                        break;
                }

                transactionEntity.UpdatedDate = DateTime.Now;
                transactionEntity.Status = (int)TransactionStautus.Reject;
                UpdateOk = scope.Repository<Transaction>().Update(transactionEntity);

                if (UpdateOk == false) return new ResponseData<Transaction>().Error(CommonConst.HasErrorWhenSellerConfirmTransaction);

                result = new ResponseData<Transaction>().Success(transactionEntity);
            }
            return result;
        }

        public ResponseData AdminConfirm(Transaction entityInput)
        {
            var confirmBy = entityInput.ConfirmBy ?? 0;
            if (entityInput == null || confirmBy == 0 || entityInput.TransactionId == 0)
            {
                return new ResponseData().Error(CommonConst.HasErrorWhenSellerConfirmTransaction);
            }
            var transactionEntity = _transactionRepository.GetbyTransactionId(entityInput.TransactionId);
            if (transactionEntity == null)
            {
                return new ResponseData().Error(CommonConst.HasErrorWhenSellerConfirmTransaction);
            }

            var statusValid = GetValuesValidToExclude(transactionEntity.Status);
            if (!statusValid.Contains(entityInput.Status))
            {
                return new ResponseData().Error(CommonConst.HasErrorWhenSellerConfirmTransaction);
            }

            using (var scope = _dbContextScopeFactory.Create())
            {
                var mode = transactionEntity.BuyAdvertisementId.HasValue ? TransactionMode.Buy : TransactionMode.Sell;
                if (mode == TransactionMode.Buy)
                {
                    //hiện tại quảng cáo mua 100% là giao dịch tức thì, admin không confirm đc
                    return new ResponseData().Error(CommonConst.HasErrorWhenAdminConfirmTransaction);
                }
                if (entityInput.Status == (int)TransactionStautus.Success)
                {
                    if (transactionEntity.EvidenceId.HasValue == false)
                    {
                        return new ResponseData().Error(CommonConst.TransactionHasNotEvidence);
                    }
                    //success
                    transactionEntity.TransferType = entityInput.TransferType;

                    var tranOK =
                        ConfirmSellAdsTransaction(scope, confirmBy, transactionEntity, true); //  ConfirmBuyTransaction(adminId, mode, transactionEntity, scope);
                    if (tranOK == false)
                        return new ResponseData().Error(CommonConst.HasErrorWhenAdminConfirmTransaction);

                    return new ResponseData().Success();
                }
                else
                {
                    //reject
                    var tranOk = CancelSellAdsTransaction(scope, confirmBy, transactionEntity,
                        (TransactionStautus)entityInput.Status, true);
                    if (tranOk == false)
                    {
                        return new ResponseData().Error(CommonConst.HasErrorWhenAdminConfirmTransaction);
                    }
                    return new ResponseData().Success();
                }
            }
        }

        public ResponseData<Evidence> GetEvidenceById(int Id)
        {
            ResponseData<Evidence> result = new ResponseData<Evidence>();
            if (Id <= 0 || Id <= 0)
            {
                return new ResponseData<Evidence>().Error(CommonConst.HasErrorWhenAdminConfirmTransaction);
            }
            using (var scope = _dbContextScopeFactory.Create())
            {
                var data = scope.Repository<Evidence>().GetByObject(new { Id = Id });
                if (data == null)
                {
                    return new ResponseData<Evidence>().Error();
                }
                return new ResponseData<Evidence>().Success(data);
            }
        }

        public IList<TransactionHistoryModel> GetTransactionHistoryByType(int userId, TransactionStatusMode statusMode, int take, int skip, out int totalRecords)
        {
            var data = _transactionRepository.GetTransactionHistoryByType(userId, statusMode, take, skip,
                out totalRecords);
            return data;
        }

        public ResponseData<IList<Transaction>> GetTransactionsByUserId(int userId)
        {
            if (userId < 1)
            {
                return new ResponseData<IList<Transaction>>().Error(CommonConst.UserIdInvalid);
            }
            using (var scope = _dbContextScopeFactory.Create())
            {
                var data = scope.Repository<Transaction>().GetWhere(new { UserId = userId });
                if (data == null)
                {
                    return new ResponseData<IList<Transaction>>().Error();
                }

                return new ResponseData<IList<Transaction>>().Success(data.ToList());
            }
        }

        public ResponseData<InfoAdvertisementDetailModel> GetDetailTrans(int transId)
        {
            if (transId < 1)
            {
                return new ResponseData<InfoAdvertisementDetailModel>().Error(CommonConst.TransIdIdInvalid);
            }
            var data = _transactionRepository.GetInfoDetailTransaction(transId);
            if (data == null) return new ResponseData<InfoAdvertisementDetailModel>().Error();
            return new ResponseData<InfoAdvertisementDetailModel>().Success(data);
        }

        public ResponseData<InfoAdvertisementDetailModel> GetDetailTransByCode(string transCode)
        {
            if (string.IsNullOrEmpty(transCode))
            {
                return new ResponseData<InfoAdvertisementDetailModel>().Error(CommonConst.TransIdIdInvalid);
            }
            var data = _transactionRepository.GetInfoDetailTransactionByCode(transCode);
            if (data == null) return new ResponseData<InfoAdvertisementDetailModel>().Error();
            return new ResponseData<InfoAdvertisementDetailModel>().Success(data);
        }

        public IList<TransactionStatisticModel> GetStatistic(int userId)
        {
            return _transactionRepository.GetStatistic(userId);
        }

        public List<TransactionAdminSearchModel> SearchAdmin(int pageIndex, int pageSize, int tranMode, string AssetTypeId,
            int Status, string orderBy,
            DateTime? FromDate, DateTime? ToDate, string transactionCode, out int totalRecords)
        {
            return _transactionRepository.SearchAdmin(pageIndex, pageSize, tranMode, AssetTypeId, Status, orderBy,
                FromDate, ToDate, transactionCode, out totalRecords);
        }

        public IPagedList<Transaction> SearchTransactions(DateTime? createdFromUtc = null, DateTime? createdToUtc = null, string transactionCode = null,
            IList<int> statuses = null, IList<string> assetTypeIds = null, string userName = "", int pageIndex = 0, int pageSize = int.MaxValue, bool countTotal = true)
        {
            if (statuses != null && statuses.Contains(0))
                statuses = null;
            if (assetTypeIds != null && (assetTypeIds.Contains("") || assetTypeIds.Contains(null)))
                assetTypeIds = null;

            int totalResultsCount = 0, filteredResultsCount = 0;
            var transactions = _transactionRepository.SearchTransactions(pageIndex * pageSize, pageSize, out totalResultsCount, out filteredResultsCount, countTotal, createdFromUtc, createdToUtc,
                transactionCode, statuses, assetTypeIds, userName);
            return new PagedList<Transaction>(transactions, pageIndex, pageSize, totalResultsCount);
        }

        public TransactionDetailAdminModel GetDetailAdmin(int transactionId)
        {
            return _transactionRepository.GetDetailAdmin(transactionId);
        }

        public ResponseData SellerConfirm(int userId, string transCode)
        {
            if (userId == 0 || string.IsNullOrEmpty(transCode))
            {
                return new ResponseData().Error(CommonConst.HasErrorWhenSellerConfirmtransaction);
            }

            bool updateOk = false;
            using (var scope = _dbContextScopeFactory.Create())
            {
                var transactionEntity = scope.Repository<Transaction>().GetFirst(new { TransactionCode = transCode });
                if (transactionEntity == null)
                {
                    return new ResponseData().Error(CommonConst.HasErrorWhenGetTransaction);
                }

                if (transactionEntity.Status != (int)TransactionStautus.Pending &&
                    transactionEntity.Status != (int)TransactionStautus.HasEvidence)
                {
                    return new ResponseData().Error(CommonConst.HasErrorWhenGetTransaction);
                }

                updateOk = ConfirmSellAdsTransaction(scope, userId, transactionEntity);
                if (updateOk == false)
                {
                    return new ResponseData().Error(CommonConst.HasErrPleaseContactAdmin);
                }
                return new ResponseData().Success();
            }
        }

        public ResponseData<Transaction> GetTransactionById(int tranId)
        {
            var tran = _transactionRepository.GetByObject(new { TransactionId = tranId });
            if (tran != null)
            {
                return new ResponseData<Transaction>().Success(tran);
            }
            else
            {
                return new ResponseData<Transaction>().Error(CommonConst.HasErrorWhenGetTransaction);
            }
        }

        public int[] GetValuesValidToExclude(int transactionStatus)
        {
            var result = new int[Enum.GetNames(typeof(TransactionStautus)).Length];
            switch (transactionStatus)
            {
                case (int)TransactionStautus.Pending:
                    result[0] = (int)TransactionStautus.Reject;
                    break;

                case (int)TransactionStautus.HasEvidence:
                    result[0] = (int)TransactionStautus.Reject;
                    result[1] = (int)TransactionStautus.Success;
                    break;
            }
            result = result.Where(x => x != 0).ToArray();
            return result;
        }

        public IPagedList<TransactionHistoryUserModel> GetTransactionHistoryForUser(int userId, TransactionStatusClient transactionStatusClient, int pageIndex, int pageSize)
        {
            var totalRecords = 0;
            var skip = pageIndex * pageSize;
            var take = pageSize;
            
            var data = _transactionRepository.GetTransactionHistoryForUser(userId, transactionStatusClient,skip,take, out totalRecords);
            return new PagedList<TransactionHistoryUserModel>(data, pageIndex, pageSize, totalRecords);
        }

        #region Private function

        /// <summary>
        ///TODO: Thiếu 1 case: người mua eth chuyển tiền vào tài khoản hệ thống => khi completed transaction => sẽ chuyern tiền vnd về tài khoản nguồi bán
        /// </summary>
        /// <param name="scope"></param>
        /// <param name="userId"></param>
        /// <param name="transactionEntity"></param>
        /// <param name="isAdmin"></param>
        /// <returns></returns>
        private bool ConfirmSellAdsTransaction(IDbContextScope scope, int userId, Transaction transactionEntity, bool isAdmin = false)
        {
            try
            {
                var sellAds = _sellAdvertisementRepository.GetActiveById(transactionEntity.SellAdvertisementId.Value);
                bool updateOk = false;
                if (sellAds == null)
                {
                    scope.Rollback();
                    return false;
                }
                if (sellAds.UserId != userId && !isAdmin)
                {
                    scope.Rollback();
                    return false;
                }

                transactionEntity.Status = (int)TransactionStautus.Success;
                transactionEntity.ConfirmBy = userId;
                transactionEntity.UpdatedDate = DateTime.Now;
                updateOk = scope.Repository<Transaction>().Update(transactionEntity);
                if (updateOk == false)
                {
                    scope.Rollback();
                    return false;
                }

                //charge fee and commission user parent of buyer
                var feePercentage = _siteSetting.FeePercentage;
                if (feePercentage == 0)
                {
                    feePercentage = _feePercentageDefault;
                }
                var amountfee = feePercentage * transactionEntity.AmountCoin;

                //set commission for buyer
                var assetBalanceBuyer =
                    _assetBalanceRepository.GetByUserIdAssetType(transactionEntity.UserId, transactionEntity.AssetTypeId);
                if (assetBalanceBuyer == null)
                {
                    scope.Rollback();
                    return false;
                }

                updateOk = _commissionService.InsertCommissionForUplines(scope, assetBalanceBuyer, transactionEntity.UserId,
                    transactionEntity.AssetTypeId, transactionEntity.AmountCoin, amountfee, transactionEntity.TransactionId, RefType.Transaction);
                if (updateOk == false)
                {
                    scope.Rollback();
                    return false;
                }

                //update balance seller
                var balanceSeller =
                    _assetBalanceRepository.GetByUserIdAssetType(sellAds.UserId, transactionEntity.AssetTypeId);
                if (balanceSeller == null || balanceSeller.Balance - transactionEntity.AmountCoin < 0 ||
                    balanceSeller.FrozenBalance - transactionEntity.AmountCoin < 0)
                {
                    scope.Rollback();
                    return false;
                }
                balanceSeller.Balance -= transactionEntity.AmountCoin;
                balanceSeller.FrozenBalance -= transactionEntity.AmountCoin;
                balanceSeller.UpdatedOn = DateTime.Now;
                updateOk = scope.Repository<AssetBalance>().Update(balanceSeller);
                if (updateOk == false)
                {
                    scope.Rollback();
                    return false;
                }

                //update balance buyer
                var balanceBuyer = _assetBalanceRepository.GetByUserIdAssetType(transactionEntity.UserId,
                    transactionEntity.AssetTypeId);
                if (balanceBuyer == null)
                {
                    scope.Rollback();
                    return false;
                }
                balanceBuyer.Balance += transactionEntity.AmountCoin;
                balanceBuyer.UpdatedOn = DateTime.Now;
                updateOk = scope.Repository<AssetBalance>().Update(balanceBuyer);
                if (updateOk == false)
                {
                    scope.Rollback();
                    return false;
                }

                //transfer balanceVND from admin to user( seller eth)
                if (transactionEntity.TransferType == (int)TransferType.Admin)
                {
                    var balanceVNDAdmin =
                        _assetBalanceRepository.GetByUserIdAssetType(CommonConst.UserIdAdmin, _assetTypeVND);
                    var balanceVNDSeller = _assetBalanceRepository.GetByUserIdAssetType(sellAds.UserId, _assetTypeVND);
                    var coinSymbolCurrent = _coinSymbolRepository.GetbyAssetIdBase(transactionEntity.AssetTypeId);

                    if (balanceVNDAdmin == null || balanceVNDSeller == null || coinSymbolCurrent == null || coinSymbolCurrent.Id == 0)
                    {
                        scope.Rollback();
                        return false;
                    }
                    var amountCoinToVND = Math.Round(transactionEntity.AmountCoin * coinSymbolCurrent.AvgPrice, 0);//quy đổi giá trị tiền ETH,BTC ... về VND
                    balanceVNDSeller.Balance += amountCoinToVND;
                    balanceVNDSeller.UpdatedOn = DateTime.Now;
                    updateOk = scope.Repository<AssetBalance>().Update(balanceVNDSeller);
                    if (updateOk == false)
                    {
                        scope.Rollback();
                        return false;
                    }
                    balanceVNDAdmin.Balance -= amountCoinToVND;
                    balanceVNDAdmin.UpdatedOn = DateTime.Now;
                    updateOk = scope.Repository<AssetBalance>().Update(balanceVNDAdmin);
                    if (updateOk == false)
                    {
                        scope.Rollback();
                        return false;
                    }
                }

                //SendEmail
                var userBuy = scope.Repository<User>().GetById(transactionEntity.UserId);
                var userSell = scope.Repository<User>().GetById(sellAds.UserId);
                Task.Run(() =>
                {
                    EngineContext.Current.Resolve<EmailJob>()
                        .SendTransaction(userBuy, transactionEntity, SendEmailType.TransactionConfirm);

                    EngineContext.Current.Resolve<EmailJob>()
                        .SendTransaction(userSell, transactionEntity, SendEmailType.TransactionConfirm);
                });
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                scope.Rollback();
                return false;
            }

            return true;
        }

        private bool CancelSellAdsTransaction(IDbContextScope scope, int userId, Transaction transactionEntity, TransactionStautus status, bool isAdmin = false)
        {
            try
            {
                var sellAds = _sellAdvertisementRepository.GetActiveById(transactionEntity.SellAdvertisementId.Value);
                bool updateOk = false;
                if (sellAds == null)
                {
                    scope.Rollback();
                    return false;
                }
                if (sellAds.UserId != userId && !isAdmin)
                {
                    scope.Rollback();
                    return false;
                }
                transactionEntity.Status = (int)status;
                transactionEntity.ConfirmBy = userId;
                transactionEntity.UpdatedDate = DateTime.Now;

                updateOk = scope.Repository<Transaction>().Update(transactionEntity);
                if (updateOk == false)
                {
                    scope.Rollback();
                    return false;
                }

                //update frozenBalance of Seller
                var assetbalanceSeller =
                    _assetBalanceRepository.GetByUserIdAssetType(transactionEntity.UserId, transactionEntity.AssetTypeId);
                if (assetbalanceSeller == null || assetbalanceSeller.FrozenBalance < transactionEntity.AmountCoin)
                {
                    scope.Rollback();
                    return false;
                }
                assetbalanceSeller.FrozenBalance -= transactionEntity.AmountCoin;
                assetbalanceSeller.UpdatedOn = DateTime.Now;
                updateOk = scope.Repository<AssetBalance>().Update(assetbalanceSeller);
                if (updateOk == false)
                {
                    scope.Rollback();
                    return false;
                }
                //SendEmail
                var userBuy = scope.Repository<User>().GetById(transactionEntity.UserId);
                var userSell = scope.Repository<User>().GetById(sellAds.UserId);
                Task.Run(() =>
                {
                    EngineContext.Current.Resolve<EmailJob>()
                        .SendTransaction(userBuy, transactionEntity, SendEmailType.TransactionCancel);

                    EngineContext.Current.Resolve<EmailJob>()
                        .SendTransaction(userSell, transactionEntity, SendEmailType.TransactionCancel);
                });
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                scope.Rollback();
                return false;
            }

            return true;
        }

        #endregion Private function
    }
}