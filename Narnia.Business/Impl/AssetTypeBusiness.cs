﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Narnia.Core.Caching;
using Narnia.Core.Const;
using Narnia.Core.Domain;
using Narnia.Data.Base.Repositories;

namespace Narnia.Business.Impl
{
    public interface IAssetTypeBusiness
    {
        IList<AssetType> GetAll(bool loadFromCache = true);
        AssetType GetById(string assetTypeId);
    }
    public class AssetTypeBusiness : IAssetTypeBusiness
    {
        private readonly IGenericRepository<AssetType> _genericRepository;
        private readonly ICacheManager _cacheManager;
        public AssetTypeBusiness(IGenericRepository<AssetType> genericRepository, ICacheManager cacheManager)
        {
            _genericRepository = genericRepository;
            _cacheManager = cacheManager;
        }

        public IList<AssetType> GetAll(bool loadFromCache = true)
        {
            Func<IList<AssetType>> loadAssetTypesFunc = () => {
                return _genericRepository.GetAll().ToList();
            };

            IList<AssetType> assetTypes;
            if (loadFromCache)
            {
                //cacheable copy
                var key = CommonCacheKeys.AssetTypesAllCacheKey;
                assetTypes = _cacheManager.Get(key, () =>
                {
                    var result = loadAssetTypesFunc();
                    return result;
                });
            }
            else
            {
                assetTypes = loadAssetTypesFunc();
            }
            return assetTypes;
        }

        public AssetType GetById(string assetTypeId)
        {
            return _genericRepository.GetByObject(new { AssetTypeId = assetTypeId});
        }
    }
}
