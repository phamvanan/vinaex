﻿using System;
using System.Collections.Generic;
using Narnia.Core;
using Narnia.Core.Domain;
using Narnia.Core.Enums;
using Narnia.Core.Helpers;
using Narnia.Data.Base.Repositories;
using Narnia.Data.Repository.Contracts;

namespace Narnia.Business.Impl
{
    public interface IExchangeOfferBusiness
    {
        ResponseData<ExchangeOffer> AddNew(ExchangeOffer exchangeOffer);
        IEnumerable<ExchangeOffer> GetAll();
        ExchangeOffer GetById(int offerId);
        bool Update(ExchangeOffer offer);
        bool UpdateStatus(int exChangeOfferId, int status);
        IPagedList<ExchangeOffer> SearchExchangeOffers(DateTime? createdFromUtc = null, DateTime? createdToUtc = null,
     IList<int> statuses = null, IList<string> assetTypeIds = null, string code = "", int? type = null, int pageIndex = 0, int pageSize = int.MaxValue, bool countTotal = true);
    }

    public class ExchangeOfferBusiness : IExchangeOfferBusiness
    {
        private readonly IGenericRepository<ExchangeOffer> _genericRepository;
        private readonly IAggregateRepository _aggregateRepository;
        public ExchangeOfferBusiness(IGenericRepository<ExchangeOffer> genericRepository, IAggregateRepository aggregateRepository)
        {
            _genericRepository = genericRepository;
            _aggregateRepository = aggregateRepository;
        }

        public ResponseData<ExchangeOffer> AddNew(ExchangeOffer exchangeOffer)
        {
            if (exchangeOffer == null)
            {
                return new ResponseData<ExchangeOffer>().Error(CommonConst.InvalidInfo);
            }

            exchangeOffer.CreatedDate = DateTime.Now;
            exchangeOffer.UpdatedDate = DateTime.Now;
            exchangeOffer.Phone = exchangeOffer.Phone?.Trim();
            exchangeOffer.UserBankAccountName = exchangeOffer.UserBankAccountName?.Trim();
            exchangeOffer.ExchangeOfferCode = exchangeOffer.ExchangeOfferCode?.Trim();
            exchangeOffer.WalletAddress = exchangeOffer.WalletAddress?.Trim();
            var result = _genericRepository.Insert(exchangeOffer, out var newId);
            if (!result)
            {
                return new ResponseData<ExchangeOffer>().Error(CommonConst.HasErrorWhenAddNewData);
            }

            exchangeOffer.Id = newId;

            return new ResponseData<ExchangeOffer>().Success(exchangeOffer);
        }

        public IEnumerable<ExchangeOffer> GetAll()
        {
            return _genericRepository.GetAll();
        }

        public IPagedList<ExchangeOffer> SearchExchangeOffers(DateTime? createdFromUtc = null, DateTime? createdToUtc = null,
     IList<int> statuses = null, IList<string> assetTypeIds = null, string code = "", int? type = null, int pageIndex = 0, int pageSize = int.MaxValue, bool countTotal = true)
        {
            if (statuses != null && statuses.Contains(0))
                statuses = null;
            if (assetTypeIds != null && (assetTypeIds.Contains("") || assetTypeIds.Contains(null)))
                assetTypeIds = null;

            var items = _aggregateRepository.FindExchangeOffers(pageIndex * pageSize, pageSize, out var totalResultsCount, countTotal, createdFromUtc, createdToUtc,
                statuses, assetTypeIds, code?.Trim(),type);

            return new PagedList<ExchangeOffer>(items, pageIndex, pageSize, totalResultsCount);
        }
        public ExchangeOffer GetById(int offerId)
        {
            return _genericRepository.GetById(offerId);
        }
        public bool Update(ExchangeOffer offer)
        {
            return _genericRepository.Update(offer,new { offer.Status},offer.Id);
        }

        public bool UpdateStatus(int exChangeOfferId, int status)
        {
            if (exChangeOfferId < 1)
            {
                return false;
            }

            var exchange = _genericRepository.GetById(exChangeOfferId);
            if (exchange == null)
            {
                return false;
            }

            exchange.Status = status;

            return _genericRepository.Update(exchange, new { exchange.Status }, exchange.Id);
        }
    }
}
