﻿using Narnia.Core;
using Narnia.Core.Domain;
using Narnia.Data.Base;
using Narnia.Data.Base.Repositories;
using Narnia.Data.Repository.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Narnia.Business.Impl
{
    public class CoinWalletBusiness : ICoinWalletBusiness
    {
        private readonly ICoinSymbolRepository _coinSymbolRepository;
        private readonly ICoinWalletRepository _coinWalletRepository;
        public readonly IGenericRepository<Currency> _currencyRepository;
        public readonly IGenericRepository<Banks> _banksRepository;
        private readonly IDbContextScopeFactory _dbContextScopeFactory;
        public CoinWalletBusiness(ICoinSymbolRepository coinSymbolRepository,
            ICoinWalletRepository coinWalletRepository,
                                    IGenericRepository<Currency> currencyRepository,
                                    IGenericRepository<Banks> banksRepository,
                                    IDbContextScopeFactory dbContextScopeFactory)
        {
            _coinSymbolRepository = coinSymbolRepository;
            _coinWalletRepository = coinWalletRepository;
            _currencyRepository = currencyRepository;
            _banksRepository = banksRepository;
            _dbContextScopeFactory = dbContextScopeFactory;
        }

        public List<CoinSymbol> GetAllSymbols(bool? activeSymbolOnly)
        {
            if (!activeSymbolOnly.HasValue)
                return _coinSymbolRepository.GetAll().ToList();
            return _coinSymbolRepository.GetWhere(new { Active = activeSymbolOnly.Value }).ToList();
        }

        public CoinSymbol GetCoinSymbolByCode(string coincode)
        {
            return _coinSymbolRepository.GetWhere(new { AssetIdBase = coincode }).FirstOrDefault();
        }

        public List<Currency> GetAllCurrencies()
        {
            return _currencyRepository.GetAll().ToList();
        }

        public Currency GetCurrencyByCode(string currencyCode)
        {
            return _currencyRepository.GetWhere(new { currencyCode = currencyCode }).FirstOrDefault();
        }

        public List<Banks> GetAllBanks()
        {
            return _banksRepository.GetAll().ToList();
        }

        public CoinWallet GetCurrentCoinWalletByUserId(int userId, string assetTypeId)
        {
            if (userId == 0 || string.IsNullOrEmpty(assetTypeId))
            {
                return null;
            }

            CoinWallet coinWallet = _coinWalletRepository.GetFirst(new { UserId = userId, AssetTypeId = assetTypeId });

            return coinWallet;
        }

        public PagedList<CoinWallet> FindWallets(DateTime? createdFromUtc = null, DateTime? createdToUtc = null,IList<string> assetTypeIds = null, string userName = "", int? userId = null,
            int pageIndex = 0, int pageSize = int.MaxValue, bool countTotal = true)
        {
            if (assetTypeIds != null && (assetTypeIds.Contains("")|| assetTypeIds.Contains(null)))
                assetTypeIds = null;

            int totalResultsCount = 0, filteredResultsCount = 0;

            var wallets = _coinWalletRepository.FindWallets(pageIndex * pageSize, pageSize, out totalResultsCount, out filteredResultsCount, countTotal, createdFromUtc,createdToUtc,assetTypeIds,userName,userId);
            return new PagedList<CoinWallet>(wallets, pageIndex, pageSize, totalResultsCount);
        }

        
    }
}