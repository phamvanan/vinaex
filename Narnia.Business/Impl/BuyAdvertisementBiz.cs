﻿using Narnia.Core;
using Narnia.Core.Domain;
using Narnia.Core.Enums;
using Narnia.Core.Models;
using Narnia.Data.Base;
using Narnia.Data.Repository.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Narnia.Business.Impl
{
    public class BuyAdvertisementBiz : IBuyAdvertisementBiz
    {
        private readonly IBuyAdvertisementRepository _buyAdvertisementRepository;
        private readonly IDbContextScopeFactory _dbContextScopeFactory;

        public BuyAdvertisementBiz(IBuyAdvertisementRepository buyAdvertisementRepository, IDbContextScopeFactory dbContextScopeFactory)
        {
            _buyAdvertisementRepository = buyAdvertisementRepository;
            _dbContextScopeFactory = dbContextScopeFactory;
        }

        public ResponseData<bool> AddBuyAdvertisement(BuyAdvertisement buyAdvertisement)
        {
            if(buyAdvertisement.UserId <= 0)//UserIdInvalid
                return new ResponseData<bool>().Error(CommonConst.UserIdInvalid);

            if (buyAdvertisement.MinCoinNumber < 0 || buyAdvertisement.MaxCoinNumber <= 0)
                return new ResponseData<bool>().Error(CommonConst.InvalidInfo);

            int newid = 0;

            buyAdvertisement.Status = (int)AdvertisementStatus.New;
            buyAdvertisement.AmountBuy = 0;
            buyAdvertisement.Approved = false;
            var updateOk = _buyAdvertisementRepository.Insert(buyAdvertisement, out newid);
            if (updateOk == false || newid == 0)
            {
                return new ResponseData<bool>().Error(CommonConst.HasErrPleaseContactAdmin);
            }
            return new ResponseData<bool>().Success(true);
        }

        public bool UpdateBuyAdvertisement(BuyAdvertisement buyAdvertisement)
        {
            bool result = false;
            if (buyAdvertisement != null)
            {
                using (var scope = _dbContextScopeFactory.Create())
                {
                    result = scope.Repository<BuyAdvertisement>().Update(buyAdvertisement);
                }
            }

            return result;
        }

        public BuyAdvertisement GetBuyAdvertisementById(int buyId)
        {
            if (buyId == 0)
                return null;

            return _buyAdvertisementRepository.GetByObject(new { Id = buyId });
        }

        public IEnumerable<BuySellViewModel> SearchBuyAdvertisement(int pageIndex, int pageSize, out int totalResultsCount, out int filteredResultsCount, bool countTotal = true
            , bool isShowOfflineUser = false, string assetTypeId = null)
        {
            return _buyAdvertisementRepository.SearchBuyAdvertisement(pageIndex, pageSize, out totalResultsCount, out filteredResultsCount, countTotal, isShowOfflineUser, assetTypeId);
        }

        public ResponseData<InfoAdvertisementForSellBuyModel> GetInfoBuyAdvertisement(int buyAdId)
        {
            if (buyAdId == 0)
                throw new Exception("Invalid Data");

            var data = _buyAdvertisementRepository.GetInfoBuyAdvertisement(buyAdId);
            return new ResponseData<InfoAdvertisementForSellBuyModel>().Success(data);
        }

        public IEnumerable<BuyAdvertisement> LoadBuyAdvertisement(int pageIndex, int pageSize, out int totalResultsCount, out int filteredResultsCount, bool countTotal = true, string sort = null, BuySellAdvertiseSearchCriteria criteria = null)
        {
            return _buyAdvertisementRepository.LoadBuyAdvertisement(pageIndex, pageSize, out totalResultsCount, out filteredResultsCount, countTotal, sort, criteria);
        }

        public BuyAdvertisement LoadBuyAdvertisementDetail(int buyId)
        {
            return _buyAdvertisementRepository.LoadBuyAdvertisementDetail(buyId);
        }

        public IEnumerable<InfoQuickBuySellAdvertisement> LoadQuickBuyAdvertisement(string assetTypeId, string currencyType, decimal coinNumber)
        {
            return _buyAdvertisementRepository.LoadQuickBuyAdvertisement(assetTypeId, currencyType, coinNumber);
        }

        public ResponseData<IList<BuyAdvertisement>> GetSellInforByUserId(int userId)
        {
            if (userId == 0)
            {
                return new ResponseData<IList<BuyAdvertisement>>().Error();
            }

            var data = _buyAdvertisementRepository.GetWhere(new { UserId = userId });

            return new ResponseData<IList<BuyAdvertisement>>().Success(data?.ToList());
        }

        public IEnumerable<BuySellViewModel> GetBuyAdvertisementByUserId(int pageIndex, int pageSize, int userId, out int totalResultsCount, bool countTotal = true, string assetTypeId = null)
        {
            return _buyAdvertisementRepository.GetBuyAdvertisementByUserId(pageIndex, pageSize, userId, out totalResultsCount, countTotal, assetTypeId);
        }
        public IPagedList<BuyAdvertisement> SearchAdvertisments(DateTime? createdFromUtc = null, DateTime? createdToUtc = null,
    IList<int> statuses = null, IList<string> assetTypeIds = null, string userName = "", int pageIndex = 0, int pageSize = int.MaxValue, bool countTotal = true)
        {
            if (statuses != null && statuses.Contains(0))
                statuses = null;
            if (assetTypeIds != null && (assetTypeIds.Contains("") || assetTypeIds.Contains(null)))
                assetTypeIds = null;

            int totalResultsCount = 0, filteredResultsCount = 0;
            var advertisements = _buyAdvertisementRepository.SearchAdvertisments(pageIndex * pageSize, pageSize, out totalResultsCount, out filteredResultsCount, countTotal, createdFromUtc, createdToUtc,
                statuses, assetTypeIds, userName);
            return new PagedList<BuyAdvertisement>(advertisements, pageIndex, pageSize, totalResultsCount);
        }
    }
}