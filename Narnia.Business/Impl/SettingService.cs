﻿using Narnia.Core;
using Narnia.Core.Domain;
using Narnia.Core.Infrastructure;
using Narnia.Data;
using Narnia.Data.Base;
using Narnia.Data.Base.Repositories;
using Narnia.Data.Repository;
using Narnia.Data.Repository.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Narnia.Business.Impl
{
    public class SettingService : ISettingService
    {
        private readonly IAggregateRepository _aggregateRepository;
        private readonly IGenericRepository<Setting> _settingRepository;
        private readonly IDbContextScopeFactory _dbContextScopeFactory;
        public SettingService(IGenericRepository<Setting> settingRepository, IDbContextScopeFactory dbContextScopeFactory, IAggregateRepository aggregateRepository)
        {
            _settingRepository = settingRepository;
            _dbContextScopeFactory = dbContextScopeFactory;
            _aggregateRepository = aggregateRepository;
        }
        public IList<Setting> LoadAllSettings()
        {
            List<Setting> settings = new List<Setting>();
            using (var scope = _dbContextScopeFactory.Create())
            {
                settings = scope.Repository<Setting>().GetAll().ToList();
            }
            return settings;
        }

        public Setting GetSettingByName(string name)
        {
            return _settingRepository.GetFirst(new { Name = name });
        }
        public string GetSettingByKey(string key, string defaultValue = "")
        {
            var setting =  _settingRepository.GetFirst(new { Name = key });
            if (setting == null)
                return defaultValue;
            return setting.Value;
        }
        

        public Setting GetSettingById(int id)
        {
            return _settingRepository.GetFirst(new { Id = id });
        }

        public int AddNewSetting(Setting setting)
        {
            if (setting == null)
            {
                return 0;
            }
            int newId = 0;
            using (var scope = _dbContextScopeFactory.Create())
            {
                scope.Repository<Setting>().Insert(setting, out newId);
                return newId;
            }
        }

        public bool Update(Setting setting)
        {
            if (setting == null)
                return false;
            using (var scope = _dbContextScopeFactory.Create())
            {
                var currentData = scope.Repository<Setting>().GetByObject(new { Id = setting.Id });
                if (currentData == null)
                {
                    return false;
                }
                bool result = false;
                var param = new { setting.Value, setting.Description };
                result = scope.Repository<Setting>().Update(currentData, param, currentData.Id);

                return result;
            }
        }

        public bool Delete(int id)
        {
            if (id < 1)
                return false;

            using (var scope = _dbContextScopeFactory.Create())
            {
                var currentData = scope.Repository<Setting>().GetByObject(new { Id = id });
                if (currentData == null)
                {
                    return false;
                }

                bool result = false;
                result = scope.Repository<Setting>().Delete(currentData);
                return result;
            }
        }

        public IPagedList<Setting> SearchSettings(string name = "", string value = "",
          int pageIndex = 0, int pageSize = int.MaxValue, bool countTotal = true)
        {
            int totalResultsCount = 0, filteredResultsCount = 0;
            var settings = _aggregateRepository.FindSettings(pageIndex * pageSize, pageSize, out totalResultsCount, out filteredResultsCount, countTotal, 
                name,value);
            return new PagedList<Setting>(settings, pageIndex, pageSize, totalResultsCount);
        }
    }
}
