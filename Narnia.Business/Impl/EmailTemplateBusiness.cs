﻿using Narnia.Core.Domain;
using Narnia.Data.Base.Repositories;

namespace Narnia.Business.Impl
{
    public interface IEmailTemplateBusiness
    {
        EmailTemplate GetEmailTemplateByName(string name);
    }

    public class EmailTemplateBusiness : IEmailTemplateBusiness
    {
        private readonly IGenericRepository<EmailTemplate> _genericRepository;
        public EmailTemplateBusiness(IGenericRepository<EmailTemplate> genericRepository)
        {
            _genericRepository = genericRepository;
        }

        public EmailTemplate GetEmailTemplateByName(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                return null;
            }

            return _genericRepository.GetFirst(new { Name  = name});
        }
    }
}
