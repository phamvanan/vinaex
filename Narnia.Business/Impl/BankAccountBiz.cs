﻿using Narnia.Core;
using Narnia.Core.Domain;
using Narnia.Core.Models;
using Narnia.Data.Base;
using Narnia.Data.Repository.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Narnia.Business.Impl
{
    public class BankAccountBiz : IBankAccountBiz
    {
        private readonly IBankAccountRepository _bankAccountRepository;
        private readonly IDbContextScopeFactory _dbContextScopeFactory;
        private readonly IBanksRepository _banksRepository;

        public BankAccountBiz(IBankAccountRepository bankAccountRepository,
            IDbContextScopeFactory dbContextScopeFactory,
            IBanksRepository banksRepository)
        {
            _bankAccountRepository = bankAccountRepository;
            _dbContextScopeFactory = dbContextScopeFactory;
            _banksRepository = banksRepository;
        }
        public List<BankAccount> GetAvailableSystemBankAccounts()
        {
            return _bankAccountRepository.GetWhere(new { UserId = -1, IsActived = true,IsDeleted = false }).ToList();
        }
        public List<BankAccountModel> GetListByUserId(int userId)
        {
            return _bankAccountRepository.GetListByUserId(userId);
        }

        public List<BankAccountModel> GetListAccountAdmin()
        {
            return _bankAccountRepository.GetListByUserId(CommonConst.UserIdAdmin);
        }

        public ResponseData CreateBankAccount(BankAccount model)
        {
            if (model.UserId < 1 || model.BankId < 1) return new ResponseData().Error("Thông tin không hợp lệ");
            var banks = _banksRepository.GetById(model.BankId);
            if (banks == null || banks.Id < 1)
            {
                return new ResponseData().Error("Thông tin không hợp lệ");
            }
            if (string.IsNullOrEmpty(model.AccountName) || string.IsNullOrEmpty(model.AccountNo))
            {
                return new ResponseData().Error("thông tin tài khoản chưa nhập");
            }

            var entityData = _bankAccountRepository.GetByUserIdAccountNo(model.UserId, model.AccountNo);
            if (entityData != null) return new ResponseData().Error("thông tin tài khoản đã tồn tại trong hệ thống");

            model.CreatedDate = DateTime.Now;
            model.IsActived = true;
            model.IsDeleted = false;
            model.DeletedDate = (DateTime?)null;
            int newid = 0;
            var oK = _bankAccountRepository.Insert(model, out newid);
            if (oK)
            {
                return new ResponseData().Success(model);
            }
            else
            {
                return new ResponseData().Error("Thêm tài khoản thất bại.");
            }
        }

        public ResponseData CreateBankAccountAdmin(BankAccount model)
        {
            if (model.BankId < 1) return new ResponseData().Error("Thông tin không hợp lệ");

            var banks = _banksRepository.GetById(model.BankId);
            if (banks == null || banks.Id < 1)
            {
                return new ResponseData().Error("Thông tin không hợp lệ");
            }
            if (string.IsNullOrEmpty(model.AccountName) || string.IsNullOrEmpty(model.AccountNo))
            {
                return new ResponseData().Error("thông tin tài khoản chưa nhập");
            }

            var entityData = _bankAccountRepository.GetByUserIdAccountNo(model.UserId, model.AccountNo);
            if (entityData != null) return new ResponseData().Error("thông tin tài khoản đã tồn tại trong hệ thống");

            model.CreatedDate = DateTime.Now;
            model.IsActived = model.IsActived;
            model.IsDeleted = false;
            model.DeletedDate = (DateTime?)null;
            int newid = 0;
            var oK = _bankAccountRepository.Insert(model, out newid);
            if (oK)
            {
                return new ResponseData().Success(model);
            }
            else
            {
                return new ResponseData().Error("Thêm tài khoản thất bại.");
            }
        }
        public ResponseData UpdateBankAccountAdmin(BankAccount model)
        {
            if (model.BankId < 1) return new ResponseData().Error("Thông tin không hợp lệ");

            var banks = _banksRepository.GetById(model.BankId);
            if (banks == null || banks.Id < 1)
            {
                return new ResponseData().Error("Thông tin không hợp lệ");
            }

            if (string.IsNullOrEmpty(model.AccountName) || string.IsNullOrEmpty(model.AccountNo))
            {
                return new ResponseData().Error("thông tin tài khoản chưa nhập");
            }
            var bankAccount = _bankAccountRepository.GetById(model.Id);
            if (bankAccount == null || bankAccount.IsDeleted == true)
            {
                return new ResponseData().Error(CommonConst.NotFoundData);
            }
            bankAccount.IsActived = model.IsActived;
            bankAccount.AccountName = model.AccountName;
            bankAccount.AccountNo = model.AccountNo;
            bankAccount.BankId = model.BankId;
            bankAccount.Branch = model.Branch;

            //var entityData = _bankAccountRepository.GetByUserIdAccountNo(model.UserId, model.AccountNo);
            //if (entityData != null) return new ResponseData().Error("thông tin tài khoản đã tồn tại trong hệ thống");
            var oK = _bankAccountRepository.Update(bankAccount);
            if (oK)
            {
                return new ResponseData().Success(bankAccount);
            }
            else
            {
                return new ResponseData().Error("Cập nhật tài khoản thất bại.");
            }
        }

        public List<BankAccountAdminPagingModel> GetBankAccountAdminPagings(int pageIndex, int pageSize, string orderBy, out int totalRecords)
        {
            return _bankAccountRepository.GetBankAccountAdminPagings(pageIndex, pageSize, orderBy, out totalRecords);
        }

        public BankAccount GetById(int id)
        {
            return _bankAccountRepository.GetById(id);
        }
    }
}