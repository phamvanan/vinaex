﻿using Narnia.Core;
using Narnia.Core.Domain;
using Narnia.Core.Models;
using Narnia.Data.Repository.Contracts;
using System;
using System.Collections.Generic;

namespace Narnia.Business.Impl
{
    public class BanksBiz : IBanksBiz
    {
        private readonly IBanksRepository _banksRepository;

        public BanksBiz(IBanksRepository banksRepository)
        {
            _banksRepository = banksRepository;
        }

        public List<Banks> GetList()
        {
            return _banksRepository.GetList();
        }

        public List<BankAdminPagingModel> GetBankAdminPagings(int pageIndex, int pageSize, string orderBy,
            out int totalRecords)
        {
            return _banksRepository.GetBankAdminPagings(pageIndex, pageSize, orderBy, out totalRecords);
        }

        public ResponseData CreateBank(Banks model)
        {
            var entityData = _banksRepository.GetByCode(model.BankCode);
            if (entityData != null) return new ResponseData().Error(CommonConst.DataBankExists);

            model.CreatedDate = DateTime.Now;
            model.IsDeleted = false;
            model.DeletedDate = (DateTime?)null;
            int newid = 0;
            var oK = _banksRepository.Insert(model, out newid);
            if (oK)
            {
                return new ResponseData().Success(model);
            }
            else
            {
                return new ResponseData().Error(CommonConst.CreateBankNotSuccess);
            }
        }

        public ResponseData UpdateBank(Banks model)
        {
            var entity = _banksRepository.GetById(model.Id);
            if (entity == null) return new ResponseData().Error(CommonConst.NotFoundData);
            var entityData = _banksRepository.GetByCode(model.BankCode);
            //if (entityData != null) return new ResponseData().Error(CommonConst.DataBankExists);

            if (entityData != null && entityData.Id != model.Id) return new ResponseData().Error(CommonConst.DataBankExists);
            entity.BankCode = model.BankCode;
            entity.BankName = model.BankName;
            entity.Description = model.Description;

            var oK = _banksRepository.Update(entity);
            if (oK)
            {
                return new ResponseData().Success(entity);
            }
            else
            {
                return new ResponseData().Error(CommonConst.UpdateBankNotSuccess);
            }
        }

        public ResponseData DeleteBank(Banks model)
        {
            var entityData = _banksRepository.GetById(model.Id);
            if (entityData != null) return new ResponseData().Error(CommonConst.NotFoundData);

            var oK = _banksRepository.Update(entityData);
            if (oK)
            {
                return new ResponseData().Success(entityData);
            }
            else
            {
                return new ResponseData().Error(CommonConst.DeleteBankNotSuccess);
            }
        }

        public Banks GetById(int Id)
        {
            return _banksRepository.GetById(Id);
        }
    }
}