﻿using Narnia.Core;
using Narnia.Core.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Business.Impl
{
    public interface ICommonBiz
    {
        IPagedList<AssetBalance> FindAssetBalances(string userName = null, IList<string> assetTypeIds = null,
          int pageIndex = 0, int pageSize = int.MaxValue, bool countTotal = true);
    }
}
