﻿using Narnia.Business.Localization;
using Narnia.Core.Caching;
using Narnia.Core.Const;
using Narnia.Core.Domain;
using Narnia.Core.Enums;
using Narnia.Data.Base.Repositories;
using Narnia.Data.Repository.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Narnia.Business.Impl
{
    public class CoinSymbolBiz : ICoinSymbolBiz
    {
        private readonly IGenericRepository<Currency> _currencyRepository;
        private readonly ICoinSymbolRepository _coinSymbolRepository;
        private readonly ICacheManager _cacheManager;

        public CoinSymbolBiz(ICoinSymbolRepository coinSymbolRepository, IGenericRepository<Currency> currencyRepository, ICacheManager cacheManager)
        {
            _coinSymbolRepository = coinSymbolRepository;
            _currencyRepository = currencyRepository;
            _cacheManager = cacheManager;
        }

        public IList<CoinSymbol> GetAll(bool loadFromCache = true)
        {
            Func<IList<CoinSymbol>> loadSymbolsFunc = () => {
                return _coinSymbolRepository.GetAll().ToList();
            };

            IList<CoinSymbol> symbols;
            if (loadFromCache)
            {
                //cacheable copy
                var key = CommonCacheKeys.SymbolsAllCacheKey;
                symbols = _cacheManager.Get(key, () =>
                {
                    var result = loadSymbolsFunc();
                    return result;
                });
            }
            else
            {
                symbols = loadSymbolsFunc();
            }
            return symbols;
        }
        public CoinSymbol GetSymbolById(int symbolId)
        {
            return _coinSymbolRepository.GetByObject(new { Id = symbolId });
        }
        public CoinSymbol GetSymbolByName(string assetIdBase)
        {
            return _coinSymbolRepository.GetWhere(new { AssetIdBase = assetIdBase }).FirstOrDefault(); 
        }
        public Currency GetCurrencyByCode(string currencyCode)
        {
            return _currencyRepository.GetWhere(new { CurrencyCode = currencyCode }).FirstOrDefault();
        }
    }
}