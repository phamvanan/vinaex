﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Narnia.Core;
using Narnia.Core.Domain;
using Narnia.Data.Base;
using Narnia.Data.Repository.Contracts;

namespace Narnia.Business.Impl
{
    public class CommonBiz : ICommonBiz
    {
        private readonly IAggregateRepository _aggregateRepository;
        private readonly IDbContextScopeFactory _dbContextScopeFactory;
        public CommonBiz(IAggregateRepository aggregateRepository, IDbContextScopeFactory dbContextScopeFactory)
        {
            _aggregateRepository = aggregateRepository;
            _dbContextScopeFactory = dbContextScopeFactory;
        }

        public IPagedList<AssetBalance> FindAssetBalances(string userName = null, IList<string> assetTypeIds = null,
          int pageIndex = 0, int pageSize = int.MaxValue, bool countTotal = true)
        {
            if (assetTypeIds != null && assetTypeIds.Contains("all"))
                assetTypeIds = null;

            int totalResultsCount = 0, filteredResultsCount = 0;
            var balances = _aggregateRepository.FindAssetBalances(pageIndex * pageSize, pageSize, out totalResultsCount, out filteredResultsCount,countTotal,
                userName, assetTypeIds);
            return new PagedList<AssetBalance>(balances, pageIndex, pageSize, totalResultsCount);
        }
    }
}
