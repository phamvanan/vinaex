﻿using Narnia.Core.Domain;
using Narnia.Data.Base;
using Narnia.Data.Base.Repositories;
using Narnia.Data.Repository.Contracts;
using System.Collections.Generic;
using System.Linq;

namespace Narnia.Business.Impl
{
    public interface IFeedbackBusiness
    {
        IList<Feedback> GetRatingByUserId(int id);
        IList<Feedback> GetTopFeedbacks(int top);
        bool AddFeedback(Feedback feedback);
    }
    public class FeedbackBusiness : IFeedbackBusiness
    {
        private readonly IGenericRepository<Feedback> _feedBackRepository;
        private readonly IAggregateRepository _aggregateRepository;
        private readonly IDbContextScopeFactory _dbContextScopeFactory;
        public FeedbackBusiness(IGenericRepository<Feedback> feedBackRepository, IAggregateRepository aggregateRepository, IDbContextScopeFactory dbContextScopeFactory)
        {
            _feedBackRepository = feedBackRepository;
            _aggregateRepository = aggregateRepository;
            _dbContextScopeFactory = dbContextScopeFactory;
        }

        public IList<Feedback> GetRatingByUserId(int userid)
        {
            if (userid < 1)
            {
                return null;
            }

            var result = _feedBackRepository.GetWhere(new { UserId = userid });

            return result != null? result.ToList(): null;
        }
        public IList<Feedback> GetTopFeedbacks(int top)
        {
            return _aggregateRepository.GetTopFeedbacks(top);
        }
        public bool AddFeedback(Feedback feedback)
        {
            return _feedBackRepository.Insert(feedback);
        }
    }
}
