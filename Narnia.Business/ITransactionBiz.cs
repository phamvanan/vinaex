﻿using Narnia.Core;
using Narnia.Core.Domain;
using Narnia.Core.Enums;
using Narnia.Core.Models;
using System;
using System.Collections.Generic;

namespace Narnia.Business
{
    public interface ITransactionBiz
    {
        IList<Transaction> GetAllTransactions();

        ResponseData<Transaction> GetTransactionForEvidence(int transId, int userId);

        ResponseData<Transaction> CreateEvidencce(Evidence evidence, string transcode, int userId);

        ResponseData<Transaction> BuyerCancel(int transId, int buyerId);

        ResponseData AdminConfirm(Transaction entityInput);

        ResponseData<Evidence> GetEvidenceById(int Id);

        ResponseData<Transaction> AddTransactionForSell(Transaction transaction);

        ResponseData<Transaction> AddTransactionForBuy(Transaction transaction);

        ResponseData<IList<Transaction>> GetTransactionsByUserId(int userId);

        ResponseData<InfoAdvertisementDetailModel> GetDetailTrans(int transId);

        IList<TransactionStatisticModel> GetStatistic(int userId);

        List<TransactionAdminSearchModel> SearchAdmin(int pageIndex, int pageSize, int tranMode, string AssetTypeId,
            int Status, string orderBy,
            DateTime? FromDate, DateTime? ToDate, string transactionCode, out int totalRecords);

        TransactionDetailAdminModel GetDetailAdmin(int transactionId);

        ResponseData<InfoAdvertisementDetailModel> GetDetailTransByCode(string transCode);

        ResponseData<Transaction> CreateDepositEvidence(Evidence evidence, out int evidenceId);

        IList<TransactionHistoryModel> GetTransactionHistoryByType(int userId, TransactionStatusMode statusMode,
            int take, int skip, out int totalRecords);

        ResponseData SellerConfirm(int userId, string transCode);

        IPagedList<Transaction> SearchTransactions(DateTime? createdFromUtc = null, DateTime? createdToUtc = null,string transactionCode = null,
          IList<int> statuses = null, IList<string> assetTypeIds = null, string userName = "",
          int pageIndex = 0, int pageSize = int.MaxValue, bool countTotal = true);

        ResponseData<Transaction> GetTransactionById(int tranId);
        int[] GetValuesValidToExclude(int transactionStatus);
        IPagedList<TransactionHistoryUserModel> GetTransactionHistoryForUser(int userId, TransactionStatusClient transactionStatusClient, int pageIndex, int pageSize);
    }
}