﻿using System;
using System.Collections.Generic;
using System.Text;
using Narnia.Core.Domain2;

namespace Narnia.Business.Trading
{
    public class OrderMatchingService : IOrderMatchingService
    {
        public OrderComparisonResult CompareOrders(Order firstOrder, Order secondOrder)
        {
            throw new NotImplementedException();
        }

        public Transaction GenerateTransactionForTwoOrders(OrderComparisonResult comparisonResult)
        {
            throw new NotImplementedException();
        }

        public IList<Order> GetMatchingOrders(decimal price, int orderTypeId, int paidAssetTypeId, int offerAssetTypeId, bool? isMarketOrder)
        {
            throw new NotImplementedException();
        }

        public Order ProcessTransaction(Transaction transaction)
        {
            throw new NotImplementedException();
        }

        public OrderValidationResult ValidateOrder(Order order)
        {
            //The customer must have sufficient assets to process the order. If not, the order is Suspended
            // (it may be re-actived if funds become available later.) If order passes validation:
            //a: the order status is changed to Active
            //b: the assets needed to pay for the order are added to the Frozen balance

            return null;
        }

        public bool IsMatch(Order item1, Order item2)
        {
            //Check if both are in the same type => not match. 
            //Will implement later.

            if(item1.IsBuyOrder)
            {
                return item2.Price <= item1.Price;
            }
            else
            {
                return item2.Price >= item1.Price;
            }
        }
    }
}
