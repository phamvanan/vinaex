﻿using Narnia.Core.Domain;
using Narnia.Core.Domain2;
using Narnia.Data.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Business.Trading
{
    public class TempTrade
    {
        private readonly IOrderMatchingService _orderMatchingService;
        private readonly IDbContextScopeFactory _dbContextScopeFactory;
        public TempTrade(IDbContextScopeFactory dbContextScopeFactory, IOrderMatchingService orderMatchingService)
        {
            _dbContextScopeFactory = dbContextScopeFactory;
            _orderMatchingService = orderMatchingService;
        }
        public void PlaceBuy(BuyRequest request, int? sellOrderId)
        {
            var order = new Order();

            order.UserId = 1;
            order.OrderTypeId = 1;
            order.OrderStatusId = 1;
            order.OriginalOrderId = 0;

            order.Quantity = request.Quantity;
            order.Price = request.Price;

            order.TakeProfitOrderId = 0;
            order.StopLossOrderId = 0;
            order.CreatedOn = DateTime.Now;
            order.UpdatedOn = null;
            order.ExpiredOn = null;
            order.OfferAssetTypeId = 0;
            order.PaidAssetTypeId = 0;

            using (var scope = _dbContextScopeFactory.Create())
            {
                var sellOrder = scope.Repository<Order>().GetById(sellOrderId.Value);
                if(sellOrder != null)
                {
                    var validationResult = _orderMatchingService.ValidateOrder(sellOrder);
                    if(validationResult!=null)
                    {
                        var matched = _orderMatchingService.IsMatch(order, sellOrder);
                        if(matched)
                        {
                            Order splittedOrder = null;
                            if(order.Quantity < sellOrder.Quantity)
                            {
                                splittedOrder = sellOrder.ShallowCopy();
                                splittedOrder.Id = 0;
                                splittedOrder.OriginalOrderId = sellOrder.Id;

                                splittedOrder.Quantity = order.Quantity;

                                //The remaining order is the sellOrder
                                sellOrder.Quantity = sellOrder.Quantity - splittedOrder.Quantity;
                            }
                            //Split order
                            Core.Domain2.Transaction transaction = new Core.Domain2.Transaction();

                        }
                    }
                }
            }
        }
    }

    public class BuyRequest
    {
        public decimal Quantity { get; set; }
        public decimal Price { get; set; }
        public decimal MinPrice { get; set; }
        public decimal MaxPrice { get; set; }
        public double MinNumber { get; set; }
        public double MaxNumber { get; set; }
        public int PaymentAllowTime { get; set; }
        public DateTime CreatedDate { get; set; }
        public bool Approved { get; set; }

        public User AdvertiseUser { get; set; }
        public CoinSymbol CoinType { get; set; }
        
        public PaymentMethod PayType { get; set; }

        public string BankAccountName { get; set; }

        public string BankAccountNo { get; set; }

    }
    public class SellRequest
    {
    }
}
