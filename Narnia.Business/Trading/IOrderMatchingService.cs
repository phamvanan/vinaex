﻿using Narnia.Core.Domain2;
using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Business.Trading
{
    public interface IOrderMatchingService
    {
        OrderValidationResult ValidateOrder(Order order);
        IList<Order> GetMatchingOrders(decimal price, int orderTypeId, int paidAssetTypeId, int offerAssetTypeId, bool? isMarketOrder);
        OrderComparisonResult CompareOrders(Order firstOrder, Order secondOrder);
        Transaction GenerateTransactionForTwoOrders(OrderComparisonResult comparisonResult);
        Order ProcessTransaction(Transaction transaction);
        bool IsMatch(Order item1, Order item2);
    }
}
