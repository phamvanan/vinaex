﻿using Narnia.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Business
{
    public interface IBaseBusiness
    {
        ResponseData Process(Func<ResponseData> action);
        ResponseData<T> Process<T>(Func<ResponseData<T>> action);
    }
    public class BaseBusiness
    {
        public ResponseData Process(Func<ResponseData> action)
        {
            var response = new ResponseData();
            try
            {
                return action();
            }
            catch (Exception e)
            {
                response.HasError = true;
                response.Message = e.Message;
            }

            return response;
        }

        public ResponseData<T> Process<T>(Func<ResponseData<T>> action)
        {
            var response = new ResponseData<T>();
            try
            {
                return action();
            }
            catch (Exception e)
            {
                response.HasError = true;
                response.Message = e.Message;
            }

            return response;
        }
    }
}
