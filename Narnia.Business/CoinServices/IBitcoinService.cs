﻿using NBitcoin;
using QBitNinja.Client.Models;
using System.Collections.Generic;
using Narnia.Business.CoinServices;

namespace Narnia.Business
{
    public interface IBitcoinService : ICoinService
    {
        string CreateWallet(string password, string walletFilePath);
        //bool Unlock(string fromAddress, string passwordToUnlock, int? duration = null);
        bool Transfer(string fromAddress, string passwordToUnlock, string toAddress, decimal amount);
        //TransactionReceipt TransferAndWaitForPolling(string fromAddress, string passwordToUnlock, string toAddress, decimal amount);
        decimal GetAccountBalance(string password, string walletFilePath);
        Dictionary<uint256, List<BalanceOperation>> GetAllTransactionsByAddress(string password, string walletFilePath);

        //string DeployContract(string abi, string contractByteCode, string from, params object[] values);
    }
}
