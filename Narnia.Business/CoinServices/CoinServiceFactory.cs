﻿using Narnia.Core.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Business.CoinServices
{
    public class CoinServiceFactory
    {
        public ICoinService GetService(string coinType)
        {
            switch (coinType)
            {
                case "BTC":
                    return EngineContext.Current.Resolve<IBitcoinService>();
                case "USDT":
                    return EngineContext.Current.Resolve<IUsdtService>();
                case "ETH":
                    return EngineContext.Current.Resolve<IEthService>();
                default:
                    return null;
            }
        }
        public List<ICoinService> GetAvalableServices(List<string> coinTypes)
        {
            List<ICoinService> retVal = new List<ICoinService>();
            if(coinTypes != null)
            {
                foreach(var type in coinTypes)
                {
                    var service = GetService(type);
                    if(service != null)
                    {
                        retVal.Add(service);
                    }
                }
            }
            return retVal;
        }
    }
}
