﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net;
using System.Text;
using Narnia.Core.Models;
using Newtonsoft.Json;

namespace Narnia.Business.CoinServices
{
    public interface ICoinPaymentService
    {
        CoinPaymentResponse CreateWithdraw(decimal amount, string address, string currency = "ETH", string currency2 = "ETH", string autoConfirm = "1");
        CoinPaymentResponse CoinBalance();

        CoinPaymentResponse CreateTransaction(decimal amount, string currency1, string currency2, string buyerEmail,
            string walletAddress = "", string ipnUrl = "");
    }
    public class CoinPaymentService : ICoinPaymentService
    {
        //reference link: https://www.coinpayments.net/apidoc-create-withdrawal

        private string privatekey = "0CeDD7eA2add0620f5778a42c2ee6f275E76B9dF7836d3AD139d21eEcb41f7f7";
        private string publicKey = "c01c5acde0485e227f8051b9ab9d0ad55f58b14285c03e42985917b1f9e9f282";

        private static readonly Encoding encoding = Encoding.UTF8;

        public CoinPaymentResponse CreateWithdraw(decimal amount, string address, string currency = "ETH", string currency2 = "ETH", string autoConfirm = "1")
        {
            SortedList<string, string> parms = new SortedList<string, string>();
            parms["amount"] = amount.ToString();
            parms["currency"] = currency;
            parms["currency2"] = currency2;
            parms["auto_confirm"] = autoConfirm;
            parms["address"] = address;
            parms["cmd"] = "create_withdrawal";

            var ret = CallAPI(parms);

            return ret;
        }

        public CoinPaymentResponse CoinBalance()
        {
            SortedList<string, string> parms = new SortedList<string, string>();
            parms["all"] = "0";
            parms["cmd"] = "balances";

            var ret = CallAPI(parms);

            return ret;
        }

        /// <summary>
        /// Create a transaction 
        /// </summary>
        /// <param name="amount">The amount of the transaction in the original currency</param>
        /// <param name="currency1">The original currency of the transaction</param>
        /// <param name="currency2"></param>
        /// <param name="buyerEmail"></param>
        /// <param name="walletAddress"></param>
        /// <param name="ipnUrl"></param>
        /// <returns></returns>
        public CoinPaymentResponse CreateTransaction(decimal amount, string currency1, string currency2, string buyerEmail, string walletAddress = "", string ipnUrl = "")
        {
            SortedList<string, string> parms = new SortedList<string, string>();
            parms["amount"] = amount.ToString(CultureInfo.InvariantCulture);
            parms["currency1"] = currency1;
            parms["currency2"] = currency2;
            parms["address"] = walletAddress;
            parms["cmd"] = "create_transaction";
            parms["ipn_url"] = ipnUrl;
            var ret = CallAPI(parms);

            return ret;
        }

        private CoinPaymentResponse CallAPI(SortedList<string, string> parms)
        {
            parms["version"] = "1";
            parms["key"] = publicKey;

            string postData = "";
            foreach (KeyValuePair<string, string> param in parms)
            {
                if (postData.Length > 0) { postData += "&"; }
                postData += param.Key + "=" + Uri.EscapeDataString(param.Value);
            }

            byte[] keyBytes = encoding.GetBytes(privatekey);
            byte[] postBytes = encoding.GetBytes(postData);
            var hmacsha512 = new System.Security.Cryptography.HMACSHA512(keyBytes);
            string hmac = BitConverter.ToString(hmacsha512.ComputeHash(postBytes)).Replace("-", string.Empty);

            // do the post:
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            System.Net.WebClient webClient = new System.Net.WebClient();
            webClient.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
            webClient.Headers.Add("HMAC", hmac);
            webClient.Encoding = encoding;

            var response = new CoinPaymentResponse();
            try
            {
                string resp = webClient.UploadString("https://www.coinpayments.net/api.php", postData);
                response = JsonConvert.DeserializeObject<CoinPaymentResponse>(resp);
            }
            catch (System.Net.WebException e)
            {
                //ret["error"] = "Exception while contacting CoinPayments.net: " + e.Message;
            }
            catch (Exception e)
            {
                //ret["error"] = "Unknown exception: " + e.Message;
            }

            return response;
        }
    }
}
