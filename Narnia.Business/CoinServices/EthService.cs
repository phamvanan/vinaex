﻿using Nethereum.Hex.HexTypes;
using Nethereum.RPC.Eth.DTOs;
using Nethereum.RPC.TransactionReceipts;
using Nethereum.Web3;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using Polly;

namespace Narnia.Business.CoinServices
{
    public class EthService : IEthService
    {
        private const int NumberOfRetries = 3;
        private const string RpcUrl = "http://localhost:8545/";
        public EthService()
        {

        }
        public string CreateWallet(string password)
        {
            var web3 = new Web3(RpcUrl);
            var newWalletAddress = string.Empty;
            try
            {
                newWalletAddress = Policy.Handle<Exception>()
                    .WaitAndRetry(NumberOfRetries,
                        retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)),
                        (exception, timeSpan) =>
                        {
                            Console.WriteLine($"CreateWallet error content {exception.Message}");
                        })
                    .Execute(() => web3.Personal.NewAccount.SendRequestAsync(password).Result);
            }
            catch (Exception ex)
            {
            }
            return newWalletAddress;
        }

        public bool Unlock(string fromAddress, string passwordToUnlock, int? duration = null)
        {
            var web3 = new Web3(RpcUrl);
            if (!duration.HasValue)
                duration = 1200;

            var unlockResul = Policy.Handle<Exception>()
                .WaitAndRetry(NumberOfRetries,
                    retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)),
                    (exception, timeSpan) =>
                    {
                        Console.WriteLine($"Unlock error content {exception.Message}");
                    })
                .Execute(() => web3.Personal.UnlockAccount.SendRequestAsync(fromAddress, passwordToUnlock, (ulong)duration.Value).Result);

            return unlockResul;
        }
        public string Transfer(string fromAddress, string passwordToUnlock, string toAddress, decimal amount)
        {
            var web3 = new Web3(RpcUrl);
            var unlockResult = Policy.Handle<Exception>()
                .WaitAndRetry(NumberOfRetries,
                    retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)),
                    (exception, timeSpan) =>
                    {
                        Console.WriteLine($"Transfer error content {exception.Message}");
                    })
                .Execute(() => web3.Personal.UnlockAccount.SendRequestAsync(fromAddress, passwordToUnlock, new HexBigInteger(120)).Result);
            TransactionInput tran = null;

            if (unlockResult)
            {
                var gasPrice = GetGasPrice(); //web3.Eth.GasPrice.SendRequestAsync().Result;
                var valueToTransfer = Web3.Convert.ToWei(amount, fromUnit: Nethereum.Util.UnitConversion.EthUnit.Ether);

                tran = new TransactionInput(null, toAddress, fromAddress, new HexBigInteger(21000), gasPrice, new HexBigInteger(valueToTransfer));

                var sendTranTask = Policy.Handle<Exception>()
                    .WaitAndRetry(NumberOfRetries,
                        retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)),
                        (exception, timeSpan) =>
                        {
                            Console.WriteLine($"CreateWallet error content {exception.Message}");
                        })
                    .Execute(() => web3.Eth.TransactionManager.SendTransactionAsync(tran).Result);

                return sendTranTask;
            }

            return string.Empty;
        }

        public TransactionReceipt TransferAndWaitForPolling(string fromAddress, string passwordToUnlock, string toAddress, decimal amount)
        {
            var web3 = new Web3(RpcUrl);
            var unlockResult = Policy.Handle<Exception>()
                .WaitAndRetry(NumberOfRetries,
                    retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)),
                    (exception, timeSpan) =>
                    {
                        Console.WriteLine($"TransferAndWaitForPolling error content {exception.Message}");
                    })
                .Execute(() => web3.Personal.UnlockAccount.SendRequestAsync(fromAddress, passwordToUnlock, 12000).Result);
            TransactionInput tran = null;
            if (unlockResult)
            {
                //var gasPrice = web3.Eth.GasPrice.SendRequestAsync().Result;
                var gasPrice = GetGasPrice();
                var valueToTransfer = Policy.Handle<Exception>()
                    .WaitAndRetry(NumberOfRetries,
                        retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)),
                        (exception, timeSpan) =>
                        {
                            Console.WriteLine($"TransferAndWaitForPolling Convert.ToWei error content {exception.Message}");
                        })
                    .Execute(() => Web3.Convert.ToWei(amount, fromUnit: Nethereum.Util.UnitConversion.EthUnit.Ether));

                tran = new TransactionInput(null, toAddress, fromAddress, new HexBigInteger(21000), gasPrice, new HexBigInteger(valueToTransfer));

                var sendTranTask = Policy.Handle<Exception>()
                    .WaitAndRetry(NumberOfRetries,
                        retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)),
                        (exception, timeSpan) =>
                        {
                            Console.WriteLine($"SendTransactionAsync Convert.ToWei error content {exception.Message}");
                        })
                    .Execute(() => web3.Eth.TransactionManager.SendTransactionAsync(tran).Result);

                var pollingService = new TransactionReceiptPollingService(web3.TransactionManager);

                return Policy.Handle<Exception>()
                    .WaitAndRetry(NumberOfRetries,
                        retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)),
                        (exception, timeSpan) =>
                        {
                            Console.WriteLine($"SendRequestAndWaitForReceiptAsync Convert.ToWei error content {exception.Message}");
                        })
                    .Execute(() => pollingService.SendRequestAndWaitForReceiptAsync(tran).Result);
            }

            return null;
        }

        public decimal GetAccountBalance(string walletAddress)
        {
            var web3 = new Web3(RpcUrl);
            var amountResult = Policy.Handle<Exception>()
                .WaitAndRetry(NumberOfRetries,
                    retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)),
                    (exception, timeSpan) =>
                    {
                        Console.WriteLine($"GetAccountBalance GetBalance error content {exception.Message}");
                    })
                .Execute(() => web3.Eth.GetBalance.SendRequestAsync(walletAddress).Result);

            var amount = Policy.Handle<Exception>()
                .WaitAndRetry(NumberOfRetries,
                    retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)),
                    (exception, timeSpan) =>
                    {
                        Console.WriteLine($"GetAccountBalance Convert.FromWei error content {exception.Message}");
                    })
                .Execute(() => Web3.Convert.FromWei(amountResult));

            return amount;
        }

        public List<Transaction> GetAllTransactionsByAddress(string address)
        {
            var web3 = new Web3(RpcUrl);

            var now = DateTime.Now;
            var minTimeStamp = now.AddDays(-10);

            long epochTicks = new DateTime(1970, 1, 1).Ticks;
            long unixTimeStamp = ((minTimeStamp.Ticks - epochTicks) / TimeSpan.TicksPerSecond);

            var syncingOutput = Policy.Handle<Exception>()
                .WaitAndRetry(NumberOfRetries,
                    retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)),
                    (exception, timeSpan) =>
                    {
                        Console.WriteLine($"GetAllTransactionsByAddress error content {exception.Message}");
                    })
                .Execute(() => web3.Eth.Syncing.SendRequestAsync().Result);
            var currentBlock = syncingOutput.CurrentBlock == null ? 0 : syncingOutput.CurrentBlock.Value;
            var highestBlock = syncingOutput.HighestBlock == null ? 0 : syncingOutput.HighestBlock.Value;

            if (!syncingOutput.IsSyncing)
            {
                currentBlock = Policy.Handle<Exception>()
                    .WaitAndRetry(NumberOfRetries,
                        retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)),
                        (exception, timeSpan) =>
                        {
                            Console.WriteLine($"GetAllTransactionsByAddress GetBlockNumber error content {exception.Message}");
                        })
                    .Execute(() => web3.Eth.Blocks.GetBlockNumber.SendRequestAsync().Result);
            }

            if (currentBlock < highestBlock)
            {
                //Show warning here;
                //Console.WriteLine("*** Warning! Node is not synced: {0}{1}", Environment.NewLine, syncingOutput);
            }
            var blockNum = currentBlock;
            List<Transaction> transactions = new List<Transaction>();
            while (true && blockNum >= 0)
            {
                var block = Policy.Handle<Exception>()
                    .WaitAndRetry(NumberOfRetries,
                        retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)),
                        (exception, timeSpan) =>
                        {
                            Console.WriteLine($"GetAllTransactionsByAddress Convert.FromWei error content {exception.Message}");
                        })
                    .Execute(() => web3.Eth.Blocks.GetBlockWithTransactionsByNumber.SendRequestAsync(new HexBigInteger(blockNum)).Result);
                if (block.Timestamp.Value < new HexBigInteger(unixTimeStamp).Value)
                    break;

                transactions.AddRange(block.Transactions.Where(tran => tran.From == address || tran.To == address));
                --blockNum;
            }
            return transactions;
        }

        public string DeployContract(string abi, string contractByteCode, string from, params object[] values)
        {
            var web3 = new Web3(RpcUrl);
            var gasUsed = Policy.Handle<Exception>()
                .WaitAndRetry(NumberOfRetries,
                    retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)),
                    (exception, timeSpan) =>
                    {
                        Console.WriteLine($"DeployContract EstimateGas error content {exception.Message}");
                    })
                .Execute(() => web3.Eth.DeployContract.EstimateGasAsync(abi, contractByteCode, from).Result);

            var transactionHash = Policy.Handle<Exception>()
                .WaitAndRetry(NumberOfRetries,
                    retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)),
                    (exception, timeSpan) =>
                    {
                        Console.WriteLine(
                            $"DeployContract DeployContract.SendRequestAsync error content {exception.Message}");
                    })
                .Execute(() => web3.Eth.DeployContract.SendRequestAsync(abi, contractByteCode, from, gasUsed).Result);

            return transactionHash;
        }

        public BigInteger GetLatestBlock()
        {
            try
            {
                HexBigInteger latestBlock = new HexBigInteger(0);
                var web3 = new Web3(RpcUrl);
                var syncingObj = Policy.Handle<Exception>()
                    .WaitAndRetry(NumberOfRetries,
                        retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)),
                        (exception, timeSpan) =>
                        {
                            Console.WriteLine(
                                $"GetLatestBlock Syncing content {exception.Message}");
                        })
                    .Execute(() => web3.Eth.Syncing.SendRequestAsync().Result);
                if (syncingObj != null && syncingObj.IsSyncing)
                {
                    latestBlock = syncingObj.CurrentBlock;
                }
                else
                {
                    latestBlock = Policy.Handle<Exception>()
                        .WaitAndRetry(NumberOfRetries,
                            retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)),
                            (exception, timeSpan) =>
                            {
                                Console.WriteLine(
                                    $"GetLatestBlock GetBlockNumber error content {exception.Message}");
                            })
                        .Execute(() => web3.Eth.Blocks.GetBlockNumber.SendRequestAsync().Result);
                }
                return latestBlock.Value;
            }
            catch (Exception ex)
            {
                return 5000000;
            }
        }

        public HexBigInteger GetGasPrice()
        {
            var web3 = new Web3(RpcUrl);
            var gasPrice = Policy.Handle<Exception>()
                .WaitAndRetry(NumberOfRetries,
                    retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)),
                    (exception, timeSpan) =>
                    {
                        Console.WriteLine(
                            $"GetGasPrice error content {exception.Message}");
                    })
                .Execute(() => web3.Eth.GasPrice.SendRequestAsync().Result);
            if (gasPrice.Value == 0)
                gasPrice = new HexBigInteger(11000000000);
            return gasPrice;
        }

        public decimal TransferFee()
        {

            return Web3.Convert.FromWei(GetGasPrice().Value * 21000);
        }
    }
}
