﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Numerics;
using HBitcoin.KeyManagement;
using Narnia.Business.BTC;
using Narnia.Business.CoinServices;
using NBitcoin;
using Newtonsoft.Json.Linq;
using QBitNinja.Client;
using QBitNinja.Client.Models;
using static Narnia.Business.BTC.QBitNinjaJutsus;

namespace Narnia.Business
{
    public class BitcoinService : IBitcoinService
    {
        public string CreateWallet(string password, string walletFilePath)
        {
            var newWallet = string.Empty;
            try
            {
                Mnemonic mnemonic;
                Safe safe = Safe.Create(out mnemonic, password, walletFilePath, Config.Network);
            }
            catch (Exception ex)
            {
                newWallet = ex.Message;
            }
            return newWallet;
        }

        public decimal GetAccountBalance(string password, string walletFilePath)
        {
            var safe = Safe.Load(password, walletFilePath);
            // 0. Query all operations, grouped by addresses
            Dictionary<BitcoinAddress, List<BalanceOperation>> operationsPerAddresses = QueryOperationsPerSafeAddresses(safe, 7);
            // 1. Get all address history record with a wrapper class
            var addressHistoryRecords = new List<AddressHistoryRecord>();
            foreach (var elem in operationsPerAddresses)
            {
                foreach (var op in elem.Value)
                {
                    addressHistoryRecords.Add(new AddressHistoryRecord(elem.Key, op));
                }
            }
            // 2. Calculate wallet balances
            Money confirmedWalletBalance;
            Money unconfirmedWalletBalance;
            GetBalances(addressHistoryRecords, out confirmedWalletBalance, out unconfirmedWalletBalance);
            var walletBalance = confirmedWalletBalance.ToDecimal(MoneyUnit.BTC);

            return walletBalance;
        }

        public Dictionary<uint256, List<BalanceOperation>> GetAllTransactionsByAddress(string password, string walletFilePath)
        {
            var safe = Safe.Load(password, walletFilePath);
            // 0. Query all operations, grouped our used safe addresses
            Dictionary<BitcoinAddress, List<BalanceOperation>> operationsPerAddresses = QueryOperationsPerSafeAddresses(safe);
            Dictionary<uint256, List<BalanceOperation>> operationsPerTransactions = GetOperationsPerTransactions(operationsPerAddresses);
            return operationsPerTransactions;
        }

        public string CreateWallet(string password)
        {
            throw new NotImplementedException();
        }
        public decimal GetAccountBalance(string walletAddress)
        {
            throw new NotImplementedException();
        }

        public BigInteger GetLatestBlock()
        {
            throw new NotImplementedException();
        }

        public bool Transfer(string walletFilePath, string passwordToUnlock, string toAddress, decimal amount)
        {
            var success = false;
            BitcoinAddress addressToSend;
            try
            {
                addressToSend = BitcoinAddress.Create(toAddress, Config.Network);

                var safe = Safe.Load(passwordToUnlock, walletFilePath);
                // 0. Query all operations, grouped our used safe addresses
                Dictionary<BitcoinAddress, List<BalanceOperation>> operationsPerAddresses = QueryOperationsPerSafeAddresses(safe);
                // 1. Gather all the not empty private keys
                var operationsPerNotEmptyPrivateKeys = new Dictionary<BitcoinExtKey, List<BalanceOperation>>();
                foreach (var elem in operationsPerAddresses)
                {
                    var balance = Money.Zero;
                    foreach (var op in elem.Value) balance += op.Amount;
                    if (balance > Money.Zero)
                    {
                        var secret = safe.FindPrivateKey(elem.Key);
                        operationsPerNotEmptyPrivateKeys.Add(secret, elem.Value);
                    }
                }
                // 2. Get the script pubkey of the change.
                Script changeScriptPubKey = null;
                Dictionary<BitcoinAddress, List<BalanceOperation>> operationsPerChangeAddresses = QueryOperationsPerSafeAddresses(safe, minUnusedKeys: 1, hdPathType: HdPathType.Change);
                foreach (var elem in operationsPerChangeAddresses)
                {
                    if (elem.Value.Count == 0)
                        changeScriptPubKey = safe.FindPrivateKey(elem.Key).ScriptPubKey;
                }
                if (changeScriptPubKey == null)
                    throw new ArgumentNullException();

                // 3. Gather coins can be spend
                Dictionary<Coin, bool> unspentCoins = GetUnspentCoins(operationsPerNotEmptyPrivateKeys.Keys);

                // 4. Get the fee
                Money fee;
                try
                {
                    var txSizeInBytes = 250;
                    using (var client = new HttpClient())
                    {
                        const string request = @"https://bitcoinfees.21.co/api/v1/fees/recommended";
                        var result = client.GetAsync(request, HttpCompletionOption.ResponseContentRead).Result;
                        var json = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                        var fastestSatoshiPerByteFee = json.Value<decimal>("fastestFee");
                        fee = new Money(fastestSatoshiPerByteFee * txSizeInBytes, MoneyUnit.Satoshi);
                    }
                }
                catch
                {
                    //Exit("Couldn't calculate transaction fee, try it again later.");
                    //throw new Exception("Can't get tx fee");
                    return false;

                }
                // 5. How much money we can spend?
                Money availableAmount = Money.Zero;
                Money unconfirmedAvailableAmount = Money.Zero;
                foreach (var elem in unspentCoins)
                {
                    // If can spend unconfirmed add all
                    if (Config.CanSpendUnconfirmed)
                    {
                        availableAmount += elem.Key.Amount;
                        if (!elem.Value)
                            unconfirmedAvailableAmount += elem.Key.Amount;
                    }
                    // else only add confirmed ones
                    else
                    {
                        if (elem.Value)
                        {
                            availableAmount += elem.Key.Amount;
                        }
                    }
                }
                // 6. How much to spend?
                Money amountToSend = new Money(amount, MoneyUnit.BTC);
                // 7. Do some checks
                if (amountToSend < Money.Zero || availableAmount < amountToSend + fee)
                    //new Exception("Not enough coins.");
                    return false;

                decimal feePc = Math.Round((100 * fee.ToDecimal(MoneyUnit.BTC)) / amountToSend.ToDecimal(MoneyUnit.BTC));


                var confirmedAvailableAmount = availableAmount - unconfirmedAvailableAmount;
                var totalOutAmount = amountToSend + fee;
                if (confirmedAvailableAmount < totalOutAmount)
                {
                    var unconfirmedToSend = totalOutAmount - confirmedAvailableAmount;
                    //WriteLine();
                    //WriteLine($"In order to complete this transaction you have to spend {unconfirmedToSend.ToDecimal(MoneyUnit.BTC).ToString("0.#############################")} unconfirmed btc.");
                    //ConsoleKey response = GetYesNoAnswerFromUser();
                    //if (response == ConsoleKey.N)
                    //{
                    //    Exit("User interruption.");
                    //}
                }
                // 8. Select coins
                //WriteLine("Selecting coins...");
                var coinsToSpend = new HashSet<Coin>();
                var unspentConfirmedCoins = new List<Coin>();
                var unspentUnconfirmedCoins = new List<Coin>();
                foreach (var elem in unspentCoins)
                    if (elem.Value) unspentConfirmedCoins.Add(elem.Key);
                    else unspentUnconfirmedCoins.Add(elem.Key);

                bool haveEnough = SelectCoins(ref coinsToSpend, totalOutAmount, unspentConfirmedCoins);
                if (!haveEnough)
                    haveEnough = SelectCoins(ref coinsToSpend, totalOutAmount, unspentUnconfirmedCoins);
                if (!haveEnough)
                    //throw new Exception("Not enough funds.");
                    return false;

                // 9. Get signing keys
                var signingKeys = new HashSet<ISecret>();
                foreach (var coin in coinsToSpend)
                {
                    foreach (var elem in operationsPerNotEmptyPrivateKeys)
                    {
                        if (elem.Key.ScriptPubKey == coin.ScriptPubKey)
                            signingKeys.Add(elem.Key);
                    }
                }
                // 10. Build the transaction
                //WriteLine("Signing transaction...");
                var builder = new TransactionBuilder();
                var tx = builder
                    .AddCoins(coinsToSpend)
                    .AddKeys(signingKeys.ToArray())
                    .Send(addressToSend, amountToSend)
                    .SetChange(changeScriptPubKey)
                    .SendFees(fee)
                    .BuildTransaction(true);

                if (!builder.Verify(tx))
                    //throw new Exception("Couldn't build the transaction.");
                    return false;

                //WriteLine($"Transaction Id: {tx.GetHash()}");

                var qBitClient = new QBitNinjaClient(Config.Network);

                // QBit's success response is buggy so let's check manually, too		
                BroadcastResponse broadcastResponse;
                
                var tried = 0;
                var maxTry = 7;
                do
                {
                    tried++;
                    //WriteLine($"Try broadcasting transaction... ({tried})");
                    broadcastResponse = qBitClient.Broadcast(tx).Result;
                    var getTxResp = qBitClient.GetTransaction(tx.GetHash()).Result;
                    if (getTxResp == null)
                    {
                        System.Threading.Thread.Sleep(3000);
                        continue;
                    }
                    else
                    {
                        success = true;
                        break;
                    }
                } while (tried <= maxTry);
            }
            catch (Exception ex)
            {
                success = false;
            }

            return success;
        }

        public bool Unlock(string address, string passwordToUnlock, int? duration = null)
        {
            throw new NotImplementedException();
        }

        string ICoinService.Transfer(string fromAddress, string passwordToUnlock, string toAddress, decimal amount)
        {
            throw new NotImplementedException();
        }
    }
}
