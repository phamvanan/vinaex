﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace Narnia.Business.CoinServices
{
    public interface ICoinService
    {
        string CreateWallet(string password);
        bool Unlock(string address, string passwordToUnlock, int? duration = null);
        string Transfer(string fromAddress, string passwordToUnlock, string toAddress, decimal amount);
        decimal GetAccountBalance(string walletAddress);
        BigInteger GetLatestBlock();
    }
}
