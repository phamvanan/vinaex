﻿using Narnia.Business.CoinServices;
using Nethereum.Hex.HexTypes;
using Nethereum.RPC.Eth.DTOs;
using System.Collections.Generic;
using System.Numerics;

namespace Narnia.Business.CoinServices
{
    public interface IEthService : ICoinService
    {
        TransactionReceipt TransferAndWaitForPolling(string fromAddress, string passwordToUnlock, string toAddress, decimal amount);
        List<Transaction> GetAllTransactionsByAddress(string address);
        string DeployContract(string abi, string contractByteCode, string from, params object[] values);
        HexBigInteger GetGasPrice();
        decimal TransferFee();
    }
}
