﻿using Narnia.Core.Domain;
using System.Collections.Generic;

namespace Narnia.Business
{
    public interface ICoinSymbolBiz
    {
        IList<CoinSymbol> GetAll(bool loadFromCache = true);
        CoinSymbol GetSymbolById(int symbolId);
        CoinSymbol GetSymbolByName(string assetIdBase);
        Currency GetCurrencyByCode(string currencyCode);
    }
}