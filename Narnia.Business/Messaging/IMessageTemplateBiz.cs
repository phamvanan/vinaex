﻿using Narnia.Core.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Business.Messaging
{
    public partial interface IMessageTemplateBiz
    {
        void DeleteMessageTemplate(EmailTemplate messageTemplate);

        void InsertMessageTemplate(EmailTemplate messageTemplate);

        void UpdateMessageTemplate(EmailTemplate messageTemplate);

        EmailTemplate GetMessageTemplateById(int messageTemplateId);

        IList<EmailTemplate> GetMessageTemplatesByName(string messageTemplateName);

        
        IList<EmailTemplate> GetAllMessageTemplates();

        EmailTemplate CopyMessageTemplate(EmailTemplate messageTemplate);
    }
}
