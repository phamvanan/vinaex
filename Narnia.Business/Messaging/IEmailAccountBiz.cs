﻿using Narnia.Core.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Business.Messaging
{
    public interface IEmailAccountBiz
    {
        void InsertEmailAccount(EmailAccount emailAccount);

        void UpdateEmailAccount(EmailAccount emailAccount);

        void DeleteEmailAccount(EmailAccount emailAccount);

        EmailAccount GetEmailAccountById(int emailAccountId);

        IList<EmailAccount> GetAllEmailAccounts();
        EmailAccount GetDefaultEmailAccount();
    }
}
