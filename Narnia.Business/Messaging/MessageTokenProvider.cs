﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Narnia.Core.Domain;

namespace Narnia.Business.Messaging
{
    public class MessageTokenProvider : IMessageTokenProvider
    {
        private Dictionary<string, IEnumerable<string>> _allowedTokens;
        public MessageTokenProvider()
        { }
        protected Dictionary<string, IEnumerable<string>> AllowedTokens
        {
            get
            {
                if (_allowedTokens != null)
                    return _allowedTokens;

                _allowedTokens = new Dictionary<string, IEnumerable<string>>();

                //user tokens
                _allowedTokens.Add(TokenGroupNames.UserTokens, new[]
                {
                    "%User.Email%",
                    "%User.Username%",
                    "%User.FullName%",
                    "%User.FirstName%",
                    "%User.LastName%",
                    "%NewMember.ActivationLink%",
                });

                //transaction tokens
                _allowedTokens.Add(TokenGroupNames.TransactionTokens, new[]
                {
                    "%Transaction.TransactionCode%",
                    "%Transaction.Status%"
                });

                return _allowedTokens;
            }
        }
        public IEnumerable<string> GetTokenGroups(EmailTemplate messageTemplate)
        {
            switch (messageTemplate.Name)
            {
                case MessageTemplateSystemNames.UserWelcomeMessage:
                    return new[] { TokenGroupNames.UserTokens };

                case MessageTemplateSystemNames.TransactionCancelled:
                case MessageTemplateSystemNames.TransactionConfirmed:
                case MessageTemplateSystemNames.TransactionNewlyCreated:
                case MessageTemplateSystemNames.TransactionUploadEvidence:
                    return new[] { TokenGroupNames.UserTokens, TokenGroupNames.TransactionTokens};

                default:
                    return new string[] { };
            }
        }
        public virtual IEnumerable<string> GetListOfAllowedTokens(IEnumerable<string> tokenGroups = null)
        {
            var allowedTokens = AllowedTokens.Where(x => tokenGroups == null || tokenGroups.Contains(x.Key))
                .SelectMany(x => x.Value).ToList();

            return allowedTokens.Distinct();
        }

    }
}
