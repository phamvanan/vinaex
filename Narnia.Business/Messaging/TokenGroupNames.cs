﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Business.Messaging
{
    public static class TokenGroupNames
    {
        public const string UserTokens = "User tokens";
        public const string OfferTokens = "Offer tokens";
        public const string TransactionTokens = "Transaction tokens";
    }
}
