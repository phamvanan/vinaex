﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Business.Messaging
{
    public static class MessageTemplateSystemNames
    {
        public const string UserWelcomeMessage = "User.WelcomeMessage";

        public const string TransactionCancelled = "Transaction.Cancelled";
        public const string TransactionConfirmed = "Transaction.Confirmed";
        public const string TransactionUploadEvidence = "Transaction.UploadEvidence";
        public const string TransactionNewlyCreated = "Transaction.NewlyCreated";
    }
}
