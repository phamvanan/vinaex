﻿using Narnia.Core.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Business.Messaging
{
    public partial interface IMessageTokenProvider
    {
        IEnumerable<string> GetListOfAllowedTokens(IEnumerable<string> tokenGroups = null);
        IEnumerable<string> GetTokenGroups(EmailTemplate messageTemplate);
    }
}
