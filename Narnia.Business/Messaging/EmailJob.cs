﻿using Narnia.Business.Impl;
using Narnia.Core;
using Narnia.Core.Domain;
using Narnia.Core.Enums;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Narnia.Business.Messaging
{
    public class EmailJob
    {
        private readonly ITokenizer _tokenizer;
        private readonly IEmailSender _emailSender;
        private readonly IEmailAccountBiz _emailAccountBiz;
        private EmailAccount _adminEmailAccount;
        private readonly IEmailTemplateBusiness _emailTemplateBusiness;
        private readonly ISiteSetting _siteSetting;

        public EmailJob(IEmailSender emailSender,
                        ITokenizer tokenizer,
                        IEmailAccountBiz emailAccountBiz,
                        IEmailTemplateBusiness emailTemplateBusiness,
                        ISiteSetting siteSetting)
        {
            _emailSender = emailSender;
            _tokenizer = tokenizer;
            _emailAccountBiz = emailAccountBiz;
            _emailTemplateBusiness = emailTemplateBusiness;
            _adminEmailAccount = _emailAccountBiz.GetDefaultEmailAccount();
            _siteSetting = siteSetting;
        }

        public Task SendActivationEmail(User user, string activationLink)
        {
            string mailBody = "";
            var emailTemplate = _emailTemplateBusiness.GetEmailTemplateByName(MessageTemplateSystemNames.UserWelcomeMessage);
            if (emailTemplate == null)
            {
                return Task.CompletedTask;
            }

            var email = new Token("NewMember.Email", user.Email);
            var activationLinkToken = new Token("NewMember.ActivationLink", activationLink);
            mailBody = emailTemplate.Body;
            var emailBody = _tokenizer.Replace(mailBody, new List<Token> { activationLinkToken, email }, true);
            emailBody = CommonHelper.ReplaceRelativeUrlByAbsoluteUrl(emailBody, _siteSetting.BaseUri);
            if (_adminEmailAccount != null)
                return _emailSender.SendEmailAsync(_adminEmailAccount, emailTemplate.Subject, emailBody, _adminEmailAccount.Email, "Admin", user.Email, user.UserName + user.LastName);

            return Task.CompletedTask;
        }

        public Task SendTransaction(User user, Transaction tran, SendEmailType type)
        {
            string emailTemplateName = string.Empty;
            var tokens = new List<Token>();
            switch (type)
            {
                case SendEmailType.NewTransactionForAdsSell:
                case SendEmailType.NewTransactionForAdsBuy:
                    emailTemplateName = MessageTemplateSystemNames.TransactionNewlyCreated;
                    tokens.AddRange(new List<Token>(){ new Token("Email", user.Email), new Token("NewTransactionCode", tran.TransactionCode) });
                    break;
                case SendEmailType.TransactionCancel:
                    emailTemplateName = MessageTemplateSystemNames.TransactionCancelled;
                    tokens.AddRange(new List<Token>() {new Token("TransactionCode", tran.TransactionCode) });
                    break;
                case SendEmailType.TransactionConfirm:
                    emailTemplateName = MessageTemplateSystemNames.TransactionConfirmed;
                    tokens.AddRange(new List<Token>() { new Token("TransactionCode", tran.TransactionCode) });
                    break;
                case SendEmailType.TransactionUploadEvidence:
                    emailTemplateName = MessageTemplateSystemNames.TransactionUploadEvidence;
                    tokens.AddRange(new List<Token>() { new Token("TransactionCode", tran.TransactionCode) });
                    break;
            }

            if (string.IsNullOrEmpty(emailTemplateName))
                return Task.CompletedTask;

            string mailBody = string.Empty;
            var emailTemplate = _emailTemplateBusiness.GetEmailTemplateByName(emailTemplateName);
            if (emailTemplate == null)
            {
                return Task.CompletedTask;
            }

            mailBody = emailTemplate.Body;
            mailBody = _tokenizer.Replace(mailBody, tokens, true);
            mailBody = CommonHelper.ReplaceRelativeUrlByAbsoluteUrl(mailBody, _siteSetting.BaseUri);
            if (_adminEmailAccount != null)
                return _emailSender.SendEmailAsync(_adminEmailAccount, emailTemplate.Subject, mailBody, _adminEmailAccount.Email, "Vinaex", user.Email, user.Alias);

            return Task.CompletedTask;
        }
    }
}
