﻿using Narnia.Core;
using Narnia.Core.Domain;
using Narnia.Core.Infrastructure;
using Narnia.Data;
using Narnia.Data.Base;
using Narnia.Data.Base.Repositories;
using Narnia.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Narnia.Business.Messaging
{
    public class EmailAccountBiz : IEmailAccountBiz
    {
        private readonly IGenericRepository<EmailAccount> _emailAccountRepository;
        private readonly IDbContextScopeFactory _dbContextScopeFactory;
        public EmailAccountBiz(IGenericRepository<EmailAccount> emailAccountRepository, IDbContextScopeFactory dbContextScopeFactory)
        {
            _emailAccountRepository = emailAccountRepository;
            _dbContextScopeFactory = dbContextScopeFactory;
        }
        public void DeleteEmailAccount(EmailAccount emailAccount)
        {
            if (emailAccount == null)
                throw new ArgumentNullException("emailAccount");

            if (GetAllEmailAccounts().Count == 1)
                throw new Exception("You cannot delete this email account. At least one account is required.");
            using (var scope = _dbContextScopeFactory.Create())
            {
                var result = scope.Repository<EmailAccount>().Delete(emailAccount);
            }
        }

        public IList<EmailAccount> GetAllEmailAccounts()
        {
            return _emailAccountRepository.GetAll().ToList();
        }

        public EmailAccount GetEmailAccountById(int emailAccountId)
        {
            if (emailAccountId == 0)
                return null;

            return _emailAccountRepository.GetByObject(new { Id = emailAccountId });
        }

        public void InsertEmailAccount(EmailAccount emailAccount)
        {
            if (emailAccount == null)
                throw new ArgumentNullException(nameof(emailAccount));
            using (var scope = _dbContextScopeFactory.Create())
            {
                int newId = 0;
                emailAccount.Email = emailAccount.Email.EnsureNotNull().Trim();
                emailAccount.DisplayName = emailAccount.DisplayName.EnsureNotNull().Trim();
                emailAccount.Host = emailAccount.Host.EnsureNotNull().Trim();
                emailAccount.Username = emailAccount.Username.EnsureNotNull().Trim();
                emailAccount.Password = emailAccount.Password.EnsureNotNull().Trim();

                var result = scope.Repository<EmailAccount>().Insert(emailAccount, out newId);
                emailAccount.Id = newId;
            }
        }

        public void UpdateEmailAccount(EmailAccount emailAccount)
        {
            using (var scope = _dbContextScopeFactory.Create())
            {
                var result = scope.Repository<EmailAccount>().Update(emailAccount);
            }
        }
        public EmailAccount GetDefaultEmailAccount()
        {
            return _emailAccountRepository.GetFirst(new { IsActive = true });
        }
    }
}
