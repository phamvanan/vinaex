﻿using Narnia.Core.Domain;
using Narnia.Data.Base.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Narnia.Business.Messaging
{
     public partial class MessageTemplateBiz: IMessageTemplateBiz
    {
        #region Fields

        private readonly IGenericRepository<EmailTemplate> _messageTemplateRepository;
        
        public MessageTemplateBiz(IGenericRepository<EmailTemplate> messageTemplateRepository)
        {
            this._messageTemplateRepository = messageTemplateRepository;
        }

        public virtual void DeleteMessageTemplate(EmailTemplate messageTemplate)
        {
            this._messageTemplateRepository.Delete(new { Id = messageTemplate.Id });
        }

        public virtual void InsertMessageTemplate(EmailTemplate messageTemplate)
        {
            if (messageTemplate == null)
                throw new ArgumentNullException(nameof(messageTemplate));

            _messageTemplateRepository.Insert(messageTemplate);
        }

        public virtual void UpdateMessageTemplate(EmailTemplate messageTemplate)
        {
            if (messageTemplate == null)
                throw new ArgumentNullException(nameof(messageTemplate));

            _messageTemplateRepository.Update(messageTemplate);
        }

        public virtual EmailTemplate GetMessageTemplateById(int messageTemplateId)
        {
            if (messageTemplateId == 0)
                return null;

            return _messageTemplateRepository.GetById(messageTemplateId);
        }

        public virtual IList<EmailTemplate> GetMessageTemplatesByName(string messageTemplateName)
        {
            throw new Exception("Not implemented yet");
        }

        /// <summary>
        /// Gets all message templates
        /// </summary>
        /// <param name="storeId">Store identifier; pass 0 to load all records</param>
        /// <returns>Message template list</returns>
        public virtual IList<EmailTemplate> GetAllMessageTemplates()
        {
            return _messageTemplateRepository.GetAll().ToList();
        }

        public virtual EmailTemplate CopyMessageTemplate(EmailTemplate messageTemplate)
        {
            if (messageTemplate == null)
                throw new ArgumentNullException(nameof(messageTemplate));

            var mtCopy = new EmailTemplate
            {
                Name = messageTemplate.Name,
                Subject = messageTemplate.Subject,
                Body = messageTemplate.Body,
                CreatedDate = DateTime.Now
            };

            InsertMessageTemplate(mtCopy);

            return mtCopy;
        }

        #endregion
    }
}

