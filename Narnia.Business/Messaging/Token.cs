﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Business.Messaging
{
    public sealed class Token
    {
        private readonly string _key;
        private readonly object _value;
        private readonly bool _neverHtmlEncoded;

        public Token(string key, object value) : this(key, value, false)
        {
        }

        public Token(string key, object value, bool neverHtmlEncoded)
        {
            this._key = key;
            this._value = value;
            this._neverHtmlEncoded = neverHtmlEncoded;
        }
        public string Key { get { return _key; } }
        public object Value { get { return _value; } }
        public bool NeverHtmlEncoded { get { return _neverHtmlEncoded; } }
        public override string ToString()
        {
            return $"{Key}: {Value}";
        }
    }
}
