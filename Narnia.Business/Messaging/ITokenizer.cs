﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Business.Messaging
{
    public partial interface ITokenizer
    {
        string Replace(string template, IEnumerable<Token> tokens, bool htmlEncode);
    }
}
