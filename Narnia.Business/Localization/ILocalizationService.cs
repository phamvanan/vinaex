﻿using Narnia.Core.Domain.Localization;
using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Business.Localization
{
    public interface ILocalizationService
    {
        void DeleteLocaleStringResource(LocaleStringResource localeStringResource);

        LocaleStringResource GetLocaleStringResourceById(int localeStringResourceId);

        LocaleStringResource GetLocaleStringResourceByName(string resourceName);

        LocaleStringResource GetLocaleStringResourceByName(string resourceName, int languageId);

        IList<LocaleStringResource> GetAllResources(int languageId);

        void InsertLocaleStringResource(LocaleStringResource localeStringResource);

        
        void UpdateLocaleStringResource(LocaleStringResource localeStringResource);

        Dictionary<string, KeyValuePair<int, string>> GetAllResourceValues(int languageId, bool? loadPublicLocales);

      
        string GetResource(string resourceKey);

        string GetResource(string resourceKey, int languageId,
            bool logIfNotFound = true, string defaultValue = "", bool returnEmptyIfNotFound = false);

        string ExportResourcesToXml(Language language);

        void ImportResourcesFromXml(Language language, string xml, bool updateExistingResources = true);
    }
}
