﻿using Narnia.Core.Domain.Localization;
using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Business.Localization
{
    public interface ILanguageService
    {
        void DeleteLanguage(Language language);

        IList<Language> GetAllLanguages(bool showHidden = false, bool loadFromCache = true);

        Language GetLanguageById(int languageId);
        Language GetLanguageByCulture(string culture);

        void InsertLanguage(Language language);

        void UpdateLanguage(Language language);
    }
}
