﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Narnia.Core.Caching;
using Narnia.Core.Domain.Localization;
using Narnia.Data.Base.Repositories;

namespace Narnia.Business.Localization
{
    public class LanguageService : ILanguageService
    {
        private readonly IGenericRepository<Language> _langRepository;
        private readonly ICacheManager _cacheManager;
        public LanguageService(IGenericRepository<Language> langRepository, ICacheManager cacheManager)
        {
            _langRepository = langRepository;
            _cacheManager = cacheManager;
        }
        public void DeleteLanguage(Language language)
        {
            _langRepository.Delete(language);
            _cacheManager.RemoveByPattern(LocalizationCacheKeys.LanguagesPatternCacheKey);
        }

        public IList<Language> GetAllLanguages(bool showHidden = false, bool loadFromCache = true)
        {
            Func<IList<Language>> loadLanguagesFunc = () =>
            {
                if (showHidden)
                    return _langRepository.GetAll().ToList();
                return _langRepository.GetWhere(new { Published = true }).ToList();
            };
            IList<Language> languages;
            if (loadFromCache)
            {
                //cacheable copy
                var key = string.Format(LocalizationCacheKeys.LanguagesAllCacheKey, showHidden);
                languages = _cacheManager.Get(key, () =>
                {
                    var result = loadLanguagesFunc();
                    return result;
                });
            }
            else
            {
                languages = loadLanguagesFunc();
            }
            return languages;
        }

        public Language GetLanguageById(int languageId)
        {
            return _langRepository.GetByObject(new { Id = languageId });
        }

        public Language GetLanguageByCulture(string culture)
        {
            return _langRepository.GetWhere(new { LanguageCulture = culture }).FirstOrDefault();
        }

        public void InsertLanguage(Language language)
        {
            _langRepository.Insert(language);
            _cacheManager.RemoveByPattern(LocalizationCacheKeys.LanguagesPatternCacheKey);
        }

        public void UpdateLanguage(Language language)
        {
            _langRepository.Update(language);
            _cacheManager.RemoveByPattern(LocalizationCacheKeys.LanguagesPatternCacheKey);
        }
    }
}
