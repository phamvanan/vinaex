﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using Dapper;
using Narnia.Core;
using Narnia.Core.Caching;
using Narnia.Core.Domain.Localization;
using Narnia.Data.Base;
using Narnia.Data.Base.Repositories;

namespace Narnia.Business.Localization
{
    public class LocalizationService : ILocalizationService
    {

        private readonly IWorkContext _workContext;
        private readonly IGenericRepository<LocaleStringResource> _lrsRepository;
        private readonly IDbContextScopeFactory _dbContextScopeFactory;
        private readonly ICacheManager _cacheManager;
        public LocalizationService(IGenericRepository<LocaleStringResource> lrsRepository, ICacheManager cacheManager, IWorkContext workContext, IDbContextScopeFactory dbContextScopeFactory)
        {
            _lrsRepository = lrsRepository;
            _cacheManager = cacheManager;
            _workContext = workContext;
            _dbContextScopeFactory = dbContextScopeFactory;
        }
        private static Dictionary<string, KeyValuePair<int, string>> ResourceValuesToDictionary(IEnumerable<LocaleStringResource> locales)
        {
            //format: <name, <id, value>>
            var dictionary = new Dictionary<string, KeyValuePair<int, string>>();
            foreach (var locale in locales)
            {
                var resourceName = locale.ResourceName.ToLowerInvariant();
                if (!dictionary.ContainsKey(resourceName))
                    dictionary.Add(resourceName, new KeyValuePair<int, string>(locale.Id, locale.ResourceValue));
            }
            return dictionary;
        }
        public void DeleteLocaleStringResource(LocaleStringResource localeStringResource)
        {
            if (localeStringResource == null)
                throw new ArgumentNullException(nameof(localeStringResource));

            _lrsRepository.Delete(localeStringResource);
            _cacheManager.RemoveByPattern(LocalizationCacheKeys.LocaleStringResourcesPatternCacheKey);
        }

        public string ExportResourcesToXml(Language language)
        {
            if (language == null)
                throw new ArgumentNullException(nameof(language));
            var sb = new StringBuilder();
            var stringWriter = new StringWriter(sb);
            var xmlWriter = new XmlTextWriter(stringWriter);
            xmlWriter.WriteStartDocument();
            xmlWriter.WriteStartElement("Language");
            xmlWriter.WriteAttributeString("Name", language.Name);

            var resources = GetAllResources(language.Id);
            foreach (var resource in resources)
            {
                xmlWriter.WriteStartElement("LocaleResource");
                xmlWriter.WriteAttributeString("Name", resource.ResourceName);
                xmlWriter.WriteElementString("Value", null, resource.ResourceValue);
                xmlWriter.WriteEndElement();
            }

            xmlWriter.WriteEndElement();
            xmlWriter.WriteEndDocument();
            xmlWriter.Close();
            return stringWriter.ToString();
        }

        public IList<LocaleStringResource> GetAllResources(int languageId)
        {
            var locales = _lrsRepository.GetWhere(new { LanguageId = languageId }).ToList();
            return locales;
        }

        /// <summary>
        /// Gets all locale string resources by language identifier
        /// </summary>
        /// <param name="languageId">Language identifier</param>
        /// <param name="loadPublicLocales">A value indicating whether to load data for the public site only (if "false", then for admin area only. If null, then load all locales.</param>
        /// <returns>Locale string resources</returns>
        public Dictionary<string, KeyValuePair<int, string>> GetAllResourceValues(int languageId, bool? loadPublicLocales)
        {
            var key = string.Format(LocalizationCacheKeys.LocaleStringResourcesAllCacheKey, languageId);

            //get all locale string resources by language identifier
            if (!loadPublicLocales.HasValue || _cacheManager.IsSet(key))
            {
                var rez = _cacheManager.Get(key, () =>
                {
                    var resourcesByLang = _lrsRepository.GetWhere(new { LanguageId = languageId });
               
                    return ResourceValuesToDictionary(resourcesByLang);
                });

                //remove separated resource 
                _cacheManager.Remove(string.Format(LocalizationCacheKeys.LocaleStringResourcesAllPublicCacheKey, languageId));
                _cacheManager.Remove(string.Format(LocalizationCacheKeys.LocaleStringResourcesAllAdminCacheKey, languageId));

                return rez;
            }

            //performance optimization of the site startup
            key = string.Format(loadPublicLocales.Value ? LocalizationCacheKeys.LocaleStringResourcesAllPublicCacheKey : LocalizationCacheKeys.LocaleStringResourcesAllAdminCacheKey, languageId);

            return _cacheManager.Get(key, () =>
            {
                string query = "";


                var resourcesByLang = _lrsRepository.GetWhere(new { LanguageId = languageId });
                return ResourceValuesToDictionary(resourcesByLang);
            });
        }

        public LocaleStringResource GetLocaleStringResourceById(int localeStringResourceId)
        {
            if (localeStringResourceId == 0)
                return null;

            return _lrsRepository.GetByObject(new { Id = localeStringResourceId});
        }

        public LocaleStringResource GetLocaleStringResourceByName(string resourceName)
        {

            if (_workContext.WorkingLanguage != null)
                return GetLocaleStringResourceByName(resourceName, _workContext.WorkingLanguage.Id);

            return null;
        }

        public LocaleStringResource GetLocaleStringResourceByName(string resourceName, int languageId)
        {
            var localeStringResource = _lrsRepository.GetWhere(new { ResourceName = resourceName, LanguageId = languageId }).FirstOrDefault();
            
            return localeStringResource;
        }

        public string GetResource(string resourceKey)
        {
            if (_workContext.WorkingLanguage != null)
                return GetResource(resourceKey, _workContext.WorkingLanguage.Id);

            return "";
        }

        public string GetResource(string resourceKey, int languageId, bool logIfNotFound = true, string defaultValue = "", bool returnEmptyIfNotFound = false)
        {
            var result = string.Empty;
            if (resourceKey == null)
                resourceKey = string.Empty;
            resourceKey = resourceKey.Trim().ToLowerInvariant();

            var resources = GetAllResourceValues(languageId, !resourceKey.StartsWith(LocalizationCacheKeys.AdminLocaleStringResourcesPrefix, StringComparison.InvariantCultureIgnoreCase));
            if (resources.ContainsKey(resourceKey))
            {
                result = resources[resourceKey].Value;
            }

            if (string.IsNullOrEmpty(result))
            {
                if (!string.IsNullOrEmpty(defaultValue))
                {
                    result = defaultValue;
                }
                else
                {
                    if (!returnEmptyIfNotFound)
                        result = resourceKey;
                }
            }
            return result;
        }

        public void ImportResourcesFromXml(Language language, string xml, bool updateExistingResources = true)
        {
            if (language == null)
                throw new ArgumentNullException(nameof(language));

            if (string.IsNullOrEmpty(xml))
                return;

            var inDoc = new XmlDocument();
            inDoc.LoadXml(xml);
            var sb = new StringBuilder();
            using (var xWriter = XmlWriter.Create(sb, new XmlWriterSettings { OmitXmlDeclaration = true }))
            {
                inDoc.Save(xWriter);
                xWriter.Close();
            }

            var outDoc = new XmlDocument();
            outDoc.LoadXml(sb.ToString());
            xml = outDoc.OuterXml;


            var parameters = new DynamicParameters();
            
            parameters.Add("@LanguageId", language.Id);
            parameters.Add("@XmlPackage", xml);
            parameters.Add("@UpdateExistingResources", updateExistingResources);

            using (var scope = _dbContextScopeFactory.Create())
            {
                scope.DbContext.OpenConnection().Execute(sql: "usp_LanguagePackImport", param: parameters, commandType: System.Data.CommandType.StoredProcedure, commandTimeout: 600, transaction: scope.DbContext.GetTransaction());
            }
            //clear cache
            _cacheManager.RemoveByPattern(LocalizationCacheKeys.LocaleStringResourcesPatternCacheKey);
        }

        public void InsertLocaleStringResource(LocaleStringResource localeStringResource)
        {
            if (localeStringResource == null)
                throw new ArgumentNullException(nameof(localeStringResource));

            _lrsRepository.Insert(localeStringResource);
            _cacheManager.RemoveByPattern(LocalizationCacheKeys.LocaleStringResourcesPatternCacheKey);
        }

        public void UpdateLocaleStringResource(LocaleStringResource localeStringResource)
        {
            if (localeStringResource == null)
                throw new ArgumentNullException(nameof(localeStringResource));

            _lrsRepository.Update(localeStringResource);
            _cacheManager.RemoveByPattern(LocalizationCacheKeys.LocaleStringResourcesPatternCacheKey);
        }
    }
}
