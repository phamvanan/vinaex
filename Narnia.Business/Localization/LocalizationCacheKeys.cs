﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Business.Localization
{
    public static class LocalizationCacheKeys
    {
        public static string LanguagesPatternCacheKey => "Narnia.language.";
        public static string LocaleStringResourcesAllPublicCacheKey => "Narnia.lsr.all.public-{0}";

        public static string LocaleStringResourcesAllCacheKey => "Narnia.lsr.all-{0}";

        public static string LocaleStringResourcesAllAdminCacheKey => "Narnia.lsr.all.admin-{0}";

        public static string LocaleStringResourcesByResourceNameCacheKey => "Narnia.lsr.{0}-{1}";

        public static string LocaleStringResourcesPatternCacheKey => "Narnia.lsr.";

        public static string AdminLocaleStringResourcesPrefix => "Admin.";

        public static string EnumLocaleStringResourcesPrefix => "Enums.";

        public static string PermissionLocaleStringResourcesPrefix => "Permission.";

        #region Languages
        public static string LanguagesAllCacheKey => "Narnia.language.all-{0}";
        #endregion
    }
}
