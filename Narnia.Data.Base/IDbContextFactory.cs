﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Data.Base
{
    public interface IDbContextFactory
    {
        IDbContext CreateDbContext();
    }
}
