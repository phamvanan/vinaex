﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Narnia.Data.Base
{
    public interface IDbContextScopeFactory
    {
        IDbContextScope Create(IsolationLevel isolationLevel = IsolationLevel.ReadCommitted);
    }
}
