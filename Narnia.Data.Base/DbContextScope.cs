﻿using Narnia.Data.Base.Repositories;
using System;
using System.Collections.Generic;
using System.Data;

namespace Narnia.Data.Base
{
    public class DbContextScope : IDbContextScope
    {
        private bool _disposed;
        private bool _completed;
        private IsolationLevel _isolationLevel = IsolationLevel.ReadCommitted;
        public IDbContext DbContext { get; private set; }
        public DbContextScope(IDbContextFactory dbContextFactory, IsolationLevel isolationLevel)
        {
            _disposed = false;
            _completed = false;
            DbContext = dbContextFactory.CreateDbContext();
            _isolationLevel = isolationLevel;
            DbContext.OpenConnection();
            DbContext.BeginTransaction(_isolationLevel);
        }

        private Dictionary<Type, object> _repositories = new Dictionary<Type, object>();
        public IGenericRepository<T> Repository<T>() where T : class,new()
        {
            return DbContext.Repository<T>();
        }

        public void Dispose()
        {
            if (_disposed)
                return;

            if (!_completed)
            {
                try
                {
                    DbContext.Commit();
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e);
                }
                finally
                {
                    _completed = true;
                }
            }
            DbContext.Dispose();
            _disposed = true;
        }
        
        public int SaveChanges()
        {
            if (_disposed)
                throw new ObjectDisposedException("DbContextScope");
            if (_completed)
                throw new InvalidOperationException("You cannot call SaveChanges() more than once on a DbContextScope.");
            DbContext.Commit();
            _completed = true;
            return 1;
        }
        public void Rollback()
        {
            if (_disposed)
                throw new ObjectDisposedException("DbContextScope");
            if (_completed)
                throw new InvalidOperationException("You cannot call Rollback() on a completed DbContextScope.");
            DbContext.Rollback();
            _completed = true;
        }
    }
}
