﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Narnia.Data.Base
{
    public class DbContextScopeFactory : IDbContextScopeFactory
    {
        private readonly IDbContextFactory _dbContextFactory;

        public DbContextScopeFactory(IDbContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public IDbContextScope Create(IsolationLevel isolationLevel = IsolationLevel.ReadCommitted)
        {
            return new DbContextScope(dbContextFactory: _dbContextFactory, isolationLevel: isolationLevel);
        }
    }
}
