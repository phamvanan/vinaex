﻿using Narnia.Data.Base.Repositories;
using System;
using System.Data;

namespace Narnia.Data.Base
{
    public interface IDbContext:IDisposable
    {
        IsolationLevel IsolationLevel { get; }
        IDbConnection OpenConnection();
        void Commit();
        void Rollback();
        void BeginTransaction(IsolationLevel isolationLevel = IsolationLevel.ReadCommitted);
        IDbTransaction GetTransaction();
        IGenericRepository<T> Repository<T>() where T : class, new();
    }
}
