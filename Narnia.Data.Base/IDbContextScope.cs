﻿using Narnia.Data.Base.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Data.Base
{
    public interface IDbContextScope : IDisposable
    {
        IDbContext DbContext { get;}
        void Rollback();
        int SaveChanges();
        IGenericRepository<T> Repository<T>() where T : class, new();
    }
}
