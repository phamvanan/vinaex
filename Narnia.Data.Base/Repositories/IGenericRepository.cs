﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Narnia.Data.Base.Repositories
{
    public interface IGenericRepository<T>:IRepository
    {
        T GetById<T1>(T1 id);
        T GetByObject(object filters);
        IEnumerable<T> SqlQuery(string sql, dynamic param = null, dynamic outParam = null);
        int ExecuteStore(string storedProcedure, dynamic param = null, dynamic outParam = null);
        /// <summary>
        /// uypc: query using store procedure
        /// </summary>
        /// <param name="storedProcedure"></param>
        /// <param name="param"></param>
        /// <param name="outParam"></param>
        /// <returns></returns>
        IEnumerable<T1> QueryStoreProc<T1>(string storedProcedure, dynamic param = null, dynamic outParam = null);

        /// <summary>
        /// uypc: get all data from a entity
        /// </summary>
        /// <returns></returns>
        IEnumerable<T> GetAll();

        /// <summary>
        /// uypc:
        /// </summary>
        /// <param name="filters"></param>
        /// <returns></returns>
        IEnumerable<T> GetWhere(object filters);

        /// <summary>
        /// uypc:
        /// </summary>
        /// <param name="filters"></param>
        /// <returns></returns>
        T GetFirst(object filters);

        T GetLast(object filters);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="instance"></param>
        /// <returns></returns>
        bool Insert(T instance);

        bool Insert(T instance, out int id);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        bool Delete(object key);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="instance"></param>
        /// <returns></returns>
        bool Update(T instance);

        bool Update(T instance, object param = null, object key = null);



        #region Async

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<T>> GetAllAsync();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filters"></param>
        /// <returns></returns>
        Task<IEnumerable<T>> GetWhereAsync(object filters);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filters"></param>
        /// <returns></returns>
        Task<T> GetFirstAsync(object filters);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="instance"></param>
        /// <returns></returns>
        Task<bool> UpdateAsync(T instance);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="instance"></param>
        /// <returns></returns>
        Task<bool> InsertAsync(T instance);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        Task<bool> DeleteAsync(object key);

        /// <summary>
        /// uypc: query asyncronous using store
        /// </summary>
        /// <param name="storedProcedure"></param>
        /// <param name="param"></param>
        /// <param name="outParam"></param>
        /// <returns></returns>
        Task<IEnumerable<T>> QueryStoreProcAsync(string storedProcedure, dynamic param = null,
            dynamic outParam = null);

        #endregion
    }
}
