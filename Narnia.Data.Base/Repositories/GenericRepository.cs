﻿using MicroOrm.Pocos.SqlGenerator;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;

namespace Narnia.Data.Base.Repositories
{
    public class GenericRepository<T> : IGenericRepository<T> where T : new()
    {
        public IDbContext DbContext { get; private set; }
        public ISqlGenerator<T> SqlGenerator { get; private set; }
        public GenericRepository(IDbContext dbContext)
        {
            DbContext = dbContext;
            SqlGenerator = new SqlGenerator<T>();
        }
        public GenericRepository()
        {
            SqlGenerator = new SqlGenerator<T>();
        }
        #region Repository sync base actions
        public  T GetById<T1>(T1 id)
        {
            var filter = new { Id = id };
            return GetByObject(filter);
        }
        public T GetByObject(object filters)
        {
            //Creates the query 
            var query = SqlGenerator.GetSelect(filters);

            //Execute the query
            return DbContext.OpenConnection().Query<T>(query, filters, transaction: DbContext.GetTransaction()).FirstOrDefault();
        }

        public IQueryable<T> Table<T1>()
        {
            //Creates the query 
            var query = SqlGenerator.GetSelectAll();

            //Execute the query
            return DbContext.OpenConnection().Query<T>(query, null, transaction: DbContext.GetTransaction()).AsQueryable();
        }

        public IEnumerable<T> SqlQuery(string sql, dynamic param = null, dynamic outParam = null)
        {
            return DbContext.OpenConnection().Query<T>(sql, param: (object)param, transaction: DbContext.GetTransaction(),
                commandType: CommandType.Text);
        }

        public int ExecuteStore(string storedProcedure, dynamic param = null, dynamic outParam = null)
        {
            var result = DbContext.OpenConnection().Execute(storedProcedure, param: (object)param, transaction: DbContext.GetTransaction(),
                commandType: CommandType.StoredProcedure);

            return result;
        }

        /// <summary>
        /// uypc: query using store procedure
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="storedProcedure"></param>
        /// <param name="param"></param>
        /// <param name="outParam"></param>
        /// <returns></returns>
        public virtual IEnumerable<T1> QueryStoreProc<T1>(string storedProcedure, dynamic param = null, dynamic outParam = null)
        {
            var output = DbContext.OpenConnection().Query<T1>(storedProcedure, param: (object)param, transaction: DbContext.GetTransaction(),
                commandType: CommandType.StoredProcedure);

            return output;
        }
   
        public virtual IEnumerable<T> GetAll()
        {
            var sql = SqlGenerator.GetSelectAll();
            return DbContext.OpenConnection().Query<T>(sql, transaction: DbContext.GetTransaction());
        }

        public virtual IEnumerable<T> GetWhere(object filters)
        {
            try
            {
                var sql = SqlGenerator.GetSelect(filters);
                var data = DbContext.OpenConnection().Query<T>(sql, filters, DbContext.GetTransaction());
                return data;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public virtual T GetFirst(object filters)
        {
            return this.GetWhere(filters).FirstOrDefault();
        }

        public virtual T GetLast(object filters)
        {
            return this.GetWhere(filters).LastOrDefault();
        }

        public virtual bool Insert(T instance)
        {
            bool added = false;
            var sql = SqlGenerator.GetInsert();

            if (SqlGenerator.IsIdentity)
            {
                var newId = DbContext.OpenConnection().Query<int>(sql, instance, transaction: DbContext.GetTransaction()).SingleOrDefault();
                added = newId > 0;

                if (added)
                {
                    var newParsedId = Convert.ChangeType(newId, SqlGenerator.IdentityProperty.PropertyInfo.PropertyType);
                    SqlGenerator.IdentityProperty.PropertyInfo.SetValue(instance, newParsedId);
                }
            }
            else
            {
                added = DbContext.OpenConnection().Execute(sql, instance, transaction: DbContext.GetTransaction()) > 0;
            }

            return added;
        }

        public virtual bool Insert(T instance, out int id)
        {
            bool added = false;
            var sql = SqlGenerator.GetInsert();

            if (SqlGenerator.IsIdentity)
            {
                var newId = DbContext.OpenConnection().Query<int>(sql, instance, transaction: DbContext.GetTransaction()).SingleOrDefault();
                added = newId > 0;
                id = newId;
                if (added)
                {
                    var newParsedId = Convert.ChangeType(newId, SqlGenerator.IdentityProperty.PropertyInfo.PropertyType);
                    SqlGenerator.IdentityProperty.PropertyInfo.SetValue(instance, newParsedId);
                }
            }
            else
            {
                added = DbContext.OpenConnection().Execute(sql, instance, transaction: DbContext.GetTransaction()) > 0;
                id = 0;
            }

            return added;
        }

        public virtual bool Delete(object key)
        {
            var sql = SqlGenerator.GetDelete();
            return DbContext.OpenConnection().Execute(sql, key, transaction: DbContext.GetTransaction()) > 0;
        }

        public virtual bool Update(T instance)
        {
            var sql = SqlGenerator.GetUpdate();
            return DbContext.OpenConnection().Execute(sql, instance, transaction: DbContext.GetTransaction()) > 0;
        }

        /// <summary>
        /// update some specific param
        /// </summary>
        /// <param name="instance"></param>
        /// <param name="param"></param>
        /// <returns></returns>
        public virtual bool Update(T instance, object param = null, object key = null)
        {
            string sql = GetUpdateQuery(SqlGenerator.TableName, param, key);
            return DbContext.OpenConnection().Execute(sql, param, transaction: DbContext.GetTransaction()) > 0;
        }

        #endregion

        #region Repository async base action
        public virtual async Task<IEnumerable<T>> GetAllAsync()
        {
            var sql = SqlGenerator.GetSelectAll();
            return await DbContext.OpenConnection().QueryAsync<T>(sql, transaction: DbContext.GetTransaction());
        }

        public virtual async Task<IEnumerable<T>> GetWhereAsync(object filters)
        {
            var sql = SqlGenerator.GetSelect(filters);

            return await DbContext.OpenConnection().QueryAsync<T>(sql, filters, transaction: DbContext.GetTransaction());
        }

        public virtual async Task<T> GetFirstAsync(object filters)
        {
            var sql = SqlGenerator.GetSelect(filters);
            Task<IEnumerable<T>> queryTask = DbContext.OpenConnection().QueryAsync<T>(sql, filters, transaction: DbContext.GetTransaction());
            IEnumerable<T> data = await queryTask;

            return data.FirstOrDefault();
        }

        public virtual async Task<bool> InsertAsync(T instance)
        {
            bool added = false;
            var sql = SqlGenerator.GetInsert();

            if (SqlGenerator.IsIdentity)
            {
                Task<IEnumerable<decimal>> queryTask = DbContext.OpenConnection().QueryAsync<decimal>(sql, instance, transaction: DbContext.GetTransaction());
                IEnumerable<decimal> result = await queryTask;
                var newId = result.Single();
                added = newId > 0;

                if (added)
                {
                    var newParsedId = Convert.ChangeType(newId, SqlGenerator.IdentityProperty.PropertyInfo.PropertyType);
                    SqlGenerator.IdentityProperty.PropertyInfo.SetValue(instance, newParsedId);
                }
            }
            else
            {
                Task<IEnumerable<int>> queryTask = DbContext.OpenConnection().QueryAsync<int>(sql, instance, transaction: DbContext.GetTransaction());
                IEnumerable<int> result = await queryTask;
                added = result.Single() > 0;
            }

            return added;
        }

        public virtual async Task<bool> DeleteAsync(object key)
        {
            var sql = SqlGenerator.GetDelete();
            Task<IEnumerable<int>> queryTask = DbContext.OpenConnection().QueryAsync<int>(sql, key, transaction: DbContext.GetTransaction());
            IEnumerable<int> result = await queryTask;

            return result.SingleOrDefault() > 0;
        }

        public virtual async Task<bool> UpdateAsync(T instance)
        {
            var sql = SqlGenerator.GetUpdate();
            Task<int> queryTask = DbContext.OpenConnection().ExecuteAsync(sql, instance, transaction: DbContext.GetTransaction());
            var result = await queryTask;

            return result > 0;
        }

        public virtual async Task<bool> UpdateAsync(T instance, Dictionary<object, object> param, object key = null)
        {
            string sql = GetUpdateQuery(SqlGenerator.TableName, param, key);
            Task<int> queryTask = DbContext.OpenConnection().ExecuteAsync(sql, instance, transaction: DbContext.GetTransaction());
            var result = await queryTask;

            return result > 0;
        }

        public virtual async Task<IEnumerable<T>> QueryStoreProcAsync(string storedProcedure, dynamic param = null, dynamic outParam = null)
        {
            Task<IEnumerable<T>> output = DbContext.OpenConnection().QueryAsync<T>(storedProcedure, param: (object)param, transaction: DbContext.GetTransaction(),
                commandType: CommandType.StoredProcedure);
            IEnumerable<T> result = await output;

            return result;
        }

        #endregion

        private string GetUpdateQuery(string tableName, object param, object key = null)
        {
            string s = "";
            if (param != null)
            {
                foreach (var prop in param.GetType().GetProperties())
                {
                    s += string.Format(",{0}=@{1}", prop.Name, prop.Name);
                }
                s = s.Substring(1);
            }

            return $"UPDATE [{tableName}] SET {s} WHERE Id=" + key;
        }
    }
}
