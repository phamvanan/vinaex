﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Narnia.Data.Base.Repositories
{
    public interface IRepository
    {
        IDbContext DbContext{ get; }
    }
}
