﻿using Microsoft.Extensions.DependencyInjection;
using Narnia.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Narnia.Extensions.DependencyInjection
{
    public static class NarniaServiceCollectionExtensions
    {
        //public static IServiceCollection AddDbContext<TContext>(
        //   this IServiceCollection serviceCollection,
        //   Func<IServiceProvider, TContext> implementationFactory,
        //   ServiceLifetime contextLifetime = ServiceLifetime.Scoped)
        //   where TContext : class, IDbContext
        //{
        //    return AddDbContext<TContext, TContext>(serviceCollection,
        //                  implementationFactory,
        //                  contextLifetime);
        //}

        //public static IServiceCollection AddDbContext<TContextService, TContextImplementation>(
        //    this IServiceCollection serviceCollection,
        //    Func<IServiceProvider, TContextImplementation> implementationFactory,
        //    ServiceLifetime contextLifetime = ServiceLifetime.Scoped)
        //    where TContextImplementation : class, TContextService
        //{
        //    if (implementationFactory != null)
        //    {
        //        throw new ArgumentException("Missing factory method for creating Implementation object ");
        //    }

        //    serviceCollection.Add(new ServiceDescriptor(typeof(TContextService), implementationFactory, contextLifetime));

        //    return serviceCollection;
        //}
        //private static void CheckContextConstructors<TContext>()
        //    where TContext : DbContext
        //{
        //    var declaredConstructors = typeof(TContext).GetTypeInfo().DeclaredConstructors.ToList();
        //    if (declaredConstructors.Count == 1
        //        && declaredConstructors[0].GetParameters().Length == 0)
        //    {
        //        throw new ArgumentException("Missing constructor " + (typeof(TContext).DeclaringType.Name));
        //    }
        //}

        public static IServiceCollection AddDbContextFactory<TContextService, TContextImplementation>(
    this IServiceCollection serviceCollection,
    TContextImplementation implementation,
    ServiceLifetime contextLifetime = ServiceLifetime.Singleton)
    where TContextImplementation : class, TContextService
        {
            if (implementation == null)
            {
                throw new ArgumentException("Service implementation cannot be null.");
            }

            serviceCollection.Add(new ServiceDescriptor(typeof(TContextService), p => implementation, contextLifetime));

            return serviceCollection;
        }

        public static IServiceCollection AddDbContextScopeFactory<TContextScopeService, TContextScopeImplementation>(
    this IServiceCollection serviceCollection,
    ServiceLifetime contextLifetime = ServiceLifetime.Singleton)
    where TContextScopeImplementation : class, TContextScopeService
        {
            serviceCollection.Add(new ServiceDescriptor(typeof(TContextScopeService), typeof(TContextScopeImplementation), contextLifetime));

            return serviceCollection;
        }
    }
}
