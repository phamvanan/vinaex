﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Data.Base
{
    public class DbContextFactory : IDbContextFactory
    {
        private string _connectionString;
        public DbContextFactory(string connectionString)
        {
            _connectionString = connectionString;
        }
        public virtual IDbContext CreateDbContext()
        {
            return new DbContext(_connectionString);
        }
    }
}
