﻿using Narnia.Data.Base.Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Narnia.Data.Base
{
    public class DbContext : IDbContext
    {
        private string _connectionString;
        private bool _disposed;
        private IDbConnection _connection;
        private IDbTransaction _transaction;
        public virtual IsolationLevel IsolationLevel => _transaction?.IsolationLevel ?? IsolationLevel.Unspecified;
        private bool _transactionCompleted;

        private string _guid;
        public DbContext(string connectionString)
        {
            _guid = Guid.NewGuid().ToString();
            _connectionString = connectionString;
            _connection = CreateConnection();
        }
        private SqlConnection CreateConnection()
        {
            return new SqlConnection(_connectionString);
        }
        public IDbTransaction GetTransaction()
        {
            return _transaction;
        }
        public virtual IDbConnection OpenConnection()
        {
            if(_connection == null || string.IsNullOrEmpty(_connection.ConnectionString))
            {
                _connection = CreateConnection();
            }
            if(_connection.State == ConnectionState.Closed || _connection.State == ConnectionState.Broken)
            {
                _connection.Open();
            }
            return _connection;
        }
        public virtual void Commit()
        {
            if (_connection.State == ConnectionState.Open && !_transactionCompleted)
            {
                _transaction?.Commit();
                _transactionCompleted = true;
            }
        }
        public virtual void Rollback()
        {
            if (_connection.State == ConnectionState.Open && !_transactionCompleted)
            {
                _transaction?.Rollback();
                _transactionCompleted = true;
            }
        }
        public virtual void BeginTransaction(IsolationLevel isolationLevel = IsolationLevel.ReadCommitted)
        {
            if(_transaction == null)
            {
                _transaction = _connection.BeginTransaction(isolationLevel);
                _transactionCompleted = false;
            }
        }
        private Dictionary<Type, object> _repositories = new Dictionary<Type, object>();
        public virtual IGenericRepository<T> Repository<T>() where T : class, new()
        {
            if (_repositories.Keys.Contains(typeof(T)))
            {
                return _repositories[typeof(T)] as IGenericRepository<T>;
            }
            IGenericRepository<T> repo = new GenericRepository<T>(this);

            _repositories.Add(typeof(T), repo);
            return repo;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #region Finalizer
        private void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    if (_transaction != null)
                    {
                        _transaction.Dispose();
                        _transaction = null;
                    }
                    if (_connection != null)
                    {
                        _connection.Dispose();
                        _connection = null;
                    }
                }
                _disposed = true;
            }
        }
        ~DbContext()
        {
            Dispose(false);
        } 
        #endregion
    }
}
