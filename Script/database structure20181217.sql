/****** Object:  Table [dbo].[AdvertisementFee]    Script Date: 18/12/2018 12:12:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AdvertisementFee](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[AssetTypeId] [varchar](5) NOT NULL,
	[Amount] [decimal](18, 9) NOT NULL,
	[Fee] [decimal](18, 9) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
	[RefId] [bigint] NOT NULL,
	[RefType] [int] NOT NULL,
 CONSTRAINT [PK_TransactionCoinWallet] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AssetBalance]    Script Date: 18/12/2018 12:12:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AssetBalance](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[AssetTypeId] [varchar](5) NOT NULL,
	[Balance] [money] NOT NULL,
	[FrozenBalance] [money] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[UpdatedOn] [datetime] NULL,
 CONSTRAINT [PK_AssetBalance] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AssetType]    Script Date: 18/12/2018 12:12:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AssetType](
	[AssetTypeId] [varchar](5) NOT NULL,
	[Name] [nchar](10) NOT NULL,
	[Description] [nvarchar](50) NULL,
 CONSTRAINT [PK_AssetType] PRIMARY KEY CLUSTERED 
(
	[AssetTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BankAccount]    Script Date: 18/12/2018 12:12:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BankAccount](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AccountName] [nvarchar](200) NOT NULL,
	[AccountNo] [varchar](200) NULL,
	[UserId] [int] NOT NULL,
	[BankId] [int] NOT NULL,
	[Branch] [nvarchar](500) NULL,
	[IsActived] [bit] NOT NULL,
	[Description] [nvarchar](100) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[DeletedUser] [int] NULL,
	[DeletedDate] [datetime] NULL,
 CONSTRAINT [PK_BankAccount] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Banks]    Script Date: 18/12/2018 12:12:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Banks](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[BankName] [nvarchar](200) NOT NULL,
	[BankCode] [varchar](200) NOT NULL,
	[Description] [nvarchar](1024) NULL,
	[CreatedDate] [datetime] NOT NULL CONSTRAINT [DF__Banks__CreatedDa__693CA210]  DEFAULT (getdate()),
	[IsDeleted] [bit] NOT NULL,
	[DeletedUser] [int] NULL,
	[DeletedDate] [datetime] NULL,
	[WebsiteUrl] [varchar](100) NULL,
 CONSTRAINT [PK_BankName] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BankTransfer]    Script Date: 18/12/2018 12:12:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BankTransfer](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[DepositId] [int] NOT NULL,
	[OrderNo] [varchar](50) NULL,
	[BankName] [nvarchar](50) NULL,
	[AccountName] [nvarchar](50) NULL,
	[AccountNumber] [nvarchar](50) NULL,
	[Amount] [money] NOT NULL CONSTRAINT [DF_BankTransfer_Amount]  DEFAULT ((0)),
	[Fee] [money] NULL CONSTRAINT [DF_BankTransfer_Fee]  DEFAULT ((0)),
	[EvidencePictureId] [int] NULL,
	[Comments] [nvarchar](50) NULL,
	[CreatedOn] [datetime] NOT NULL,
	[UpdatedOn] [datetime] NULL,
	[Status] [int] NOT NULL,
	[AssetTypeId] [varchar](5) NOT NULL,
	[BillingDate] [datetime] NOT NULL,
 CONSTRAINT [PK_BankTransfer] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BuyAdvertisement]    Script Date: 18/12/2018 12:12:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BuyAdvertisement](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[CurrencyId] [char](5) NULL,
	[AssetTypeId] [varchar](5) NOT NULL,
	[BankId] [int] NOT NULL,
	[BankAccountNumber] [varchar](100) NULL,
	[CoinExchangeId] [int] NOT NULL,
	[CoinPrice] [decimal](18, 5) NULL,
	[MaxCoinPrice] [decimal](18, 5) NULL CONSTRAINT [DF_BuyAdvertisement_MaxCoinPrice]  DEFAULT ((0)),
	[MinCoinNumber] [decimal](18, 9) NULL,
	[MaxCoinNumber] [decimal](18, 9) NULL,
	[PaymentMethodId] [int] NULL,
	[PaymentAllowTime] [int] NOT NULL,
	[CountryId] [int] NULL,
	[CreatedDate] [datetime] NOT NULL CONSTRAINT [DF__BuyAdvert__Creat__6A30C649]  DEFAULT (getdate()),
	[UpdatedDate] [datetime] NOT NULL CONSTRAINT [DF__BuyAdvert__Updat__6B24EA82]  DEFAULT (getdate()),
	[RowVersionNo] [timestamp] NOT NULL,
	[Approved] [bit] NULL CONSTRAINT [DF__BuyAdvert__Appro__6C190EBB]  DEFAULT ((0)),
	[AmountBuy] [decimal](18, 9) NULL,
	[Status] [int] NULL,
 CONSTRAINT [PK_BuyAdvertisement] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CoinExchange]    Script Date: 18/12/2018 12:12:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CoinExchange](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CurrencyName] [nvarchar](200) NOT NULL,
	[Description] [nvarchar](1024) NULL,
 CONSTRAINT [PK_CoinExchange] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CoinSymbol]    Script Date: 18/12/2018 12:12:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CoinSymbol](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ExchangeId] [varchar](50) NOT NULL,
	[SymbolType] [varchar](50) NOT NULL,
	[AssetIdBase] [varchar](5) NOT NULL,
	[AssetIdQuote] [varchar](5) NOT NULL,
	[Active] [bit] NOT NULL CONSTRAINT [DF_CoinSymbol_Active]  DEFAULT ((0)),
	[AvgPrice] [decimal](18, 4) NOT NULL CONSTRAINT [DF_CoinSymbol_AvgPrice]  DEFAULT ((1)),
	[BuyPrice] [decimal](18, 5) NULL,
	[SellPrice] [decimal](18, 5) NULL,
	[IsDefault] [bit] NOT NULL,
 CONSTRAINT [PK_CoinSymbol] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CoinWallet]    Script Date: 18/12/2018 12:12:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CoinWallet](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [bigint] NOT NULL,
	[WalletAddress] [varchar](256) NULL,
	[WalletPassword] [varchar](256) NULL,
	[LastBlockSync] [int] NULL,
	[Amount] [decimal](29, 18) NOT NULL,
	[CreatedDate] [datetime] NOT NULL CONSTRAINT [DF__CoinWalle__Creat__6D0D32F4]  DEFAULT (getdate()),
	[UpdatedDate] [datetime] NOT NULL CONSTRAINT [DF__CoinWalle__Updat__6E01572D]  DEFAULT (getdate()),
	[AssetTypeId] [varchar](5) NOT NULL,
 CONSTRAINT [PK_CoinWallet] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Commission]    Script Date: 18/12/2018 12:12:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Commission](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[BeneficiaryUserId] [int] NOT NULL,
	[CommLevel] [int] NOT NULL CONSTRAINT [DF_Commission_CommLevel]  DEFAULT ((1)),
	[CommRate] [float] NOT NULL,
	[CommAmt] [decimal](18, 4) NOT NULL,
	[CreatedDate] [datetime] NOT NULL CONSTRAINT [DF_Commission_CreatedDate]  DEFAULT (getdate()),
	[RefFromChildId] [int] NULL,
	[RefId] [bigint] NOT NULL,
	[RefType] [int] NOT NULL,
	[Status] [int] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[AssetTypeId] [varchar](5) NULL,
 CONSTRAINT [PK_Commission] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CommissionLevel]    Script Date: 18/12/2018 12:12:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CommissionLevel](
	[LevelId] [int] NOT NULL,
	[LevelName] [nvarchar](50) NOT NULL,
	[CommRate] [float] NOT NULL CONSTRAINT [DF_UserLevel_Commission]  DEFAULT ((0)),
 CONSTRAINT [PK_UserLevel] PRIMARY KEY CLUSTERED 
(
	[LevelId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Country]    Script Date: 18/12/2018 12:12:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Country](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CountryCode] [varchar](50) NOT NULL,
	[CountryName] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_Country] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Currency]    Script Date: 18/12/2018 12:12:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Currency](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CurrencyCode] [varchar](200) NOT NULL,
	[CurrencyName] [nvarchar](200) NOT NULL,
	[Description] [nvarchar](1024) NULL,
	[Rate] [float] NOT NULL CONSTRAINT [DF_Currency_Rate]  DEFAULT ((1)),
 CONSTRAINT [PK_Currency] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Deposit]    Script Date: 18/12/2018 12:12:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Deposit](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Amount] [decimal](18, 5) NOT NULL,
	[Fee] [decimal](18, 5) NULL,
	[AssetTypeId] [varchar](5) NOT NULL,
	[UserId] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[Status] [int] NOT NULL,
	[Description] [nvarchar](50) NULL,
 CONSTRAINT [PK_Deposit] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EmailAccount]    Script Date: 18/12/2018 12:12:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EmailAccount](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Email] [varchar](100) NOT NULL,
	[DisplayName] [nvarchar](255) NULL,
	[Host] [varchar](255) NOT NULL,
	[Port] [int] NOT NULL,
	[Username] [varchar](255) NOT NULL,
	[Password] [varchar](255) NOT NULL,
	[EnableSsl] [bit] NOT NULL,
	[UseDefaultCredentials] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL DEFAULT ((0)),
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EmailTemplate]    Script Date: 18/12/2018 12:12:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EmailTemplate](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](200) NOT NULL,
	[Subject] [varchar](1024) NOT NULL,
	[Body] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NULL CONSTRAINT [DF_EmailTemplate_CreatedDate]  DEFAULT (getdate()),
	[CreatedBy] [varchar](200) NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_EmailTemplate] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Evidence]    Script Date: 18/12/2018 12:12:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Evidence](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[FileName] [nvarchar](250) NULL,
	[ContentType] [varchar](50) NULL,
	[Image] [varbinary](max) NULL,
	[Description] [nvarchar](100) NULL,
	[CreatedDate] [datetime] NOT NULL CONSTRAINT [DF_Evidence_CreatedDate]  DEFAULT (getdate()),
	[Code] [varchar](256) NULL,
 CONSTRAINT [PK_Evidence] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Feedback]    Script Date: 18/12/2018 12:12:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Feedback](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Content] [nvarchar](1000) NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UserId] [int] NOT NULL,
 CONSTRAINT [PK_Comment] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[GatewayAccount]    Script Date: 18/12/2018 12:12:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GatewayAccount](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NULL,
	[GatewayId] [int] NOT NULL,
	[AccName] [varchar](100) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[IsSystemAccount] [bit] NOT NULL CONSTRAINT [DF_GatewayAccount_IsSystemAccount]  DEFAULT ((0)),
	[Description] [nvarchar](50) NULL,
 CONSTRAINT [PK_GatewayAccount] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[GatewayTransfer]    Script Date: 18/12/2018 12:12:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GatewayTransfer](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[GatewayId] [int] NOT NULL,
	[DepositId] [int] NOT NULL,
	[GatewayAccFrom] [varchar](50) NOT NULL,
	[GatewayAccTo] [varchar](50) NOT NULL,
	[Amount] [decimal](18, 5) NOT NULL,
	[Fee] [decimal](18, 5) NOT NULL,
	[AssetTypeId] [varchar](5) NOT NULL,
	[Status] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[Description] [nchar](10) NULL,
 CONSTRAINT [PK_GatewayTransfer] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Language]    Script Date: 18/12/2018 12:12:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Language](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[LanguageCulture] [varchar](5) NOT NULL,
	[FlagImageFileName] [nvarchar](50) NULL,
	[Published] [bit] NOT NULL,
	[DisplayOrder] [int] NOT NULL,
 CONSTRAINT [PK_Language] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LocaleStringResource]    Script Date: 18/12/2018 12:12:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LocaleStringResource](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[LanguageId] [int] NOT NULL,
	[ResourceName] [varchar](50) NOT NULL,
	[ResourceValue] [nvarchar](4000) NOT NULL,
 CONSTRAINT [PK_LocaleStringResource] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PaymentGateway]    Script Date: 18/12/2018 12:12:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PaymentGateway](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[GatewaySystemName] [varchar](50) NOT NULL,
	[GatewayFriendlyName] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](256) NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_PaymentGateway_IsActive]  DEFAULT ((0)),
 CONSTRAINT [PK_PaymentGateway] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PaymentMethod]    Script Date: 18/12/2018 12:12:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PaymentMethod](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PaymentMethodCode] [varchar](50) NOT NULL,
	[PaymentMethodName] [nvarchar](100) NULL,
	[Description] [nvarchar](1024) NOT NULL,
 CONSTRAINT [PK_PaymentMethod] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Picture]    Script Date: 18/12/2018 12:12:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Picture](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MimeType] [varchar](50) NULL,
	[AltAttribute] [nvarchar](50) NULL,
	[TitleAttribute] [nvarchar](50) NULL,
	[Data] [varbinary](max) NULL,
	[IsNew] [bit] NOT NULL CONSTRAINT [DF_Picture_IsNew]  DEFAULT ((1))
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Policy]    Script Date: 18/12/2018 12:12:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Policy](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Content] [nvarchar](max) NOT NULL,
	[Desciption] [nvarchar](500) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_Policy] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Role]    Script Date: 18/12/2018 12:12:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Role](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RoleName] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](50) NULL,
 CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SellAdvertisement]    Script Date: 18/12/2018 12:12:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SellAdvertisement](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[CurrencyId] [char](5) NULL,
	[AssetTypeId] [varchar](5) NOT NULL,
	[BankId] [int] NOT NULL,
	[BankAccountNumber] [varchar](100) NULL,
	[BankAccountName] [nvarchar](255) NULL,
	[CoinExchangeId] [int] NOT NULL,
	[CoinPrice] [decimal](18, 9) NULL CONSTRAINT [DF_SellAdvertisement_CoinPrice]  DEFAULT ((0)),
	[MinCoinPrice] [decimal](18, 9) NULL,
	[MinCoinNumber] [decimal](18, 9) NULL,
	[MaxCoinNumber] [decimal](18, 9) NULL,
	[PaymentMethodId] [int] NULL,
	[PaymentAllowTime] [int] NOT NULL,
	[CountryId] [int] NULL,
	[CreatedDate] [datetime] NOT NULL CONSTRAINT [DF__SellAdver__Creat__276EDEB3]  DEFAULT (getdate()),
	[UpdatedDate] [datetime] NOT NULL CONSTRAINT [DF__SellAdver__Updat__286302EC]  DEFAULT (getdate()),
	[RowVersionNo] [timestamp] NOT NULL,
	[Approved] [bit] NULL CONSTRAINT [DF__SellAdver__Appro__29572725]  DEFAULT ((0)),
	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_SellAdvertisement_IsDeleted]  DEFAULT ((0)),
	[AmountSell] [decimal](18, 9) NULL,
	[Status] [int] NULL,
 CONSTRAINT [PK_SellAdvertisement] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Setting]    Script Date: 18/12/2018 12:12:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Setting](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Value] [varchar](256) NOT NULL,
	[Description] [nvarchar](256) NULL,
 CONSTRAINT [PK_Setting] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Transaction]    Script Date: 18/12/2018 12:12:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Transaction](
	[TransactionId] [int] IDENTITY(1,1) NOT NULL,
	[SellAdvertisementId] [int] NULL,
	[BuyAdvertisementId] [int] NULL,
	[AmountCoin] [decimal](18, 8) NOT NULL,
	[AssetTypeId] [varchar](5) NOT NULL,
	[CreatedDate] [datetime] NOT NULL CONSTRAINT [DF_Transaction_CreatedDate]  DEFAULT (getdate()),
	[UserId] [int] NOT NULL,
	[Status] [int] NOT NULL,
	[Description] [nvarchar](256) NULL,
	[TransactionType] [int] NOT NULL,
	[RowVersionNo] [timestamp] NOT NULL,
	[ConfirmBy] [int] NULL,
	[EvidenceId] [int] NULL,
	[TransactionCode] [varchar](256) NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[PaymentType] [int] NOT NULL,
	[TransferType] [int] NOT NULL,
 CONSTRAINT [PK_Transaction] PRIMARY KEY CLUSTERED 
(
	[TransactionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TransactionWallet]    Script Date: 18/12/2018 12:12:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TransactionWallet](
	[TransactionId] [int] IDENTITY(1,1) NOT NULL,
	[AssetTypeId] [varchar](5) NOT NULL,
	[Amount] [decimal](18, 8) NOT NULL,
	[UserId] [int] NOT NULL,
	[BankAccountId] [int] NULL,
	[Status] [int] NOT NULL,
	[TransactionType] [int] NOT NULL,
	[Description] [nvarchar](500) NULL,
	[TransactionCode] [varchar](256) NOT NULL,
	[ConfirmBy] [int] NULL,
	[ConfirmDate] [datetime] NULL,
	[EvidenceId] [int] NULL,
	[EvidenceDate] [datetime] NULL,
	[VersionNo] [timestamp] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[OrderNo] [varchar](256) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[User]    Script Date: 18/12/2018 12:12:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[User](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [varchar](100) NOT NULL,
	[Email] [varchar](100) NOT NULL,
	[Phone] [varchar](50) NULL,
	[Alias] [nvarchar](100) NOT NULL,
	[UpdatedDate] [datetime] NOT NULL CONSTRAINT [DF_User_UpdatedDate]  DEFAULT (getdate()),
	[CreatedDate] [datetime] NOT NULL CONSTRAINT [DF_User_CreatedDate]  DEFAULT (getdate()),
	[ReferenceCode] [varchar](50) NULL,
	[ActivationKey] [varchar](256) NULL,
	[PasswordSalt] [varchar](100) NULL,
	[PasswordHash] [varchar](256) NULL,
	[FirstName] [nvarchar](50) NULL,
	[LastName] [nvarchar](50) NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_User_IsActive]  DEFAULT ((0)),
	[IsVerify] [bit] NOT NULL CONSTRAINT [DF_User_IsVerify]  DEFAULT ((0)),
	[UplineId] [int] NULL,
	[AvatarPictureId] [int] NULL,
	[IsAdmin] [bit] NULL CONSTRAINT [DF_User_IsAdmin]  DEFAULT ((0)),
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserInfor]    Script Date: 18/12/2018 12:12:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserInfor](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[HasSendEmailNewTransaction] [bit] NOT NULL,
	[HasSendEmailExchangeTransaction] [bit] NOT NULL,
	[HasSendEmailMessageTransaction] [bit] NOT NULL,
	[HasSendSMSNewTransaction] [bit] NOT NULL,
	[HasSendSMSTransactionPaid] [bit] NOT NULL,
	[CreatedDate] [datetime] NOT NULL CONSTRAINT [DF__UserNotic__Creat__797309D9]  DEFAULT (getdate()),
	[UpdatedDate] [datetime] NOT NULL CONSTRAINT [DF__UserNotic__Updat__7A672E12]  DEFAULT (getdate()),
	[IDNumber] [varchar](50) NULL,
	[UrlFaceBook] [varchar](200) NULL,
	[UrlTwitter] [varchar](200) NULL,
	[Image] [varbinary](max) NULL,
	[Phone] [varchar](20) NULL,
	[HasReceivedDailyPrice] [bit] NOT NULL CONSTRAINT [DF_UserInfor_IsReceivedDailyPrice]  DEFAULT ((1)),
	[ContentType] [varchar](50) NULL,
 CONSTRAINT [PK_UserNoticeSetting] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserLoginHistory]    Script Date: 18/12/2018 12:12:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserLoginHistory](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[IPAddress] [varchar](100) NOT NULL,
	[LoginDate] [datetime] NOT NULL DEFAULT (getdate()),
 CONSTRAINT [PK_UserLoginHistory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Feedback] ADD  CONSTRAINT [DF_Feedback_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
/****** Object:  StoredProcedure [dbo].[GenerateClassFromDataTable]    Script Date: 18/12/2018 12:12:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GenerateClassFromDataTable]
 @TableName VARCHAR(200)
AS
BEGIN
--exec GenerateClassFromDataTable 'Transaction'
declare @Result varchar(max) = 'public class ' + @TableName + '
{'

select @Result = @Result + '
    public ' + ColumnType + NullableSign + ' ' + ColumnName + ' { get; set; }
'
from
(
    select 
        replace(col.name, ' ', '_') ColumnName,
        column_id ColumnId,
        case typ.name 
            when 'bigint' then 'long'
            when 'binary' then 'byte[]'
            when 'bit' then 'bool'
            when 'char' then 'string'
            when 'date' then 'DateTime'
            when 'datetime' then 'DateTime'
            when 'datetime2' then 'DateTime'
            when 'datetimeoffset' then 'DateTimeOffset'
            when 'decimal' then 'decimal'
            when 'float' then 'float'
            when 'image' then 'byte[]'
            when 'int' then 'int'
            when 'money' then 'decimal'
            when 'nchar' then 'string'
            when 'ntext' then 'string'
            when 'numeric' then 'decimal'
            when 'nvarchar' then 'string'
            when 'real' then 'double'
            when 'smalldatetime' then 'DateTime'
            when 'smallint' then 'short'
            when 'smallmoney' then 'decimal'
            when 'text' then 'string'
            when 'time' then 'TimeSpan'
            when 'timestamp' then 'Byte[]'
            when 'tinyint' then 'byte'
            when 'uniqueidentifier' then 'Guid'
            when 'varbinary' then 'byte[]'
            when 'varchar' then 'string'
            else 'UNKNOWN_' + typ.name
        end ColumnType,
        case 
            when col.is_nullable = 1 and typ.name in ('bigint', 'bit', 'date', 'datetime', 'datetime2', 'datetimeoffset', 'decimal', 'float', 'int', 'money', 'numeric', 'real', 'smalldatetime', 'smallint', 'smallmoney', 'time', 'tinyint', 'uniqueidentifier') 
            then '?' 
            else '' 
        end NullableSign
    from sys.columns col
        join sys.types typ on
            col.system_type_id = typ.system_type_id AND col.user_type_id = typ.user_type_id
    where object_id = object_id(@TableName)
) t
order by ColumnId

set @Result = @Result  + '
}'

print @Result
END


GO
/****** Object:  StoredProcedure [dbo].[GetTransactionWalletDetailAdmin]    Script Date: 18/12/2018 12:12:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetTransactionWalletDetailAdmin]
	@TransactionID int
AS
BEGIN
	--exec GetTransactionWalletDetailAdmin 3
	SET NOCOUNT ON;
	select
	tr.TransactionCode,
		tr.TransactionId,
		tr.Amount,
		tr.CreatedDate,
		tr.UpdatedDate,
		u.UserName,
		tr.Status,
		ba.AccountName,
		ba.AccountNo,
		ba.Branch,
		b.BankName,
		cw.WalletAddress,
		uc.UserName UserConfirm,
		tr.EvidenceId,
		tr.TransactionType,
		tr.ConfirmDate,
		tr.EvidenceDate,
		e.Code EvidenceCode,
		tr.AssetTypeId Unit,
		tr.OrderNo
 from dbo.[TransactionWallet] tr
 inner join [User] u with(nolock) on tr.UserId=u.Id
  left join [User] uc with(nolock) on tr.ConfirmBy=uc.Id
  left join CoinWallet cw with(nolock) on tr.UserId=cw.UserId and tr.AssetTypeId = cw.AssetTypeId
  left join BankAccount ba with(nolock) on tr.BankAccountId=ba.Id
  left join Banks b with(nolock) on ba.BankId=b.Id
  left join Evidence e with(nolock) on tr.EvidenceId=e.Id
 -- left join CoinSymbol cs with(nolock) on tr.CoinType=cs.Id
  where tr.TransactionId=@TransactionID
END


GO
/****** Object:  StoredProcedure [dbo].[sp_GetAllUplineUsers]    Script Date: 18/12/2018 12:12:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetAllUplineUsers]
	-- Add the parameters for the stored procedure here
	@UserId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	--exec sp_GetAllUplineUsers 14
	with myTable as
        (
	        select u.Id , u.UserName,u.IsActive,u.IsVerify,u.UplineId, 0 as Generation, 0 ChildId
	        from [User] u where u.Id = @UserId
	        union all
	        select u.Id as UserId, u.UserName,u.IsActive,u.IsVerify, u.UplineId, myTable.Generation + 1 as Generation,myTable.Id ChildId
	        from [User] u inner join myTable on myTable.UplineId = u.Id
        )
        select * from myTable
        where Generation > 0 
        order by Generation
   
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetUplineUser]    Script Date: 18/12/2018 12:12:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetUplineUser]
	-- Add the parameters for the stored procedure here
	@Id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--exec sp_GetUplineUser 14
     select * from [User] where UplineId in (select UplineId from [User] where Id = @Id)
END

GO
/****** Object:  StoredProcedure [dbo].[usp_Bank_GetListSearchAdmin_Paging]    Script Date: 18/12/2018 12:12:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_Bank_GetListSearchAdmin_Paging] 
  @SelectedPage INT = NULL,  
@PageSize INT = NULL,  
@TotalRecords INT OUT,  
@OrderBy NVARCHAR(1000) = null  
AS
/*
 declare @t1 int   
 exec usp_Bank_GetListSearchAdmin_Paging 0,10,@t1 out,null  
 select @t1  
 */

	DECLARE @strSQL_0 NVARCHAR(MAX),@strSQL_1 NVARCHAR(MAX),@strSQL_2 NVARCHAR(MAX),@strSQL_3 NVARCHAR(MAX)  
DECLARE @param NVARCHAR(MAX)  
SET @strSQL_0 = '';SET @strSQL_1 = '';SET @strSQL_2 = ''; SET @strSQL_3 = '';  
SET @param = ''  
BEGIN  
  
 BEGIN  
  SET @strSQL_2 = ' Where b.IsDeleted=0 '  
 END  
--Order  
 BEGIN  
  IF (@OrderBy IS NULL or @OrderBy = '')  
   SET @strSQL_3 = @strSQL_3 + ' ORDER BY Id desc'  

  IF(@OrderBy = '0-ASC')  
    Set @strSQL_3 = @strSQL_3 + ' ORDER BY BankName asc '  
  IF(@OrderBy = '0-DESC')  
    Set @strSQL_3 = @strSQL_3 + ' ORDER BY BankName DESC '  
  
    IF(@OrderBy = '1-ASC')  
    Set @strSQL_3 = @strSQL_3 + ' ORDER BY BankCode asc '  
  IF(@OrderBy = '1-DESC')  
    Set @strSQL_3 = @strSQL_3 + ' ORDER BY BankCode DESC '  
  
    IF(@OrderBy = '2-ASC')  
    Set @strSQL_3 = @strSQL_3 + ' ORDER BY CreatedDate asc '  
  IF(@OrderBy = '2-DESC')  
    Set @strSQL_3 = @strSQL_3 + ' ORDER BY CreatedDate DESC '  
  
    IF(@OrderBy = '3-ASC')  
    Set @strSQL_3 = @strSQL_3 + ' ORDER BY Id asc '  
  IF(@OrderBy = '3-DESC')  
    Set @strSQL_3 = @strSQL_3 + ' ORDER BY Id DESC '  
  
  
 END  
--For Total  
SET @strSQL_0 =  
    N' Select @TotalRecords_t = Count(b.Id)  
    from Banks b with(nolock) '   
SET @strSQL_0 = @strSQL_0 + @strSQL_2   
--For Detail  
SET @strSQL_1 =  
N'   
BEGIN  
  select id,BankName,BankCode,CreatedDate
  from (select id,BankName,BankCode,CreatedDate from Banks b with(nolock)'+@strSQL_2+' ) tmp'  
SET @strSQL_1 =@strSQL_1+ @strSQL_3 + '  
    Offset @SelectedPage_t rows   
 fetch next @PageSize_t rows only'  
SET @strSQL_1 = @strSQL_1 + ' END'  
--ELSE  
--SET @strSQL_1 = @strSQL_1 + ' END'  
SET @param = @param + N'  
@SelectedPage_t INT =null ,  
@PageSize_t INT =null,  
@TotalRecords_t int output,  
@OrderBy_t NVARCHAR(1000) = null  
'  
DECLARE @str NVARCHAR(MAX) = (@strSQL_0 + @strSQL_1)  
PRINT @str  
EXECUTE sp_executesql @str,  
@param,  
@SelectedPage_t =@SelectedPage,  
@PageSize_t = @PageSize,  
@TotalRecords_t =@TotalRecords OUTPUT,   
@OrderBy_t =@OrderBy;  
END



GO
/****** Object:  StoredProcedure [dbo].[usp_BankAccount_GetListAccountAdmin_Paging]    Script Date: 18/12/2018 12:12:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_BankAccount_GetListAccountAdmin_Paging] 
  @SelectedPage INT = NULL,  
@PageSize INT = NULL,  
@TotalRecords INT OUT,  
@OrderBy NVARCHAR(1000) = null  
AS
/*
 declare @t1 int   
 exec usp_BankAccount_GetListAccountAdmin_Paging 0,10,@t1 out,null  
 select @t1  
 */

	DECLARE @strSQL_0 NVARCHAR(MAX),@strSQL_1 NVARCHAR(MAX),@strSQL_2 NVARCHAR(MAX),@strSQL_3 NVARCHAR(MAX)  
DECLARE @param NVARCHAR(MAX)  
SET @strSQL_0 = '';SET @strSQL_1 = '';SET @strSQL_2 = ''; SET @strSQL_3 = '';  
SET @param = ''  
BEGIN  
  
 BEGIN  
  SET @strSQL_2 = ' Where ba.IsDeleted=0 and ba.Userid=-1'  
 END  
--Order  
 BEGIN  
  IF (@OrderBy IS NULL or @OrderBy = '')  
   SET @strSQL_3 = @strSQL_3 + ' ORDER BY Id desc'  

  IF(@OrderBy = '0-ASC')  
    Set @strSQL_3 = @strSQL_3 + ' ORDER BY BankName asc '  
  IF(@OrderBy = '0-DESC')  
    Set @strSQL_3 = @strSQL_3 + ' ORDER BY BankName DESC '  
  
    IF(@OrderBy = '1-ASC')  
    Set @strSQL_3 = @strSQL_3 + ' ORDER BY AccountName asc '  
  IF(@OrderBy = '1-DESC')  
    Set @strSQL_3 = @strSQL_3 + ' ORDER BY AccountName DESC '  
  
    IF(@OrderBy = '2-ASC')  
    Set @strSQL_3 = @strSQL_3 + ' ORDER BY AccountNo asc '  
  IF(@OrderBy = '2-DESC')  
    Set @strSQL_3 = @strSQL_3 + ' ORDER BY AccountNo DESC '  
  
    IF(@OrderBy = '3-ASC')  
    Set @strSQL_3 = @strSQL_3 + ' ORDER BY Branch asc '  
  IF(@OrderBy = '3-DESC')  
    Set @strSQL_3 = @strSQL_3 + ' ORDER BY Branch DESC '  
  
    IF(@OrderBy = '4-ASC')  
    Set @strSQL_3 = @strSQL_3 + ' ORDER BY CreatedDate asc '  
  IF(@OrderBy = '4-DESC')  
    Set @strSQL_3 = @strSQL_3 + ' ORDER BY CreatedDate DESC '  
  
    IF(@OrderBy = '5-ASC')  
    Set @strSQL_3 = @strSQL_3 + ' ORDER BY IsActived asc '  
  IF(@OrderBy = '5-DESC')  
    Set @strSQL_3 = @strSQL_3 + ' ORDER BY IsActived DESC '  
  
    IF(@OrderBy = '6-ASC')  
    Set @strSQL_3 = @strSQL_3 + ' ORDER BY Id asc '  
  IF(@OrderBy = '6-DESC')  
    Set @strSQL_3 = @strSQL_3 + ' ORDER BY Id DESC '  
  
  
  --  IF(@OrderBy = '7-ASC')  
  --  Set @strSQL_3 = @strSQL_3 + ' ORDER BY UserConfirm asc '  
  --IF(@OrderBy = '7-DESC')  
  --  Set @strSQL_3 = @strSQL_3 + ' ORDER BY UserConfirm DESC '  

	 --IF(@OrderBy = '8-ASC')  
  --  Set @strSQL_3 = @strSQL_3 + ' ORDER BY TransactionType asc '  
  --IF(@OrderBy = '8-DESC')  
  --  Set @strSQL_3 = @strSQL_3 + ' ORDER BY TransactionType DESC '  
  
 END  
--For Total  
SET @strSQL_0 =  
    N' Select @TotalRecords_t = Count(ba.Id)  
    from BankAccount ba with(nolock)
		inner join Banks b with(nolock) on ba.BankId=b.Id '   
SET @strSQL_0 = @strSQL_0 + @strSQL_2   
--For Detail  
SET @strSQL_1 =  
N'   
BEGIN  
  select BankName,AccountName,AccountNo,Branch,CreatedDate,IsActived,Id
  from (select b.BankName,ba.AccountName,ba.AccountNo,ba.Branch,ba.CreatedDate,ba.IsActived,ba.Id
		from BankAccount ba with(nolock)
		inner join Banks b with(nolock) on ba.BankId=b.Id'+@strSQL_2+' ) tmp'  
SET @strSQL_1 =@strSQL_1+ @strSQL_3 + '  
    Offset @SelectedPage_t rows   
 fetch next @PageSize_t rows only'  
SET @strSQL_1 = @strSQL_1 + ' END'  
--ELSE  
--SET @strSQL_1 = @strSQL_1 + ' END'  
SET @param = @param + N'  
@SelectedPage_t INT =null ,  
@PageSize_t INT =null,  
@TotalRecords_t int output,  
@OrderBy_t NVARCHAR(1000) = null  
'  
DECLARE @str NVARCHAR(MAX) = (@strSQL_0 + @strSQL_1)  
PRINT @str  
EXECUTE sp_executesql @str,  
@param,  
@SelectedPage_t =@SelectedPage,  
@PageSize_t = @PageSize,  
@TotalRecords_t =@TotalRecords OUTPUT,   
@OrderBy_t =@OrderBy;  
END


GO
/****** Object:  StoredProcedure [dbo].[usp_BuyAdvertisement]    Script Date: 18/12/2018 12:12:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROC [dbo].[usp_BuyAdvertisement]
(
	@AssetTypeId VARCHAR(5),
	@IsGetOfflineUser BIT,
	@From INT,
	@To INT
)
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @avgPrice decimal(18,4) = 0
	select top 1 @avgPrice = AvgPrice from CoinSymbol with (nolock) where AssetIdBase = @AssetTypeId

	-- lay tat ca user online va offline
	IF(@IsGetOfflineUser = 1)
	BEGIN 	
		select temp.id,temp.UserId,MaxCoinPrice,MinCoinNumber,MaxCoinNumber,CurrencyId,CurrencyId as CurrencyName,b.BankName,AssetTypeId as CoinName,p.PaymentMethodName, Amount,alias as UserName
		,/*(case when CoinPrice > 0 then CoinPrice else c.Rate end)*/ @avgPrice as CoinPrice
		from (
			Select * From (
				select b.id,b.UserId,MaxCoinPrice,MinCoinNumber,MaxCoinNumber,CurrencyId,BankId,PaymentMethodId,b.AssetTypeId,(MaxCoinNumber - ISNULL(b.AmountBuy,0)) as Amount
				,CoinPrice,b.CreatedDate,(Balance - FrozenBalance) as Balance 
				From BuyAdvertisement b with (nolock) 				
				INNER JOIN [AssetBalance] ab with (nolock) on (ab.UserId=b.UserId and ab.AssetTypeId='VND')				
				WHERE b.Approved = 1 AND b.AssetTypeId = @AssetTypeId and (MaxCoinNumber - ISNULL(b.AmountBuy,0)) > 0 
			) temp 
			
			where Balance - (Amount * @avgPrice) > 0

			order by CreatedDate desc 
			OFFSET @From ROWS FETCH NEXT @To ROWS ONLY
		) as temp
		
		INNER JOIN [User] u with (nolock) on temp.UserId=u.Id
		--LEFT JOIN Currency c with (nolock) on c.CurrencyCode=temp.CurrencyId
		LEFT JOIN Banks b with (nolock) on b.Id=temp.BankId
		LEFT JOIN PaymentMethod p with (nolock) on p.Id=temp.PaymentMethodId		

		option(recompile)
	END	
	ELSE	-- chi lay nhung user online
	BEGIN 
				
		select temp.id,temp.UserId,MaxCoinPrice,MinCoinNumber,MaxCoinNumber,CurrencyId,CurrencyId as CurrencyName,b.BankName,AssetTypeId as CoinName,p.PaymentMethodName, Amount,alias as UserName
		,/*(case when CoinPrice > 0 then CoinPrice else c.Rate end)*/ @avgPrice as CoinPrice
		from (
			Select * From (
				select b.id,b.UserId,MaxCoinPrice,MinCoinNumber,MaxCoinNumber,CurrencyId,BankId,PaymentMethodId,b.AssetTypeId,(MaxCoinNumber - ISNULL(b.AmountBuy,0)) as Amount
				,CoinPrice,b.CreatedDate,(Balance - FrozenBalance) as Balance 
				From BuyAdvertisement b with (nolock) 				
				INNER JOIN [AssetBalance] ab with (nolock) on (ab.UserId=b.UserId and ab.AssetTypeId='VND')				
				WHERE b.Approved = 1 AND b.AssetTypeId = @AssetTypeId and (MaxCoinNumber - ISNULL(b.AmountBuy,0)) > 0 and exists (select top 1 * from UserLoginHistory uh with (nolock) where uh.UserId=b.UserId)
			) temp 
			
			where Balance - (Amount * @avgPrice) > 0

			order by CreatedDate desc 
			OFFSET @From ROWS FETCH NEXT @To ROWS ONLY
		) as temp
		
		INNER JOIN [User] u with (nolock) on temp.UserId=u.Id
		--LEFT JOIN Currency c with (nolock) on c.CurrencyCode=temp.CurrencyId
		LEFT JOIN Banks b with (nolock) on b.Id=temp.BankId
		LEFT JOIN PaymentMethod p with (nolock) on p.Id=temp.PaymentMethodId					

		option(recompile)
	END	
END




GO
/****** Object:  StoredProcedure [dbo].[usp_Commission_Get_HistoryBy]    Script Date: 18/12/2018 12:12:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_Commission_Get_HistoryBy]  
(  
	@skip INT = NULL,  
	@take INT = NULL,  
	@TotalRecords INT OUT,  
	@UserName varchar(100) = null,
	@StartDate datetime =null,
	@EndDate datetime=null,
	@Status bit = null
)  
AS  

/*  
 declare @t1 int   
 exec usp_Commission_Get_HistoryBy 0,10,@t1 out
 select @t1  

*/  
  
DECLARE @strSQL_0 NVARCHAR(MAX),@strSQL_1 NVARCHAR(MAX),@strSQL_CON NVARCHAR(MAX),@strSQL_Order NVARCHAR(MAX)  
DECLARE @param NVARCHAR(MAX)  
SET @strSQL_0 = '';SET @strSQL_1 = '';SET @strSQL_CON = ''; SET @strSQL_Order = '';  
SET @param = ''  
BEGIN  
  
 BEGIN  
  SET @strSQL_CON = ' Where BeneficiaryUserId != -1 '
   
  if @UserName is not null
  begin
    set @strSQL_CON=@strSQL_CON+' and u.UserName=@UserName_t'
  end

  if @StartDate is not null 
  begin
   set @strSQL_CON = @strSQL_CON + ' and c.CreatedDate >= @StartDate_t'
  end

   if @EndDate is not null 
  begin
   set @strSQL_CON = @strSQL_CON + ' and c.CreatedDate <= @EndDate_t'
  end

  if @Status is not null
  begin
   set @strSQL_CON=@strSQL_CON + ' and c.Status=@Status_t '
  end
 END  
--Order  
 BEGIN  
   SET @strSQL_Order = @strSQL_Order + ' ORDER BY TransactionId desc'  
 END  
--For Total  
SET @strSQL_0 =  
    N' Select @TotalRecords_t = Count(c.Id)  
    FROM Commission c with(nolock) 
	 inner join [user] u with(nolock) on c.BeneficiaryUserId= u.id '   
SET @strSQL_0 = @strSQL_0 + @strSQL_CON   
--For Detail  
SET @strSQL_1 =  
N'   
BEGIN  

    select c.Id,c.BeneficiaryUserId,u.alias,u.UserName,
		c.CommLevel,c.CommRate,c.CommAmt,
		c.CreatedDate,c.RefId,c.RefType,c.Status,c.UpdatedDate,c.AssetTypeId
	from Commission c with(nolock)
	inner join [User] u with(nolock) on c.BeneficiaryUserId=u.Id
	'+@strSQL_CON+'
	order by c.CreatedDate desc
	offset @Skip_t rows
	fetch next @take_t rows only option(recompile) 

 END'  

SET @param = @param + N'  
@Skip_t INT =null ,  
@take_t INT =null,  
@TotalRecords_t int output,  
@UserName_t varchar(100) = null,
@StartDate_t datetime =null,
@EndDate_t datetime =null,
@Status_t  bit = null
'  
DECLARE @str NVARCHAR(MAX) = (@strSQL_0 + @strSQL_1)  
PRINT @str  
EXECUTE sp_executesql @str,  
@param,  
@skip_t =@skip,  
@take_t = @take,  
@TotalRecords_t =@TotalRecords OUTPUT,  
@UserName_t = @UserName,
@StartDate_t =@StartDate,
@EndDate_t =@EndDate,
@Status_t  =@Status
;  
END

GO
/****** Object:  StoredProcedure [dbo].[usp_Commission_Get_HistoryByBeneficiaryUserId]    Script Date: 18/12/2018 12:12:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_Commission_Get_HistoryByBeneficiaryUserId]  
(  
	@skip INT = NULL,  
	@take INT = NULL,  
	@TotalRecords INT OUT,  
	@UserId int
)  
AS  

/*  
 declare @t1 int   
 exec usp_Commission_Get_HistoryByBeneficiaryUserId 0,10,@t1 out,-1
 select @t1  

*/  
  
DECLARE @strSQL_0 NVARCHAR(MAX),@strSQL_1 NVARCHAR(MAX),@strSQL_CON NVARCHAR(MAX),@strSQL_Order NVARCHAR(MAX)  
DECLARE @param NVARCHAR(MAX)  
SET @strSQL_0 = '';SET @strSQL_1 = '';SET @strSQL_CON = ''; SET @strSQL_Order = '';  
SET @param = ''  
BEGIN  
  
 BEGIN  
  SET @strSQL_CON = ' Where c.BeneficiaryUserId=@UserId_t '  
 END  
--Order  
 BEGIN  
   SET @strSQL_Order = @strSQL_Order + ' ORDER BY TransactionId desc'  
 END  
--For Total  
SET @strSQL_0 =  
    N' Select @TotalRecords_t = Count(c.Id)  
    FROM Commission c with(nolock)  '   
SET @strSQL_0 = @strSQL_0 + @strSQL_CON   
--For Detail  
SET @strSQL_1 =  
N'   
BEGIN  

    select c.Id,c.RefFromChildId,u.alias,u.UserName,
		c.CommLevel,c.CommRate,c.CommAmt,
		c.CreatedDate,c.RefId,c.RefType,c.Status,c.UpdatedDate,c.AssetTypeId
	from Commission c with(nolock)
	 left join [User] u with(nolock) on c.RefFromChildId=u.Id
	'+@strSQL_CON+'
	order by c.CreatedDate desc
	offset @Skip_t rows
	fetch next @take_t rows only option(recompile) 

 END'  

SET @param = @param + N'  
@Skip_t INT =null ,  
@take_t INT =null,  
@TotalRecords_t int output,  
@UserId_t int 
'  
DECLARE @str NVARCHAR(MAX) = (@strSQL_0 + @strSQL_1)  
PRINT @str  
EXECUTE sp_executesql @str,  
@param,  
@skip_t =@skip,  
@take_t = @take,  
@TotalRecords_t =@TotalRecords OUTPUT,  
@UserId_t= @UserId
;  
END

GO
/****** Object:  StoredProcedure [dbo].[usp_GetAllBankAccountByUserId]    Script Date: 18/12/2018 12:12:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_GetAllBankAccountByUserId]
	@Userid int
AS
BEGIN
--exec usp_GetAllBankAccountByUserId 1
	SET NOCOUNT ON;

	select ba.Id bankAccountId,ba.AccountName,ba.AccountNo,ba.Branch,b.Id as BankId,
	b.BankName,b.BankCode
	from BankAccount ba with(nolock)
	 inner join Banks b with(nolock) on ba.BankId=b.Id
    where ba.IsDeleted=0 and b.IsDeleted=0 and ba.UserId=@Userid and ba.IsActived=1
END


GO
/****** Object:  StoredProcedure [dbo].[usp_GetBankAccountOfSystem]    Script Date: 18/12/2018 12:12:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_GetBankAccountOfSystem]	
AS
BEGIN
	SET NOCOUNT ON;

	select ba.Id bankAccountId,ba.AccountName,ba.AccountNo,ba.Branch,
	b.BankName,b.BankCode
	from BankAccount ba with(nolock)
	 inner join Banks b with(nolock) on ba.BankId=b.Id
	 where ba.IsDeleted=0 and b.IsDeleted=0 and ba.IsActived=1
    --where ba.IsDeleted=0 and b.IsDeleted=0 and ba.IsSystemBank=1 and ba.IsActived=1
END






GO
/****** Object:  StoredProcedure [dbo].[usp_GetBuyAdvertisementByUserId]    Script Date: 18/12/2018 12:12:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[usp_GetBuyAdvertisementByUserId]
(
	@AssetTypeId varchar(5),
	@UserId INT,
	@From INT,
	@To INT
)
AS
BEGIN
	SET NOCOUNT ON;
	
	select temp.id,temp.UserId,MaxCoinPrice,MinCoinNumber,MaxCoinNumber,CurrencyId,c.CurrencyName,b.BankName,AssetTypeId as CoinName,p.PaymentMethodCode, p.PaymentMethodName,
		(case when CoinPrice > 0 then CoinPrice else c.Rate end) as CoinPrice , temp.CreatedDate, temp.UpdatedDate
		from (
			select b.id,b.UserId,CoinPrice,MaxCoinPrice,MinCoinNumber,MaxCoinNumber,CurrencyId,BankId,PaymentMethodId,AssetTypeId, b.CreatedDate, b.UpdatedDate
			,u.UserName 
			FROM BuyAdvertisement b with (nolock) 
			inner join [User] u with (nolock) on b.UserId=u.Id
			Where b.AssetTypeId = @AssetTypeId AND b.UserId = @UserId
			order by b.CreatedDate desc 
			OFFSET @From ROWS FETCH NEXT @To ROWS ONLY
		) as temp

		LEFT JOIN Currency c with (nolock) on c.CurrencyCode=temp.CurrencyId
		LEFT JOIN Banks b with (nolock) on b.Id=temp.BankId
		LEFT JOIN PaymentMethod p with (nolock) on p.Id=temp.PaymentMethodId

		option(recompile)
END



GO
/****** Object:  StoredProcedure [dbo].[usp_GetInfoBuyAdvertisement]    Script Date: 18/12/2018 12:12:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_GetInfoBuyAdvertisement]
 @BuyAdId int
AS
BEGIN
	--exec usp_GetInfoBuyAdvertisement 9
	SET NOCOUNT ON;

   SELECT sa.Id,
		(sa.MaxCoinNumber - isnull(sa.AmountBuy,0)) MaxCoinNumber,
		sa.MinCoinNumber,
		 u.UserName,
		 pm.PaymentMethodName,
		 c.CountryName,
		 sa.PaymentAllowTime,
		 b.BankName,
		 cs.AvgPrice,
		u.Alias,
		 cs.AssetIdBase,
		 sa.UserId,
		-- cur.CurrencyName,
	     case when sa.Approved =1 then cast(1 as bit) else cast( 0 as bit) end IsActived
FROM dbo.BuyAdvertisement sa WITH(NOLOCK)
 INNER JOIN dbo.[User] u WITH(NOLOCK) ON sa.UserId=u.Id
 INNER JOIN dbo.PaymentMethod pm WITH(NOLOCK) ON sa.PaymentMethodId=pm.Id
 INNER JOIN dbo.Country c WITH(NOLOCK) ON sa.CountryId=c.Id
 INNER JOIN dbo.Banks b WITH(NOLOCK) ON sa.BankId=b.Id
 --INNER JOIN dbo.Currency cur WITH(NOLOCK) ON sa.CurrencyId=cur.Id
 INNER JOIN dbo.CoinSymbol cs WITH(NOLOCK) ON sa.AssetTypeId=cs.AssetIdBase
  WHERE sa.Id=@BuyAdId
END



GO
/****** Object:  StoredProcedure [dbo].[usp_GetInfoDetailSellTransactionByUserIdTransId]    Script Date: 18/12/2018 12:12:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_GetInfoDetailSellTransactionByUserIdTransId]
  @UserId int,
  @TransId int
AS
BEGIN
 --exec usp_GetInfoDetailSellTransactionByUserIdTransId 1,7
	SET NOCOUNT ON;

	select t.TransactionId,t.AmountCoin,cs.AssetIdBase,
			u.UserName,cw.WalletAddress,
			t.CreatedDate,p.PaymentMethodName,
			b.BankName,sa.BankAccountName,sa.BankAccountNumber,
			t.EvidenceId, e.Image,e.ContentType,e.FileName,
			sa.PaymentAllowTime,
			t.TransactionCode
	from [Transaction] t
	 inner join CoinWallet cw with(nolock) on t.UserId=cw.UserId and t.AssetTypeId=cw.AssetTypeId
	 inner join CoinSymbol cs with(nolock) on t.AssetTypeId = cs.AssetIdBase
	 inner join SellAdvertisement sa with(nolock) on t.SellAdvertisementId=sa.Id
	 inner join [User] u with(nolock) on  sa.UserId=u.Id
	 inner join PaymentMethod p with(nolock) on sa.PaymentMethodId=p.Id
	 inner join Banks b with(nolock) on sa.BankId= b.Id
	 left join Evidence e with(nolock) on t.EvidenceId=e.Id
	where t.TransactionId=@TransId and t.UserId=@UserId

END


GO
/****** Object:  StoredProcedure [dbo].[usp_GetInfoDetailTransactionByUserIdTransCode]    Script Date: 18/12/2018 12:12:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_GetInfoDetailTransactionByUserIdTransCode]
  @transCode varchar(256)
AS
BEGIN
 --exec usp_GetInfoDetailTransactionByUserIdTransCode '17d1c311954d44d88347f32b1c36fb13'	
	SET NOCOUNT ON;

	select tmp.TransactionId,tmp.AmountCoin,tmp.AssetTypeId,
			u.UserName,u.Alias,cw.WalletAddress,
			tmp.CreatedDate,p.PaymentMethodName,
			b.BankName,tmp.BankAccountNameAds,tmp.BankAccountNumberAds,
			tmp.EvidenceId, tmp.EvidenceCode,
			tmp.PaymentAllowTimeAds,
			tmp.TransactionCode,tmp.IsSellTransaction,
			tmp.Status,
			tmp.UserIdAds,
			tmp.UserIdTran,
			tmp.PaymentType
	from 
		  (
			  select t.TransactionId,
				t.AmountCoin,
				t.EvidenceId,
				e.code EvidenceCode,
				t.TransactionCode,
				t.AssetTypeId,
				t.CreatedDate,
				t.Status,
				t.UserId UserIdTran,
				t.PaymentType,
				case when t.SellAdvertisementId is null then b.UserId else sa.UserId end UserIdAds,
				case when t.SellAdvertisementId is null then b.PaymentMethodId else sa.PaymentMethodId end PaymentMethodIdAds,
				case when t.SellAdvertisementId is null then b.BankId else sa.BankId end BankIdAds,
				case when t.SellAdvertisementId is null then b.BankAccountNumber else sa.BankAccountNumber end BankAccountNumberAds,
				case when t.SellAdvertisementId is null then '' else sa.BankAccountName end BankAccountNameAds,
				case when t.SellAdvertisementId is null then b.PaymentAllowTime else sa.PaymentAllowTime end PaymentAllowTimeAds,
				case when t.SellAdvertisementId is null then cast (0 as bit) else cast(1 as bit) end IsSellTransaction
				--case when t.SellAdvertisementId is null then b.WalletAddress else '' end WalletAddressAds
		from [Transaction] t
		 left join SellAdvertisement sa with(nolock) on t.SellAdvertisementId=sa.Id
		 left join BuyAdvertisement b with(nolock) on t.BuyAdvertisementId=b.Id
		 left join Evidence e with(nolock) on t.EvidenceId=e.Id
		 where t.TransactionCode=@transCode) tmp
	 inner join CoinWallet cw with(nolock) on tmp.UserIdAds=cw.UserId and tmp.AssetTypeId=cw.AssetTypeId
	 --inner join CoinSymbol cs with(nolock) on tmp.CoinType = cs.Id
	 inner join [User] u with(nolock) on  tmp.UserIdAds=u.Id
	 inner join PaymentMethod p with(nolock) on tmp.PaymentMethodIdAds=p.Id
	 inner join Banks b with(nolock) on tmp.BankIdAds= b.Id


END




GO
/****** Object:  StoredProcedure [dbo].[usp_GetInfoSellAdvertisement]    Script Date: 18/12/2018 12:12:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_GetInfoSellAdvertisement]
 @SellAdId int,
 @UserId int
AS
BEGIN
	--exec usp_GetInfoSellAdvertisement 1,1
	SET NOCOUNT ON;

   SELECT sa.Id,
		(sa.MaxCoinNumber - isnull(sa.AmountSell,0) ) MaxCoinNumber,
		sa.MinCoinNumber,
		 u.UserName,
		 pm.PaymentMethodName,
		 c.CountryName,
		 sa.PaymentAllowTime,
		 b.BankName,
		 cs.AvgPrice,
		 u.Alias,
		 cs.AssetIdBase,
		 sa.UserId,
		-- cur.CurrencyName,
		 case when sa.Approved =1 then cast(1 as bit) else cast( 0 as bit) end IsActived,
		 (select top 1 _cw.WalletAddress
		 from  CoinWallet _cw with(nolock) where _cw.UserId=@UserId and _cw.AssetTypeId=sa.AssetTypeId) WalletAddressUser
FROM dbo.SellAdvertisement sa WITH(NOLOCK)
 INNER JOIN dbo.[User] u WITH(NOLOCK) ON sa.UserId=u.Id
 INNER JOIN dbo.PaymentMethod pm WITH(NOLOCK) ON sa.PaymentMethodId=pm.Id
 INNER JOIN dbo.Country c WITH(NOLOCK) ON sa.CountryId=c.Id
 INNER JOIN dbo.Banks b WITH(NOLOCK) ON sa.BankId=b.Id
 --INNER JOIN dbo.Currency cur WITH(NOLOCK) ON sa.CurrencyId=cur.Id
 INNER JOIN dbo.CoinSymbol cs WITH(NOLOCK) ON sa.AssetTypeId=cs.AssetIdBase
  WHERE sa.Id=@SellAdId
END

GO
/****** Object:  StoredProcedure [dbo].[usp_GetSellAdvertisementByUserId]    Script Date: 18/12/2018 12:12:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[usp_GetSellAdvertisementByUserId]
(
	@AssetTypeId VARCHAR(5),
	@UserId INT,
	@From INT,
	@To INT
)
AS
BEGIN
	SET NOCOUNT ON;
	
	select temp.id,temp.UserId,MinCoinPrice,MinCoinNumber,MaxCoinNumber,CurrencyId,c.CurrencyName,b.BankName,AssetTypeId as CoinName,p.PaymentMethodCode,p.PaymentMethodName ,
		(case when CoinPrice > 0 then CoinPrice else c.Rate end) as CoinPrice, temp.CreatedDate, temp.UpdatedDate
		from (
			select s.id,s.UserId,CoinPrice,MinCoinPrice,MinCoinNumber,MaxCoinNumber,CurrencyId,BankId,PaymentMethodId,AssetTypeId, s.CreatedDate, s.UpdatedDate
			,u.UserName 
			FROM SellAdvertisement s with (nolock) 
			inner join [User] u with (nolock) on s.UserId=u.Id
			Where AssetTypeId = @AssetTypeId AND s.UserId = @UserId
			order by s.CreatedDate desc 
			OFFSET @From ROWS FETCH NEXT @To ROWS ONLY
		) as temp

		LEFT JOIN Currency c with (nolock) on c.CurrencyCode=temp.CurrencyId
		LEFT JOIN Banks b with (nolock) on b.Id=temp.BankId
		LEFT JOIN PaymentMethod p with (nolock) on p.Id=temp.PaymentMethodId
		--LEFT JOIN CoinSymbol ct with (nolock) on ct.Id=temp.AssetTypeId 

		option(recompile)
END

GO
/****** Object:  StoredProcedure [dbo].[usp_GetStatisticTransaction]    Script Date: 18/12/2018 12:12:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<uypc>
-- Create date: <2018-06-10>
-- Description:	<Get user information>
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetStatisticTransaction]
	-- Add the parameters for the stored procedure here
    @UserId INT
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;
        DECLARE @TotalTransactionSuccess INT;
        SET @TotalTransactionSuccess = ( SELECT COUNT(TransactionId) Total
                                         FROM   [Transaction]
                                         WHERE  UserId = @UserId
                                                AND [Status] = 1
                                       )
        SELECT  SUM(t.AmountCoin) AS TotalCoin ,
                t.AssetTypeId ,
                t.UserId ,
                @TotalTransactionSuccess TotalTransactionSuccess
        FROM    [Transaction] t
        WHERE   t.UserId = @UserId
        GROUP BY t.AssetTypeId ,
                t.UserId

    END

--EXEC usp_GetUserInfor_With_Transaction 9

GO
/****** Object:  StoredProcedure [dbo].[usp_GetTransactionDetailAdmin]    Script Date: 18/12/2018 12:12:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_GetTransactionDetailAdmin]
	@TransactionID int
AS
BEGIN
	--exec usp_GetTransactionDetailAdmin 10
	SET NOCOUNT ON;
	select
	tr.TransactionCode,
		tr.TransactionId,
		tr.AmountCoin,
		tr.CreatedDate,
		tr.UpdatedDate,
		u.UserName,
		tr.Status,
		tr.AssetTypeId,
		case  when _sa.PaymentAllowTime is null
			then _ba.PaymentAllowTime
			else _sa.PaymentAllowTime
		end PaymentAllowTime,
		uc.UserName UserConfirm,
		tr.EvidenceId,
		e.Code EvidenceCode
 from dbo.[Transaction] tr
 inner join [User] u with(nolock) on tr.UserId=u.Id
  left join SellAdvertisement _sa with(nolock) on _sa.Id= tr.SellAdvertisementId
  left join  BuyAdvertisement _ba with(nolock) on _ba.Id= tr.BuyAdvertisementId
  left join [User] uc with(nolock) on tr.ConfirmBy=uc.Id
  left join Evidence e with(nolock) on tr.EvidenceId=e.Id
  where tr.TransactionId=@TransactionID
END


GO
/****** Object:  StoredProcedure [dbo].[usp_GetUserFullInforByUserId]    Script Date: 18/12/2018 12:12:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<uypc>
-- Create date: <2018-12-16>
-- vn vo dich
-- Description:	<Get user information>
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetUserFullInforByUserId] 
	-- Add the parameters for the stored procedure here
    @UserId INT
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;
		WITH temp AS (
						SELECT ROW_NUMBER() OVER(PARTITION BY ul.UserId ORDER BY ul.LoginDate DESC) rn,*
						FROM dbo.UserLoginHistory ul
						WHERE ul.UserId = @UserId
						)

        SELECT  u.* ,
                ui.*,
				ul.IPAddress,
				ul.LoginDate
        FROM    [User] u
                LEFT JOIN UserInfor ui WITH ( NOLOCK ) ON u.Id = ui.UserId
							LEFT JOIN (
										SELECT * FROM temp WHERE rn = 2
                          ) ul ON u.Id = ul.UserId
        WHERE   u.Id = @UserId
    END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetUserInforById]    Script Date: 18/12/2018 12:12:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<uypc>
-- Create date: <2018-06-09>
-- Description:	<Get user information>
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetUserInforById] 
	-- Add the parameters for the stored procedure here
    @UserId INT
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;
    --    SELECT  ui.UserId ,
    --            ui.IDNumber ,
    --            ui.UrlFaceBook ,
    --            ui.CreatedDate ,
    --            ui.Phone ,
    --            ui.UpdatedDate ,
    --            ui.UrlTwitter ,
    --            ui.HasReceivedDailyPrice ,
    --            ui.HasSendEmailExchangeTransaction ,
    --            ui.HasSendEmailNewTransaction ,
    --            ui.HasSendSMSNewTransaction ,
    --            ui.HasSendSMSTransactionPaid ,
				--ui.[Image],
    --            u.Email ,
    --            u.UserName ,
    --            u.Alias,
				--u.IsVerify,
				--ul.LoginDate LastLoginDate,
				--u.ReferenceCode
    --    FROM    [User] u
    --            INNER JOIN UserInfor ui WITH ( NOLOCK ) ON u.Id = ui.UserId
    --            LEFT JOIN dbo.UserLoginHistory  ul ON ul.UserId = u.Id
    --    WHERE   u.Id = @UserId


	SELECT
			u.*,
			ui.*
        FROM    [User] u
                LEFT JOIN UserInfor ui WITH ( NOLOCK ) ON u.Id = ui.UserId
        WHERE   u.Id = @UserId
    END


GO
/****** Object:  StoredProcedure [dbo].[usp_GetUserInforByUserName]    Script Date: 18/12/2018 12:12:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<uypc>
-- Create date: <2018-06-09>
-- Description:	<Get user information>
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetUserInforByUserName] 
	-- Add the parameters for the stored procedure here
    @UserName VARCHAR(50)
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;
        SELECT  u.*,ui.*
        FROM    [User] u
                LEFT JOIN UserInfor ui WITH ( NOLOCK ) ON u.Id = ui.UserId
        WHERE   u.UserName = @UserName
    END


GO
/****** Object:  StoredProcedure [dbo].[usp_QuickBuyAdvertisement]    Script Date: 18/12/2018 12:12:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[usp_QuickBuyAdvertisement]
(
	@AssetTypeId VARCHAR(5),
	@CurrencyType VARCHAR(5),
	@CoinNumber Decimal(18,5)
)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @BuyPrice Decimal(18,5) = 0, @CurrencyRate Decimal(18,5) = 0
	
	select top 1 @BuyPrice = BuyPrice from CoinSymbol  with (nolock) where AssetIdBase=@AssetTypeId
	select top 1 @CurrencyRate = Rate from Currency  with (nolock) where CurrencyCode=@CurrencyType  
	
	-- chi lay nhung user dang online
	select temp.id,u.UserName,b.BankName,(CoinPrice * @CoinNumber * @BuyPrice) as Amount, (@BuyPrice * CoinPrice) as ExchangeRate
	from (
		select top 10 b.id,b.UserId,BankId,(case when CoinPrice > 0 then CoinPrice else @CurrencyRate end) as CoinPrice
		From BuyAdvertisement b with (nolock) 		
		Where Approved = 1 and AssetTypeId = @AssetTypeId and CurrencyId = @CurrencyType and b.MinCoinNumber > = @CoinNumber
		and exists (select top 1 * from UserLoginHistory uh with (nolock) where uh.UserId=b.UserId)

	) as temp

	INNER JOIN [User] u with (nolock) on u.Id = temp.UserId
	LEFT JOIN Banks b with (nolock) on b.Id=temp.BankId		
END





GO
/****** Object:  StoredProcedure [dbo].[usp_QuickSellAdvertisement]    Script Date: 18/12/2018 12:12:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[usp_QuickSellAdvertisement]
(
	@AssetTypeId VARCHAR(5),
	@CurrencyType VARCHAR(5),
	@CoinNumber Decimal(18,5)
)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @BuyPrice Decimal(18,5) = 0, @CurrencyRate Decimal(18,5) = 0
	
	select top 1 @BuyPrice = BuyPrice from CoinSymbol  with (nolock) where AssetIdBase=@AssetTypeId
	select top 1 @CurrencyRate = Rate from Currency  with (nolock) where CurrencyCode=@CurrencyType 
	
	-- chi lay nhung user dang online
	select temp.id,u.UserName,b.BankName,(CoinPrice * @CoinNumber * @BuyPrice) as Amount, (@BuyPrice * CoinPrice) as ExchangeRate
	from (
		select top 10 s.id,s.UserId,BankId,(case when CoinPrice > 0 then CoinPrice else @CurrencyRate end) as CoinPrice
		From SellAdvertisement s with (nolock) 				
		Where Approved = 1 and AssetTypeId = @AssetTypeId and CurrencyId = @CurrencyType and s.MinCoinNumber > = @CoinNumber
		and exists (select top 1 * from UserLoginHistory uh with (nolock) where uh.UserId=s.UserId)
	) as temp

	INNER JOIN [User] u with (nolock) on u.Id = temp.UserId
	LEFT JOIN Banks b with (nolock) on b.Id=temp.BankId		
END





GO
/****** Object:  StoredProcedure [dbo].[usp_SearchUser_Paging]    Script Date: 18/12/2018 12:12:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<uypc>
-- Create date: <2018-06-13>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_SearchUser_Paging] 
	@Email VARCHAR(100) = NULL,
	@FromDate DATETIME = NULL,
	@ToDate DATETIME = NULL,
	@Take INT = 10,
	@Skip INT = 0,
	@TotalRecords  INT OUTPUT
AS
BEGIN
 /*
  declare @i int
  exec usp_SearchUser_Paging  @Email=null,@FromDate=null,@ToDate=null,@Take=10,@Skip=0,@TotalRecords=@i out
  select @i
 */
	SET NOCOUNT ON;

	SET @TotalRecords = (SELECT COUNT(*) 
	FROM dbo.[User] u WITH (NOLOCK)
	INNER JOIN dbo.UserInfor ui WITH (NOLOCK) ON ui.UserId = u.Id	
	WHERE 
		(@Email IS NULL OR @Email = u.Email)
	AND (@FromDate IS NULL OR u.CreatedDate >= @FromDate)
	AND (@ToDate IS NULL OR u.CreatedDate <= @ToDate))

	SELECT u.Id, 
		u.UserName,
		u.Alias,
		u.Email,
		u.IsVerify,
		u.IsActive,
		u.CreatedDate, 
		u.UpdatedDate,
		ui.HasSendEmailNewTransaction,
		ui.HasSendEmailExchangeTransaction,
		ui.HasSendEmailMessageTransaction,
		ui.HasSendSMSNewTransaction,
		ui.HasSendSMSTransactionPaid,
		ui.IDNumber,
		ui.UrlFaceBook,
		ui.UrlTwitter,
		ui.[Image],
		ui.Phone
	FROM dbo.[User] u WITH (NOLOCK)
	INNER JOIN dbo.UserInfor ui WITH (NOLOCK) ON ui.UserId = u.Id	
	WHERE 
		(@Email IS NULL OR @Email = u.Email)
	AND (@FromDate IS NULL OR u.CreatedDate >= @FromDate)
	AND (@ToDate IS NULL OR u.CreatedDate <= @ToDate)
	ORDER BY u.Id DESC
	OFFSET @Skip ROWS  FETCH NEXT @Take ROWS ONLY OPTION(RECOMPILE)

END


GO
/****** Object:  StoredProcedure [dbo].[usp_SelectPaging_TransactionAdminSearch]    Script Date: 18/12/2018 12:12:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_SelectPaging_TransactionAdminSearch]  
(  
@SelectedPage INT = NULL,  
@PageSize INT = NULL,  
@TotalRecords INT OUT,  
@TransMode INT = NULL,  
@AssetTypeId varchar(5) = NULL,  
@Status int =null,  
@FromDate date = null,  
@ToDate date = null,  
@TransactionCode varchar(256) = null,
@OrderBy NVARCHAR(1000) = null  
)  
AS  
/*  
 declare @t1 int   
 exec usp_SelectPaging_TransactionAdminSearch 0,10,@t1 out,1,null,null,null,null,'177925352403495b954d9318b3aa1398',null  
 select @t1  
*/  
  
DECLARE @strSQL_0 NVARCHAR(MAX),@strSQL_1 NVARCHAR(MAX),@strSQL_2 NVARCHAR(MAX),@strSQL_3 NVARCHAR(MAX)  
DECLARE @param NVARCHAR(MAX)  
SET @strSQL_0 = '';SET @strSQL_1 = '';SET @strSQL_2 = ''; SET @strSQL_3 = '';  
SET @param = ''  
BEGIN  
  
 BEGIN  
  SET @strSQL_2 = ' Where 1=1 '  
  IF(@TransMode IS NOT NULL)  
    BEGIN  
       if (@TransMode = 1)  
     begin  
    Set @strSQL_2 = @strSQL_2 + ' And tr.BuyAdvertisementId is not null '  
     end  
     else  
     begin  
    Set @strSQL_2 = @strSQL_2 + ' And tr.SellAdvertisementId is not null '  
     end  
   END  
   if (@TransactionCode is not null)
	 begin
	   Set @strSQL_2 = @strSQL_2 + ' And tr.TransactionCode = @TransactionCode_t' 
	 end

  IF(@AssetTypeId IS NOT NULL)  
   BEGIN  
    Set @strSQL_2 = @strSQL_2 + ' And tr.AssetTypeId = @AssetTypeId_t '  
   END  
  IF(@Status IS NOT NULL)  
   BEGIN  
    Set @strSQL_2 = @strSQL_2 + ' And tr.Status = @Status_t '  
   END  
    
  IF(@FromDate IS NOT NULL)  
   BEGIN  
    Set @strSQL_2 = @strSQL_2 + ' And tr.CreatedDate >=  @FromDate_t '  
   END  
    
  IF(@ToDate IS NOT NULL)  
   BEGIN  
    Set @strSQL_2 = @strSQL_2 + ' And tr.CreatedDate  <= @ToDate_t '  
   END  
 END  
--Order  
 BEGIN  
  IF (@OrderBy IS NULL or @OrderBy = '')  
   SET @strSQL_3 = @strSQL_3 + ' ORDER BY TransactionId desc'  
  IF(@OrderBy = '0-ASC')  
    Set @strSQL_3 = @strSQL_3 + ' ORDER BY TransactionCode asc '  
  IF(@OrderBy = '0-DESC')  
    Set @strSQL_3 = @strSQL_3 + ' ORDER BY TransactionCode DESC '  
  
    IF(@OrderBy = '1-ASC')  
    Set @strSQL_3 = @strSQL_3 + ' ORDER BY CreatedDate asc '  
  IF(@OrderBy = '1-DESC')  
    Set @strSQL_3 = @strSQL_3 + ' ORDER BY CreatedDate DESC '  
  
    IF(@OrderBy = '2-ASC')  
    Set @strSQL_3 = @strSQL_3 + ' ORDER BY AmountCoin asc '  
  IF(@OrderBy = '2-DESC')  
    Set @strSQL_3 = @strSQL_3 + ' ORDER BY AmountCoin DESC '  
  
    IF(@OrderBy = '3-ASC')  
    Set @strSQL_3 = @strSQL_3 + ' ORDER BY UpdatedDate asc '  
  IF(@OrderBy = '3-DESC')  
    Set @strSQL_3 = @strSQL_3 + ' ORDER BY UpdatedDate DESC '  
  
    IF(@OrderBy = '4-ASC')  
    Set @strSQL_3 = @strSQL_3 + ' ORDER BY UserName asc '  
  IF(@OrderBy = '4-DESC')  
    Set @strSQL_3 = @strSQL_3 + ' ORDER BY UserName DESC '  
  
      
  
    IF(@OrderBy = '5-ASC')  
    Set @strSQL_3 = @strSQL_3 + ' ORDER BY Status asc '  
  IF(@OrderBy = '5-DESC')  
    Set @strSQL_3 = @strSQL_3 + ' ORDER BY Status DESC '  
  
    IF(@OrderBy = '6-ASC')  
    Set @strSQL_3 = @strSQL_3 + ' ORDER BY AssetTypeId asc '  
  IF(@OrderBy = '6-DESC')  
    Set @strSQL_3 = @strSQL_3 + ' ORDER BY AssetTypeId DESC '  
  
    IF(@OrderBy = '7-ASC')  
    Set @strSQL_3 = @strSQL_3 + ' ORDER BY PaymentAllowTime asc '  
  IF(@OrderBy = '7-DESC')  
    Set @strSQL_3 = @strSQL_3 + ' ORDER BY PaymentAllowTime DESC '  
  
    IF(@OrderBy = '8-ASC')  
    Set @strSQL_3 = @strSQL_3 + ' ORDER BY UserConfirm asc '  
  IF(@OrderBy = '8-DESC')  
    Set @strSQL_3 = @strSQL_3 + ' ORDER BY UserConfirm DESC '  
  
 END  
--For Total  
SET @strSQL_0 =  
    N' Select @TotalRecords_t = Count(TransactionId)  
    FROM [Transaction] tr '   
SET @strSQL_0 = @strSQL_0 + @strSQL_2   
--For Detail  
SET @strSQL_1 =  
N'   
BEGIN  
    select TransactionCode,TransactionId,AmountCoin,CreatedDate,UpdatedDate,UserName,  
     Status,AssetTypeId,PaymentAllowTime,UserConfirm  
    from (  
    select   
     tr.TransactionCode,  
     tr.TransactionId,  
     tr.AmountCoin,  
     tr.CreatedDate,  
     tr.UpdatedDate,  
     u.UserName,  
     tr.Status,  
     tr.AssetTypeId,  
     case  when _sa.PaymentAllowTime is null  
      then _ba.PaymentAllowTime  
      else _sa.PaymentAllowTime  
     end PaymentAllowTime,  
     uc.UserName UserConfirm  
    from dbo.[Transaction] tr  
    inner join [User] u with(nolock) on tr.UserId=u.Id  
     left join SellAdvertisement _sa with(nolock) on _sa.Id= tr.SellAdvertisementId  
     left join  BuyAdvertisement _ba with(nolock) on _ba.Id= tr.BuyAdvertisementId  
     left join [User] uc with(nolock) on tr.ConfirmBy=uc.Id '+@strSQL_2+' ) tmp'  
SET @strSQL_1 =@strSQL_1+ @strSQL_3 + '  
    Offset @SelectedPage_t rows   
 fetch next @PageSize_t rows only'  
SET @strSQL_1 = @strSQL_1 + ' END'  
--ELSE  
--SET @strSQL_1 = @strSQL_1 + ' END'  
SET @param = @param + N'  
@SelectedPage_t INT =null ,  
@PageSize_t INT =null,  
@TotalRecords_t int output,  
@TransMode_t INT =null,  
@AssetTypeId_t varchar(5) =null,  
@Status_t int =null ,  
@FromDate_t date =null ,  
@ToDate_t date =null,  
@TransactionCode_t varchar(256) = null,
@OrderBy_t NVARCHAR(1000) = null  
  
  
'  
DECLARE @str NVARCHAR(MAX) = (@strSQL_0 + @strSQL_1)  
PRINT @str  
EXECUTE sp_executesql @str,  
@param,  
@SelectedPage_t =@SelectedPage,  
@PageSize_t = @PageSize,  
@TotalRecords_t =@TotalRecords OUTPUT,  
@TransMode_t =@TransMode,  
@AssetTypeId_t =@AssetTypeId,  
@Status_t =@Status ,  
@FromDate_t =@FromDate,  
@ToDate_t =@ToDate,  
@OrderBy_t =@OrderBy,  
@TransactionCode_t = @TransactionCode
;  
END

GO
/****** Object:  StoredProcedure [dbo].[usp_SelectPaging_TransactionWalletAdminSearch]    Script Date: 18/12/2018 12:12:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_SelectPaging_TransactionWalletAdminSearch]  
(  
	@skip INT = NULL,  
	@take INT = NULL,  
	@TotalRecords INT OUT,  
	@AssetTypeId varchar(200) = NULL,  
	@Status varchar(200) = null,
	@FromDate date = null,  
	@ToDate date = null,  
	@TransactionCode varchar(256) = null,
	@UserName varchar(100) = null,
	@OrderBy NVARCHAR(1000) = null  
)  
AS  
/*  
 declare @t1 int   
 exec usp_SelectPaging_TransactionWalletAdminSearch 0,10,@t1 out,"'VND','BTC'",'1,2',null,null,null,null,null  
 select @t1  
*/  
  
DECLARE @strSQL_0 NVARCHAR(MAX),@strSQL_1 NVARCHAR(MAX),@strSQL_2 NVARCHAR(MAX),@strSQL_3 NVARCHAR(MAX)  
DECLARE @param NVARCHAR(MAX)  
SET @strSQL_0 = '';SET @strSQL_1 = '';SET @strSQL_2 = ''; SET @strSQL_3 = '';  
SET @param = ''  
BEGIN  
  
 BEGIN  
  SET @strSQL_2 = ' Where 1=1 '  
   if (@TransactionCode is not null)
	 begin
	   Set @strSQL_2 = @strSQL_2 + ' And tr.TransactionCode like concat(''%'', @TransactionCode_t,''%'') '
	 end

  IF(@AssetTypeId IS NOT NULL)  
   BEGIN  
    Set @strSQL_2 = @strSQL_2 + ' And tr.AssetTypeId in ( '+@AssetTypeId+') '  
   END  
  IF(@Status IS NOT NULL)  
   BEGIN  
    Set @strSQL_2 = @strSQL_2 + ' And tr.Status in ('+@Status+') '  
   END  
    
  IF(@FromDate IS NOT NULL)  
   BEGIN  
    Set @strSQL_2 = @strSQL_2 + ' And tr.CreatedDate >=  @FromDate_t '  
   END  
    
  IF(@ToDate IS NOT NULL)  
   BEGIN  
    Set @strSQL_2 = @strSQL_2 + ' And tr.CreatedDate  <= @ToDate_t '  
   END  
   if (@UserName is not null)
   begin
	set @strSQL_2 = @strSQL_2 + ' and u.UserName LIKE CONCAT(''%'',@UserName_t,''%'')'
   end
 END  
--Order  
 BEGIN  
  IF (@OrderBy IS NULL or @OrderBy = '')  
   SET @strSQL_3 = @strSQL_3 + ' ORDER BY TransactionId desc'  
 END  
--For Total  
SET @strSQL_0 =  
    N' Select @TotalRecords_t = Count(TransactionId)  
    FROM [TransactionWallet] tr  inner join [User] u with(nolock) on tr.UserId=u.Id  '   
SET @strSQL_0 = @strSQL_0 + @strSQL_2   
--For Detail  
SET @strSQL_1 =  
N'   
BEGIN  
    select TransactionCode,TransactionId,Amount,CreatedDate,UpdatedDate,UserName,  
     Status,UserConfirm ,EvidenceDate,TransactionType, ConfirmDate,Unit
    from (  
    select   
     tr.TransactionCode,  
     tr.TransactionId,  
     tr.Amount,  
     tr.CreatedDate,  
     tr.UpdatedDate,  
     u.UserName ,  
     tr.Status,  
	 tr.EvidenceDate,
	 tr.TransactionType,
	 tr.ConfirmDate,
     uc.UserName UserConfirm ,
	 tr.AssetTypeId Unit 
    from dbo.[TransactionWallet] tr  
    inner join [User] u with(nolock) on tr.UserId=u.Id 
     left join [User] uc with(nolock) on tr.ConfirmBy=uc.Id 
	 '+@strSQL_2+' ) tmp'  
SET @strSQL_1 =@strSQL_1+ @strSQL_3 + '  
    Offset @skip_t rows   
 fetch next @take_t rows only'  
SET @strSQL_1 = @strSQL_1 + ' END'  
--ELSE  
--SET @strSQL_1 = @strSQL_1 + ' END'  
SET @param = @param + N'  
@Skip_t INT =null ,  
@take_t INT =null,  
@TotalRecords_t int output,  
@AssetTypeId_t varchar(200) =null,  
@FromDate_t date =null ,  
@ToDate_t date =null,  
@TransactionCode_t varchar(256) = null,
@UserName_t varchar(100) = null,
@OrderBy_t NVARCHAR(1000) = null  
'  
DECLARE @str NVARCHAR(MAX) = (@strSQL_0 + @strSQL_1)  
PRINT @str  
EXECUTE sp_executesql @str,  
@param,  
@skip_t =@skip,  
@take_t = @take,  
@TotalRecords_t =@TotalRecords OUTPUT,  
@AssetTypeId_t =@AssetTypeId,  
@FromDate_t =@FromDate,  
@ToDate_t =@ToDate,  
@OrderBy_t =@OrderBy,  
@TransactionCode_t = @TransactionCode,
@UserName_t= @UserName
;  
END

GO
/****** Object:  StoredProcedure [dbo].[usp_SellAdvertisement]    Script Date: 18/12/2018 12:12:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[usp_SellAdvertisement]
(
	@AssetTypeId VARCHAR(5),
	@IsGetOfflineUser BIT,
	@From INT,
	@To INT
)
AS
BEGIN
	SET NOCOUNT ON;
	
	-- lay tat ca user online va offline

	DECLARE @avgPrice decimal(18,4) = 0
	select top 1 @avgPrice = AvgPrice from CoinSymbol with (nolock) where AssetIdBase = @AssetTypeId

	IF(@IsGetOfflineUser = 1)
	BEGIN
		select temp.id,temp.UserId,MinCoinPrice,MinCoinNumber,MaxCoinNumber,CurrencyId,CurrencyId as CurrencyName,b.BankName,AssetTypeId as CoinName,p.PaymentMethodName ,
		/*(case when CoinPrice > 0 then CoinPrice else c.Rate end)*/ @avgPrice as CoinPrice,Amount,alias as UserName
		from (
			select s.id,s.UserId,CoinPrice,MinCoinPrice,MinCoinNumber,MaxCoinNumber,CurrencyId,BankId,PaymentMethodId,AssetTypeId,u.alias,(MaxCoinNumber - ISNULL(s.AmountSell,0)) as Amount 
			From SellAdvertisement s with (nolock) 
			inner join [User] u with (nolock) on s.UserId=u.Id
			WHERE s.Approved = 1 AND AssetTypeId = @AssetTypeId and (MaxCoinNumber - ISNULL(s.AmountSell,0)) > 0
			order by s.CreatedDate desc 
			OFFSET @From ROWS FETCH NEXT @To ROWS ONLY
		) as temp

		--LEFT JOIN Currency c with (nolock) on c.CurrencyCode=temp.CurrencyId
		LEFT JOIN Banks b with (nolock) on b.Id=temp.BankId
		LEFT JOIN PaymentMethod p with (nolock) on p.Id=temp.PaymentMethodId		

		option(recompile)
	END	
	ELSE	-- chi lay nhung user online
	BEGIN
		select temp.id,temp.UserId,MinCoinPrice,MinCoinNumber,MaxCoinNumber,CurrencyId,CurrencyId as CurrencyName,b.BankName,AssetTypeId as CoinName,p.PaymentMethodName,
		/*(case when CoinPrice > 0 then CoinPrice else c.Rate end)*/ @avgPrice as CoinPrice,Amount,alias as UserName 
		from (
			select s.id,s.UserId,CoinPrice,MinCoinPrice,MinCoinNumber,MaxCoinNumber,CurrencyId,BankId,PaymentMethodId,AssetTypeId,u.alias,(MaxCoinNumber - ISNULL(s.AmountSell,0)) as Amount  
			From SellAdvertisement s with (nolock) 
			inner join [User] u with (nolock) on s.UserId=u.Id
			WHERE s.Approved = 1 AND AssetTypeId = @AssetTypeId and exists (select top 1 * from UserLoginHistory uh with (nolock) where uh.UserId=s.UserId) and (MaxCoinNumber - ISNULL(s.AmountSell,0)) > 0
			order by s.CreatedDate desc 
			OFFSET @From ROWS FETCH NEXT @To ROWS ONLY
		) as temp

		--LEFT JOIN Currency c with (nolock) on c.CurrencyCode=temp.CurrencyId
		LEFT JOIN Banks b with (nolock) on b.Id=temp.BankId
		LEFT JOIN PaymentMethod p with (nolock) on p.Id=temp.PaymentMethodId		

		option(recompile)
	END	
END



GO
/****** Object:  StoredProcedure [dbo].[usp_Transaction_GetForCancel]    Script Date: 18/12/2018 12:12:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_Transaction_GetForCancel]

AS
BEGIN
--exec usp_Transaction_GetForCancel
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;
    select tra.*
	from [Transaction] tra
	 inner join SellAdvertisement sel on tra.SellAdvertisementId = sel.Id
	where tra.[Status]=1 and DATEADD(MINUTE,sel.PaymentAllowTime,tra.CreatedDate) < getdate()
END

GO
/****** Object:  StoredProcedure [dbo].[usp_Transaction_GetHistoryBy_UserIdStatus]    Script Date: 18/12/2018 12:12:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_Transaction_GetHistoryBy_UserIdStatus]
	@UserId int,
	@Status int,
	@Skip int,
	@Take int,
	@TotalRecords INT OUT
AS
/*
declare @t1 int  
exec usp_Transaction_GetHistoryBy_UserIdStatus 14,3,0,5,@t1 out
 select @t1
*/

DECLARE @strSQL_0 NVARCHAR(MAX),@strSQL_1 NVARCHAR(MAX),@strSQL_CON NVARCHAR(MAX),@strSQL_Order NVARCHAR(MAX)  
DECLARE @param NVARCHAR(MAX)  
SET @strSQL_0 = '';SET @strSQL_1 = '';SET @strSQL_CON = ''; SET @strSQL_Order = '';  
SET @param = ''  
BEGIN  
  
 BEGIN  
  SET @strSQL_CON = ' Where trans.UserId=@UserId_t '  
  if (@Status = 1)
	begin
	 set @strSQL_CON = @strSQL_CON +' and trans.Status in (1,4)'
	end

	if (@Status = 2)
	begin
	 set @strSQL_CON = @strSQL_CON +' and trans.Status in (3,5)'
	end

	if (@Status = 3)
	begin
	 set @strSQL_CON = @strSQL_CON +' and trans.Status =2'
	end

 END  
--Order  
 --BEGIN  
 --  SET @strSQL_Order = @strSQL_Order + ' ORDER BY TransactionId desc'  
 --END  
--For Total  
SET @strSQL_0 =  
    N' Select @TotalRecords_t = Count(trans.TransactionId)  
    FROM [Transaction] trans with(nolock)  '   
SET @strSQL_0 = @strSQL_0 + @strSQL_CON   
--For Detail  
SET @strSQL_1 =  
N'   
BEGIN  
  select t.TransactionId,t.TransactionCode,t.AmountCoin,t.AssetTypeId,
			t.CreatedDate, t.UpdatedDate,t.EvidenceId,t.Description,t.PaymentAllowTime,t.UserAdsId,
			t.AdsId,t.IsSelAds,u.Alias,u.UserName
   from 
 (select trans.TransactionId,trans.TransactionCode,trans.AmountCoin,trans.AssetTypeId,
			trans.CreatedDate, trans.UpdatedDate,trans.ConfirmBy,trans.EvidenceId,trans.Description,
			case when sel.Id is null then buy.PaymentAllowTime else sel.PaymentAllowTime end PaymentAllowTime,
			case when sel.Id is null then buy.UserId else sel.UserId end UserAdsId,
			case when sel.Id is null then buy.Id else sel.Id end AdsId,
			CAST(IIF ( sel.Id is not null, 1, 0 ) AS BIT) IsSelAds
	from [Transaction] trans with(nolock)
		left join SellAdvertisement sel with(nolock) on trans.SellAdvertisementId =sel.Id
		left join BuyAdvertisement buy with(nolock) on trans.BuyAdvertisementId =buy.Id
	'+@strSQL_CON+'
	order by trans.transactionid desc
	offset @Skip_t rows
	fetch next @Take_t rows only  ) t
	inner join [User] u with(nolock) on t.UserAdsId=u.id
 END'  

SET @param = @param + N'  
@UserId_t int ,
@Skip_t INT  ,  
@take_t INT ,  
@TotalRecords_t int output  
'  
DECLARE @str NVARCHAR(MAX) = ( @strSQL_0 +@strSQL_1)
PRINT @str  
EXECUTE sp_executesql @str,  
@param,  
@UserId_t= @UserId,
@Skip_t =@Skip,  
@Take_t = @Take,  
@TotalRecords_t =@TotalRecords OUTPUT
;  

END


GO
/****** Object:  StoredProcedure [dbo].[usp_TransactionHistoryByUserIdStatus_Paging]    Script Date: 18/12/2018 12:12:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_TransactionHistoryByUserIdStatus_Paging]
(  
	@UserId int,
	@Status int,/* 1 : giao dịch đang mở, 2: giao dịch đã đóng*/
	@Take INT = 10,
	@Skip INT = 0,
	@TotalRecords  INT OUTPUT
)  
AS  
/*  
 declare @t1 int   
 exec usp_TransactionHistoryByUserIdStatus_Paging 11,1,10,0,@t1 out
 select @t1  
*/  
  
DECLARE @strSQL_0 NVARCHAR(MAX),@strSQL_1 NVARCHAR(MAX),@strSQL_2 NVARCHAR(MAX),@strSQL_3 NVARCHAR(MAX) , @strSQL_4 nvarchar(max) ,@strSQL_5 nvarchar(max), @strSQL_6 nvarchar(max)
DECLARE @param NVARCHAR(MAX)  
SET @strSQL_0 = '';SET @strSQL_1 = '';SET @strSQL_2 = '';set @strSQL_4=''; SET @strSQL_3 = '';set  @strSQL_5=''; set @strSQL_6='';
SET @param = ''    
  
 BEGIN  
  SET @strSQL_2 = ' Where sb.UserId = @UserId_t '  
  set @strSQL_4= ' where t.UserId = @UserId_t '
  begin
	   if (@Status =1 )
		 begin
		   Set @strSQL_5 = @strSQL_5 + ' and t.Status in (1,4)' /*1: mới, 4: chờ xác nhận*/
		 end
		 else
		 begin
			Set @strSQL_5 = @strSQL_5 + ' and t.Status in (2,3,5)'  /*2: thành công, 3: từ chối, 5: hủy*/
		 end
	set @strSQL_2 = @strSQL_2 + @strSQL_5
	set @strSQL_4 = @strSQL_4 + @strSQL_5
   END  
--Order  
   set @strSQL_3=@strSQL_3 +' order by tmp.CreatedDate desc'

-- query detail
 set @strSQL_6= N'
			 select t.TransactionCode,t.CreatedDate,t.UpdatedDate,t.AmountCoin,t.Status,t.AssetTypeId, 2 as TransactionMode,t.UserId
					from dbo.[Transaction] t with(nolock)
					 inner join SellAdvertisement sb with(nolock)  on t.SellAdvertisementId=sb.Id
					 '+@strSQL_2+'
					union
					select t.TransactionCode,t.CreatedDate,t.UpdatedDate,t.AmountCoin,t.Status,t.AssetTypeId, 1 as TransactionMode,t.UserId
					from dbo.[Transaction] t with(nolock)
					 inner join BuyAdvertisement sb with(nolock)  on t.BuyAdvertisementId=sb.Id
					 '+@strSQL_2+'
					 union
					 select t.TransactionCode,t.CreatedDate,t.UpdatedDate,t.AmountCoin,t.Status,t.AssetTypeId, 
						case when t.SellAdvertisementId is null then 1 else 2 end TransactionMode,
						case when t.SellAdvertisementId is null then b.UserId else s.UserId end UserId
					from dbo.[Transaction] t with(nolock)
					 left join SellAdvertisement s with(nolock) on t.SellAdvertisementId = s.Id
					 left join BuyAdvertisement b with(nolock) on t.BuyAdvertisementId= b.Id
					 ' + @strSQL_4

--For Total  
SET @strSQL_0 =  N'
			;with tmp as ('+@strSQL_6+') Select @TotalRecords_t = Count(*)  from tmp
    '
--For Detail  
SET @strSQL_1 =  
N'   
   begin
	;with tmp as ('+@strSQL_6+') 
	select tmp.TransactionCode,tmp.CreatedDate,tmp.UpdatedDate,tmp.Status,tmp.AssetTypeId,tmp.TransactionMode, tmp.AmountCoin,u.UserName,u.Alias
	from tmp 
	  inner join dbo.[User] u with(nolock) on tmp.UserId = u.Id
	' 
SET @strSQL_1 =@strSQL_1+ @strSQL_3 + '  
    OFFSET @Skip_t ROWS  FETCH NEXT @Take_t ROWS ONLY OPTION(RECOMPILE)'  
SET @strSQL_1 = @strSQL_1 + ' END'  

SET @param = @param + N'  
@UserId_t INT  ,  
@Status_t INT ,  
@Take_t int,
@Skip_t int,
@TotalRecords_t int output
'  
DECLARE @str NVARCHAR(MAX) =(@strSQL_0 + @strSQL_1)  
PRINT @str  
EXECUTE sp_executesql @str,  
@param,  
@UserId_t =@UserId,  
@Status_t = @Status,  
@Take_t=@Take,
@Skip_t =@Skip,
@TotalRecords_t =@TotalRecords OUTPUT
	;  
END

GO
/****** Object:  StoredProcedure [dbo].[usp_TransactionWallet_DetailVND_Get]    Script Date: 18/12/2018 12:12:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[usp_TransactionWallet_DetailVND_Get]
	@transcode varchar(256)
AS
BEGIN
  --exec usp_TransactionWallet_DetailVND_Get ''
	SET NOCOUNT ON;
	select tw.Amount,
			tw.TransactionCode,
			ba.Branch,	b.BankName,
			ba.AccountNo,ba.AccountName,
			tw.TransactionId,tw.Status,tw.EvidenceId,tw.UserId,
			tw.TransactionType,tw.ConfirmDate,tw.CreatedDate,tw.UpdatedDate
	from TransactionWallet tw
	  inner join BankAccount ba with(nolock) on tw.BankAccountId=ba.Id
	  inner join Banks b with(nolock) on ba.BankId=b.Id
	where tw.TransactionCode=@transcode
END


GO
/****** Object:  StoredProcedure [dbo].[usp_TransactionWallet_Paging]    Script Date: 18/12/2018 12:12:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_TransactionWallet_Paging]
	@UserId int,
	@AssetTypeId varchar(5),
	@TransactionType int=0,
	@Take INT = 10,
	@Skip INT = 0,
	@TotalRecords  INT OUTPUT
AS
BEGIN
/*
 declare @t1 int
 set @t1=null
 exec usp_TransactionWallet_Paging @UserId=1,@CointTypeName='VND',@TransactionType=0,@Take=10,@Skip=0,@TotalRecords=@t1 output
 select @t1
*/
	SET NOCOUNT ON;

	select @TotalRecords=count(*)
	from TransactionWallet tw
	where tw.UserId=@UserId and tw.AssetTypeId=@AssetTypeId and (@TransactionType = 0 or tw.TransactionType = @TransactionType)


	select tw.TransactionId,tw.TransactionCode,
			tw.CreatedDate,tw.Amount,
			tw.Status, tw.TransactionType, tw.Description, tw.ConfirmBy, tw.ConfirmDate
	from TransactionWallet tw
	where tw.UserId=@UserId and tw.AssetTypeId=@AssetTypeId and (@TransactionType = 0 or tw.TransactionType = @TransactionType)
	order by tw.CreatedDate desc
	OFFSET @Skip ROWS  FETCH NEXT @Take ROWS ONLY OPTION(RECOMPILE)
END



GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The account receives commission' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Commission', @level2type=N'COLUMN',@level2name=N'BeneficiaryUserId'
GO
