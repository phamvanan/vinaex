﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Narnia.Core.Domain
{
    public class WithdrawHistory
    {
        public int Id { get; set; }
        public DateTime CreatedDate { get; set; }
        public WithdrawalStatus Status { get; set; }
        public decimal Amount { get; set; }
        public string ToAddress { get; set; }
        public int AccountId { get; set; }
        public string TransactionHash { get; set; }
    }
    public enum WithdrawalStatus
    {
        Pending = 0,
        Processing = 1,
        Success = 2,
        Rejected = 3
    }
}
