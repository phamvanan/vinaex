﻿using MicroOrm.Pocos.SqlGenerator.Attributes;
using System;

namespace Narnia.Eth.TransactionPolling.Domain
{
    public class TransactionWallet
    {
        [KeyProperty(Identity = true)]
        public int TransactionId { get; set; }

        public string AssetTypeId { get; set; }

        public decimal Amount { get; set; }

        public int UserId { get; set; }

        public int? BankAccountId { get; set; }

        public int Status { get; set; }

        public string Description { get; set; }

        public string TransactionCode { get; set; }

        public int? ConfirmBy { get; set; }

        public DateTime? ConfirmDate { get; set; }

        public int? EvidenceId { get; set; }

        public DateTime? EvidenceDate { get; set; }

        [NonStored]
        public Byte[] VersionNo { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime? UpdatedDate { get; set; }
        public int TransactionType { get; set; }
        public string OrderNo { get; set; }
    }
}