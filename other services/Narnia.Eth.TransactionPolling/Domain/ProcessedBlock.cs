﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Narnia.Eth.TransactionPolling.Domain
{
    public class ProcessedBlock
    {
        public int BlockNumber { get; set; }
    }
}
