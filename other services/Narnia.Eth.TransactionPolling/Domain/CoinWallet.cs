﻿using MicroOrm.Pocos.SqlGenerator.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Core.Domain
{
    public class CoinWallet : BaseEntity
    {
        public int UserId { get; set; }
        public string WalletAddress { get; set; }
        public string WalletPassword { get; set; }
        public int LastBlockSync { get; set; }
        public decimal Amount { get; set; }
        public int CoinType { get; set; }
        public string Type { get; set; }
    }
}
