﻿using MicroOrm.Pocos.SqlGenerator.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Core.Domain
{
    public class Currency : BaseEntity
    {
        public string CurrencyCode { get; set; }
        public string CurrencyName { get; set; }
        public string Description { get; set; }
        public float Rate { get; set; }
    }
}
