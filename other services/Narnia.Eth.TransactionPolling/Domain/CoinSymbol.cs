﻿using MicroOrm.Pocos.SqlGenerator.Attributes;

namespace Narnia.Core.Domain
{
    //{
    //    "symbol_id": "BITFINEX_SPOT_BTC_USD",
    //    "exchange_id": "BITFINEX",
    //    "symbol_type": "SPOT",
    //    "asset_id_base": "BTC",
    //    "asset_id_quote": "USD",
    //    "data_start": "2013-03-31",
    //    "data_end": "2018-04-27",
    //    "data_quote_start": "2014-02-24T17:43:05.0000000Z",
    //    "data_quote_end": "2018-04-27T11:57:00.3607402Z",
    //    "data_orderbook_start": "2014-02-24T17:43:05.0000000Z",
    //    "data_orderbook_end": "2018-04-27T11:57:00.3607402Z",
    //    "data_trade_start": "2013-03-31T22:07:48.0000000Z",
    //    "data_trade_end": "2018-04-27T11:57:07.0000000Z"
    //}
    public class CoinSymbol:BaseEntity
    {
        [NonStored]
        public string SymbolId
        {
            get
            {
                return $"{ExchangeId}_{SymbolType}_{AssetIdBase}_{AssetIdQuote}";
            }
        }
        public string ExchangeId { get; set; }
        public string SymbolType { get; set; }
        public string AssetIdBase { get; set; }
        public string AssetIdQuote { get; set; }
        public bool Active { get; set; }
        public decimal AvgPrice { get; set; }
    }
}
