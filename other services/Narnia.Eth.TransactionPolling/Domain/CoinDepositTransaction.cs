﻿namespace Narnia.Core.Domain
{
    public class CoinDepositTransaction : BaseEntity
    {
        public string TransactionHash { get; set; }
        public decimal Amount { get; set; }
        public string FromAddress { get; set; }
        public string ToAddress { get; set; }
        public CoinDepositTransactionStatus Status { get; set; }
    }
    public enum CoinDepositTransactionStatus
    {
        Pending = 0,
        Success = 1,
        Cancelled = 2
    }
}
