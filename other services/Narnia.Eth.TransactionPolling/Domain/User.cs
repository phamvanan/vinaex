﻿using System;
namespace Narnia.Core.Domain
{
    public class User : BaseEntity
    {
        public string UserName { get; set; }
        public string Email { get; set; }
        public string PasswordHash { get; set; }
        public string PassWordSalt { get; set; }

        public DateTime CreatedDate { get; set; }
        public int? UplineId { get; set; }
        public User Referer { get; set; }
        public int UserLevelId { get; set; }
        public string ReferenceCode { get; set; }
        public string WalletAddress { get; set; }
        public string ActivationKey { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool IsActive { get; set; }

    }
}
