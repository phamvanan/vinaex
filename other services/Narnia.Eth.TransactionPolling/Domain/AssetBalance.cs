﻿using Narnia.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Narnia.Eth.TransactionPolling.Domain
{
    public class AssetBalance : BaseEntity
    {
        public int UserId { get; set; }
        public string AssetTypeId { get; set; }
        public decimal Balance { get; set; }
        public decimal FrozenBalance { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime UpdatedOn { get; set; }
        public User User { get; set; }
    }
}
