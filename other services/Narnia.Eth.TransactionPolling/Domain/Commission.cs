﻿using Narnia.Core.Domain;
using System;

namespace Narnia.Eth.TransactionPolling.Domain
{
    public class Commission : BaseEntity
    {
        public int UserId { get; set; }
        public int BeneficiaryUserId { get; set; }
        public int CommLevel { get; set; }
        public double CommRate { get; set; }
        public decimal CommAmt { get; set; }
        public DateTime CreatedDate { get; set; }
        public User BeneficiaryUser { get; set; }
        public int RefId { get; set; }
        public int RefType { get; set; }
        public int Status { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }
}