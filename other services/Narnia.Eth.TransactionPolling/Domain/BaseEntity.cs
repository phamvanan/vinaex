﻿using MicroOrm.Pocos.SqlGenerator.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Core.Domain
{
    public class BaseEntity
    {
        [KeyProperty(Identity = true)]
        public int Id { get; set; }
    }
}
