﻿

using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Narnia.Eth.TransactionPolling.Services
{
    public class ExchangeRateService
    {
        private const string ApiUrl = "http://www.vietcombank.com.vn/ExchangeRates/ExrateXML.aspx";
        public ExchangeRateService()
        {
            
        }
        public IList<Exrate> GetExchangeRates()
        {
            var client = new RestClient("http://www.vietcombank.com.vn");
            var request = new RestRequest("ExchangeRates/ExrateXML.aspx", Method.POST);

            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            string _byteOrderMarkUtf8 = Encoding.UTF8.GetString(Encoding.UTF8.GetPreamble());
            if (content.StartsWith(_byteOrderMarkUtf8))
            {
                content = content.Remove(0, _byteOrderMarkUtf8.Length);
            }

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(content);
            XmlNodeList nodes = doc.DocumentElement.SelectNodes("/ExrateList/Exrate");

            List<Exrate> allExRates = new List<Exrate>();
            foreach (XmlNode node in nodes)
            {
                Exrate rate = new Exrate();
                rate.CurrencyCode = node.Attributes["CurrencyCode"].Value;
                rate.CurrencyName = node.Attributes["CurrencyName"].Value;
                rate.Buy = Convert.ToDecimal(node.Attributes["Buy"].Value);
                rate.Sell = Convert.ToDecimal(node.Attributes["Sell"].Value);
                rate.Transfer = Convert.ToDecimal(node.Attributes["Transfer"].Value);
                allExRates.Add(rate);
            }

            return allExRates;
        }
    }

    public class Exrate
    {
        public string CurrencyCode { get; set; }
        public string CurrencyName { get; set; }
        public decimal Buy { get; set; }
        public decimal Sell { get; set; }
        public decimal Transfer { get; set; }
    }
}
