﻿using Nethereum.RPC.Eth.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Narnia.Eth.TransactionPolling.Services
{
    public class EthReceipt
    {
        public TransactionReceipt TransactionReceipt { get; set; }
        public Transaction Transaction { get; set; }
        public DateTime DateTime { get; set; }
        public decimal ValueInEther { get; set; }
    }
}
