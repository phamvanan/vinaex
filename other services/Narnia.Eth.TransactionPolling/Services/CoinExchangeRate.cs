﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Narnia.Eth.TransactionPolling.Services
{
    public class CoinExchangeRate
    {
        [JsonProperty("time")]
        public string TimeString { get; set; }
        [JsonProperty("asset_id_base")]
        public string AssetIdBase { get; set; }
        [JsonProperty("asset_id_quote")]
        public string AssetIdQuote { get; set; }
        [JsonProperty("rate")]
        public decimal Rate { get; set; }
    }
}
