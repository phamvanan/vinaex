﻿using Narnia.Eth.TransactionPolling.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Narnia.Eth.TransactionPolling.Services
{
    public interface ISettingService
    {
        IList<Setting> LoadAllSettings();
        Setting GetSettingById(int id);
    }
}
