﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Narnia.Eth.TransactionPolling.Services
{
    public class RestCoinApi
    {
        private string _coinApiKey = "99FE2545-CBFE-4C62-8BD4-C1FFA61D25CB";
        private string _coinApiUrl = "https://rest.coinapi.io";

        public RestCoinApi()
        {
        }
        public CoinExchangeRate GetExchangeRate(string assetIdBase, string assetIdQuote)
        {
            try
            {
                var client = new RestClient(_coinApiUrl + $"/v1/exchangerate/{assetIdBase}/{assetIdQuote}");
                var request = new RestRequest(Method.GET);
                request.AddHeader("X-CoinAPI-Key", _coinApiKey);

                var response = client.Execute<CoinExchangeRate>(request);
                if(response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    return response.Data;
                }
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
