﻿using Narnia.Core;
using Narnia.Eth.TransactionPolling;
using Nethereum.Hex.HexTypes;
using Nethereum.StandardTokenEIP20.CQS;
using Nethereum.Web3;
using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace Narnia.Business.CoinServices
{
    public class UsdtService : IUsdtService
    {
        private const string RpcUrl = "http://localhost:8545/";
        private readonly SiteSetting _siteSettings;
        public UsdtService(SiteSetting siteSettings)
        {
            _siteSettings = siteSettings;
        }
        public string CreateWallet(string password)
        {
            var web3 = new Web3(RpcUrl);
            var newWalletAddress = string.Empty;
            try
            {
                newWalletAddress = web3.Personal.NewAccount.SendRequestAsync(password).Result;
            }
            catch (Exception ex)
            {
            }
            return newWalletAddress;
        }

        public decimal GetAccountBalance(string walletAddress)
        {
            var web3 = new Web3(RpcUrl);

            var tokenService = new Nethereum.StandardTokenEIP20.StandardTokenService(web3, _siteSettings.USDTContractAddress);
            var balance = tokenService.GetBalanceOfAsync<BigInteger>(walletAddress).Result;
            
            DecimalsFunction f = new DecimalsFunction();
            var decimals = tokenService.DecimalsQueryAsync(f).Result;
            var usdt = (decimal)balance / (long)Math.Pow(10, decimals);
            return usdt;
        }

        public BigInteger GetLatestBlock()
        {
            try
            {
                HexBigInteger latestBlock = new HexBigInteger(0);
                var web3 = new Web3(RpcUrl);
                var syncingObj = web3.Eth.Syncing.SendRequestAsync().Result;
                if (syncingObj != null && syncingObj.IsSyncing)
                {
                    latestBlock = syncingObj.CurrentBlock;
                }
                else
                {
                    latestBlock = web3.Eth.Blocks.GetBlockNumber.SendRequestAsync().Result;
                }
                return latestBlock.Value;
            }
            catch (Exception ex)
            {
                return 5000000;
            }
        }

        /// <summary>
        /// Return transaction hash
        /// </summary>
        /// <param name="fromAddress"></param>
        /// <param name="passwordToUnlock"></param>
        /// <param name="toAddress"></param>
        /// <param name="amount">Number of tokens</param>
        /// <returns></returns>
        public string Transfer(string fromAddress, string passwordToUnlock, string toAddress, decimal amount)
        {
            try
            {
                var web3 = new Web3(RpcUrl);
                var unlockResult = web3.Personal.UnlockAccount.SendRequestAsync(fromAddress, passwordToUnlock, 60, null).Result;

                var tokenService = new Nethereum.StandardTokenEIP20.StandardTokenService(web3, _siteSettings.USDTContractAddress);
                DecimalsFunction f = new DecimalsFunction();
                var decimals = tokenService.DecimalsQueryAsync(f).Result;

                var transactionMessage = new TransferFunction()
                {
                    FromAddress = fromAddress,
                    To = toAddress,
                    //AmountToSend = tokens,
                    Value = (BigInteger)(amount * (decimal)Math.Pow(10, decimals)),
                };
                var transferHandler = web3.Eth.GetContractTransactionHandler<TransferFunction>().SendRequestAsync(transactionMessage, _siteSettings.USDTContractAddress);
                return transferHandler.Result;
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public bool Unlock(string address, string passwordToUnlock, int? duration = null)
        {
            var web3 = new Web3(RpcUrl);
            if (!duration.HasValue)
                duration = 1200;
            var unlockResult = web3.Personal.UnlockAccount.SendRequestAsync(address, passwordToUnlock, (ulong)duration.Value).Result;

            return unlockResult;
        }
    }
}
