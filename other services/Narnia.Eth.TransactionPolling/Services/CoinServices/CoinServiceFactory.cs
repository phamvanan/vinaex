﻿using Narnia.Core.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Business.CoinServices
{
    public class CoinServiceFactory
    {
        public ICoinService GetService(string coinType)
        {
            switch (coinType)
            {
                case "USDT":
                    return EngineContext.Instance.Resolve<IUsdtService>();
                case "ETH":
                    return EngineContext.Instance.Resolve<IEthService>();
                default:
                    return null;
            }
        }
        public List<ICoinService> GetAvalableServices(List<string> coinTypes)
        {
            List<ICoinService> retVal = new List<ICoinService>();
            if(coinTypes != null)
            {
                foreach(var type in coinTypes)
                {
                    var service = GetService(type);
                    if(service != null)
                    {
                        retVal.Add(service);
                    }
                }
            }
            return retVal;
        }
    }
}
