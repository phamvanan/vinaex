﻿using Narnia.Data.Base;
using Narnia.Data.Base.Repositories;
using Narnia.Eth.TransactionPolling.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Narnia.Eth.TransactionPolling.Services
{
    public class SettingService : ISettingService
    {
        private readonly IGenericRepository<Setting> _settingRepository;
        private readonly IDbContextScopeFactory _dbContextScopeFactory;
        public SettingService(IGenericRepository<Setting> settingRepository, IDbContextScopeFactory dbContextScopeFactory)
        {
            _settingRepository = settingRepository;
            _dbContextScopeFactory = dbContextScopeFactory;
        }
        public IList<Setting> LoadAllSettings()
        {
            List<Setting> settings = new List<Setting>();
            using (var scope = _dbContextScopeFactory.Create())
            {
                settings = scope.Repository<Setting>().GetAll().ToList();
            }
            return settings;
        }

        public Setting GetSettingByName(string name)
        {
            return _settingRepository.GetFirst(new { Name = name });
        }

        public Setting GetSettingById(int id)
        {
            return _settingRepository.GetFirst(new { Id = id });
        }
    }
}
