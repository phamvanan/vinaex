﻿using log4net;
using System;

namespace Narnia.Eth.TransactionPolling
{
    public sealed class Loggers
    {
        private static readonly Lazy<Loggers> lazy =
        new Lazy<Loggers>(() => new Loggers());

        public static Loggers Instance { get { return lazy.Value; } }

        public ILog InfoLog { get; private set; }
        public ILog ErrorLog { get; private set; }
        private Loggers()
        {
            InfoLog = LogManager.GetLogger("InfoLog");
            ErrorLog = LogManager.GetLogger("ErrorLog");
        }
    }
}
