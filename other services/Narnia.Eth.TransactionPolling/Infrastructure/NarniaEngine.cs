﻿using System;
using System.Linq;
using DryIoc;

namespace Narnia.Core.Infrastructure
{
    public class NarniaEngine
    {
        private IContainer _serviceProvider { get; set; }
        public virtual IContainer ServiceProvider => _serviceProvider;
     

        public void Initialize()
        {
            _serviceProvider = new Container(rules => rules.WithoutThrowIfDependencyHasShorterReuseLifespan().WithoutThrowOnRegisteringDisposableTransient());
        }
        protected IContainer GetServiceProvider()
        {
            var scope = ServiceProvider.OpenScope();
            return scope;
        }
        public T Resolve<T>() where T : class
        {
            return (T)GetServiceProvider().Resolve(typeof(T));
        }

        public object Resolve(Type type)
        {
            return GetServiceProvider().Resolve(type);
        }

        public object ResolveUnregistered(Type type)
        {
            Exception innerException = null;
            foreach (var constructor in type.GetConstructors())
            {
                try
                {
                    //try to resolve constructor parameters
                    var parameters = constructor.GetParameters().Select(parameter =>
                    {
                        var service = Resolve(parameter.ParameterType);
                        if (service == null)
                            throw new Exception("Unknown dependency");
                        return service;
                    });

                    //all is ok, so create instance
                    return Activator.CreateInstance(type, parameters.ToArray());
                }
                catch (Exception ex)
                {
                    innerException = ex;
                }
            }
            throw new Exception("No constructor was found that had all the dependencies satisfied.", innerException);
        }
    }
}
