﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace Narnia.Core.Infrastructure
{
    public class EngineContext
    {
        private static readonly Lazy<NarniaEngine> lazy = new Lazy<NarniaEngine>(() => new NarniaEngine());

        public static NarniaEngine Instance { get { return lazy.Value; } }
    }
}
