﻿using Narnia.Data.Base;
using Narnia.Eth.TransactionPolling.Data.Repository;
using Narnia.Eth.TransactionPolling.Domain;
using Narnia.Eth.TransactionPolling.Enums;
using Quartz;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Eth.TransactionPolling
{
    [PersistJobDataAfterExecution]
    [DisallowConcurrentExecution]
    public class TransactionCancelJob : IJob
    {
        private readonly ITransactionRepository transactionRepository;
        private readonly ISellAdvertisementRepository sellAdvertisementRepository;
        private readonly IDbContextScopeFactory dbContextScopeFactory;

        public TransactionCancelJob(ITransactionRepository transactionRepository,
                ISellAdvertisementRepository sellAdvertisementRepository,
                IDbContextScopeFactory dbContextScopeFactory)
        {
            this.transactionRepository = transactionRepository;
            this.sellAdvertisementRepository = sellAdvertisementRepository;
            this.dbContextScopeFactory = dbContextScopeFactory;
            var threadId = System.Threading.Thread.CurrentThread.ManagedThreadId;
            Loggers.Instance.InfoLog.Info(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss.fff") + ": TransactionCancelJob Created in thread " + threadId);
        }

        public Task Execute(IJobExecutionContext context)
        {
            try
            {
                var listItem = transactionRepository.GetSellAdsForCancel();
                if (listItem != null && listItem.Count() > 0)
                {
                    foreach (var item in listItem)
                    {
                        using (var scope = dbContextScopeFactory.Create())
                        {
                            var selAds = sellAdvertisementRepository.GetById((item.SellAdvertisementId ?? 0));
                            if (selAds == null) continue;

                            item.Status = (int)TransactionStautus.Cancel;
                            item.UpdatedDate = DateTime.Now;
                            var updateOk = scope.Repository<Transaction>().Update(item);
                            if (updateOk == true)
                            {
                                selAds.AmountSell -= item.AmountCoin;
                                selAds.UpdatedDate = DateTime.Now;
                                updateOk = scope.Repository<SellAdvertisement>().Update(selAds);
                                if (updateOk == false)
                                {
                                    scope.Rollback();
                                    continue;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Loggers.Instance.ErrorLog.Info($"Cannot  TransactionCancelJob. Please check!!! Message: {ex.Message} StackTrace: {ex.StackTrace}" + Environment.NewLine);
            }
            Loggers.Instance.ErrorLog.Info("=====Complete TransactionCancelJob task===============");
            return Task.CompletedTask;
        }
    }
}