﻿using Narnia.Data.Base.Repositories;
using Narnia.Eth.TransactionPolling.Domain;

namespace Narnia.Eth.TransactionPolling.Data.Repository
{
    public interface IAssetBalanceRepository : IGenericRepository<AssetBalance>
    {
        AssetBalance GetByUserIdAssetType(int userID, string assetTypeID);
    }
}