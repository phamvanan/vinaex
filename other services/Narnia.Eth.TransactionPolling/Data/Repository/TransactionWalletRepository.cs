﻿using Narnia.Data.Base;
using Narnia.Data.Base.Repositories;
using Narnia.Eth.TransactionPolling.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Narnia.Eth.TransactionPolling.Data.Repository
{
    public class  TransactionWalletRepository:GenericRepository<TransactionWallet>,ITransactionWalletRepository
    {
        public TransactionWalletRepository(IDbContext dbContext):base(dbContext)
        {

        }
        public TransactionWallet GetByTranId(int transactionId)
        {
            return GetFirst(new { TransactionId = transactionId });
        }
    }
}
