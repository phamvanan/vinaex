﻿using Narnia.Data.Base;
using Narnia.Data.Base.Repositories;
using Narnia.Eth.TransactionPolling.Domain;
using Narnia.Eth.TransactionPolling.Enums;
using System.Collections.Generic;
using System.Linq;

namespace Narnia.Eth.TransactionPolling.Data.Repository
{
    public class CommissionRepository : GenericRepository<Commission>, ICommissionRepository
    {
        public CommissionRepository(IDbContext dbContext) : base(dbContext)
        {
        }
        public List<Commission> GetCommissionsForCronJob()
        {
            return GetWhere(new { Status = (int)CommisionStatusEnum.NotPaid }).ToList();
        }
    }
}