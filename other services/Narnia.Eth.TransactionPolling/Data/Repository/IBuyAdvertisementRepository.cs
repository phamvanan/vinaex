﻿using Narnia.Data.Base.Repositories;
using Narnia.Eth.TransactionPolling.Domain;

namespace Narnia.Eth.TransactionPolling.Data.Repository
{
    public interface IBuyAdvertisementRepository : IGenericRepository<BuyAdvertisement>
    {
        BuyAdvertisement GetById(int Id);
    }
}