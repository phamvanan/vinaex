﻿using Narnia.Data.Base;
using Narnia.Data.Base.Repositories;
using Narnia.Eth.TransactionPolling.Domain;
using System.Collections.Generic;

namespace Narnia.Eth.TransactionPolling.Data.Repository
{
    public class TransactionRepository : GenericRepository<Transaction>, ITransactionRepository
    {
        public TransactionRepository(IDbContext dbContext) : base(dbContext)
        {
        }

        public Transaction GetByTranId(int transactionId)
        {
            return GetFirst(new { TransactionId = transactionId });
        }
        public IEnumerable<Transaction> GetSellAdsForCancel()
        {
            var data = QueryStoreProc<Transaction>("usp_Transaction_GetForCancel");
            return data;
        }
    }
}