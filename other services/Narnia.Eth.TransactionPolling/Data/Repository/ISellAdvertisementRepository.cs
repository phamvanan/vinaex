﻿using Narnia.Data.Base.Repositories;
using Narnia.Eth.TransactionPolling.Domain;

namespace Narnia.Eth.TransactionPolling.Data.Repository
{
    public interface ISellAdvertisementRepository : IGenericRepository<SellAdvertisement>
    {
        SellAdvertisement GetById(int Id);
    }
}