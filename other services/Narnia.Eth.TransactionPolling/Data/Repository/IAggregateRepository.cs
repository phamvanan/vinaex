﻿using Narnia.Core.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Repository
{
    public interface IAggregateRepository
    {
        bool CalculateCommission();
        bool DispatchCommission(DateTime fromDate, DateTime toDate);
        IList<CoinDepositTransaction> GetPendingDepositTransactions();
        IList<WithdrawHistory> GetWithdrawalsByStatus(WithdrawalStatus status);
    }
}
