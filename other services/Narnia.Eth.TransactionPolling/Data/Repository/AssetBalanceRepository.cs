﻿using Narnia.Data.Base;
using Narnia.Data.Base.Repositories;
using Narnia.Eth.TransactionPolling.Domain;

namespace Narnia.Eth.TransactionPolling.Data.Repository
{
    public class AssetBalanceRepository : GenericRepository<AssetBalance>, IAssetBalanceRepository
    {
        public AssetBalanceRepository(IDbContext dbContext) : base(dbContext)
        {
        }

        public AssetBalance GetByUserIdAssetType(int userID, string assetTypeID)
        {
            return GetFirst(new { UserId = userID, AssetTypeId = assetTypeID });
        }
    }
}