﻿using Dapper;
using Narnia.Core.Domain;
using Narnia.Eth.TransactionPolling;
using System;
using System.Collections.Generic;
using System.Data;

namespace Narnia.Repository
{
    public class AggregateRepository : BaseRepository, IAggregateRepository
    {
        public AggregateRepository() : base()
        {
            
        }
        public bool CalculateCommission()
        {
            using (var conn = CreateConnection())
            {
                conn.Open();
                var query = "usp_CalculateCommission";
                try
                {
                    conn.Query(query, commandType: CommandType.StoredProcedure);
                    return true;
                }
                catch (System.Exception ex)
                {
                    Loggers.Instance.InfoLog.Info("Cannot calculate commission. ", ex);
                    return false;
                }
            }
        }
        public bool DispatchCommission(DateTime fromDate, DateTime toDate)
        {
            using (var conn = CreateConnection())
            {
                try
                {
                    conn.Open();
                    var query = "usp_DispatchCommission";
                    var parameters = new DynamicParameters();
                    parameters.Add("FromDate", fromDate);
                    parameters.Add("ToDate", toDate);

                    conn.Query(query, parameters, commandType: CommandType.StoredProcedure);
                    return true;
                }
                catch (System.Exception ex)
                {
                    Loggers.Instance.InfoLog.Info($"Cannot dispatch commission from {fromDate} to {toDate}. ", ex);
                    return false;
                }
            }
        }
        public IList<CoinDepositTransaction> GetPendingDepositTransactions()
        {
            using (var conn = CreateConnection())
            {
                conn.Open();
                var query = $"select * from CoinDepositTransaction where Status = {(int)CoinDepositTransactionStatus.Pending}";
                try
                {
                    return conn.Query<CoinDepositTransaction>(query).AsList();
                }
                catch (System.Exception ex)
                {
                    Loggers.Instance.InfoLog.Info("Cannot get PendingDepositTransactions. ", ex);
                    return null;
                }
            }
        }

        public IList<WithdrawHistory> GetWithdrawalsByStatus(WithdrawalStatus status)
        {
            using (var conn = CreateConnection())
            {
                conn.Open();
                var query = $"select * from WithdrawHistory where Status = {(int)status}";
                try
                {
                    return conn.Query<WithdrawHistory>(query).AsList();
                }
                catch (System.Exception ex)
                {
                    Loggers.Instance.InfoLog.Info("Cannot get GetPendingWithdrawals. ", ex);
                    return null;
                }
            }
        }
    }
}
