﻿using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Narnia.Repository
{
    public class BaseRepository
    {
        public string ConnectionString { get; protected set; }
        public BaseRepository()
        {
            ConnectionString = ConfigurationManager.ConnectionStrings["CoinDb"].ConnectionString;
        }
        public IDbConnection CreateConnection()
        {
            return new SqlConnection(ConnectionString);
        }
    }
}
