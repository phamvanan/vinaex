﻿using Narnia.Data.Base.Repositories;
using Narnia.Eth.TransactionPolling.Domain;
using System.Collections.Generic;

namespace Narnia.Eth.TransactionPolling.Data.Repository
{
    public interface ITransactionRepository : IGenericRepository<Transaction>
    {
        Transaction GetByTranId(int transactionId);
        IEnumerable<Transaction> GetSellAdsForCancel();
    }
}