﻿using Narnia.Data.Base.Repositories;
using Narnia.Eth.TransactionPolling.Domain;

namespace Narnia.Eth.TransactionPolling.Data.Repository
{
    public interface ITransactionWalletRepository : IGenericRepository<TransactionWallet>
    {
        TransactionWallet GetByTranId(int transactionId);
    }
}