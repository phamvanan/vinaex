﻿using Narnia.Data.Base;
using Narnia.Data.Base.Repositories;
using Narnia.Eth.TransactionPolling.Domain;

namespace Narnia.Eth.TransactionPolling.Data.Repository
{
    public class SellAdvertisementRepository : GenericRepository<SellAdvertisement>, ISellAdvertisementRepository
    {
        public SellAdvertisementRepository(IDbContext dbContext) : base(dbContext)
        {
        }

        public SellAdvertisement GetById(int Id)
        {
            return GetFirst(new { Id = Id });
        }
    }
}