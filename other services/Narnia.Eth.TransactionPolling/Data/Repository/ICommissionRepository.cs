﻿using Narnia.Data.Base.Repositories;
using Narnia.Eth.TransactionPolling.Domain;
using System.Collections.Generic;

namespace Narnia.Eth.TransactionPolling.Data.Repository
{
    public interface ICommissionRepository : IGenericRepository<Commission>
    {
        List<Commission> GetCommissionsForCronJob();
    }
}