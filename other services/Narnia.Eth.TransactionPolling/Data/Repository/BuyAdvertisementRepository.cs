﻿using Narnia.Data.Base;
using Narnia.Data.Base.Repositories;
using Narnia.Eth.TransactionPolling.Domain;

namespace Narnia.Eth.TransactionPolling.Data.Repository
{
    public class BuyAdvertisementRepository : GenericRepository<BuyAdvertisement>, IBuyAdvertisementRepository
    {
        public BuyAdvertisementRepository(IDbContext dbContext) : base(dbContext)
        {
        }

        public BuyAdvertisement GetById(int Id)
        {
            return GetFirst(new { Id = Id });
        }
    }
}