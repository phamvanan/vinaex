﻿using Narnia.Business.CoinServices;
using Narnia.Core.Domain;
using Narnia.Data.Base;
using Narnia.Data.Base.Repositories;
using Narnia.Eth.TransactionPolling.Domain;
using Quartz;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Eth.TransactionPolling
{
    [PersistJobDataAfterExecution]
    [DisallowConcurrentExecution]
    public class EthProcessingJob : IJob
    {
        private readonly IDbContextScopeFactory _dbContextScopeFactory;
        private readonly IGenericRepository<CoinSymbol> _coinSymbolRepository;
        private readonly IGenericRepository<CoinWallet> _coinWalletRepository;
        private readonly IEthService _ethService;
        private readonly SiteSetting _siteSetting;

        public EthProcessingJob(IDbContextScopeFactory dbContextScopeFactory, IGenericRepository<CoinSymbol> coinSymbolRepository, IGenericRepository<CoinWallet> coinWalletRepository,
            IEthService ethService, SiteSetting siteSetting)
        {
            _dbContextScopeFactory = dbContextScopeFactory;
            _coinSymbolRepository = coinSymbolRepository;
            _coinWalletRepository = coinWalletRepository;
            _ethService = ethService;
            _siteSetting = siteSetting;
            var threadId = System.Threading.Thread.CurrentThread.ManagedThreadId;
            Loggers.Instance.InfoLog.Info(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss.fff") + ": TransactionProcessingJob Created in thread " + threadId);
        }

        public Task Execute(IJobExecutionContext context)
        {
            try
            {
                var threadId = System.Threading.Thread.CurrentThread.ManagedThreadId;

                //  Loggers.Instance.InfoLog.Info("Start loading transactions from blockchain. Thread: " + threadId);

                //var latestBlock = _ethService.GetLatestBlock();
                //Loggers.Instance.InfoLog.Info("Latest block: " + latestBlock);
                //BigInteger blockId;

                int symbolId = 0;
                var allSymbols = _coinSymbolRepository.GetAll().ToList();
                foreach (var symbol in allSymbols)
                {
                    if (symbol.AssetIdBase == "ETH")
                        symbolId = symbol.Id;
                }

                if (symbolId == 0)
                    return Task.CompletedTask;

                var allWallets = _coinWalletRepository.GetWhere(new { CoinType = symbolId }).Where(x => x.WalletAddress != "").ToList();

                Loggers.Instance.InfoLog.Info("Num of Wallets : " + allWallets.Count);

                // ung voi moi wallet address, goi len server
                // lay balance ve, neu balance <> balance duoi DB thi update
                if (allWallets != null && allWallets.Count > 0)
                {
                    foreach (var wallet in allWallets)
                    {
                        Loggers.Instance.InfoLog.Info("uypc WalletAddress : " + wallet.WalletAddress);
                        decimal amount = 0;
                        decimal amountDiff = 0;
                        try
                        {
                            amount = _ethService.GetAccountBalance(wallet.WalletAddress.Trim());
                        }
                        catch (Exception ex)
                        {
                            Loggers.Instance.InfoLog.InfoFormat("goi ham  GetAccountBalance bi loi:{0}\n{1} ", ex.Message, ex);
                            throw;
                        }
                        Loggers.Instance.InfoLog.Info("uypc balance : " + amount);
                        Loggers.Instance.InfoLog.Info("uypc wallet id : " + wallet.Id);

                        if (amount != wallet.Amount)
                        {
                            amountDiff = amount - wallet.Amount;
                            // implement update
                            using (var scope = _dbContextScopeFactory.Create())
                            {
                                scope.Repository<CoinWallet>().Update(wallet, new { Amount = amount }, wallet.Id);
                                if (amountDiff > 0)//Deposit
                                {
                                    var updateQuery = "update AssetBalance set Balance = Balance + @AmountDiff,UpdatedOn = getdate() where UserId = @UserId and AssetTypeId = @AssetTypeId";
                                    scope.Repository<AssetBalance>().SqlQuery(updateQuery, new { AmountDiff = amountDiff, wallet.UserId, AssetTypeId = wallet.Type });
                                }
                                Loggers.Instance.InfoLog.Info("uypc success");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Loggers.Instance.ErrorLog.Info("Cannot get data. Please check!!!" + Environment.NewLine, ex);
            }
            Loggers.Instance.ErrorLog.Info("=====Complete task===============");
            return Task.CompletedTask;
        }

        //protected void SyncData(List<CoinWallet> allWallets, BigInteger blockNumber)
        //{
        //    Loggers.Instance.InfoLog.Info("Syncing block: " + blockNumber);
        //    var allTransactions = _ethService.GetAllTransactionsByBlockNumber(blockNumber);
        //    Loggers.Instance.InfoLog.Info("Num of Transactions: " + allTransactions.Count);
        //    foreach (var tran in allTransactions)
        //    {
        //        foreach(var wallet in allWallets)
        //        {
        //            try
        //            {
        //                if (tran.To.ToLower() == wallet.WalletAddress.ToLower())
        //                {
        //                    Loggers.Instance.InfoLog.Info($"Found:--------------------- {tran.To}, wallet address: {wallet.WalletAddress}, transaction hash: {tran.TransactionHash} ");
        //                    using (var scope = _dbContextScopeFactory.Create())
        //                    {
        //                        var existingTran = scope.Repository<CoinDepositTransaction>().GetWhere(new { tran.TransactionHash }).FirstOrDefault();
        //                        if (existingTran == null)
        //                        {
        //                            scope.Repository<CoinDepositTransaction>().Insert(new CoinDepositTransaction()
        //                            {
        //                                Amount = Web3.Convert.FromWei(tran.Value),
        //                                FromAddress = tran.From,
        //                                ToAddress = tran.To,
        //                                TransactionHash = tran.TransactionHash,
        //                                Status = CoinDepositTransactionStatus.Pending
        //                            });
        //                        }
        //                    }
        //                    break;
        //                }
        //            }
        //            catch (Exception exd)
        //            {
        //                Loggers.Instance.InfoLog.Info("Error:--------------------- ", exd);
        //            }
        //        }
        //    }
        //    Loggers.Instance.InfoLog.Info("End of Syncing block: " + blockNumber);
        //    foreach (var wallet in allWallets)
        //    {
        //        Loggers.Instance.InfoLog.Info($"Go to UPDATE function : wallet block {wallet.LastBlockSync}, currentBlock {blockNumber}, wallet.LastBlockSync < blockNumber: " + (wallet.LastBlockSync < blockNumber));
        //        if (wallet.LastBlockSync < blockNumber)
        //        {
        //            _coinWalletRepository.Update(wallet, new { LastBlockSync = (int)blockNumber }, wallet.Id);
        //        }
        //    }
        //}
    }

    //[PersistJobDataAfterExecution]
    //[DisallowConcurrentExecution]
    //public class CompleteDepositTransactionJob : IJob
    //{
    //    private readonly IDbContextScopeFactory _dbContextScopeFactory;
    //    private readonly IAggregateRepository _aggregateRepository;
    //    private readonly EthService _ethService;
    //    public CompleteDepositTransactionJob(IDbContextScopeFactory dbContextScopeFactory, IAggregateRepository aggregateRepository, EthService ethService)
    //    {
    //        _dbContextScopeFactory = dbContextScopeFactory;
    //        _aggregateRepository = aggregateRepository;
    //        _ethService = ethService;
    //    }
    //    public Task Execute(IJobExecutionContext context)
    //    {
    //        var pendingTransactions = _aggregateRepository.GetPendingDepositTransactions();
    //        if(pendingTransactions != null)
    //        {
    //            foreach(var tran in pendingTransactions)
    //            {
    //                if (!string.IsNullOrEmpty(tran.TransactionHash))
    //                {
    //                    var receipt = _ethService.GetTransactionReceiptByTransactionHash(tran.TransactionHash);
    //                    if(receipt != null)
    //                    {
    //                        using (var scope = _dbContextScopeFactory.Create())
    //                        {
    //                            if (receipt.Status.Value == 1)//Ok
    //                            {
    //                                scope.Repository<CoinDepositTransaction>().Update(tran, new { Status = CoinDepositTransactionStatus.Success }, tran.Id);
    //                                if (tran.Amount > 0)
    //                                {
    //                                    var wallet = scope.Repository<CoinWallet>().GetFirst(new { WalletAddress = tran.ToAddress });
    //                                    scope.Repository<CoinWallet>().Update(wallet, new { Amount = tran.Amount + wallet.Amount }, wallet.Id);
    //                                }
    //                            }
    //                            else //failed
    //                            {
    //                                scope.Repository<CoinDepositTransaction>().Update(tran, new { Status = CoinDepositTransactionStatus.Cancelled }, tran.Id);
    //                            }
    //                        }

    //                    }

    //                }
    //            }
    //        }

    //        return Task.CompletedTask;
    //    }
    //}
}