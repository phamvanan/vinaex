﻿using DryIoc;
using Narnia.Business.CoinServices;
using Narnia.Core.Infrastructure;
using Narnia.Data.Base;
using Narnia.Data.Base.Repositories;
using Narnia.Eth.TransactionPolling.Data.Repository;
using Narnia.Eth.TransactionPolling.Domain;
using Narnia.Eth.TransactionPolling.Services;
using Narnia.Repository;
using System;
using System.Configuration;
using System.IO;
using System.ServiceProcess;

namespace Narnia.Eth.TransactionPolling
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            Initialize();

            if (Environment.UserInteractive)
            {
                EngineContext.Instance.Resolve<Service1>().TestStartup(null);
            }
            else
            {
                Loggers.Instance.InfoLog.Info("UserInteractive2:" + Environment.UserInteractive);
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[]
                {
                    EngineContext.Instance.Resolve<Service1>()
                };
                ServiceBase.Run(ServicesToRun); 
            }
        }
        static void Initialize()
        {
            //string directoryName = System.IO.Path.GetDirectoryName(new System.Uri(System.Reflection.Assembly.GetExecutingAssembly().CodeBase).LocalPath);
            var directoryName = System.AppDomain.CurrentDomain.BaseDirectory;
            var fileInfo = new FileInfo(Path.Combine(directoryName, "Narnia.Eth.TransactionPolling.exe.config"));
            log4net.Config.XmlConfigurator.Configure(fileInfo);

            EngineContext.Instance.Initialize();
            var container = EngineContext.Instance.ServiceProvider;
            container.Register<Service1, Service1>(Reuse.Singleton);
            container.Register<UpdateExchangeRateJob, UpdateExchangeRateJob>(Reuse.Transient);
            container.Register<UpdateCoinRateJob, UpdateCoinRateJob>(Reuse.Transient);
            container.Register<EthProcessingJob, EthProcessingJob>(Reuse.Transient);
            container.Register<UpdateStatusCommissionJob, UpdateStatusCommissionJob>(Reuse.Transient);
            container.Register<TransactionCancelJob, TransactionCancelJob>(Reuse.Transient);
            //container.Register<CompleteDepositTransactionJob, CompleteDepositTransactionJob>(Reuse.Transient);

            container.RegisterInstance<IDbContextFactory>(new DbContextFactory(ConfigurationManager.ConnectionStrings["CoinDb"].ConnectionString), reuse: Reuse.Singleton);
            container.Register<IDbContextScopeFactory, DbContextScopeFactory>(Reuse.Singleton);
            container.RegisterDelegate<IDbContext>((c) => { return c.Resolve<IDbContextFactory>().CreateDbContext(); }, Reuse.Transient);
            

            container.Register(typeof(IGenericRepository<>), typeof(GenericRepository<>),made:Made.Of(typeof(GenericRepository<>).GetConstructor(new[] { typeof(IDbContext) })) , reuse: Reuse.Transient);
            container.Register<IUsdtService, UsdtService>(Reuse.Transient);
            container.Register<IEthService, EthService>(Reuse.Transient);
            container.Register<IAggregateRepository, AggregateRepository>(Reuse.Transient);
            container.Register<ExchangeRateService, ExchangeRateService>(Reuse.Transient);
            container.Register<RestCoinApi, RestCoinApi>(Reuse.Transient);
            container.Register<ISettingService, SettingService>(Reuse.Transient);
            container.Register<ICommissionRepository, CommissionRepository>(Reuse.Transient);

            container.Register<ISellAdvertisementRepository, SellAdvertisementRepository>(Reuse.Transient);
            container.Register<IBuyAdvertisementRepository, BuyAdvertisementRepository>(Reuse.Transient);
            container.Register<ITransactionRepository, TransactionRepository>(Reuse.Transient);
            container.Register<ITransactionWalletRepository, TransactionWalletRepository>(Reuse.Transient);
            container.Register<IAssetBalanceRepository, AssetBalanceRepository>(Reuse.Transient);

            //container.Register<ICommissionService,CommissionService>(Reuse.Transient);

            SiteSetting setting = new SiteSetting();
            var settingService = container.Resolve<ISettingService>();
            var allSettings = settingService.LoadAllSettings();
            setting.MapSetings(allSettings);
            container.RegisterInstance<SiteSetting>(setting, reuse: Reuse.Singleton);
        }
    }
}
