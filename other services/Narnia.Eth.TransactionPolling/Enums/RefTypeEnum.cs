﻿namespace Narnia.Eth.TransactionPolling.Enums
{
    public enum RefTypeEnum : int
    {
        BuyAds = 1,
        SellAds = 2,
        Transaction = 3,
        TransactionWallet = 4
    }
}