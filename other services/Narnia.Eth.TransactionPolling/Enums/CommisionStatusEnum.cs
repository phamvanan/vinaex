﻿namespace Narnia.Eth.TransactionPolling.Enums
{
    public enum CommisionStatusEnum : int
    {
        NotPaid = 0,
        Paid = 1
    }
}