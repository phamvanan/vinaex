﻿using System.ComponentModel.DataAnnotations;

namespace Narnia.Eth.TransactionPolling.Enums
{
    public enum TransactionStautus
    {
        [Display(Name = "Mới")]
        Pending = 1,

        [Display(Name = "Thành công")]
        Success = 2,

        [Display(Name = "Từ chối")]
        Reject = 3,

        [Display(Name = "Chờ xác nhận")]
        HasEvidence = 4,

        [Display(Name = "Hủy")]
        Cancel = 5
    }
}