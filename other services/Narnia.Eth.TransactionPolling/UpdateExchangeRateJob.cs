﻿using log4net;
using Narnia.Core.Domain;
using Narnia.Data.Base.Repositories;
using Narnia.Eth.TransactionPolling.Services;
using Narnia.Repository;
using Quartz;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Eth.TransactionPolling
{
    [PersistJobDataAfterExecution]
    [DisallowConcurrentExecution]
    public class UpdateExchangeRateJob : IJob
    {
        private readonly ExchangeRateService _exchangeRateService;
        private readonly IAggregateRepository _aggregateRepository;
        private readonly IGenericRepository<Currency> _currencyRepository;
        public UpdateExchangeRateJob(IAggregateRepository aggregateRepository, ExchangeRateService exchangeRateService, IGenericRepository<Currency> currencyRepository)
        {
            this._aggregateRepository = aggregateRepository;
            this._exchangeRateService = exchangeRateService;
            this._currencyRepository = currencyRepository;
        }
        public Task Execute(IJobExecutionContext context)
        {
            try
            {
                Loggers.Instance.InfoLog.Info("Start UpdateExchangeRateJob.");
                var allCurrencies = this._currencyRepository.GetAll().ToList();

                var liveExchangeRates = _exchangeRateService.GetExchangeRates();
                var USD_Online = liveExchangeRates.FirstOrDefault(item => item.CurrencyCode == "USD");
                
                if(USD_Online != null)
                {
                    
                    foreach (var dbCurrency in allCurrencies)
                    {
                        //VND doesn't exist in response data. => Hard code
                        if(dbCurrency.CurrencyCode == "VND")
                        {
                            dbCurrency.Rate = (float)USD_Online.Transfer;
                        }
                        else
                        {
                            var onlineItem = liveExchangeRates.FirstOrDefault(item => item.CurrencyCode == dbCurrency.CurrencyCode);
                            if (onlineItem != null)
                            {
                                dbCurrency.Rate = (float)(USD_Online.Transfer / onlineItem.Transfer);
                            }
                        }
                        _currencyRepository.Update(dbCurrency, new { Rate = dbCurrency.Rate }, dbCurrency.Id);
                    }
                }
                

                return Task.CompletedTask;

            }
            catch (Exception ex)
            {
                Loggers.Instance.ErrorLog.Info("UpdateExchangeRateJob Error. Please check!!!" + Environment.NewLine, ex);
            }
            return Task.CompletedTask;
        }
    }
}
