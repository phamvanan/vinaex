﻿using Narnia.Data.Base;
using Narnia.Data.Base.Repositories;
using Narnia.Eth.TransactionPolling.Data.Repository;
using Narnia.Eth.TransactionPolling.Domain;
using Narnia.Eth.TransactionPolling.Enums;
using Quartz;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Eth.TransactionPolling
{
    [PersistJobDataAfterExecution]
    [DisallowConcurrentExecution]
    public class UpdateStatusCommissionJob : IJob
    {
        private readonly IDbContextScopeFactory _dbContextScopeFactory;
        private readonly ICommissionRepository _commissionRepository;
        private readonly IBuyAdvertisementRepository _buyAdvertisementRepository;
        private readonly ISellAdvertisementRepository _sellAdvertisementRepository;
        private readonly ITransactionRepository _transactionRepository;
        private readonly ITransactionWalletRepository _transactionWalletRepository;
        private readonly IAssetBalanceRepository _assetBalanceRepository;

        public UpdateStatusCommissionJob(IDbContextScopeFactory dbContextScopeFactory, ICommissionRepository commissionRepository,
                IBuyAdvertisementRepository buyAdvertisementRepository,
                ISellAdvertisementRepository sellAdvertisementRepository,
                ITransactionRepository transactionRepository,
                ITransactionWalletRepository transactionWalletRepository,
            IAssetBalanceRepository assetBalanceRepository
            )
        {
            this._dbContextScopeFactory = dbContextScopeFactory;
            this._commissionRepository = commissionRepository;
            this._buyAdvertisementRepository = buyAdvertisementRepository;
            this._sellAdvertisementRepository = sellAdvertisementRepository;
            this._transactionRepository = transactionRepository;
            this._transactionWalletRepository = transactionWalletRepository;
            this._assetBalanceRepository = assetBalanceRepository;
            var threadId = System.Threading.Thread.CurrentThread.ManagedThreadId;
            Loggers.Instance.InfoLog.Info(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss.fff") + ": UpdateStatusCommissionJob Created in thread " + threadId);
        }

        public Task Execute(IJobExecutionContext context)
        {
            try
            {

                var listCommissions = _commissionRepository.GetCommissionsForCronJob();
                if (listCommissions != null && listCommissions.Any())
                {
                    foreach (var item in listCommissions)
                    {
                        var assetTypeId = string.Empty;
                        int refId = item.RefId;
                        switch (item.RefType)
                        {
                            case (int)RefTypeEnum.BuyAds:
                                var adsBuyEntity = _buyAdvertisementRepository.GetById(refId);
                                assetTypeId = adsBuyEntity?.AssetTypeId;
                                break;

                            case (int)RefTypeEnum.SellAds:
                                var adsSellerEntity = _sellAdvertisementRepository.GetById(refId);
                                assetTypeId = adsSellerEntity?.AssetTypeId;
                                break;

                            case (int)RefTypeEnum.Transaction:
                                var tranEntity = _transactionRepository.GetByTranId(refId);
                                assetTypeId = tranEntity?.AssetTypeId;
                                break;

                            case (int)RefTypeEnum.TransactionWallet:
                                var transWalletEntity = _transactionWalletRepository.GetByTranId(refId);
                                assetTypeId = transWalletEntity?.AssetTypeId;
                                break;

                            default:
                                break;
                        }
                        if (string.IsNullOrEmpty(assetTypeId))
                        {
                            continue;
                        }

                        // update balance user level parent
                        using (var scope = _dbContextScopeFactory.Create())
                        {
                            var balanceCurrent =
                            _assetBalanceRepository.GetByUserIdAssetType(item.BeneficiaryUserId, assetTypeId);
                            if (balanceCurrent == null)
                            {
                                continue;
                            }
                            balanceCurrent.Balance += item.CommAmt;
                            balanceCurrent.UpdatedOn = DateTime.Now;
                            var updateOk = scope.Repository<AssetBalance>().Update(balanceCurrent);
                            if (updateOk == true)
                            {
                                item.Status = (int)CommisionStatusEnum.Paid;
                                item.UpdatedDate = DateTime.Now;
                                updateOk = scope.Repository<Commission>().Update(item);
                                if (updateOk == false)
                                {
                                    scope.Rollback();
                                }
                            }
                        }
                    };
                }
            }
            catch (Exception ex)
            {
                Loggers.Instance.ErrorLog.Info($"Cannot  UpdateStatusCommissionJob get data. Please check!!! Message: {ex.Message} StackTrace: {ex.StackTrace}" + Environment.NewLine);
            }
            Loggers.Instance.ErrorLog.Info("=====Complete UpdateStatusCommissionJob task===============");
            return Task.CompletedTask;
        }
    }
}