﻿using log4net;
using Narnia.Core.Domain;
using Narnia.Data.Base.Repositories;
using Narnia.Eth.TransactionPolling.Services;
using Narnia.Repository;
using Quartz;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Narnia.Eth.TransactionPolling
{
    [PersistJobDataAfterExecution]
    [DisallowConcurrentExecution]
    public class UpdateCoinRateJob : IJob
    {
        private readonly RestCoinApi _coinService;
        
        private readonly IGenericRepository<CoinSymbol> _coinSymbolRepository;
        private readonly IGenericRepository<Currency> _currencyRepository;
        public UpdateCoinRateJob(IGenericRepository<CoinSymbol> coinSymbolRepository, IGenericRepository<Currency> currencyRepository, RestCoinApi coinService)
        {
            this._coinSymbolRepository = coinSymbolRepository;
            _currencyRepository = currencyRepository;
            this._coinService = coinService;
        }
        public Task Execute(IJobExecutionContext context)
        {
            try
            {
                Loggers.Instance.InfoLog.Info("Start UpdateCoinRateJob.");
                var allSymbols = this._coinSymbolRepository.GetAll().ToList();
                foreach (var symbol in allSymbols)
                {
                    var currency = _currencyRepository.GetWhere(new { CurrencyCode = symbol.AssetIdQuote}).FirstOrDefault();
                    if (currency == null)
                        continue;

                    var coinRate = this._coinService.GetExchangeRate(symbol.AssetIdBase, "USD");
                    if (coinRate != null)
                    {
                        this._coinSymbolRepository.Update(symbol, new { AvgPrice = coinRate.Rate * (decimal)currency.Rate }, symbol.Id);
                    }

                    //var coinRate = this._coinService.GetExchangeRate(symbol.AssetIdBase, symbol.AssetIdQuote);
                    //if(coinRate != null)
                    //{
                    //    this._coinSymbolRepository.Update(symbol, new { AvgPrice = coinRate.Rate },symbol.Id);
                    //}
                }
                return Task.CompletedTask;

            }
            catch (Exception ex)
            {
                Loggers.Instance.ErrorLog.Info("UpdateCoinRateJob Error. Please check!!!" + Environment.NewLine, ex);
            }
            return Task.CompletedTask;
        }
    }
}
