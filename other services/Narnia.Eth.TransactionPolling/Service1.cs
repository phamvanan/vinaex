﻿using Quartz;
using Quartz.Impl;
using Quartz.Impl.Matchers;
using Quartz.Listener;
using Quartz.Simpl;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Narnia.Eth.TransactionPolling
{
    public partial class Service1 : ServiceBase
    {
        IScheduler scheduler;
        public Service1()
        {
            InitializeComponent(); 
            scheduler = StdSchedulerFactory.GetDefaultScheduler().Result;
            scheduler.JobFactory = new NarniaJobFactory();
        }
        internal void TestStartup(string[] args)
        {
            this.OnStart(args);
        }

        protected override void OnStart(string[] args)
        {
            Loggers.Instance.InfoLog.Info(Environment.NewLine + "==============================Service started==============================" + Environment.NewLine);
            
            int updateExRateJobInterval = 10;//In seconds
            int.TryParse(ConfigurationManager.AppSettings["UpdateExRateJobInterval"], out updateExRateJobInterval);

            int updateCoinRateJobInterval = 10;//In seconds
            int.TryParse(ConfigurationManager.AppSettings["UpdateCoinRateJobInterval"], out updateCoinRateJobInterval);

            //Update ExchangeRate Job
            ITrigger updateExRateTrigger = TriggerBuilder.Create()
               .WithIdentity("triggerUpdateExRate", "exRateGroup")
               .WithSimpleSchedule(x => x
                   .WithIntervalInSeconds(updateExRateJobInterval)
                   .RepeatForever()
                   )
               .Build();

            IJobDetail updateExRateJob = JobBuilder.Create<UpdateExchangeRateJob>()
                .WithIdentity("exrateJob", "exRateGroup")
                .Build();

            //Update CoinRate Job
            ITrigger updateCoinRateTrigger = TriggerBuilder.Create()
              .WithIdentity("triggerUpdateCoinRate", "coinRateGroup")
              .WithSimpleSchedule(x => x
                  .WithIntervalInSeconds(updateExRateJobInterval)
                  .RepeatForever()
                  )
              .Build();

            IJobDetail updateCoinRateJob = JobBuilder.Create<UpdateCoinRateJob>()
                .WithIdentity("coinRateJob", "coinRateGroup")
                .Build();

            //Schedule Jobs:
            scheduler.ScheduleJob(updateExRateJob, updateExRateTrigger);
            scheduler.ScheduleJob(updateCoinRateJob, updateCoinRateTrigger);


            //Deposit:
            int depositJobInterval = 1;//In seconds
            int.TryParse(ConfigurationManager.AppSettings["DepositJobInterval"], out depositJobInterval);

            int completeDepositJobInterval = 5;//In seconds
            int.TryParse(ConfigurationManager.AppSettings["CompleteDepositJobInterval"], out completeDepositJobInterval);

            //IJobDetail completeDepositJob = JobBuilder.Create<CompleteDepositTransactionJob>()
            //   .WithIdentity("completeDepJob", "group2")
            //   .Build();

            //ITrigger completeDepositTrigger = TriggerBuilder.Create()
            //    .WithIdentity("triggercompleteDep", "group2")
            //    .WithSimpleSchedule(x => x
            //        .WithIntervalInSeconds(completeDepositJobInterval)
            //        .RepeatForever()
            //        )
            //    .Build();

            IJobDetail checkDepositJob = JobBuilder.Create<EthProcessingJob>()
                .WithIdentity("checkDepJob", "group1")
                .Build();

            ITrigger checkDepositTrigger = TriggerBuilder.Create()
                .WithIdentity("trigger1", "group1")
                .WithSimpleSchedule(x => x
                    .WithIntervalInSeconds(depositJobInterval)
                    .RepeatForever()
                    )
                .Build();

            //

            scheduler.ScheduleJob(checkDepositJob, checkDepositTrigger);
            //scheduler.ScheduleJob(completeDepositJob, completeDepositTrigger);

            #region UpdateStatusCommissionJob
            //int updateStatusCommissionJobInterval = 5;//In seconds
            //int.TryParse(ConfigurationManager.AppSettings["UpdateStatusCommissionJobInterval"],out updateStatusCommissionJobInterval);
            IJobDetail updatestautsCommissionJob = JobBuilder.Create<UpdateStatusCommissionJob>()
                .WithIdentity("UpdateStatusCommissionJob", "UpdateStatusCommissionGroup")
                .Build()
                ;

            IScheduleBuilder scheduleBuilder = CronScheduleBuilder
                .CronSchedule(ConfigurationManager.AppSettings["UpdateStatusCommissionJobSchedule"])
                .InTimeZone(TimeZoneInfo.Utc);

            ITrigger updateStatusCommissionTrigger = TriggerBuilder.Create()
                .WithIdentity("UpdateStatusCommissionTrigger", "UpdateStatusCommissionGroup")
                .WithSchedule(scheduleBuilder)
                    .Build();
            scheduler.ScheduleJob(updatestautsCommissionJob, updateStatusCommissionTrigger);
            #endregion


            #region TransactionForCancel

            int transactionCancelJobInterval = 5;//In seconds
            int.TryParse(ConfigurationManager.AppSettings["TransactionCancelJobInterval"], out transactionCancelJobInterval);

            IJobDetail transactionforCancelJob = JobBuilder.Create<TransactionCancelJob>()
               .WithIdentity("TransactionforCancelJob", "TransactionForCancelGroup")
               .Build();

            ITrigger transactionforCancelTrigger = TriggerBuilder.Create()
                .WithIdentity("TransactionforCancelTrigger", "TransactionForCancelGroup")
                .WithSimpleSchedule(x => x
                    .WithIntervalInMinutes(transactionCancelJobInterval)
                    .RepeatForever()
                    )
                .Build();

            scheduler.ScheduleJob(transactionforCancelJob, transactionforCancelTrigger);
            #endregion

            scheduler.Start();
            if (Environment.UserInteractive)
                Thread.Sleep(10000000);
        }

        protected override void OnStop()
        {
            scheduler.Shutdown(true);
            base.OnStop();
        }
    }
}
