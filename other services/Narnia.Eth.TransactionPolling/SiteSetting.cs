﻿using Narnia.Eth.TransactionPolling.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Narnia.Eth.TransactionPolling
{
    public class SiteSetting
    {
        private IList<Setting> _allSettings;
        public SiteSetting()
        {
        }
    
        public int TotalUsersOnline { get; set; }
        public decimal WithdrawalFee { get; set; }
        public string SystemWallet { get; set; }
        public string SystemWalletPassword { get; set; }
        public decimal FeePercentage { get; set; }
        public int PaymentWaitingTime { get; set; }
        public double MinNumberOfBTC { get; set; }
        public double MaxNumberOfBTC { get; set; }
        public double MinNumberOfBCH { get; set; }
        public double MaxNumberOfBCH { get; set; }
        public double MinNumberOfETH { get; set; }
        public double MaxNumberOfETH { get; set; }
        public double MinNumberOfUSDT { get; set; }
        public double MaxNumberOfUSDT { get; set; }
        public string USDTContractAddress { get; set; }
        public List<string> SupportedSymbols { get; set; }
        public string BTCWalletPath { get; set; }
        public Uri BaseUri { get; set; }

        private Setting GetSettingByName(string settingName)
        {
            foreach (var setting in _allSettings)
            {
                if (string.Equals(setting.Name, settingName, StringComparison.InvariantCultureIgnoreCase))
                    return setting;
            }
            return null;
        }
        private T GetSettingValue<T>(string settingName)
        {
            var setting = GetSettingByName(settingName);
            if (setting != null)
            {
                TypeConverter tc = TypeDescriptor.GetConverter(typeof(T));
                return (T)tc.ConvertFrom(setting.Value);
            }
            return default(T);
        }
        private string GetSettingRawValue(string settingName)
        {
            var setting = GetSettingByName(settingName);
            if (setting != null)
            {
                return setting.Value;
            }
            return null;
        }
        protected void ParseSettings()
        {
            TotalUsersOnline = GetSettingValue<int>("TotalUsersOnline");
            WithdrawalFee = GetSettingValue<decimal>("WithdrawalFee");
            SystemWallet = GetSettingRawValue("SystemWallet");
            SystemWalletPassword = GetSettingRawValue("SystemWalletPassword");
            PaymentWaitingTime = GetSettingValue<int>("PaymentWaitingTime");
            MinNumberOfBTC = GetSettingValue<double>("MinNumberOfBTC");
            MaxNumberOfBTC = GetSettingValue<double>("MaxNumberOfBTC");
            MinNumberOfBCH = GetSettingValue<double>("MinNumberOfBCH");
            MaxNumberOfBCH = GetSettingValue<double>("MaxNumberOfBCH");
            MinNumberOfETH = GetSettingValue<double>("MinNumberOfETH");
            MaxNumberOfETH = GetSettingValue<double>("MaxNumberOfETH");
            MinNumberOfUSDT = GetSettingValue<double>("MinNumberOfUSDT");
            MaxNumberOfUSDT = GetSettingValue<double>("MaxNumberOfUSDT");
            USDTContractAddress = GetSettingValue<string>("USDTContractAddress");
            FeePercentage = GetSettingValue<decimal>("FeePercentage");
            BTCWalletPath = GetSettingValue<string>("BTCWalletPath");


        }
        public void MapSetings(IList<Setting> settings)
        {
            this._allSettings = settings;
            ParseSettings();
        }
    }

}
