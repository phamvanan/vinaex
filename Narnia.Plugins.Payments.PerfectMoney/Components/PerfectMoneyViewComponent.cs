﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Narnia.Plugins.Payments.PerfectMoney.Components
{
    [ViewComponent(Name ="PerfectMoney")]
    public class PerfectMoneyViewComponent: ViewComponent
    {
        public async Task<IViewComponentResult> InvokeAsync()
        {
            return View("~/Areas/Payments/Views/Components/PerfectMoney/Default.cshtml");
        }
    }
}
