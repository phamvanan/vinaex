﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Web.Framework
{
    public static class AreaNames
    {
        public const string Admin = "Admin";
        public const string Payments = "Payments";
    }
}
