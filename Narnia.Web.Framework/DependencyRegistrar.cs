﻿using DryIoc;
using Narnia.Business;
using Narnia.Business.CoinServices;
using Narnia.Business.Commissions;
using Narnia.Business.Impl;
using Narnia.Business.Localization;
using Narnia.Business.Messaging;
using Narnia.Business.Multimedia;
using Narnia.Core;
using Narnia.Core.Caching;
using Narnia.Core.Infrastructure;
using Narnia.Data.Base;
using Narnia.Data.Base.Repositories;
using Narnia.Data.Repository.Contracts;
using Narnia.Data.Repository.Impl;
using Narnia.Web.Framework.UI;
using Narnia.Exchange.ClientProxy;
using Narnia.Exchange.ClientProxy.Connections;
using Narnia.Exchange.Core.Serialization;
using Narnia.Web.Framework.Mvc.Routing;
using Narnia.Business.Transactions;

namespace Narnia.Web.Framework
{
    public class DependencyRegistrar : DryIocModule
    {
        protected override void Load(IRegistrator builder)
        {
            builder.Register<IPageHeadBuilder, PageHeadBuilder>(Reuse.InWebRequest);
            builder.Register<CoinServiceFactory, CoinServiceFactory>(Reuse.Singleton);
            
            //Register for cache
            builder.Register<ICacheManager, MemoryCacheManager>(Reuse.Singleton);
            builder.Register<INarniaFileProvider, NarniaFileProvider>(Reuse.InCurrentScope);
            builder.Register<IWebHelper, WebHelper>(Reuse.InCurrentScope);
            //Register for Data Repositories
            builder.Register(typeof(IGenericRepository<>), typeof(GenericRepository<>), reuse: Reuse.InWebRequest);
            builder.RegisterDelegate<IDbContext>((c) => { return c.Resolve<IDbContextFactory>().CreateDbContext(); }, Reuse.Transient);
            builder.Register<IUserRepository, UserRepository>(Reuse.InWebRequest);
            builder.Register<ISellAdvertisementRepository, SellAdvertisementRepository>(Reuse.InWebRequest);
            builder.Register<IBuyAdvertisementRepository, BuyAdvertisementRepository>(Reuse.InWebRequest);
            builder.Register<ITransactionRepository, TransactionRepository>(Reuse.InWebRequest);
            builder.Register<IAdvertisementFeeRepository, AdvertisementFeeRepository>(Reuse.InWebRequest);
            builder.Register<IEvidenceRepository, EvidenceRepository>(Reuse.InWebRequest);
            builder.Register<ICoinWalletRepository, CoinWalletRepository>(Reuse.InWebRequest);
            builder.Register<IBankAccountRepository, BankAccountRepository>(Reuse.InWebRequest);
            builder.Register<IBanksRepository, BanksRepository>(Reuse.InWebRequest);
            builder.Register<ICoinSymbolRepository, CoinSymbolRepository>(Reuse.InWebRequest);
            builder.Register<ITransactionWalletRepository, TransactionWalletRepository>(Reuse.InWebRequest);
            builder.Register<IAssetBalanceRepository, AssetBalanceRepository>(Reuse.InWebRequest);
            builder.Register<ICommissionRepository, CommissionRepository>(Reuse.InWebRequest);

            builder.Register<IDepositRepository, DepositRepository>(Reuse.InWebRequest);

            builder.Register<IMessageTokenProvider, MessageTokenProvider>(Reuse.InWebRequest);
            builder.Register<IPictureService, PictureService>(Reuse.InWebRequest);


            //Register for Socket Clients
            //MessagePusher messagePusher = new MessagePusher();

            builder.Register<ISerializer, ProtobufSerializer>(Reuse.Singleton);
            builder.Register<IMessagePusher, MessagePusher>(Reuse.Singleton);
            builder.Register<IServerMessageSubscriber, ServerMessageSubscriber>(Reuse.Singleton);
            builder.Register<IClient, NarniaClient>(Reuse.Singleton);

            //Register for Data Services
            builder.Register<IEmailSender, EmailSender>(Reuse.Transient);
            builder.Register<IEthService, EthService>(Reuse.InWebRequest);
            builder.Register<IBitcoinService, BitcoinService>(Reuse.InWebRequest);
            builder.Register<IUsdtService, UsdtService>(Reuse.InWebRequest);

            builder.Register<IUserBusiness, UserBusiness>(Reuse.InWebRequest);
            builder.Register<IWorkContext, WorkContext>(Reuse.InWebRequest);

            builder.Register<IBuyAdvertisementBiz, BuyAdvertisementBiz>(Reuse.InWebRequest);
            builder.Register<ISellAdvertisementBiz, SellAdvertisementBiz>(Reuse.InWebRequest);
            builder.Register<ITransactionBiz, TransactionBiz>(Reuse.InWebRequest);
            builder.Register<EmailJob, EmailJob>(Reuse.Transient);
            builder.Register<ITokenizer, Tokenizer>(Reuse.Transient);
            builder.Register<ISiteSetting, SiteSetting>(Reuse.Singleton);
            builder.Register<ISettingService, SettingService>(Reuse.InWebRequest);
            builder.Register<ICoinWalletBusiness, CoinWalletBusiness>(Reuse.InWebRequest);
            builder.Register<IFeedbackBusiness, FeedbackBusiness>(Reuse.InWebRequest);
            builder.Register<IEmailTemplateBusiness, EmailTemplateBusiness>(Reuse.Transient);
            builder.Register<IBankAccountBiz, BankAccountBiz>(Reuse.Transient);
            builder.Register<IBanksBiz, BanksBiz>(Reuse.Transient);
            builder.Register<ICoinSymbolBiz, CoinSymbolBiz>(Reuse.Transient);
            builder.Register<ITransactionWalletBiz, TransactionWalletBiz>(Reuse.Transient);
            builder.Register<IMessageTemplateBiz, MessageTemplateBiz>(Reuse.InWebRequest);
            builder.Register<IWithdrawBusiness, WithdrawBusiness>(Reuse.InWebRequest);
            builder.Register<IAdvertisementFeeBusiness, AdvertisementFeeBusiness>(Reuse.InWebRequest);
            builder.Register<IEvidenceBiz, EvidenceBiz>(Reuse.InWebRequest);
            builder.Register<ICommonBiz, CommonBiz>(Reuse.InWebRequest);
            builder.Register<ICommissionService, CommissionService>(Reuse.InWebRequest);
            builder.Register<IPaymentBiz, PaymentBiz>(Reuse.InWebRequest);
            //builder.Register<IDepositService, DepositService>(Reuse.InWebRequest);
            builder.Register<IExchangeOfferBusiness, ExchangeOfferBusiness>(Reuse.InWebRequest);

            builder.Register<IOrderService, OrderService>(Reuse.InWebRequest);
            builder.Register<ILanguageService, LanguageService>(Reuse.InWebRequest);
            builder.Register<ILocalizationService, LocalizationService>(Reuse.InWebRequest);
            builder.Register<IAssetTypeBusiness, AssetTypeBusiness>(Reuse.InWebRequest);
            builder.Register<IAssetBalanceBusiness, AssetBalanceBusiness>(Reuse.InWebRequest);
            builder.Register<IRoutePublisher, RoutePublisher>(Reuse.Singleton);
            builder.Register<ICoinAccountSystemBusiness, CoinAccountSystemBusiness>(Reuse.InWebRequest);
        }
    }
}