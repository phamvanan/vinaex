﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Web.Framework.UI
{
    public enum ResourceLocation
    {
        /// <summary>
        /// Header
        /// </summary>
        Head,
        /// <summary>
        /// Footer
        /// </summary>
        Footer,
    }
}
