﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Web.Framework.UI
{
    public enum NotifyType
    {
        /// <summary>
        /// Success
        /// </summary>
        Success,
        /// <summary>
        /// Error
        /// </summary>
        Error,
        /// <summary>
        /// Warning
        /// </summary>
        Warning
    }
}
