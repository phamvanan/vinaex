﻿using System.ComponentModel.DataAnnotations;
using FluentValidation;
using Narnia.Core.Infrastructure;

namespace Narnia.Web.Framework.Mvc.Validators
{
    public class BaseAbstractValidator<T> : AbstractValidator<T> where T : class
    {
    }
}
