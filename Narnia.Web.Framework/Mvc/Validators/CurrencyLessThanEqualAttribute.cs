﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using Narnia.Business.Localization;
using Narnia.Core.Infrastructure;

namespace Narnia.Web.Framework.Mvc.Validators
{
    public class CurrencyLessThanEqualAttribute : LessThanEqualAttribute
    {
        //"{0} must be less than or equal to {1}"
        protected override string ValidationType
        {
            get
            {
                return "currencylessthanequal";
            }
        }
        public CurrencyLessThanEqualAttribute(string otherProperty):base(otherProperty)
        {
        }
        protected override string FormatErrorMessage(string name, string otherName)
        {
            return string.Format(ErrorMessage = EngineContext.Current.Resolve<ILocalizationService>().GetResource("LessThanEqual"), name, "{0}");
        }
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var firstValue = value as IComparable;
            var propertyInfo = validationContext.ObjectType.GetProperty(OtherProperty);
            IComparable secondValue = null;
            if (propertyInfo != null)
            {
                secondValue = propertyInfo.GetValue(validationContext.ObjectInstance, null) as IComparable;

                if (firstValue != null && secondValue != null)
                {
                    if (firstValue.CompareTo(secondValue) > 0)
                    {
                        var displayNameAttribute = (DisplayNameAttribute)GetCustomAttribute(propertyInfo, typeof(DisplayNameAttribute));
                        var secondDisplayName = propertyInfo.Name;
                        if (displayNameAttribute != null)
                        {
                            secondDisplayName = displayNameAttribute.DisplayName;
                        }
                        return new ValidationResult(FormatErrorMessage(validationContext.DisplayName, secondDisplayName));
                    }
                }
            }

            return ValidationResult.Success;
        }
    }
}
