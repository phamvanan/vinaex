﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using Narnia.Core.Infrastructure;
using Narnia.Business.Localization;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;

namespace Narnia.Web.Framework.Mvc.Validators
{
    public class LessThanEqualAttribute : ValidationAttribute, IClientModelValidator
    {
        //"{0} must be less than or equal to {1}"
        protected virtual string ValidationType
        {
            get
            {
                return "lessthanequal";
            }
        }
        public string OtherProperty { get; set; }
        public LessThanEqualAttribute(string otherProperty)
        {
            ErrorMessage = EngineContext.Current.Resolve<ILocalizationService>().GetResource("GreaterThanEqual");
            OtherProperty = otherProperty;
        }
        protected virtual string FormatErrorMessage(string name, string otherName)
        {
            return string.Format(ErrorMessage, name, otherName);
        }
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var firstValue = value as IComparable;
            var propertyInfo = validationContext.ObjectType.GetProperty(OtherProperty);
            IComparable secondValue = null;
            if (propertyInfo != null)
            {
                secondValue = propertyInfo.GetValue(validationContext.ObjectInstance, null) as IComparable;

                if (firstValue != null && secondValue != null)
                {
                    if (firstValue.CompareTo(secondValue) > 0)
                    {
                        var displayNameAttribute = (DisplayNameAttribute)GetCustomAttribute(propertyInfo, typeof(DisplayNameAttribute));
                        var secondDisplayName = propertyInfo.Name;
                        if (displayNameAttribute != null)
                        {
                            secondDisplayName = displayNameAttribute.DisplayName;
                        }
                        return new ValidationResult(FormatErrorMessage(validationContext.DisplayName, secondDisplayName));
                    }
                }
            }

            return ValidationResult.Success;
        }

        private void MergeAttribute(IDictionary<string, string> dict, string name, string val)
        {
            if (dict.ContainsKey(name))
            {
                dict[name] = val;
            }
            else
            {
                dict.Add(name, val);
            }
        }
        public void AddValidation(ClientModelValidationContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }
            MergeAttribute(context.Attributes, "data-val", "true");
            MergeAttribute(context.Attributes, $"data-val-{ValidationType}", FormatErrorMessage("TestName", "TestOtherName"));
            MergeAttribute(context.Attributes, $"data-val-{ValidationType}-otherpropertyname", OtherProperty);
        }
    }
}
