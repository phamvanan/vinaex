﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Narnia.Web.Framework.Mvc.Validators
{
    public class RangeLocalizedAttribute: RangeAttribute
    {
        public RangeLocalizedAttribute(double minimum, double maximum) : base(minimum, maximum) { }
        public RangeLocalizedAttribute(int minimum, int maximum) : base(minimum, maximum) { }
        public RangeLocalizedAttribute(Type type, string minimum, string maximum) : base(type, minimum, maximum) { }
        public override string FormatErrorMessage(string name)
        {
            return base.FormatErrorMessage(name);
        }
    }
}
