﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using Narnia.Core.Infrastructure;
using Narnia.Business.Localization;

namespace Narnia.Web.Framework.Mvc.Validators
{
    public class CurrencyGreaterThanEqualAttribute : GreaterThanEqualAttribute
    {
        protected override string ValidationType
        {
            get
            {
                return "currencygreaterthanequal";
            }
        }
        //: base("{0} must be greater than or equal to {1}")
        public CurrencyGreaterThanEqualAttribute(string otherProperty):base(otherProperty)
        {
        }
        protected override string FormatErrorMessage(string name, string otherName)
        {
            return string.Format(EngineContext.Current.Resolve<ILocalizationService>().GetResource("GreaterThanEqual"), name,"{0}");
        }
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var firstValue = value as IComparable;
            var propertyInfo = validationContext.ObjectType.GetProperty(OtherProperty);
            IComparable secondValue = null;
            if (propertyInfo != null)
            {
                secondValue = propertyInfo.GetValue(validationContext.ObjectInstance, null) as IComparable;

                if (firstValue != null && secondValue != null)
                {
                    if (firstValue.CompareTo(secondValue) < 0)
                    {
                        var displayNameAttribute = (DisplayNameAttribute)GetCustomAttribute(propertyInfo, typeof(DisplayNameAttribute));
                        var secondDisplayName = propertyInfo.Name;
                        if (displayNameAttribute != null)
                        {
                            secondDisplayName = displayNameAttribute.DisplayName;
                        }
                        return new ValidationResult(FormatErrorMessage(validationContext.DisplayName, secondDisplayName));
                    }
                }
            }

            return ValidationResult.Success;
        }
    }
}
