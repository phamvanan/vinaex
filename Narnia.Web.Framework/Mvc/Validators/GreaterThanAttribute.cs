﻿using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using Narnia.Business.Localization;
using Narnia.Core.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
namespace Narnia.Web.Framework.Mvc.Validators
{
    public class GreaterThanAttribute : ValidationAttribute, IClientModelValidator
    {
        protected virtual string ValidationType
        {
            get
            {
                return "greaterthan";
            }
        }
        //: base("{0} must be greater than {1}")
        public GreaterThanAttribute(string otherProperty)
        {
            ErrorMessage = EngineContext.Current.Resolve<ILocalizationService>().GetResource("GreaterThan");
            OtherProperty = otherProperty;
        }
        public string OtherProperty { get; set; }

        protected string FormatErrorMessage(string name, string otherName)
        {
            return string.Format(ErrorMessage, name, otherName);
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var firstValue = value as IComparable;
            var propertyInfo = validationContext.ObjectType.GetProperty(OtherProperty);
            IComparable secondValue = null;
            if(propertyInfo != null)
            {
                secondValue = propertyInfo.GetValue(validationContext.ObjectInstance, null) as IComparable;

                if (firstValue != null && secondValue != null)
                {
                    if (firstValue.CompareTo(secondValue) < 1)
                    {
                        var displayNameAttribute = (DisplayNameAttribute)GetCustomAttribute(propertyInfo, typeof(DisplayNameAttribute));
                        var secondDisplayName = propertyInfo.Name;
                        if(displayNameAttribute != null)
                        {
                            secondDisplayName = displayNameAttribute.DisplayName;
                        }
                        return new ValidationResult(FormatErrorMessage(validationContext.DisplayName, secondDisplayName));
                    }
                }
            }
            
            return ValidationResult.Success;
        }

        //public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        //{
        //    var rule = new ModelClientValidationRule();
        //    var propertyInfo = metadata.ContainerType.GetProperty(OtherProperty);
        //    if(propertyInfo != null)
        //    {
        //        var displayNameAttribute = (DisplayNameAttribute)GetCustomAttribute(propertyInfo, typeof(DisplayNameAttribute));
        //        var secondDisplayName = propertyInfo.Name;
        //        if (displayNameAttribute != null)
        //        {
        //            secondDisplayName = displayNameAttribute.DisplayName;
        //        }
        //        rule.ErrorMessage = FormatErrorMessage(metadata.GetDisplayName(), secondDisplayName);
        //        rule.ValidationType = "greaterthan";
        //        rule.ValidationParameters.Add("otherpropertyname", OtherProperty); 
        //    }
        //    yield return rule;
        //}
        private void MergeAttribute(IDictionary<string,string> dict, string name, string val)
        {
            if (dict.ContainsKey(name))
            {
                dict[name] = val;
            }
            else
            {
                dict.Add(name, val);
            }
        }
        public void AddValidation(ClientModelValidationContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }
            MergeAttribute(context.Attributes, "data-val", "true");
            MergeAttribute(context.Attributes, $"data-val-{ValidationType}", FormatErrorMessage("TestName", "TestOtherName"));
            MergeAttribute(context.Attributes, $"data-val-{ValidationType}-otherpropertyname", OtherProperty);
        }
    }
}
