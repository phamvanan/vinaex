﻿namespace Narnia.Web.Framework.Mvc
{
    public class BaseModel
    {
    }

    public class BaseEntityModel : BaseModel
    {
        public virtual int Id { get; set; }
    }
}