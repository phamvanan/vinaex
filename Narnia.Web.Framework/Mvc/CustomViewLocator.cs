﻿using Microsoft.AspNetCore.Mvc.Razor;
using Narnia.Core;
using Narnia.Core.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Narnia.Web.Framework.Mvc
{
    public class CustomViewLocator : IViewLocationExpander
    {
        public void PopulateValues(ViewLocationExpanderContext context)
        { }

        public virtual IEnumerable<string> ExpandViewLocations(ViewLocationExpanderContext context, IEnumerable<string> viewLocations)
        {
            var workContext = EngineContext.Current.Resolve<IWorkContext>();
            //Replace folder view with CustomViews
            var viewLangLocations = viewLocations.Select(f => f.Replace("/Views/", "/Views/" + workContext.WorkingLanguage.LanguageCulture + "/"));
            return viewLangLocations.Concat(viewLocations);
        }
    }
}
