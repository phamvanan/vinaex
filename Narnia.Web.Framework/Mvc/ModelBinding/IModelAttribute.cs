﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Web.Framework.Mvc.ModelBinding
{
    public interface IModelAttribute
    {
        /// <summary>
        /// Gets name of the attribute
        /// </summary>
        string Name { get; }
    }
}
