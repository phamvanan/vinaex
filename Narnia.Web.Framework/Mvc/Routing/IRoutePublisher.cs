﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Routing;
namespace Narnia.Web.Framework.Mvc.Routing
{
    public interface IRoutePublisher
    {
        void RegisterRoutes(IRouteBuilder routeBuilder);
    }
}
