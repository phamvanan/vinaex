﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using Narnia.Business;
using Narnia.Business.Localization;
using Narnia.Core;
using Narnia.Core.Const;
using Narnia.Core.Domain;
using Narnia.Core.Domain.Localization;
using Narnia.Core.Infrastructure;
using System;
using System.Linq;
using System.Security.Claims;

namespace Narnia.Web.Framework
{
    public class WorkContext : IWorkContext
    {
        private readonly ILanguageService _languageService;
        private readonly IHttpContextAccessor _httpContextAccessor;
        public WorkContext(ILanguageService languageService, IHttpContextAccessor httpContextAccessor)
        {
            this._languageService = languageService;
            this._httpContextAccessor = httpContextAccessor;
        }
        private Language _cachedLanguage;
        private CoinSymbol _cachedSymbol;
        /// <summary>
        /// Language will be detected in order: session -> user preferences -> system default
        /// </summary>
        public virtual Language WorkingLanguage
        {
            get
            {
                //whether there is a cached value
                if (_cachedLanguage != null)
                    return _cachedLanguage;

                Language detectedLanguage = null;

                int? langId = _httpContextAccessor.HttpContext.Session.Get<int?>(NarniaSessionKeys.LanguageIdAttribute);
                if(langId == null)
                {
                    detectedLanguage = GetLanguageFromRequest();
                    if(detectedLanguage != null)
                    {
                        _httpContextAccessor.HttpContext.Session.Set(NarniaSessionKeys.LanguageIdAttribute, detectedLanguage.Id);
                    }
                }
                else
                {
                    detectedLanguage = _languageService.GetAllLanguages().FirstOrDefault(language =>
                                        language.Id == langId && language.Published);
                }
                if(detectedLanguage == null)
                {
                    detectedLanguage = _languageService.GetAllLanguages().FirstOrDefault();
                }
                //cache the found language
                _cachedLanguage = detectedLanguage;

                return _cachedLanguage;
            }
            set
            {
                //get passed language identifier
                var languageId = value?.Id ?? 0;
                _httpContextAccessor.HttpContext.Session.Set(NarniaSessionKeys.LanguageIdAttribute, languageId);
                //then reset the cached value
                _cachedLanguage = null;
            }
        }
        public virtual CoinSymbol WorkingSymbol
        {
            get
            {
                //whether there is a cached value
                if (_cachedSymbol != null)
                    return _cachedSymbol;

                CoinSymbol detectedSymbol = null;
                ICoinSymbolBiz coinSymbolService = EngineContext.Current.Resolve<ICoinSymbolBiz>();
                int? symbolId = _httpContextAccessor.HttpContext.Session.Get<int?>(NarniaSessionKeys.SymbolIdAttribute);
                if (symbolId == null)
                {
                    detectedSymbol = coinSymbolService.GetAll().FirstOrDefault(symbol => symbol.IsDefault);;
                    if (detectedSymbol != null)
                    {
                        _httpContextAccessor.HttpContext.Session.Set(NarniaSessionKeys.SymbolIdAttribute, detectedSymbol.Id);
                    }
                }
                else
                {
                    detectedSymbol = coinSymbolService.GetAll().FirstOrDefault(symbol => symbol.Id == symbolId);
                }
                if (detectedSymbol == null)
                {
                    detectedSymbol = coinSymbolService.GetAll().FirstOrDefault();
                }
                //cache the found language
                _cachedSymbol = detectedSymbol;

                return _cachedSymbol;
            }
            set
            {
                //get passed language identifier
                var symbolId = value?.Id ?? 0;
                _httpContextAccessor.HttpContext.Session.Set(NarniaSessionKeys.SymbolIdAttribute, symbolId);
                //then reset the cached value
                _cachedSymbol = null;
            }
        }
    
        private User _currentUser = null;
        private bool userInfoLoaded = false;
        public User CurrentUser
        {
            get
            {
                if (!userInfoLoaded)
                {
                    var httpContext = EngineContext.Current.Resolve<IHttpContextAccessor>().HttpContext;
                    ClaimsIdentity ident = (ClaimsIdentity)httpContext.User.Identity;
                    var idClaim = ident.Claims.FirstOrDefault(claim => claim.Type == ClaimTypes.Sid);
                    int userId = 0;
                    string username = string.Empty;
                    string referalCode = string.Empty;

                    if (idClaim != null)
                    {
                        int.TryParse(idClaim.Value, out userId);
                    }
                    var usernameClaim = ident.Claims.FirstOrDefault(claim => claim.Type == ClaimTypes.Name);
                    if (usernameClaim != null)
                    {
                        username = usernameClaim.Value;
                    }

                    var referalCodeClaim = ident.Claims.FirstOrDefault(claim => claim.Type == "referalCode");
                    if (referalCodeClaim != null)
                    {
                        referalCode = referalCodeClaim.Value;
                    }

                    var userAlias = ident.Claims.FirstOrDefault(claim => claim.Type.Equals("UserAlias"));
                    if (userId > 0)
                        _currentUser = new User { Id = userId, UserName = username, ReferenceCode = referalCode, Alias = userAlias?.Value };
                    userInfoLoaded = true;
                }

                return _currentUser;
            }
        }

        #region
        protected virtual Language GetLanguageFromRequest()
        {
            if (_httpContextAccessor.HttpContext?.Request == null)
                return null;

            //get request culture
            var requestCulture = _httpContextAccessor.HttpContext.Features.Get<IRequestCultureFeature>()?.RequestCulture;
            if (requestCulture == null)
                return null;

            var requestLanguage = _languageService.GetAllLanguages().FirstOrDefault(language =>
                language.LanguageCulture.Equals(requestCulture.Culture.Name, StringComparison.InvariantCultureIgnoreCase));

            if (requestLanguage == null || !requestLanguage.Published)
                return null;

            return requestLanguage;
        }
        #endregion
    }
}
