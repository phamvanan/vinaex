﻿using Narnia.Web.Framework.KendoUI;
using Narnia.Web.Framework.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Narnia.Web.Framework.Extensions
{
    public static class CommonExtensions
    {
        public static IEnumerable<T> PagedForCommand<T>(this IEnumerable<T> current, DataSourceRequest command)
        {
            return current.Skip((command.Page - 1) * command.PageSize).Take(command.PageSize);
        }
        public static IEnumerable<T> PaginationByRequestModel<T>(this IEnumerable<T> collection, IPagingRequestModel requestModel)
        {
            return collection.Skip((requestModel.Page - 1) * requestModel.PageSize).Take(requestModel.PageSize);
        }
    }
}
