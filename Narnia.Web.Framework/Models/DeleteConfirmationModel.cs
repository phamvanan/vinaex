﻿using Narnia.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Web.Framework.Models
{
    public class DeleteConfirmationModel : BaseEntityModel
    {
        /// <summary>
        /// Controller name
        /// </summary>
        public string ControllerName { get; set; }
        /// <summary>
        /// Action name
        /// </summary>
        public string ActionName { get; set; }
        /// <summary>
        /// Window ID
        /// </summary>
        public string WindowId { get; set; }
    }
}
