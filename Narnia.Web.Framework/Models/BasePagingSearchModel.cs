﻿namespace Narnia.Web.Framework.Models
{
    public abstract class BasePagingSearchModel : IPagingRequestModel
    {
        public BasePagingSearchModel()
        {
            this.SetGridPageSize();
        }

        public int Page { get; set; }
        public int PageSize { get; set; }

        public string AvailablePageSizes { get; set; }

        public void SetGridPageSize()
        {
            this.Page = 1;
            this.PageSize = 10;
            this.AvailablePageSizes = "10,20,50,100";
        }

        public void SetPopupGridPageSize()
        {
            this.Page = 1;
            this.PageSize = 10;
            this.AvailablePageSizes = "10,20,50,100";
        }
    }
}