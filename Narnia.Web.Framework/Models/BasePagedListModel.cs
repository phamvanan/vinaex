﻿using Narnia.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Web.Framework.Models
{
    public class BasePagedListModel<T>: IPagingRequestModel where T: BaseModel
    {
        public object ExtraData { get; set; }

        public object Errors { get; set; }
        public IEnumerable<T> Data { get; set; }
        public int Total { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
    }
}
