﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Web.Framework.Models
{
    public class ActionConfirmationModel
    {
        public string ControllerName { get; set; }
        /// <summary>
        /// Action name
        /// </summary>
        public string ActionName { get; set; }
        /// <summary>
        /// Window ID
        /// </summary>
        public string WindowId { get; set; }
        /// <summary>
        /// Additionl confirm text
        /// </summary>
        public string AdditonalConfirmText { get; set; }
    }
}
