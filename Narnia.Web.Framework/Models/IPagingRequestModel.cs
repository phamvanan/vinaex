﻿using Narnia.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Web.Framework.Models
{
    public interface IPagingRequestModel
    {
        int Page { get; set; }
        int PageSize { get; set; }
    }
}
