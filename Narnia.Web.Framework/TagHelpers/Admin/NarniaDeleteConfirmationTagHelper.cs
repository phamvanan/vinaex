﻿using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Narnia.Web.Framework.Extensions;
using Narnia.Web.Framework.Models;

namespace Narnia.Web.Framework.TagHelpers.Admin
{
    [HtmlTargetElement("narnia-delete-confirmation",Attributes =ModelIdAttributeName + "," + ButtonIdAttributeName, TagStructure = TagStructure.WithoutEndTag)]
    public class NarniaDeleteConfirmationTagHelper:TagHelper
    {
        private const string ModelIdAttributeName = "asp-model-id";
        private const string ButtonIdAttributeName = "asp-button-id";
        private const string ActionAttributeName = "asp-action";

        [HtmlAttributeName(ModelIdAttributeName)]
        public string ModelId { get; set; }

        [HtmlAttributeName(ButtonIdAttributeName)]
        public string ButtonId { get; set; }

        [HtmlAttributeName(ActionAttributeName)]
        public string Action { get; set; }


        private readonly IHtmlHelper _htmlHelper;
        protected IHtmlGenerator Generator { get; set; }

        [HtmlAttributeNotBound]
        [ViewContext]
        public ViewContext ViewContext { get; set; }
        public NarniaDeleteConfirmationTagHelper(IHtmlGenerator generator, IHtmlHelper htmlHelper)
        {
            Generator = generator;
            _htmlHelper = htmlHelper;
        }
        public override async Task ProcessAsync(TagHelperContext context, TagHelperOutput output)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            if (output == null)
            {
                throw new ArgumentNullException(nameof(output));
            }

            var viewContextAware = _htmlHelper as IViewContextAware;
            viewContextAware?.Contextualize(ViewContext);

            if (string.IsNullOrEmpty(Action))
                Action = "Delete";

            var modelName = _htmlHelper.ViewData.ModelMetadata.ModelType.Name.ToLower();
            modelName += "-" + Action;
            var modalPanelId = new HtmlString(modelName + "-delete-confirmation").ToHtmlString();

            if (int.TryParse(ModelId, out int modelId))
            {
                var deleteConfirmationModel = new DeleteConfirmationModel
                {
                    Id = modelId,
                    ControllerName = _htmlHelper.ViewContext.RouteData.Values["controller"].ToString(),
                    ActionName = Action,
                    WindowId = modalPanelId
                };
                output.TagName = "div";
                output.TagMode = TagMode.StartTagAndEndTag;

                output.Attributes.Add("id", modalPanelId);
                output.Attributes.Add("class", "modal fade");
                output.Attributes.Add("tabindex", "-1");
                output.Attributes.Add("role", "dialog");
                output.Attributes.Add("aria-labelledby", $"{modalPanelId}-title");

                output.Content.SetHtmlContent(await _htmlHelper.PartialAsync("Delete",deleteConfirmationModel));
                //Add modal script
                var script = new TagBuilder("script");
                script.InnerHtml.AppendHtml("$(document).ready(function () {" +
                                            $"$('#{ButtonId}').attr(\"data-toggle\", \"modal\").attr(\"data-target\", \"#{modalPanelId}\")" + "});");
                output.PostContent.SetHtmlContent(script.ToHtmlString());
            }
        }
    }
}
