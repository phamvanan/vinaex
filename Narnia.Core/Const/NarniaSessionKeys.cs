﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Core.Const
{
    public class NarniaSessionKeys
    {
        public static string LanguageIdAttribute => "LanguageId";
        public static string SymbolIdAttribute => "SymbolId";
    }
}
