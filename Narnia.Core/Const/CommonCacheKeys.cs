﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Core.Const
{
    public static class CommonCacheKeys
    {
        public static string SymbolsAllCacheKey => "Narnia.symbol.all";
        public static string AssetTypesAllCacheKey => "Narnia.assettype.all";
    }
}
