﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Core
{
    public class ResponseData
    {
        public bool HasError { get; set; }
        public object Data { get; set; }
        public string Message { get; set; }

        public ResponseData()
        {
            HasError = false;
            Message = string.Empty;
        }

        public ResponseData Success()
        {
            return new ResponseData
            {
                HasError = false
            };
        }

        public ResponseData Success(object data)
        {
            return new ResponseData
            {
                HasError = false,
                Data = data
            };
        }

        public ResponseData Error()
        {
            return new ResponseData
            {
                HasError = true
            };
        }

        public ResponseData Error(string message)
        {
            return new ResponseData
            {
                HasError = true,
                Message = message
            };
        }
    }

    public class ResponseData<T>
    {
        public bool HasError { get; set; }
        public T Data { get; set; }
        public string Message { get; set; }

        public ResponseData()
        {
            HasError = false;
            Message = string.Empty;
        }

        public ResponseData<T> Success()
        {
            return new ResponseData<T>
            {
                HasError = false
            };
        }

        public ResponseData<T> Success(T data)
        {
            return new ResponseData<T>
            {
                HasError = false,
                Data = data
            };
        }

        public ResponseData<T> Error()
        {
            return new ResponseData<T>
            {
                HasError = true
            };
        }

        public ResponseData<T> Error(string message)
        {
            return new ResponseData<T>
            {
                HasError = true,
                Message = message
            };
        }
    }
}
