﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Core
{
    public class CommonConst
    {
        public const string InvalidEmail = "Email không hợp lệ.";
        public const string NotExistsEmail = "Email chưa tồn tại trong hệ thống.";
        public const string HasExistsEmail = "Email đã tồn tại trong hệ thống.";
        public const string HasErrorWhenAddNewUser = "Có lỗi khi thêm mới user.";

        public const string HasErrorWhenAddNewEvidenceTransaction = "Có lỗi khi cập nhật bằng chứng";
        public const string HasErrorWhenSellerConfirmTransaction = "Có lỗi khi cập nhật giao dịch";
        public const string HasErrorWhenBuyerCancelTransaction = "Có lỗi khi hủy giao dịch";
        public const string HasErrorWhenAdminConfirmTransaction = "Có lỗi khi admin xác thực giao dịch";
        public const string HasErrorWhenCreateTransaction = "Có lỗi khi tạo giao dịch";
        public const string HasErrorWhenTransferAmount = "Có lỗi khi thực hiện chuyển tiền";
        public const string UserIdInvalid = "Mã user không hợp lệ";
        public const string TransIdIdInvalid = "Mã giao dịch không hợp lệ";
        public const string HasErrorWhenGetUserInfor = "Có lỗi khi lấy thông tin user.";
        public const string HasErrorWhenSellerConfirmtransaction = "Có lỗi khi xác thực giao dịch";
        public const string HasErrorWhenGetTransaction = "Giao dịch không hợp lệ";

        public const string UpdateTransactionSuccess = "Cập nhật giao dịch thành công";
        public const string TransactionHasNotEvidence = "Giao dịch chưa có thông tin chứng từ";

        // user
        public const string UserNotExists = "Không tồn tại user trong hệ thông.";
        public const string NotEnoughBalance = "Số dư tài khoản của bạn không đủ để rút.";
        public const string InvalidCoinWalletAddress = "Địa chỉ ví không hợp lệ.";
        public const string InvalidWithdrawAmount = "Số tiền rút không hợp lệ.";
        public const string NotFoundData = "Không tìm thấy dữ liệu.";
        public const string HasErrorWhenCreateNewTransactionWallet = "Có lỗi xảy ra khi thêm mới giao dich.";

        //userIdAdmin
        public const int UserIdAdmin = -1;

        public const string HasErrPleaseContactAdmin = "có lỗi xãy ra, xin vui lòng liên hệ quản trị";

        //transactionWallet
        public const string TransactionWithDrawalSuccess = "Giao dịch rút tiền thành công";
        public const string TransactionWithDepositSuccess = "Giao dịch nạp tiền thành công";
        public  const  string BankidEmptyForWithdrawal= "Bạn chưa chọn ngân hàng để rút tiền";
        public const string BankidEmptyForDeposit = "Bạn chưa chọn ngân hàng để nạp tiền";
        public const string AddBankAccountSuccess = "Thêm tài khoản ngân hàng thành công";
        public const string NotEnoughMoneyForWithdrawal = "Không đủ tiền trong tài khoản để rút";
        public const string NotPermissionToUpdateEvidence = "Bạn không có quyền cập nhật bằng chứng giao dịch này";
        public const string NotPermissionToCancelTranaction = "Bạn không có quyền hủy giao dịch này";
        public const string UpdateEvidenceSuccess = "Cập nhật bằng chứng thành công";
        public const string HasErrWhenCancelTransaction = "Có lỗi xảy ra khi hủy giao dịch";
        public const string CancelTransactionSuccess = "Hủy giao dịch thành công";
        public const string HasErrWhenConfirmTransaction = "Confirm giao dịch thất bại";
        public const string ConfirmTransactionSuccess = "Confirm giao dịch thành công";
        public const string NotPermissionToConfirmTransaction = "Bạn không có quyền confirm giao dịch này";

        //bank
        public const string DataBankExists = "Thông tin ngân hàng đã tồn tại trên hệ thống";
        public const string CreateBankNotSuccess = "Thêm ngân hàng thất bại";
        public const string UpdateBankNotSuccess = "Cập nhật ngân hàng thất bại";
        public const string DeleteBankNotSuccess = "Xóa ngân hàng thất bại";
        public const string NotFileUploadEvidence = "Chưa chọn file upload chứng từ" ;

        //SellAds
        public const string NotEnoughBalanceForSell = "Số dư tài khoản của bạn không đủ để tạo quảng cáo bán";        

        public const string NotEnoughBalanceVNDForBuy = "Số dư tài khoản VND của người mua không đủ để hoàn tất giao dịch";

        public const string InvalidInfo = "Dữ liệu không hợp lệ";
        public const string CreateAdsSuccess = "Đăng tin quảng cáo thành công";
        public const string NotEnoughBalanceCoinForTransaction = "Số dư tài khoản Coin của bạn không đủ để thực hiện giao dịch";

        // buy
        public const string NotEnoughBalanceForBuy = "Số dư tài khoản của bạn không đủ để tạo quảng cáo mua";

        // Exchange offer
        public const string ExchangeOfferErrorBuy = "Có lỗi xảy ra khi thực hiện trao đổi mua";
        public const string ExchangeOfferErrorSell = "Có lỗi xảy ra khi thực hiện trao đổi bán";
        public const string HasErrorWhenAddNewData = "Có lỗi xảy ra khi thê mới dữ liệu";
    }
}
