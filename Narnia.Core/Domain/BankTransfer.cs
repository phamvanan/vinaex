﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Core.Domain
{
    public class BankTransfer:BaseEntity
    {
        public int UserId { get; set; }
        public int DepositId { get; set; }
        public string OrderNo { get; set; }
        public string BankName { get; set; }
        public string AccountName { get; set; }
        public string AccountNumber { get; set; }
        public string Comments { get; set; }
        public decimal Amount { get; set; }
        public decimal Fee { get; set; }
        public int EvidencePictureId { get; set; }
        public string AssetTypeId { get; set; }
        public DateTime BillingDate { get; set; }
        public BankTransferStatus Status { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime UpdatedOn { get; set; }
        public User User { get; set; }
        public AssetType AssetType { get; set; }
    }
    public enum BankTransferStatus
    {
        Pending = 1,
        Rejected = 2,
        Success = 3
    }
}
