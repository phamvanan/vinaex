﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Core.Domain
{
    public class PaymentSetting
    {
        public decimal WithdrawalFee { get; set; }
        public decimal MinWithdrawalLimit { get; set; }
    }
}
