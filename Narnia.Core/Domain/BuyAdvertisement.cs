﻿using MicroOrm.Pocos.SqlGenerator.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Core.Domain
{
    public class BuyAdvertisement
    {
        [KeyProperty(Identity = true)]
        public long Id { get; set; }
        public int UserId { get; set; }
        public string CurrencyId { get; set; }
        public string AssetTypeId { get; set; }
        public int BankId { get; set; }
        public string BankAccountNumber { get; set; }
        public int CoinExchangeId { get; set; }
        public decimal CoinPrice { get; set; }      // gia ban user nhap
        public decimal MaxCoinPrice { get; set; }   // gia ban toi da
        public decimal? MinCoinNumber { get; set; }

        public decimal? MaxCoinNumber { get; set; }

        public int PaymentMethodId { get; set; }
        public int PaymentAllowTime { get; set; }
        public int CountryId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        [NonStored]
        public byte[] RowVersionNo { get; set; }
        public Boolean Approved { get; set; }
       
        [NonStored()]
        public User AdvertiseUser { get; set; }
        //[NonStored()]
        //public CoinSymbol CoinType { get; set; }
        [NonStored()]
        public Currency CurrencyType { get; set; }
        [NonStored()]
        public Banks Bank { get; set; }
        [NonStored()]
        public PaymentMethod PayType { get; set; }
        public decimal? AmountBuy { get; set; }
        public int? Status { get; set; }
    }
}
