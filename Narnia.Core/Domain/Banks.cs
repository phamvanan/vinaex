﻿using System;

namespace Narnia.Core.Domain
{
    public class Banks : BaseEntity
    {

        public string BankName { get; set; }

        public string BankCode { get; set; }

        public string Description { get; set; }

        public DateTime CreatedDate { get; set; }

        public bool IsDeleted { get; set; }

        public int? DeletedUser { get; set; }

        public DateTime? DeletedDate { get; set; }
        public string WebsiteUrl { get; set; }
    }
}