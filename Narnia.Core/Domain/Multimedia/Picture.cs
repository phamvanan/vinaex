﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Core.Domain.Multimedia
{
    public partial class Picture : BaseEntity
    {
        public string MimeType { get; set; }
        public string AltAttribute { get; set; }
        public string TitleAttribute { get; set; }
        public byte[] Data { get; set; }
        public bool IsNew { get; set; }
    }
}
