﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Core.Domain.Multimedia
{
    public enum PictureType
    {
        /// <summary>
        /// Entities (users' pictures, evidence.....)
        /// </summary>
        Entity = 1,

        /// <summary>
        /// Avatar
        /// </summary>
        Avatar = 10
    }
}
