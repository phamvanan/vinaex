﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Core.Domain
{
    public class PaymentMethod : BaseEntity
    {
        public string PaymentMethodCode { get; set; }
        public string PaymentMethodName { get; set; }
        public string Description { get; set; }
    }
}
