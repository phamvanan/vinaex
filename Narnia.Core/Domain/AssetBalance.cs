﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Core.Domain
{
    public class AssetBalance:BaseEntity
    {
        public int UserId { get; set; }
        public string AssetTypeId { get; set; }
        public decimal Balance { get; set; }
        public decimal FrozenBalance { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime UpdatedOn { get; set; }
        public User User { get; set; }
        public AssetType AssetType { get; set; }
        
    }
}
