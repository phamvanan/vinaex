﻿using System;
using System.Numerics;

namespace Narnia.Core.Domain
{
    public class CoinWallet : BaseEntity
    {
        public long UserId { get; set; }

        public string WalletAddress { get; set; }

        public string WalletPassword { get; set; }
        
        public decimal Amount { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime UpdatedDate { get; set; }

        public string AssetTypeId { get; set; }
        public User User { get; set; }
    }
}
