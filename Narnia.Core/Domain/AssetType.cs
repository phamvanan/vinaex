﻿using MicroOrm.Pocos.SqlGenerator.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Core.Domain
{
    public class AssetType
    {
        [KeyProperty()]
        public string AssetTypeId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
