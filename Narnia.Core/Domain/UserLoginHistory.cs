﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Core.Domain
{
    public class UserLoginHistory : BaseEntity
    {
        public int UserId { get; set; }
        public string IPAddress { get; set; }
        public DateTime LoginDate { get; set; }

    }
}
