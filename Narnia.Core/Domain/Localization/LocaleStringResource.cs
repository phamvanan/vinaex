﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Core.Domain.Localization
{
    public class LocaleStringResource: BaseEntity
    {
        public int LanguageId { get; set; }

        /// <summary>
        /// Gets or sets the resource name
        /// </summary>
        public string ResourceName { get; set; }

        /// <summary>
        /// Gets or sets the resource value
        /// </summary>
        public string ResourceValue { get; set; }

        /// <summary>
        /// Gets or sets the language
        /// </summary>
        public virtual Language Language { get; set; }
    }
}
