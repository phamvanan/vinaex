﻿using MicroOrm.Pocos.SqlGenerator.Attributes;
using System;

namespace Narnia.Core.Domain
{
    public class User : BaseEntity
    {
        public User()
        {
        }

        public string UserName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Alias { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string ReferenceCode { get; set; }
        public string ActivationKey { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool IsActive { get; set; }
        public bool IsVerify { get; set; }
        public int UplineId { get; set; }
        public int? AvatarPictureId { get; set; }

        [NonStored]
        public int Generation { get; set; }

        [NonStored]
        public int? RefFromChildId { get; set; }

        [NonStored]
        public UserInfor UserInfo { get; set; }

        public string PasswordSalt { get; set; }
        public string PasswordHash { get; set; }
        public bool IsAdmin { get; set; }
    }
}