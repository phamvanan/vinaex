﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Core.Domain
{
    public class EmailTemplate : BaseEntity
    {
        public string Name { get; set; }
        public string Body { get; set; }
        public string Subject { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public bool IsActive { get; set; }
    }
}
