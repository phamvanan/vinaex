﻿using MicroOrm.Pocos.SqlGenerator.Attributes;
using System;

namespace Narnia.Core.Domain
{
    public class Evidence
    {
        [KeyProperty(Identity = true)]
        public long Id { get; set; }

        public string FileName { get; set; }

        public byte[] Image { get; set; }

        public string Description { get; set; }
        public string ContentType { get; set; }

        public DateTime CreatedDate { get; set; }
        public string Code { get; set; }
    }
}
