﻿using MicroOrm.Pocos.SqlGenerator.Attributes;

namespace Narnia.Core.Domain
{
    public class CoinSymbol : BaseEntity
    {
        [NonStored]
        public string SymbolId
        {
            get
            {
                return $"{ExchangeId}_{SymbolType}_{AssetIdBase}_{AssetIdQuote}";
            }
        }

        public string ExchangeId { get; set; }
        public string SymbolType { get; set; }
        public string AssetIdBase { get; set; }
        public string AssetIdQuote { get; set; }
        public bool Active { get; set; }
        public decimal AvgPrice { get; set; }
        public decimal BuyPrice { get; set; }
        public decimal SellPrice { get; set; }
        public bool IsDefault { get; set; }
    }
}