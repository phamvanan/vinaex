﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Core.Domain
{
    public class CoinAccountSystem : BaseEntity
    {
        public string AssetTypeId { get; set; }
        public string WalletAddress { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
