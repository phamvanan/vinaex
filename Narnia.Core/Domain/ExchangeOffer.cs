﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Core.Domain
{
    public class ExchangeOffer : BaseEntity
    {
        public string AssetTypeId { get; set; }
        public decimal Amount { get; set; }
        public decimal VNDAmount { get; set; }
        public decimal Fee { get; set; }
        public int Type { get; set; }
        public string UserBankAccountNo { get; set; }
        public string UserBankAccountName { get; set; }
        public int Status { get; set; }
        public string ExchangeOfferCode { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string WalletAddress { get; set; }
        public string Phone { get; set; }
        public string PerfectMoneyUSDAccount { get; set; }
    }
}
