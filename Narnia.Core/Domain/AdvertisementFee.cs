﻿using MicroOrm.Pocos.SqlGenerator.Attributes;
using System;

namespace Narnia.Core.Domain
{
    public class AdvertisementFee
    {
        [KeyProperty(Identity = true)]
        public long Id { get; set; }

        public int UserId { get; set; }

        //public int? BuyAdvertisementId { get; set; }
        public string AssetTypeId { get; set; }

        //public long? SellAdvertisementId { get; set; }

        //public long? TransactionId { get; set; }

        public decimal Amount { get; set; }

        public decimal Fee { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime UpdatedDate { get; set; }

        public int Status { get; set; }
        public int RefId { get; set; }
        public int RefType { get; set; }

    }
}