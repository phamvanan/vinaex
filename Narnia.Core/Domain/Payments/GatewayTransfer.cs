﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Core.Domain.Payments
{
    public class GatewayTransfer : BaseEntity
    {
        public int GatewayId { get; set; }
        public int DepositId { get; set; }
        public string GatewayAccFrom { get; set; }
        public string GatewayAccTo { get; set; }
        public decimal Amount { get; set; }
        public decimal Fee { get; set; }
        public string AssetTypeId { get; set; }
        public GatewayTransferStatus Status { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string Description { get; set; }
    }

    public enum GatewayTransferStatus
    {
        Pending = 1,
        Success = 2,
        Rejected = 3
    }
}
