﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Core.Domain.Payments
{
    public class GatewayAccount: BaseEntity
    {
        public int? UserId { get; set; }
        
        public int GatewayId { get; set; }
        public string AccName { get; set; }
        public DateTime CreatedDate { get; set; }
        public string Description { get; set; }
        public bool IsSystemAccount { get; set; }
        public User User { get; set; }
    }
}
