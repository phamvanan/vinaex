﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Core.Domain.Payments
{
    public class PaymentGateway : BaseEntity
    {
        public string GatewaySystemName { get; set; }
        public string GatewayFriendlyName { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
    }
}
