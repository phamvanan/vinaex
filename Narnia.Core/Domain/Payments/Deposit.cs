﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Core.Domain.Payments
{
    public class Deposit:BaseEntity
    {
        public decimal Amount { get; set; }
        public decimal Fee { get; set; }
        public string AssetTypeId { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public DepositStatus Status { get; set; }
        public string Description { get; set; }
    }
    public enum DepositStatus
    {
        Pending = 1,
        Success = 2,
        Rejected = 3
    }
}
