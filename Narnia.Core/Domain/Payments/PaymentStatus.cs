﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Core.Domain.Payments
{
    public enum PaymentStatus
    {
        Pending = 1,
        Success = 2,
        Rejected = 3
    }
}
