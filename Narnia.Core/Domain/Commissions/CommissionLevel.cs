﻿using MicroOrm.Pocos.SqlGenerator.Attributes;

namespace Narnia.Core.Domain.Commissions
{
    public class CommissionLevel
    {
        [KeyProperty()]
        public int LevelId { get; set; }

        public string LevelName { get; set; }

        public double CommRate { get; set; }
    }
}