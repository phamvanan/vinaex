﻿using System;

namespace Narnia.Core.Domain
{
    public class UserInfor : BaseEntity
    {
        public int UserId { get; set; }
        public bool HasSendEmailNewTransaction { get; set; }
        public bool HasSendEmailExchangeTransaction { get; set; }
        public bool HasSendEmailMessageTransaction { get; set; }
        public bool HasSendSMSNewTransaction { get; set; }
        public bool HasSendSMSTransactionPaid { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string IDNumber { get; set; }
        public string UrlTwitter { get; set; }
        public byte[] Image { get; set; }
        public string ContentType { get; set; }
        public string Phone { get; set; }
        public bool HasReceivedDailyPrice { get; set; }
    }
}
