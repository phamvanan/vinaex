﻿using MicroOrm.Pocos.SqlGenerator.Attributes;
using System;

namespace Narnia.Core.Domain
{
    public class BankAccount : BaseEntity
    {

        public string AccountName { get; set; }

        public string AccountNo { get; set; }

        public int UserId { get; set; }

        public int BankId { get; set; }

        public string Branch { get; set; }
        public  bool IsActived { get; set; }

        public string Description { get; set; }

        public DateTime CreatedDate { get; set; }

        public bool IsDeleted { get; set; }

        public int? DeletedUser { get; set; }

        public DateTime? DeletedDate { get; set; }
    }
}