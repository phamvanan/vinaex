﻿using MicroOrm.Pocos.SqlGenerator.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Core.Domain
{
    public class Deposit
    {
        [KeyProperty(Identity = true)]
        public int Id { get; set; }
        public decimal Amount { get; set; }
        public string AssetTypeId { get; set; }
        public int UserId { get; set; }
        public DateTime CreatedDate { get; set; }

        public DateTime UpdatedDate { get; set; }

        public int Status { get; set; }
        public string Description { get; set; }

        [NonStored()]
        public User DepositUser { get; set; }
    }
}
