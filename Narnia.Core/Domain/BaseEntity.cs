﻿using MicroOrm.Pocos.SqlGenerator.Attributes;

namespace Narnia.Core.Domain
{
    public class BaseEntity
    {
        [KeyProperty(Identity = true)]
        public int Id { get; set; }
    }
}