﻿using MicroOrm.Pocos.SqlGenerator.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Narnia.Core.Domain
{
    public class Transaction
    {
        [KeyProperty(Identity = true)]
        public int TransactionId { get; set; }

        public int? SellAdvertisementId { get; set; }

        public int? BuyAdvertisementId { get; set; }

        public decimal AmountCoin { get; set; }

       
        public string AssetTypeId { get; set; }

        public DateTime CreatedDate { get; set; }

        public int UserId { get; set; }

        public int Status { get; set; }

        public string Description { get; set; }

        public int TransactionType { get; set; }

        [NonStored]
        public Byte[] RowVersionNo { get; set; }

        public int? ConfirmBy { get; set; }

        public int? EvidenceId { get; set; }

        public string TransactionCode { get; set; }

        public DateTime? UpdatedDate { get; set; }
        public int PaymentType { get; set; }

        public  int TransferType { get; set; }

        public SellAdvertisement SellAdvertisement { get; set; }

        public BuyAdvertisement BuyAdvertisement { get; set; }
        public User User { get; set; }
    }
}
