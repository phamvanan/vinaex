﻿using MicroOrm.Pocos.SqlGenerator.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Core.Domain
{
    public class SellAdvertisement
    {
        [KeyProperty(Identity = true)]
        public long Id { get; set; }
        public int UserId { get; set; }
        public string CurrencyId { get; set; }
        public string AssetTypeId { get; set; }
        public int BankId { get; set; }
        public string BankAccountNumber { get; set; }
        public string BankAccountName { get; set; }
        public int CoinExchangeId { get; set; }
        public decimal CoinPrice { get; set; }      // gia ban user nhap
        public decimal MinCoinPrice { get; set; }   // gia ban toi thieu
        public decimal MinCoinNumber { get; set; }
        public decimal MaxCoinNumber { get; set; }
        public int PaymentMethodId { get; set; }
        public int PaymentAllowTime { get; set; }
        public int CountryId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        [NonStored()]
        public Byte[] RowVersionNo { get; set; }
        public Boolean Approved { get; set; }        
        public decimal AmountSell { get; set; }

        [NonStored()]
        public User AdvertiseUser { get; set; }
        //[NonStored()]
        //public CoinSymbol CoinType { get; set; }
        [NonStored()]
        public Currency CurrencyType { get; set; }
        [NonStored()]
        public Banks Bank { get; set; }
        [NonStored()]
        public PaymentMethod PayType { get; set; }
        public int? Status { get; set; }
        public bool IsDeleted { get; set; }
    }
}
