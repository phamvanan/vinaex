﻿using System;

namespace Narnia.Core.Domain
{
    public class Feedback : BaseEntity
    {
        public string Content { get; set; }
        public int CreatedBy { get; set; }
        public User CreatedUser { get; set; }
        public DateTime CreatedDate { get; set; }
        public int UserId { get; set; }
        public User ReceivedUser { get; set; }
    }
}
