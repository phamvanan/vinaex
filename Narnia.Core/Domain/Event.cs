﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Core.Domain
{
    public class Event : BaseEntity
    {
        public decimal Amount { get; set; }
        public char BetValue { get; set; }
        public DateTime DateCreated { get; set; }
        public int DrawId { get; set; }
        public int UserId { get; set; }
    }
}
