﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Narnia.Core.Helpers
{
    public class WebRequestHelper
    {
        #region Syncronous

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="requestAddress"></param>
        /// <returns></returns>
        public static T GetJson<T>(string requestAddress)
        {
            var http = (HttpWebRequest)WebRequest.Create(new Uri(requestAddress));
            http.Accept = "application/json";
            http.ContentType = "application/json";
            http.Method = WebRequestMethods.Http.Get;

            var response = http.GetResponse();
            var stream = response.GetResponseStream();
            T t = default(T);

            if (stream != null)
            {
                var streamReader = new StreamReader(stream);
                var content = streamReader.ReadToEnd();
                t = JsonConvert.DeserializeObject<T>(content);
            }

            return t;
        }
        #endregion

        #region Asyncronous

        public static async Task<T> GetAsync<T>(string requestAddress)
        {
            using (var client = new HttpClient())
            {
                Uri requesetUri = new Uri(requestAddress);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage httpResponseMessage = await client.GetAsync(requesetUri);
                T t = default(T);
                if (httpResponseMessage.IsSuccessStatusCode)
                {
                    var content = httpResponseMessage.Content.ReadAsStringAsync().Result;
                    t = JsonConvert.DeserializeObject<T>(content);
                }

                return t;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="requestAddress"></param>
        /// <param name="param"></param>
        /// <returns></returns>
        public static async Task<T> PutAsync<T>(string requestAddress, object param)
        {
            using (var client = new HttpClient())
            {
                Uri requesetUri = new Uri(requestAddress);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage httpResponseMessage = await client.PutAsync(requesetUri, new StringContent(JsonConvert.SerializeObject(param).ToString(), Encoding.UTF8, "application/json"));
                T t = default(T);
                if (httpResponseMessage.IsSuccessStatusCode)
                {
                    var content = await httpResponseMessage.Content.ReadAsStringAsync();
                    var result = JsonConvert.DeserializeObject<T>(content);

                    return result;
                }

                return t;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="requestAddress"></param>
        /// <param name="param"></param>
        /// <returns></returns>
        public static async Task<T> PostAsync<T>(string requestAddress, object param)
        {
            using (var client = new HttpClient())
            {
                Uri requesetUri = new Uri(requestAddress);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage httpResponseMessage = await client.PostAsync(requesetUri, new StringContent(JsonConvert.SerializeObject(param).ToString(), Encoding.UTF8, "application/json"));
                T t = default(T);
                if (httpResponseMessage.IsSuccessStatusCode)
                {
                    var content = await httpResponseMessage.Content.ReadAsStringAsync();
                    var result = JsonConvert.DeserializeObject<T>(content);

                    return result;
                }

                return t;
            }
        }
        #endregion
    }
}
