﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Core.Helpers
{
    public static class DateTimeHelper
    {
        public static DateTime GetFirstDateOfNextMonth(DateTime currentDate)
        {
            return new DateTime(currentDate.Year, currentDate.Month, 1).AddMonths(1);
        }

        public static DateTime GetEndDateOfMonth(DateTime currentDate)
        {
            DateTime firstOfNextMonth = new DateTime(currentDate.Year, currentDate.Month, 1).AddMonths(1);
            return firstOfNextMonth.AddDays(-1);
        }

        public static int GetWeek(this DateTime datetime)
        {
            return (int)(Math.Round(datetime.DayOfYear / 7.0, 0) + 1);
        }

        public static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
        {
            if (unixTimeStamp == 0.0) return DateTime.MinValue;

            // Unix timestamp is seconds past epoch
            var dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();

            return dtDateTime;
        }
        public static DateTime UnixTimeStampToDateTime(this long unixTimeStamp)
        {
            if (unixTimeStamp == 0) return DateTime.MinValue;

            // Unix timestamp is seconds past epoch
            var dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();

            return dtDateTime;
        }

        //public static double DateTimeToUnixTimestamp(DateTime dateTime)
        //{
        //    return (dateTime - new DateTime(1970, 1, 1).ToLocalTime()).TotalSeconds;
        //}

        public static long ToUnixTimestamp(this DateTime dateTime)
        {
            return (long)(dateTime - new DateTime(1970, 1, 1).ToLocalTime()).TotalSeconds;
        }

        public static Int32 DateTimeToUnixTimestampInt32(DateTime dateTime)
        {
            return (Int32)(dateTime - new DateTime(1970, 1, 1).ToLocalTime()).TotalSeconds;
        }

        public static DateTime ConvertFromUnixTimestamp(this long timestamp)
        {
            DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0).ToLocalTime();
            return origin.AddSeconds(timestamp);
        }
        public static bool IsValidDate(string dateOfBirth, out DateTime date)
        {
            if (!DateTime.TryParse(dateOfBirth, out date))
                return false;
            if (date < new DateTime(1900, 01, 01)) return false;

            return true;
        }
    }
}
