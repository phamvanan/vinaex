﻿namespace Narnia.Core.Helpers
{
    public static class UploadHelper
    {
        public static bool IsImage(string contentType, string fileName)
        {
            //-------------------------------------------
            //  Check the image mime types
            //-------------------------------------------
            if (contentType.ToLower() != "image/jpg" &&
                 contentType.ToLower() != "image/jpeg" &&
                 contentType.ToLower() != "image/pjpeg" &&
                 contentType.ToLower() != "image/gif" &&
                 contentType.ToLower() != "image/x-png" &&
                 contentType.ToLower() != "image/png")
            {
                return false;
            }

            //-------------------------------------------
            //  Check the image extension
            //-------------------------------------------
            //if (fileName.ToLower() != ".jpg"
            //    && fileName.ToLower() != ".png"
            //    && fileName.ToLower() != ".gif"
            //    && fileName.ToLower() != ".jpeg")
            //{
            //    return false;
            //}
            return true;
        }
    }
}