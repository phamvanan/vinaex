﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Narnia.Core.Infrastructure;

namespace Narnia.Core.Events
{
    public class EventPublisher : IEventPublisher
    {
        private readonly IEventSubscriberService _eventSubscriberService;
        public EventPublisher(IEventSubscriberService eventSubscriberService)
        {
            _eventSubscriberService = eventSubscriberService;
        }
        public void Publish<T>(T eventObject)
        {
            //Find all subscribers:
            var subscribers = _eventSubscriberService.GetAllSubscribers<T>();
            subscribers.ForEach(subscriber => PublishToConsumer(subscriber, eventObject));
        }
        protected virtual void PublishToConsumer<T>(IHandleEvent<T> x, T eventObject)
        {
            try
            {
                x.HandleEvent(eventObject);
            }
            catch (Exception exc)
            {
            }
        }
    }
}
