﻿using Narnia.Core.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Narnia.Core.Events
{
    public class EventSubscriberService : IEventSubscriberService
    {
        public List<IHandleEvent<T>> GetAllSubscribers<T>()
        {
            return EngineContext.Current.ResolveAll<IHandleEvent<T>>().ToList();
        }
    }
}
