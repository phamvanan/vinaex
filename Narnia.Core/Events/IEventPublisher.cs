﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Core.Events
{
    public interface IEventPublisher
    {
        void Publish<T>(T eventObject);
    }
}
