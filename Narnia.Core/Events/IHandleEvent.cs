﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Core.Events
{
    public interface IHandleEvent<T>
    {
        void HandleEvent(T eventObject);
    }
}
