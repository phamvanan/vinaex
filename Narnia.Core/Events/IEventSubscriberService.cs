﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Core.Events
{
    public interface IEventSubscriberService
    {
        List<IHandleEvent<T>> GetAllSubscribers<T>();
    }
}
