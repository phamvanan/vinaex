﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Narnia.Core.Infrastructure;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

namespace Narnia.Core
{
    public class CommonHelper
    {
        public static string EnsureNotNull(string str)
        {
            return str ?? string.Empty;
        }
        public static string EnsureMaximumLength(string str, int maxLength, string postfix = null)
        {
            if (string.IsNullOrEmpty(str))
                return str;

            if (str.Length <= maxLength)
                return str;

            var pLen = postfix?.Length ?? 0;

            var result = str.Substring(0, maxLength - pLen);
            if (!string.IsNullOrEmpty(postfix))
            {
                result += postfix;
            }

            return result;
        }
        public static string GetBaseUrl()
        {
            var request = EngineContext.Current.Resolve<IHttpContextAccessor>().HttpContext.Request;
            var host = request.Host.ToUriComponent();
            var pathBase = request.PathBase.ToUriComponent();
            return $"{request.Scheme}://{host}{pathBase}";
        }
        public static Uri GetBaseUri()
        {
            var request = EngineContext.Current.Resolve<IHttpContextAccessor>().HttpContext.Request;
            var host = request.Host.ToUriComponent();
            var pathBase = request.PathBase.ToUriComponent();
            return new Uri($"{request.Scheme}://{host}{pathBase}");
        }
        public static string GetRootPath()
        {
            var hostingEnvironment = EngineContext.Current.Resolve<IHostingEnvironment>();
            var path = hostingEnvironment.ContentRootPath ?? string.Empty;
            if (File.Exists(path))
                path = Path.GetDirectoryName(path);

            return path;
        }
        public static string ConvertDateTimeToISO8601String(DateTime datetime)
        {
            return datetime.ToString("o");
        }
        public static decimal GenerateRandomDecimal(decimal from, decimal to)
        {
            Random rnd = new Random();
            byte fromScale = new System.Data.SqlTypes.SqlDecimal(from).Scale;
            byte toScale = new System.Data.SqlTypes.SqlDecimal(to).Scale;

            byte scale = (byte)(fromScale + toScale);
            if (scale > 28)
                scale = 28;

            decimal r = new decimal(rnd.Next(), rnd.Next(), rnd.Next(), false, scale);
            if (Math.Sign(from) == Math.Sign(to) || from == 0 || to == 0)
                return decimal.Remainder(r, to - from) + from;

            bool getFromNegativeRange = (double)from + rnd.NextDouble() * ((double)to - (double)from) < 0;
            return getFromNegativeRange ? decimal.Remainder(r, -from) + from : decimal.Remainder(r, to);
        }
        public static int GenerateRandomInt(int from, int to)
        {
            if (from > to)
            {
                var temp = from;
                from = to;
                to = temp;
            }
            return new Random().Next(from, to + 1);
        }
        
        public static string ReplaceRelativeUrlByAbsoluteUrl(string originalHtml, Uri baseUri)
        {
            var pattern = @"(?<name>src|href)='(?<value>/[^']*)'|(?<name>src|href)=""(?<value>/[^""]*)""";
            
            var matchEvaluator = new MatchEvaluator(
                  match =>
                  {
                      var value = match.Groups["value"].Value;
                      Uri uri;

                      if (Uri.TryCreate(baseUri, value, out uri))
                      {
                          var name = match.Groups["name"].Value;
                          return string.Format("{0}=\"{1}\"", name, uri.AbsoluteUri);
                      }

                      return null;
                  });
            var adjustedHtml = Regex.Replace(originalHtml, pattern, matchEvaluator);
            return adjustedHtml;
        }
        public static object GetPrivateFieldValue(object target, string fieldName)
        {
            if (target == null)
            {
                throw new ArgumentNullException("target", "The assignment target cannot be null.");
            }

            if (string.IsNullOrEmpty(fieldName))
            {
                throw new ArgumentException("fieldName", "The field name cannot be null or empty.");
            }

            var t = target.GetType();
            FieldInfo fi = null;

            while (t != null)
            {
                fi = t.GetField(fieldName, BindingFlags.Instance | BindingFlags.NonPublic);

                if (fi != null) break;

                t = t.BaseType;
            }

            if (fi == null)
            {
                throw new Exception($"Field '{fieldName}' not found in type hierarchy.");
            }

            return fi.GetValue(target);
        }
        public static int GenerateRandomInteger(int min = 0, int max = int.MaxValue)
        {
            var randomNumberBuffer = new byte[10];
            new RNGCryptoServiceProvider().GetBytes(randomNumberBuffer);
            return new Random(BitConverter.ToInt32(randomNumberBuffer, 0)).Next(min, max);
        }
        public static string ConvertEnum(string str)
        {
            if (string.IsNullOrEmpty(str)) return string.Empty;
            var result = string.Empty;
            foreach (var c in str)
                if (c.ToString() != c.ToString().ToLower())
                    result += " " + c.ToString();
                else
                    result += c.ToString();

            //ensure no spaces (e.g. when the first letter is upper case)
            result = result.TrimStart();
            return result;
        }
    }
}
