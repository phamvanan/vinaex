﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Narnia.Core.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Narnia.Core
{
    public interface ISiteSetting
    {
        void MapSetings(IList<Setting> settings);


        int TotalUsersOnline { get; set; }
        decimal WithdrawalFee { get; set; }
        string SystemWallet { get; set; }
        string SystemWalletPassword { get; set; }
        decimal FeePercentage { get; set; }
        int PaymentWaitingTime { get; set; }

        double MinNumberOfBTC { get; set; }
        double MaxNumberOfBTC { get; set; }
        double MinNumberOfBCH { get; set; }
        double MaxNumberOfBCH { get; set; }
        double MinNumberOfETH { get; set; }
        double MaxNumberOfETH { get; set; }
        double MinNumberOfUSDT { get; set; }
        double MaxNumberOfUSDT { get; set; }
        //List<string> SupportedSymbols { get; set; }
        Uri BaseUri { get; set; }
        AppSettings AppSettings { get; set; }
        MediaSettings MediaSettings { get; set; }
        string USDTContractAddress { get; set; }
        string BTCWalletPath { get; set; }
        decimal ReferralAmount { get; set; }
        decimal VNDReserves { get; set; }
        decimal PMReserves { get; set; }
        decimal BTCReserves { get; set; }
        decimal USDTReserves { get; set; }
        decimal ETHReserves { get; set; }
    }

    public class SiteSetting : ISiteSetting
    {
        private IList<Setting> _allSettings;
        public SiteSetting(IOptions<AppSettings> appSettings)
        {
            AppSettings = appSettings.Value;
            BaseUri = new Uri(appSettings.Value.BaseUrl);

            MediaSettings = new MediaSettings();
        }
        public AppSettings AppSettings { get; set; }
        public MediaSettings MediaSettings { get; set; }
        public int TotalUsersOnline { get; set; }
        public decimal WithdrawalFee { get; set; }
        public string SystemWallet { get; set; }
        public string SystemWalletPassword { get; set; }
        public decimal FeePercentage { get; set; }
        public int PaymentWaitingTime { get; set; }
        public double MinNumberOfBTC { get; set; }
        public double MaxNumberOfBTC { get; set; }
        public double MinNumberOfBCH { get; set; }
        public double MaxNumberOfBCH { get; set; }
        public double MinNumberOfETH { get; set; }
        public double MaxNumberOfETH { get; set; }
        public double MinNumberOfUSDT { get; set; }
        public double MaxNumberOfUSDT { get; set; }
        public string USDTContractAddress { get; set; }
        public List<string> SupportedSymbols { get; set; }
        public string BTCWalletPath { get; set; }
        public Uri BaseUri { get; set; }
        public decimal ReferralAmount { get; set; }
        public decimal VNDReserves { get; set; }
        public decimal PMReserves { get; set; }
        public decimal BTCReserves { get; set; }
        public decimal USDTReserves { get; set; }
        public decimal ETHReserves { get; set; }

        private Setting GetSettingByName(string settingName)
        {
            foreach (var setting in _allSettings)
            {
                if (string.Equals(setting.Name, settingName, StringComparison.InvariantCultureIgnoreCase))
                    return setting;
            }
            return null;
        }
        private T GetSettingValue<T>(string settingName)
        {
            var setting = GetSettingByName(settingName);
            if (setting != null)
            {
                TypeConverter tc = TypeDescriptor.GetConverter(typeof(T));
                return (T)tc.ConvertFrom(setting.Value);
            }
            return default(T);
        }
        private string GetSettingRawValue(string settingName)
        {
            var setting = GetSettingByName(settingName);
            if (setting != null)
            {
                return setting.Value;
            }
            return null;
        }
        protected void ParseSettings()
        {
            TotalUsersOnline = GetSettingValue<int>("TotalUsersOnline");
            WithdrawalFee = GetSettingValue<decimal>("WithdrawalFee");
            SystemWallet = GetSettingRawValue("SystemWallet");
            SystemWalletPassword = GetSettingRawValue("SystemWalletPassword");
            PaymentWaitingTime = GetSettingValue<int>("PaymentWaitingTime");
            MinNumberOfBTC = GetSettingValue<double>("MinNumberOfBTC");
            MaxNumberOfBTC = GetSettingValue<double>("MaxNumberOfBTC");
            MinNumberOfBCH = GetSettingValue<double>("MinNumberOfBCH");
            MaxNumberOfBCH = GetSettingValue<double>("MaxNumberOfBCH");
            MinNumberOfETH = GetSettingValue<double>("MinNumberOfETH");
            MaxNumberOfETH = GetSettingValue<double>("MaxNumberOfETH");
            MinNumberOfUSDT = GetSettingValue<double>("MinNumberOfUSDT");
            MaxNumberOfUSDT = GetSettingValue<double>("MaxNumberOfUSDT");
            USDTContractAddress = GetSettingValue<string>("USDTContractAddress");
            FeePercentage = GetSettingValue<decimal>("FeePercentage");
            BTCWalletPath = GetSettingValue<string>("BTCWalletPath");
            ReferralAmount = GetSettingValue<decimal>("ReferralAmount");
            VNDReserves = GetSettingValue<decimal>("VNDReserves");
            PMReserves = GetSettingValue<decimal>("PMReserves");
            BTCReserves = GetSettingValue<decimal>("BTCReserves");
            USDTReserves = GetSettingValue<decimal>("USDTReserves");
            ETHReserves = GetSettingValue<decimal>("ETHReserves");
        }
        public void MapSetings(IList<Setting> settings)
        {
            this._allSettings = settings;
            ParseSettings();
        }
    }

    public class AppSettings
    {
        public string BaseUrl { get; set; }
        public string CoinApiKey { get; set; }
        public string TradeExchangeServer { get; set; }
        public int PushPort { get; set; }
        public int SubscriberPort { get; set; }
    }
    public class MediaSettings
    {
        public int AvatarPictureSize { get; set; }
        public int MaximumImageSize { get; set; }
        public int DefaultImageQuality { get; set; }
    }
}
