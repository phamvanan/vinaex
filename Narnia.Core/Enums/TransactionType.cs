﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Narnia.Core.Enums
{
    public enum TransactionType
    {
        //Deposit = 1,
        //Withdraw = 2,
        //WinLoss = 3,
        //Commission = 4
        BuyAds = 1,

        SellAds = 2,
        Deposit = 3,
        Withdraw = 4
    }

    public enum TransactionStatusClient : int
    {
        [Display(Name = "Giao dịch đang chờ xử lý")]
        Pending = 1,

        [Display(Name = "Giao dịch bị hủy")]
        Cancel = 2,

        [Display(Name = "Giao dịch đã hoàn thành")]
        Complete = 3
    }

    public enum TransactionStautus
    {
        [Display(Name = "Mới")]
        Pending = 1,

        [Display(Name = "Thành công")]
        Success = 2,

        [Display(Name = "Từ chối")]
        Reject = 3,

        [Display(Name = "Chờ xác nhận")]
        HasEvidence = 4,

        [Display(Name = "Hủy")]
        Cancel = 5
    }

    public enum TransactionMode
    {
        [Display(Name = "Mua")]
        Buy = 1,

        [Display(Name = "Bán")]
        Sell = 2
    }

    public enum TransactionWalletType
    {
        [Display(Name = "Tất cả")]
        None = 0,

        [Display(Name = "Nạp tiền")]
        Deposit = 1,

        [Display(Name = "Rút tiền")]
        Withdrawal = 2
    }

    public enum PaymentType
    {
        [Display(Name = "Chuyển tiền")]
        Transfer = 1,

        [Display(Name = "Giao dịch nhanh")]
        Inside = 2
    }

    public enum TransactionStatusMode
    {
        [Display(Name = "Giao dịch đang mở")]
        Open = 1,

        [Display(Name = "Giao dịch đã đóng")]
        Close = 2
    }

    public enum TransferType
    {
        [Display(Name = "AccountUser")]
        User = 1,

        [Display(Name = "AccountAdmin")]
        Admin = 2
    }

    public enum ExchangeOfferStatus : int
    {
        [Display(Name = "Giao dịch đang chờ xử lý")]
        Pending = 1,

        [Display(Name = "Giao dịch bị hủy")]
        Cancel = 2,

        [Display(Name = "Giao dịch đã hoàn thành")]
        Complete = 3
    }

    public enum ExchangeOfferType
    {
        [Description("giao dich mua")]
        Buy = 1,
        [Description("giao dich ban")]
        Sell = 2,
        [Description("giao dich trao doi")]
        Exchange = 3
    }
}