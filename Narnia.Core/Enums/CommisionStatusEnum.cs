﻿namespace Narnia.Core.Enums
{
    public enum CommisionStatusEnum : int
    {
        NotPaid = 0,
        Paid = 1
    }
}