﻿namespace Narnia.Core.Enums
{
    public enum AdvertisementStatus : int
    {
        New = 1,
        Done = 2
    }
}