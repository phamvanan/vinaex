﻿namespace Narnia.Core.Enums
{
    public enum SendEmailType
    {
        NewTransactionForAdsBuy,
        NewTransactionForAdsSell,
        TransactionCancel,
        TransactionUploadEvidence,
        TransactionConfirm
    }
}