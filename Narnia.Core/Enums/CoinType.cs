﻿using System.ComponentModel.DataAnnotations;

namespace Narnia.Core.Enums
{
    public enum CoinTypeEnum
    {
        [Display(Name = "Bitcoin")]
        BTC = 1,
        [Display(Name = "Nethereum")]
        ETH = 2,
        [Display(Name = "Tether")]
        USDT = 3,
        [Display(Name = "Bitcoin Cash")]
        BCH = 4,
        [Display(Name = "VND")]
        VND = 5
    }
}
