﻿namespace Narnia.Core.Enums
{
    public enum RefType
    {
        BuyAds = 1,
        SellAds = 2,
        Transaction = 3,
        TransactionWallet = 4
    }
}