﻿using System.ComponentModel.DataAnnotations;

namespace Narnia.Core.Enums
{
    public enum AssetTypeEnum
    {
        [Display(Name = "Bitcoin")]
        BTC,

        [Display(Name = "Ethereum")]
        ETH,

        [Display(Name = "Perfect Money")]
        PM,

        [Display(Name = "Tether")]
        USDT,

        [Display(Name = "Netherum")]
        TETH,

        [Display(Name = "Dollar")]
        USD,

        [Display(Name = "Tien VN")]
        VND
    }
}