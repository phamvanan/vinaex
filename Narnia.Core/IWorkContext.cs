﻿using Narnia.Core.Domain;
using Narnia.Core.Domain.Localization;
using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Core
{
    public interface IWorkContext
    {
        User CurrentUser { get; }
        Language WorkingLanguage { get; set; }

        CoinSymbol WorkingSymbol { get; set; }
    }
}
