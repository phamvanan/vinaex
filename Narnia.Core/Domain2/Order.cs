﻿using Narnia.Core.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Core.Domain2
{
    public class Order:BaseEntity
    {
        public int UserId { get; set; }
        public User User { get; set; }

        public int OrderTypeId { get; set; }
        public OrderType OrderType { get; set; }

        public int OrderStatusId { get; set; }
        public OrderStatus OrderStatus { get; set; }

        public int OriginalOrderId { get; set; }

        public decimal Quantity { get; set; }

        public decimal Price { get; set; }
        public bool IsBuyOrder { get; set; }
        public bool IsMarketOrder { get; set; }
        public int TakeProfitOrderId { get; set; }
        public Order TakeProfitOrder { get; set; }
        public int StopLossOrderId { get; set; }
        public Order StopLossOrder { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public DateTime? ExpiredOn { get; set; }
        public int OfferAssetTypeId { get; set; }
        public int PaidAssetTypeId { get; set; }

        public Order ShallowCopy()
        {
            return (Order)this.MemberwiseClone();
        }
    }
}
