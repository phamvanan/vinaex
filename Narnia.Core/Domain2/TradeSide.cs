﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Core.Domain2
{
    public enum TradeSide
    {
        NotSet = 0,
        Buy = 1,
        Sell = 2
    }
}
