﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Core.Domain2
{
    public enum OrderType
    {
        NotSet = 0,
        MarketOrder = 1,
        LimitOrder = 2,
        StopLossOrder = 3
    }
}
