﻿using MicroOrm.Pocos.SqlGenerator.Attributes;
using Narnia.Core.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Core.Domain2
{
    public class Offer:BaseEntity
    {
        public int UserId { get; set; }
        public int SymbolId { get; set; }

        public TradeSide Side { get; set; }
        public OfferStatus Status { get; set; }
        public decimal Quantity { get; set; }
        public decimal Price { get; set; }
        public decimal? MaxPrice { get; set; }
        public decimal? MinPrice { get; set; }
        public int CountryId { get; set; }

        public DateTime CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public DateTime? ExpiredDate { get; set; }
        public bool Approved { get; set; }
        public decimal ProcessedQuantity { get; set; }
        [NonStored()]
        public decimal AvailableQuantity
        {
            get
            {
                return this.Quantity - this.ProcessedQuantity;
            }
        }
    }
}
