﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Core.Domain2
{
    public class Transaction
    {
        public int TransactionId { get; set; }
        public int SellOrderId { get; set; }
        public int BuyOrderId { get; set; }

        public int SymbolId { get; set; }
        public decimal Amount { get; set; }
        public DateTime CreatedDate { get; set; }

        public int Status { get; set; }
        public string Description { get; set; }
        public int TransactionType { get; set; }
    }
}
