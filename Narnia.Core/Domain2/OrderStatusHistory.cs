﻿using Narnia.Core.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Core.Domain2
{
    public class OrderStatusHistory:BaseEntity
    {
        public int OrderId { get; set; }
        public int OrderStatusId { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public int UpdatedBy { get; set; }
    }
}
