﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Core.Domain2
{
    public enum OrderStatus
    {
        Pending = 1,
        Canceled = 2,
        Suspended = 3,
        Success = 4
    }
}
