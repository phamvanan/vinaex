﻿using DryIoc;
using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Core.Infrastructure
{
    public abstract class DryIocModule
    {
        protected abstract void Load(IRegistrator builder);
        public void Configure(IRegistrator builder)
        {
            Load(builder);
        }
    }
}
