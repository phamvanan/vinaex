﻿using Microsoft.AspNetCore.Identity;
using Narnia.Core.Domain;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Narnia.Core.Infrastructure.DapperIdentity
{
    public class ApplicationClaimsPrincipalFactory : IUserClaimsPrincipalFactory<User>
    {
        public Task<ClaimsPrincipal> CreateAsync(User user)
        {
            return Task.FromResult<ClaimsPrincipal>(new ClaimsPrincipal());
        }
    }
}
