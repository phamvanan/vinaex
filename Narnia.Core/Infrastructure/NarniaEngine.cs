﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DryIoc;
using DryIoc.Microsoft.DependencyInjection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Narnia.Core.Domain;
using Narnia.Exchange.ClientProxy;

namespace Narnia.Core.Infrastructure
{
    public class NarniaEngine : IEngine
    {
        private IServiceProvider _serviceProvider { get; set; }
        public virtual IServiceProvider ServiceProvider => _serviceProvider;
        public ISiteSetting SiteSetting { get { return this.Resolve<ISiteSetting>(); } }
        public void ConfigureRequestPipeline(IApplicationBuilder application)
        {
            
        }

        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            
             _serviceProvider = new Container(rules => rules.WithoutThrowIfDependencyHasShorterReuseLifespan()).WithDependencyInjectionAdapter(services).ConfigureServiceProvider<CompositionRoot>();
            return _serviceProvider;
        }

        public void Initialize(IServiceCollection services)
        {
           
        }
        public void InitDbSetting(IList<Setting> allSettings)
        {
            SiteSetting.MapSetings(allSettings);
        }
        public void StartOrderbook()
        {
            var settings = Resolve<ISiteSetting>();
            var client = Resolve<IClient>();
            client.Start(1,settings.AppSettings.TradeExchangeServer, settings.AppSettings.PushPort, settings.AppSettings.SubscriberPort);
        }
        protected IServiceProvider GetServiceProvider()
        {
            var accessor = ServiceProvider.GetService<IHttpContextAccessor>();
            var context = accessor.HttpContext;
            if (context != null && context.RequestServices != null)
            {
                return context.RequestServices;
                //return context.RequestServices.CreateScope().ServiceProvider;
            }
            else
            {
                return ServiceProvider.CreateScope().ServiceProvider;
            }
            //return context != null ? context.RequestServices : ServiceProvider;

            //return _serviceProvider.CreateScope().ServiceProvider;
        }
        public T Resolve<T>() where T : class
        {
            return (T)GetServiceProvider().GetRequiredService(typeof(T));
        }

        public object Resolve(Type type)
        {
            return GetServiceProvider().GetRequiredService(type);
        }

        public IEnumerable<T> ResolveAll<T>()
        {
            return (IEnumerable<T>)GetServiceProvider().GetServices(typeof(T));
        }

        public object ResolveUnregistered(Type type)
        {
            Exception innerException = null;
            foreach (var constructor in type.GetConstructors())
            {
                try
                {
                    //try to resolve constructor parameters
                    var parameters = constructor.GetParameters().Select(parameter =>
                    {
                        var service = Resolve(parameter.ParameterType);
                        if (service == null)
                            throw new Exception("Unknown dependency");
                        return service;
                    });

                    //all is ok, so create instance
                    return Activator.CreateInstance(type, parameters.ToArray());
                }
                catch (Exception ex)
                {
                    innerException = ex;
                }
            }
            throw new Exception("No constructor was found that had all the dependencies satisfied.", innerException);
        }
    }
}
