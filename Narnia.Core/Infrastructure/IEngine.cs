﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Narnia.Core.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Core.Infrastructure
{
    public interface IEngine
    {
        void Initialize(IServiceCollection services);

        IServiceProvider ConfigureServices(IServiceCollection services);

        void ConfigureRequestPipeline(IApplicationBuilder application);


        #region Resolve services
        T Resolve<T>() where T : class;

        object Resolve(Type type);

        IEnumerable<T> ResolveAll<T>();

        object ResolveUnregistered(Type type);
        void InitDbSetting(IList<Setting> allSettings);
        void StartOrderbook();
        #endregion
    }
}
