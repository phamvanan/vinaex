﻿using DryIoc;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyModel;
using Narnia.Core.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Narnia.Core.Infrastructure
{
    public class CompositionRoot
    {
        private readonly string[] frameworkLibs = new string[] { "Narnia.Web.Framework" };
        public CompositionRoot(IContainer container)
        {
            var env = container.Resolve<IHostingEnvironment>();
            var rootPath = env.ContentRootPath;

            var assemblies = GetAllCoreLibraries();
            container.RegisterMany(AppDomain.CurrentDomain.GetAssemblies(), serviceTypeCondition: type => type == typeof(DryIocModule));
            foreach (var module in container.ResolveMany<DryIocModule>())
                module.Configure(container);

            container.RegisterMany(AppDomain.CurrentDomain.GetAssemblies(), serviceTypeCondition: type => {
                return type == typeof(IHandleEvent<>) && type.IsGenericType && type.IsAssignableFrom(type.GetGenericTypeDefinition());
                }, reuse: Reuse.Transient);

            container.Register<IEventPublisher, EventPublisher>(Reuse.Singleton);
            container.Register<IEventSubscriberService, EventSubscriberService>(Reuse.Singleton);
        }

        private IEnumerable<Assembly> GetAllCoreLibraries()
        {
            var assemblies = new List<Assembly>();
            var runtimeLibraries = DependencyContext.Default.RuntimeLibraries;
            foreach (var library in runtimeLibraries)
            {
                if (IsCandidateLibrary(library))
                {
                    var assembly = Assembly.Load(new AssemblyName(library.Name));
                    assemblies.Add(assembly);
                }
            }
            return assemblies;
        }
        private bool IsCandidateLibrary(RuntimeLibrary library)
        {
            return frameworkLibs.Contains(library.Name);
        }
    }
}
