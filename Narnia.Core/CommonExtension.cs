﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

namespace Narnia.Core
{
    public static class CommonExtension
    {
        public static string EnsureNotNull(this string src)
        {
            return src ?? string.Empty;
        }
        public static bool IsValidEmail(this string src)
        {
            if (string.IsNullOrEmpty(src))
                return false;

            return Regex.IsMatch(src.Trim(), @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
        }

        public static int ConvertToInt32(this string src, int defaultValue = 0)
        {
            int value;
            bool isNumeric = int.TryParse(src, out value);
            if (isNumeric)
                return value;

            return defaultValue;
        }

        public static int ConvertToInt32(this object src, int defaultValue = 0)
        {
            int value;
            bool isNumeric = int.TryParse(src.ToString(), out value);
            if (isNumeric)
                return value;

            return defaultValue;
        }

        public static double ConvertToDouble(this string src, double defaultValue = 0.0)
        {
            double value;
            bool isNumeric = double.TryParse(src, NumberStyles.Any, CultureInfo.InvariantCulture, out value);
            if (isNumeric)
                return value;

            return defaultValue;
        }

        public static double ConvertToDouble(this object src, double defaultValue = 0.0)
        {
            double value;
            bool isNumeric = double.TryParse(src.ToString(), out value);
            if (isNumeric)
                return value;

            return defaultValue;
        }

        public static void Set<T>(this ISession session, string key, T value)
        {
            session.SetString(key, JsonConvert.SerializeObject(value));
        }

        public static T Get<T>(this ISession session, string key)
        {
            var value = session.GetString(key);
            return value == null ? default(T) : JsonConvert.DeserializeObject<T>(value);
        }

        public static string GetDisplayName(this Enum enumValue)
        {
            return enumValue.GetType()?
                .GetMember(enumValue.ToString())?
                .First()?
                .GetCustomAttribute<DisplayAttribute>()?
                .Name;
        }
    }
}
