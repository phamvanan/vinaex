﻿namespace Narnia.Core.Models
{
    public class TransactionWallletDetailAdminModel : TransactionWalletAdminSearchModel
    {
        public int EvidenceId { get; set; }
        public string AccountName { get; set; }
        public  string AccountNo { get; set; }
        public string Branch { get; set; }
        public string BankName { get; set; }
        public string WalletAddress { get; set; }
        public  string EvidenceCode { get; set; }
        public  string OrderNo { get; set; }
        //public string AssetIdBase { get; set; }
    }
}