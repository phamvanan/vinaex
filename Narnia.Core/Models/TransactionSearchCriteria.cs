﻿using System;

namespace Narnia.Core.Models
{
    public class TransactionSearchCriteria
    {
        public int TransMode { get; set; }
        public int TransactionType { get; set; }
        public string AssetTypeId { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int Status { get; set; }
        public string TransactionCode { get; set; }
    }
}