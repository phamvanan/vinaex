﻿using System;

namespace Narnia.Core.Models
{
    public class BankAccountAdminPagingModel
    {
        public string BankName { get; set; }
        public string AccountName { get; set; }
        public string AccountNo { get; set; }
        public string Branch { get; set; }
        public DateTime CreatedDate { get; set; }
        public bool IsActived { get; set; }
        public int Id { get; set; }
    }
}