﻿using System;
using Narnia.Core.Enums;

namespace Narnia.Core.Models
{
    public class TransactionWalletPagingModel
    {
        public string TransactionId { get; set; }
        public string TransactionCode { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedDateDisplay
        {
            get { return CreatedDate.ToString("dd/MM/yyyy hh:mm:ss"); }
        }
        public decimal Amount { get; set; }
        public int Status { get; set; }

        public string StatusDisplay
        {
            get
            {
                switch (Status)
                {
                    case (int)TransactionStautus.Cancel:
                        return TransactionStautus.Cancel.GetDisplayName();
                    case (int)TransactionStautus.HasEvidence:
                        return TransactionStautus.HasEvidence.GetDisplayName();
                    case (int)TransactionStautus.Pending:
                        return TransactionStautus.Pending.GetDisplayName();
                    case (int)TransactionStautus.Reject:
                        return TransactionStautus.Reject.GetDisplayName();
                    case (int)TransactionStautus.Success:
                        return TransactionStautus.Success.GetDisplayName();
                }

                return "";
            }
        }

        public int TransactionType { get; set; }

        public string TransactionTypeDisplay
        {
            get
            {
                switch (TransactionType)
                {
                    case (int)TransactionWalletType.Deposit:
                        return Enum.GetName(typeof(TransactionWalletType), TransactionWalletType.Deposit);
                    case (int)TransactionWalletType.Withdrawal:
                        return Enum.GetName(typeof(TransactionWalletType), TransactionWalletType.Withdrawal);
                    case (int)TransactionWalletType.None:
                        return Enum.GetName(typeof(TransactionWalletType), TransactionWalletType.None);
                }

                return "";
            }
        }
    }
}