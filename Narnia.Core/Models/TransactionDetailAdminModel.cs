﻿namespace Narnia.Core.Models
{
    public class TransactionDetailAdminModel : TransactionAdminSearchModel
    {
        public int EvidenceId { get; set; }
        public string EvidenceCode { get; set; }
    }
}