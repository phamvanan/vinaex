﻿using System;
using System.Collections.Generic;
using System.Text;
using Narnia.Core.Enums;

namespace Narnia.Core.Models
{
    public class TransactionHistorySearchModel : BaseSearchModel
    {
        public TransactionHistorySearchModel()
        {
        }

        public string AssetTypeId { get; set; }
        public TransactionWalletType TransactionType { get; set; }
        public int UserId { get; set; }
    }
}
