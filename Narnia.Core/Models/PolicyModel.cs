﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Core.Models
{
    public class PolicyModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Content { get; set; }
        public string Desciption { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
}
