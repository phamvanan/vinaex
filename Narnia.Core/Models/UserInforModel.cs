﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using Narnia.Core.Domain;

namespace Narnia.Core.Models
{
    public class UserInforModel
    {
        public UserInforModel()
        {
            this.TransactionStatisticModel = new List<TransactionStatisticModel>();
        }
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Alias { get; set; }
        public string IDNumber { get; set; }
        public string UrlFaceBook { get; set; }
        public string UrlTwitter { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedDateDisplay { get { return CreatedDate.ToString(); } }
        public DateTime UpdatedDate { get; set; }
        public string UpDatedDisplay { get { return UpdatedDate.ToString(); } }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        [Display(Name = "Avatar")]
        public byte[] Image { get; set; }
        public string ImageDisplay
        {
            get
            {
                if (Image == null || Image.Length == 0)
                {
                    return string.Empty;
                }
                return $"data:{ContentType};base64,{Convert.ToBase64String(Image)}";
            }
        }
        public bool HasReceivedDailyPrice { get; set; }
        public bool HasSendEmailExchangeTransaction { get; set; }
        public bool HasSendEmailNewTransaction { get; set; }
        public bool HasSendSMSNewTransaction { get; set; }
        public bool HasSendSMSTransactionPaid { get; set; }
        public string AssetTypeId { get; set; }
        public IList<TransactionStatisticModel> TransactionStatisticModel { get; set; }
        public IList<AssetBalance> AssetBalances { get; set; }
        public string ReferralLink { get; set; }
        public string ReferenceCode { get; set; }
        public string ContentType { get; set; }
        public string IPAddress { get; set; }
        public DateTime LoginDate { get; set; }
    }
}
