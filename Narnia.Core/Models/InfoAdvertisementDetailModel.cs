﻿using System;

namespace Narnia.Core.Models
{
    public class InfoAdvertisementDetailModel
    {
        public int TransactionId { get; set; }
        public decimal AmountCoin { get; set; }
        public string AssetTypeId { get; set; }
        public string UserName { get; set; }
        public string WalletAddress { get; set; }
        public DateTime CreatedDate { get; set; }
        public string PaymentMethodName { get; set; }
        public string BankName { get; set; }
        public string BankAccountNameAds { get; set; }
        public string BankAccountNumberAds { get; set; }
        public int? EvidenceId { get; set; }
        public int PaymentAllowTime { get; set; }
        public string TransactionCode { get; set; }
        public bool IsSellTransaction { get; set; }
        public string Alias { get; set; }
        public int Status { get; set; }
        public int UserIdAds { get; set; }
        public int UserIdTran { get; set; }
        public  string EvidenceCode { get; set; }
        public  int PaymentType { get; set; }
    }
}