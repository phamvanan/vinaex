﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace Narnia.Core.Models
{
    public class BankCreateModel
    {
       public int Id { get; set; }
        public string BankName { get; set; }
        public string BankCode { get; set; }
        public string Description { get; set; }
    }
}