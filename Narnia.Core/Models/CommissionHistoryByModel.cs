﻿using System;

namespace Narnia.Core.Models
{
    public class CommissionHistoryByModel
    {
        public int Id { get; set; }
        public int BeneficiaryUserId { get; set; }
        public string Alias { get; set; }
        public string UserName { get; set; }
        public int CommLevel { get; set; }
        public decimal CommRate { get; set; }
        public decimal CommAmt { get; set; }
        public DateTime CreatedDate { get; set; }
        public int RefId { get; set; }
        public int RefType { get; set; }
        public int Status { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string AssetTypeId { get; set; }
    }
}