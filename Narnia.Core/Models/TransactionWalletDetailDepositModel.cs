﻿using System;

namespace Narnia.Core.Models
{
    public class TransactionWalletDetailVNDModel
    {
        public int TransactionId { get; set; }
        public decimal Amount { get; set; }
        public string TransactionCode { get; set; }
        public string Branch { get; set; }
        public string BankName { get; set; }
        public string AccountNo { get; set; }
        public string AccountName { get; set; }
        public int Status { get; set; }
        public int? EvidenceId { get; set; }
        public int UserId { get; set; }
        public int TransactionType { get; set; }
        public DateTime? ConfirmDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }
}