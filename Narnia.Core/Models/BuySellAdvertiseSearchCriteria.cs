﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Core.Models
{
    public class BuySellAdvertiseSearchCriteria
    {
        public string BuyUserName { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public byte Status { get; set; }
        public byte CoinType { get; set; }      
        public string TransactionCode { get; set; }
    }
}
