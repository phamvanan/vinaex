﻿
namespace Narnia.Core.Models
{
    public class SearchUserModel : BaseSearchModel
    {
        public string UserName { get; set; }
        public string Email { get; set; }
    }
}
