﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Core.Models
{
    public class InfoQuickBuySellAdvertisement
    {
        public long Id { get; set; }
        public string UserName { get; set; }
        public string BankName { get; set; }
        public decimal Amount { get; set; }
        public decimal ExchangeRate { get; set; }        
    }
}
