﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Narnia.Core.Models
{
    public class WithdrawViewModel
    {
        public WithdrawViewModel()
        {
            this.Balance = 0;
            this.CoinCode = string.Empty;
            this.CoinWalletAddress = string.Empty;
            this.UserId = 0;
            this.WithdrawAmount = 0;
        }

        public decimal Balance { get; set; }
        public string CoinWalletAddress { get; set; }
        public decimal WithdrawAmount { get; set; }

        public string CoinCode { get; set; }
        public int UserId { get; set; }
        [Description("so tien rut toi da")]
        public decimal MaximumWithdrawAmount { get; set; }
    }
}
