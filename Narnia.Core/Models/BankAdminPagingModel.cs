﻿using System;

namespace Narnia.Core.Models
{
    public class BankAdminPagingModel
    {
        public int id { get; set; }
        public string BankName { get; set; }
        public string BankCode { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}