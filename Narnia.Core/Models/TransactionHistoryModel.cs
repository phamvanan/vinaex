﻿using System;

namespace Narnia.Core.Models
{
    public class TransactionHistoryModel
    {
        public TransactionHistoryModel()
        {
            this.AmountCoin = 0;
        }
        public string TransactionCode { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public decimal AmountCoin { get; set; }
        public int Status { get; set; }
        public string AssetTypeId { get; set; }
        public int TransactionMode { get; set; }
        public string UserName { get; set; }
        public string Alias { get; set; }
        public string CreatedDateDisplay
        {
            get { return CreatedDate.ToString("dd/MM/yyyy hh:mm:ss"); }
        }
        public string UpdatedDateDisplay
        {
            get
            {
                if (UpdateDate == null)
                {
                    return string.Empty;
                }

                return UpdateDate.Value.ToString("dd/MM/yyyy hh:mm:ss");
            }
        }
    }
}