﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace Narnia.Core.Models
{
    public class CoinPaymentResponse
    {
        [JsonProperty("error")]
        public string error { get; set; }
        [JsonProperty("result")]
        public object result { get; set; }
    }

    public class TransactionResponseModel
    {
        [JsonProperty("time_created")]
        public long TimeCreated { get; set; }
        [JsonProperty("time_expires")]
        public long TimeExpireds { get; set; }
        [JsonProperty("status")]
        public int Status { get; set; }
        [JsonProperty("status_text")]
        public string StatusText { get; set; }
        [JsonProperty("type")]
        public string Type { get; set; }
        [JsonProperty("coin")]
        public string Coin { get; set; }
        [JsonProperty("amount")]
        public decimal Amount { get; set; }
        [JsonProperty("amountf")]
        public decimal Amountf { get; set; }
        [JsonProperty("received")]
        public decimal Received { get; set; }
        [JsonProperty("receivedf")]
        public decimal Receivedf { get; set; }
        [JsonProperty("recv_confirms")]
        public decimal RecvConfirms { get; set; }
        [JsonProperty("payment_address")]
        public string PaymentAddress { get; set; }
    }

    public class CreateTransferModel
    {
        public int Id { get; set; }
        public string Status { get; set; }
    }
}
