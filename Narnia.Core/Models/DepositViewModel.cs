﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Core.Models
{
    public class DepositViewModel
    {
        public DepositViewModel()
        {
            this.CoinCode = string.Empty;
            this.AddressWallet = string.Empty;
        }

        public string AddressWallet { get; set; }
        public int UserId { get; set; }
        public string CoinCode { get; set; }
    }
}
