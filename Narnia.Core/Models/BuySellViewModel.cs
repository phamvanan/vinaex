﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace Narnia.Core.Models
{
    public class BuySellViewModel
    {
        public long Id { get; set; }
        public int UserId { get; set; }
        public decimal CoinPrice { get; set; }
        public decimal MinCoinPrice { get; set; }
        public decimal MaxCoinPrice { get; set; }
        public double MinCoinNumber { get; set; }
        public double MaxCoinNumber { get; set; }
        public string CurrencyId { get; set; }
        public string CurrencyName { get; set; }
        public string BankName { get; set; }
        public string CoinName { get; set; }
        public string PaymentMethodName { get; set; }
        public string UserName { get; set; }
        public double Amount { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedDateDisplay => CreatedDate.ToString(CultureInfo.InvariantCulture);
        public int PaymentMethodId { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string UpdatedDateDisplay => UpdatedDate.ToLongDateString();

    }
}
