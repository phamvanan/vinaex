﻿namespace Narnia.Core.Models
{
    public class BankAccountModel
    {
        public int bankAccountId { get; set; }
        public string AccountName { get; set; }
        public string AccountNo { get; set; }
        public string Branch { get; set; }
        public string BankName { get; set; }
        public string BankCode { get; set; }
        public int BankId { get; set; }
        public bool IsActived { get; set; }
    }
}