﻿using System;

namespace Narnia.Core.Models
{
    public class TransactionAdminSearchModel
    {
        public string TransactionCode { get; set; }
        public int TransactionId { get; set; }
        public decimal AmountCoin { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string UserName { get; set; }
        public int Status { get; set; }
        public string AssetTypeId { get; set; }
        public int PaymentAllowTime { get; set; }
        public string UserConfirm { get; set; }
        public int TotalRecords { get; set; }
    }
}