﻿using System;

namespace Narnia.Core.Models
{
    public class BaseSearchModel
    {
        public BaseSearchModel()
        {
            this.Skip = 0;
            this.Take = 20;
        }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public int Skip { get; set; }
        public int Take { get; set; }
    }
}
