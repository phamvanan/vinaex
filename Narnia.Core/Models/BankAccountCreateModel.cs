﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace Narnia.Core.Models
{
    public class BankAccountCreateModel
    {
        public BankAccountCreateModel()
        {
            banks = new List<SelectListItem>();
        }

        public int bankAccountId { get; set; }
        public string AccountName { get; set; }
        public string AccountNo { get; set; }
        public string Branch { get; set; }
        public string BankName { get; set; }
        public string BankCode { get; set; }
        public int BankId { get; set; }
        public bool IsActived { get; set; }
        public List<SelectListItem> banks { get; set; }
    }
}