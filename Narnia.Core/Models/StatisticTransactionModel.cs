﻿using Narnia.Core.Enums;

namespace Narnia.Core.Models
{
    public class TransactionStatisticModel
    {
        public decimal TotalCoin { get; set; }
        public string AssetTypeId { get; set; }
        public int UserId { get; set; }
        public int TotalTransactionSuccess { get; set; }
        public string CoinTypeDisplay
        {
            get
            {
                switch (AssetTypeId)
                {
                    case "BTC":
                        return "Bitcoin";
                    case "BCH":
                        return "Bitcoin Cash";
                    case "ETH":
                        return "Nethereum";
                    case "USDT":
                        return "Tether";
                    default:
                        return "VND";
                }
            }
        }
    }
}
