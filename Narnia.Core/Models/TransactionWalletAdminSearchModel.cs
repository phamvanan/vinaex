﻿using System;

namespace Narnia.Core.Models
{
    public class TransactionWalletAdminSearchModel
    {
        public string TransactionCode { get; set; }
        public int TransactionId { get; set; }
        public decimal Amount { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string UserName { get; set; }
        public int Status { get; set; }
        public string Unit { get; set; }
        public string UserConfirm { get; set; }
        public int TransactionType { get; set; }
        public DateTime? ConfirmDate { get; set; }
        public  DateTime? EvidenceDate { get; set; }
        
    }
}