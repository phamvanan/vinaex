﻿namespace Narnia.Core.Models
{
    public class InfoAdvertisementForSellBuyModel
    {
        public long Id { get; set; }
        public decimal MaxCoinNumber { get; set; }
        public decimal MinCoinNumber { get; set; }
        public string UserName { get; set; }
        public string PaymentMethodName { get; set; }
        public string CountryName { get; set; }
        public int PaymentAllowTime { get; set; }
        public string BankName { get; set; }
        public decimal AvgPrice { get; set; }
        public  decimal Rate { get; set; }
        public string AssetIdBase { get; set; }
        public string CurrencyName { get; set; }
        public bool IsActived { get; set; }
        public  string WalletAddressUser { get; set; }
        public string Alias { get; set; }
        public int UserId { get; set; }
    }
}