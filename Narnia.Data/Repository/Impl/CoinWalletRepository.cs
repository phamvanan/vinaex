﻿using Dapper;
using Narnia.Core.Domain;
using Narnia.Data.Base;
using Narnia.Data.Base.Repositories;
using Narnia.Data.Repository.Contracts;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Narnia.Data.Repository.Impl
{
    public class CoinWalletRepository : GenericRepository<CoinWallet>, ICoinWalletRepository
    {
        public CoinWalletRepository(IDbContext dbContext) : base(dbContext)
        {
        }

        public CoinWallet GetByAddress(string address)
        {
            return GetFirst(new { WalletAddress = address });
        }

        public CoinWallet GetbyUserId(int userId)
        {
            return GetFirst(new { UserId = userId });
        }

        public CoinWallet GetbyUserIdCointType(int userId, int coinType)
        {
            return GetFirst(new { UserId = userId, CoinType = coinType });
        }
        public IEnumerable<CoinWallet> FindWallets(int skip, int take, out int totalResultsCount, out int filteredResultsCount, bool countTotal = true,
            DateTime? createdFromUtc = null, DateTime? createdToUtc = null,
          IList<string> assetTypeIds = null, string userName = "", int? userId = null)
        {
            totalResultsCount = 0;
            filteredResultsCount = 0;
            string whereString = " where 1=1 ";

            string sAssetTypes = "";

            if(userId != null && userId.Value > 0)
            {
                whereString += $" and u.Id = {userId.Value}";
            }
            else
            {
                if (!string.IsNullOrEmpty(userName))
                {
                    whereString += $" and u.Email like CONCAT('%',@UserName,'%') ";
                }
            }
            
            if (createdFromUtc.HasValue)
            {
                whereString += " and w.CreatedDate >= @CreatedFrom ";
            }
            if (createdToUtc.HasValue)
            {
                whereString += " and w.CreatedDate <= @CreatedTo ";
            }
            if (assetTypeIds != null && assetTypeIds.Count > 0)
            {
                sAssetTypes = String.Join(",",assetTypeIds.Select(item => { return "'" + item + "'"; }));
                whereString += $" and w.AssetTypeId in ({sAssetTypes}) ";
            }

            string query = "";
            string queryTotal = "";
            string orderString = " w.Id desc ";

            query = $"select w.*,u.* from CoinWallet w inner join [User] u on w.UserId = u.Id {whereString} order by {orderString} offset {skip} rows fetch next {take} rows only option(recompile) ";
            queryTotal = $"select count(*) from CoinWallet w inner join [User] u on w.UserId = u.Id {whereString}";

            IEnumerable<CoinWallet> data = null;

            using (var conn = DbContext.OpenConnection())
            {
                DynamicParameters parameter = new DynamicParameters();

                parameter.Add("@UserName", userName, DbType.String, ParameterDirection.Input);
                parameter.Add("@CreatedFrom", createdFromUtc, DbType.DateTime, ParameterDirection.Input);
                parameter.Add("@CreatedTo", createdToUtc, DbType.DateTime, ParameterDirection.Input);

                data = conn.Query<CoinWallet,User,CoinWallet>(query,(w,u)=> { w.User = u;return w; }, parameter, null);

                totalResultsCount = conn.ExecuteScalar<int>(queryTotal, parameter);
                filteredResultsCount = totalResultsCount;
            }

            return data;
        }

        public CoinWallet GetByUserIdType(int userId, string type)
        {
            return GetFirst(new { UserId = userId, type = type });
        }
    }
}