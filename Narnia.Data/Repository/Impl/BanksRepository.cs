﻿using Narnia.Core.Domain;
using Narnia.Data.Base;
using Narnia.Data.Base.Repositories;
using Narnia.Data.Repository.Contracts;
using System.Collections.Generic;
using System.Linq;
using Narnia.Core.Models;
using Dapper;
using System.Data;

namespace Narnia.Data.Repository.Impl
{
    public class BanksRepository : GenericRepository<Banks>, IBanksRepository
    {
        public BanksRepository(IDbContext dbContext) : base(dbContext)
        {
            
        }
        public List<Banks> GetList()
        {
            return GetWhere(new {IsDeleted = false}).ToList();
        }

        public Banks GetById(int id)
        {
            return GetFirst(new { Id=id, IsDeleted = false });
        }

        public List<BankAdminPagingModel> GetBankAdminPagings(int pageIndex, int pageSize, string orderBy, out int totalRecords)
        {
            var p = new DynamicParameters();
            p.Add("SelectedPage", pageIndex);
            p.Add("PageSize", pageSize);
            p.Add("TotalRecords", dbType: DbType.Int32, direction: ParameterDirection.Output);
            p.Add("OrderBy", orderBy);

            var data = QueryStoreProc<BankAdminPagingModel>("usp_Bank_GetListSearchAdmin_Paging", p).ToList();
            totalRecords = p.Get<int>("TotalRecords");
            return data;
        }

        public Banks GetByCode(string code)
        {
            return GetFirst(new {BankCode = code, IsDeleted=false});
        }
    }
}