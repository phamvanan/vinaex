﻿using Narnia.Core.Domain;
using Narnia.Data.Repository.Contracts;
using Narnia.Data.Base.Repositories;
using Narnia.Data.Base;

namespace Narnia.Data.Repository.Impl
{
    public class EvidenceRepository:GenericRepository<Evidence>, IEvidenceRepository
    {
        public EvidenceRepository(IDbContext dbContext) : base(dbContext)
        {
        }

        public Evidence GetbyCode(string code)
        {
            return GetFirst(new {Code = code});
        }
        public Evidence GetbyId(int id)
        {
            return GetFirst(new { Id = id });
        }
    }
}
