﻿using Dapper;
using Narnia.Core.Domain;
using Narnia.Core.Models;
using Narnia.Data.Base;
using Narnia.Data.Base.Repositories;
using Narnia.Data.Repository.Contracts;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Narnia.Data.Repository.Impl
{
    public class BankAccountRepository : GenericRepository<BankAccount>, IBankAccountRepository
    {
        public BankAccountRepository(IDbContext dbContext) : base(dbContext)
        {
        }

        public List<BankAccountModel> GetListByUserId(int userId)
        {
            var p = new DynamicParameters();
            p.Add("Userid", userId);
            var data = QueryStoreProc<BankAccountModel>("usp_GetAllBankAccountByUserId", p).ToList();
            return data;
        }

        public BankAccount GetById(int id)
        {
            return GetFirst(new {Id = id, IsDeleted = false });
        }
        public BankAccount GetByUserIdAccountNo(int userId, string accountNo)
        {
            return GetFirst(new {UserId = userId, AccountNo = accountNo, IsDeleted = false});
        }

        public List<BankAccountAdminPagingModel> GetBankAccountAdminPagings(int pageIndex, int pageSize, string orderBy, out int totalRecords)
        {
            var p = new DynamicParameters();
            p.Add("SelectedPage", pageIndex);
            p.Add("PageSize", pageSize);
            p.Add("TotalRecords", dbType: DbType.Int32, direction: ParameterDirection.Output);
            p.Add("OrderBy", orderBy);

            var data = QueryStoreProc<BankAccountAdminPagingModel>("usp_BankAccount_GetListAccountAdmin_Paging", p).ToList();
            totalRecords = p.Get<int>("TotalRecords");
            return data;
        }
    }
}