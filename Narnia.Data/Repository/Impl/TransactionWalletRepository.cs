﻿

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using Narnia.Core.Domain;
using Narnia.Core.Enums;
using Narnia.Core.Models;
using Narnia.Data.Base;
using Narnia.Data.Base.Repositories;
using Narnia.Data.Repository.Contracts;

namespace Narnia.Data.Repository.Impl
{
    public class TransactionWalletRepository : GenericRepository<TransactionWallet>, ITransactionWalletRepository
    {
        public TransactionWalletRepository(IDbContext dbContext) : base(dbContext)
        {

        }

        public List<TransactionWalletPagingModel> GetTransactionWalletPaging(TransactionHistorySearchModel searchModel, out int totalRecords)
        {
            var p = new DynamicParameters();
            p.Add("UserId", searchModel.UserId);
            p.Add("AssetTypeId", new DbString() { Value = searchModel.AssetTypeId, IsAnsi = true, Length = 5 });
            p.Add("TransactionType", (int)searchModel.TransactionType);
            p.Add("Take", searchModel.Take);
            p.Add("Skip", searchModel.Skip);
            p.Add("TotalRecords", dbType: DbType.Int32, direction: ParameterDirection.Output);

            var data = QueryStoreProc<TransactionWalletPagingModel>("usp_TransactionWallet_Paging", p).ToList();
            totalRecords = p.Get<int>("TotalRecords");
            return data;
        }

        public TransactionWalletDetailVNDModel GetDetailVNDByTrancodeUserId(string transcode)
        {
            var p = new DynamicParameters();
            p.Add("transcode", new DbString() { Value = transcode, IsAnsi = true, Length = 256 });
            return QueryStoreProc<TransactionWalletDetailVNDModel>("usp_TransactionWallet_DetailVND_Get", p).FirstOrDefault();
        }

        public TransactionWallet GetByTranscode(string transcode)
        {
            return GetFirst(new { TransactionCode = transcode });
        }

        public TransactionWallletDetailAdminModel GetDetailAdmin(int transactionId)
        {
            return QueryStoreProc<TransactionWallletDetailAdminModel>($"GetTransactionWalletDetailAdmin", new { TransactionID = transactionId }).FirstOrDefault();
        }

        public TransactionWallet GetByTranId(int transactionId)
        {
            return GetFirst(new { TransactionId = transactionId });
        }

        public IEnumerable<TransactionWalletAdminSearchModel> SearchTransactions(int skip, int take, out int totalResultsCount, 
            DateTime? createdFromUtc = null, DateTime? createdToUtc = null, string transactionCode = null,
            IList<int> statuses = null, IList<string> assetTypeIds = null, string userName = "")
        {

            var p = new DynamicParameters();
            p.Add("skip", skip);
            p.Add("take", take);
            p.Add("TotalRecords", dbType: DbType.Int32, direction: ParameterDirection.Output);
            if (assetTypeIds != null && assetTypeIds.Count > 0)
            {
                var assetString = String.Join(",", assetTypeIds.Select(item => { return "'" + item + "'"; }));
                p.Add("AssetTypeId", new DbString() { Value = assetString, IsAnsi = true, Length = 200 });
            }

            if (statuses != null && statuses.Count > 0)
            {
                var statsusString = String.Join(",", statuses);
                p.Add("Status", new DbString() { Value = statsusString, IsAnsi = true, Length = 200 });
            }

            p.Add("FromDate", !createdFromUtc.HasValue || createdFromUtc.Value == DateTime.MinValue ? (DateTime?)null : createdFromUtc.Value);
            p.Add("ToDate", !createdToUtc.HasValue || createdToUtc.Value == DateTime.MinValue ? (DateTime?)null : createdToUtc.Value);
            if (!string.IsNullOrEmpty(transactionCode))
            {
                p.Add("TransactionCode", transactionCode.Trim());
            }
            if (!string.IsNullOrEmpty(userName))
            {
                p.Add("UserName", userName);
            }
            //p.Add("OrderBy", orderBy);

            var data = QueryStoreProc<TransactionWalletAdminSearchModel>("usp_SelectPaging_TransactionWalletAdminSearch", p).ToList();
            totalResultsCount = p.Get<int>("TotalRecords");
            return data;
        }
    }
}