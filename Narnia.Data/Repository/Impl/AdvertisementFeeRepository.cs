﻿using Microsoft.AspNetCore.Diagnostics;
using Narnia.Core.Domain;
using Narnia.Core.Enums;
using Narnia.Data.Repository.Contracts;
using Narnia.Data.Base.Repositories;
using Narnia.Data.Base;

namespace Narnia.Data.Repository.Impl
{
    public class AdvertisementFeeRepository:GenericRepository<AdvertisementFee> ,IAdvertisementFeeRepository
    {
        public AdvertisementFeeRepository(IDbContext dbContext) : base(dbContext)
        {
        }

        public AdvertisementFee GetBySellAdtId(int sellAdtId)
        {
            if (sellAdtId <= 0) return null;

            return GetFirst(new { SellAdvertisementId=sellAdtId, Status = (int)AdvertisementFeeStatus.Actived });
        }

        public AdvertisementFee GetByBuyAdtId(int buyAdtId)
        {
            if (buyAdtId <= 0) return null;

            return GetFirst(new { BuyAdvertisementId = buyAdtId,Status = (int)AdvertisementFeeStatus.Actived });
        }

        public AdvertisementFee GetByBuyTransactionId(int transactionId)
        {
            if (transactionId <= 0) return null;

            return GetFirst(new { TransactionId = transactionId, Status = (int)AdvertisementFeeStatus.Actived });
        }

        public AdvertisementFee GetAmountFreezeByUserIdCoinType(int userId, string assetTypeId)
        {
            return GetFirst(new {UserId = userId, Status = AdvertisementFeeStatus.Actived, AssetTypeId = assetTypeId });
        }

        public AdvertisementFee GetByUserIdAssetTypeTranId(int userId, string assetType,int transId)
        {
            return GetFirst(new { UserId = userId, Status = AdvertisementFeeStatus.Actived, AssetTypeId = assetType, TransactionId=transId });
        }
    }
}
