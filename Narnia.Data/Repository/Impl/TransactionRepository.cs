﻿using Dapper;
using Narnia.Core.Domain;
using Narnia.Core.Models;
using Narnia.Data.Repository.Contracts;
using Narnia.Data.Base.Repositories;
using Narnia.Data.Base;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using System;
using Narnia.Core.Enums;

namespace Narnia.Data.Repository.Impl
{
    public class TransactionRepository : GenericRepository<Transaction>, ITransactionRepository
    {
        public TransactionRepository(IDbContext dbContext) : base(dbContext)
        {
        }

        //public override Transaction GetById<T1>(T1 id)
        //{
        //    using (var conn = DbContext.OpenConnection())
        //    {
        //        var query = @"select t.*,u.Id,u.Email,u.UserName,buy.*,u1.Id,u1.Email,u1.UserName, sell.*,u2.Id,u2.Email,u2.UserName from 
        //                    [Transaction] t left join [User] u on t.UserId = u.Id
        //                    left join [BuyAdvertisement] buy on t.BuyAdvertisementId = buy.Id
        //                    left join [User] u1 on buy.UserId = u1.Id
        //                    left join [SellAdvertisement] sell on t.SellAdvertisementId = sell.Id
        //                    left join [User] u2 on sell.UserId = u2.Id
        //                    where t.TransactionId = @TransactionId";

        //        DynamicParameters parameter = new DynamicParameters();

        //        parameter.Add("@TransactionId", id, DbType.Int32, ParameterDirection.Input);

        //        var data = conn.Query<Transaction, User, BuyAdvertisement, User, SellAdvertisement, User, Transaction>(query, (t, u, buyad, buyer, sellad, seller) =>
        //        {
        //            if (sellad != null)
        //                sellad.AdvertiseUser = seller;
        //            if (buyad != null)
        //                buyad.AdvertiseUser = buyer;
        //            t.User = u;
        //            t.BuyAdvertisement = buyad;
        //            t.SellAdvertisement = sellad;

        //            return t;
        //        }, parameter, null).FirstOrDefault();

        //        return data;
        //    }
        //}

        public InfoAdvertisementDetailModel GetInfoDetailTransaction(int transId)
        {
            var p = new DynamicParameters();
            p.Add("TransId", transId);
            return QueryStoreProc<InfoAdvertisementDetailModel>("usp_GetInfoDetailTransactionByUserIdTransId", p).FirstOrDefault();
        }

        public InfoAdvertisementDetailModel GetInfoDetailTransactionByCode(string transCode)
        {
            var p = new DynamicParameters();
            p.Add("transCode", new DbString() { Value = transCode, IsAnsi = true, Length = 256 });
            return QueryStoreProc<InfoAdvertisementDetailModel>("usp_GetInfoDetailTransactionByUserIdTransCode", p).FirstOrDefault();
        }

        public IList<TransactionStatisticModel> GetStatistic(int userId)
        {
            var data = QueryStoreProc<TransactionStatisticModel>("usp_GetStatisticTransaction" , new { UserId = userId }).ToList();

            return data;
        }

        public List<TransactionAdminSearchModel> SearchAdmin(int pageIndex,int pageSize, int tranMode, string AssetTypeId, int Status, string orderBy,
                DateTime? FromDate, DateTime? ToDate,string transactionCode ,out int totalRecords)
        {
            var p= new DynamicParameters();
            p.Add("SelectedPage",pageIndex);
            p.Add("PageSize", pageSize);
            p.Add("TotalRecords", dbType: DbType.Int32, direction: ParameterDirection.Output);
            p.Add("TransMode", tranMode == 0 ? (int?)null :tranMode);
            if (!string.IsNullOrEmpty(AssetTypeId))
            {
                p.Add("AssetTypeId", new DbString() { Value = AssetTypeId, IsAnsi = true, Length = 5 });
            }
            
            p.Add("Status", Status == 0 ? (int?)null : Status);
            p.Add("FromDate", FromDate.Value == DateTime.MinValue ? (DateTime?)null : FromDate.Value);
            p.Add("ToDate", ToDate.Value == DateTime.MinValue ? (DateTime?)null : ToDate.Value);
            if (!string.IsNullOrEmpty(transactionCode))
            {
                p.Add("TransactionCode",transactionCode.Trim());
            }
            p.Add("OrderBy", orderBy);
            
            var data = QueryStoreProc<TransactionAdminSearchModel>("usp_SelectPaging_TransactionAdminSearch",p).ToList();
            totalRecords = p.Get<int>("TotalRecords");
            return data;
        }
        public IEnumerable<Transaction> SearchTransactions(int skip, int take, out int totalResultsCount, out int filteredResultsCount, bool countTotal = true,
            DateTime? createdFromUtc = null, DateTime? createdToUtc = null, string transactionCode = null,
            IList<int> statuses = null, IList<string> assetTypeIds = null, string userName = "")
        {
            string query = "";
            string queryTotal = "";
            string fromString = "";
            string orderString = " t.TransactionId desc ";
            string whereString = " where 1=1 ";

            if (!string.IsNullOrEmpty(transactionCode))
            {
                whereString += " and t.TransactionCode  LIKE CONCAT('%',@TransactionCode,'%') ";
            }

            if (!string.IsNullOrEmpty(userName))
            {
                whereString += " and (u.UserName LIKE CONCAT('%',@UserName,'%') or u1.UserName LIKE CONCAT('%',@UserName,'%') or u2.UserName LIKE CONCAT('%',@UserName,'%'))";
            }

            if (createdFromUtc.HasValue)
            {
                whereString += " and t.CreatedDate >= @CreatedFrom ";
            }
            if (createdToUtc.HasValue)
            {
                whereString += " and t.CreatedDate <= @CreatedTo ";
            }
            if (statuses != null && statuses.Count > 0)
            {
                var statsusString = String.Join(",", statuses);
                whereString += $" and t.Status in ({statsusString}) ";
            }
            if (assetTypeIds != null && assetTypeIds.Count > 0)
            {
                var assetString = String.Join(",", assetTypeIds.Select(item => { return "'" + item + "'"; }));
                whereString += $" and t.AssetTypeId in ({assetString}) ";
            }

            fromString = @" [Transaction] t left join [User] u on t.UserId = u.Id
                            left join [BuyAdvertisement] buy on t.BuyAdvertisementId = buy.Id
                            left join [User] u1 on buy.UserId = u1.Id
                            left join [SellAdvertisement] sell on t.SellAdvertisementId = sell.Id
                            left join [User] u2 on sell.UserId = u2.Id";
            

            query = $"select t.*,u.Id,u.Email,u.UserName,buy.*,u1.Id,u1.Email,u1.UserName, sell.*,u2.Id,u2.Email,u2.UserName from {fromString} { whereString} order by {orderString} offset {skip} rows fetch next {take} rows only option(recompile)";
            queryTotal = $"select count(*) from {fromString} { whereString}";

            IEnumerable<Transaction> data = null;

            using (var conn = DbContext.OpenConnection())
            {
                DynamicParameters parameter = new DynamicParameters();

                parameter.Add("@TransactionCode", transactionCode, DbType.String, ParameterDirection.Input);
                parameter.Add("@UserName", userName, DbType.String, ParameterDirection.Input);
                parameter.Add("@CreatedFrom", createdFromUtc, DbType.DateTime, ParameterDirection.Input);
                parameter.Add("@CreatedTo", createdToUtc, DbType.DateTime, ParameterDirection.Input);

                data = conn.Query<Transaction,User,BuyAdvertisement,User,SellAdvertisement,User,Transaction>(query,(t,u,buyad,buyer,sellad,seller)=> 
                {
                    if(sellad != null)
                        sellad.AdvertiseUser = seller;
                    if(buyad != null)
                        buyad.AdvertiseUser = buyer;
                    t.User = u;
                    t.BuyAdvertisement = buyad;
                    t.SellAdvertisement = sellad;

                    return t;
                }, parameter, null);

                totalResultsCount = conn.ExecuteScalar<int>(queryTotal, parameter);
                filteredResultsCount = totalResultsCount;
            }

            return data;
        }
        public TransactionDetailAdminModel GetDetailAdmin(int transactionId)
        {
            return QueryStoreProc<TransactionDetailAdminModel>($"usp_GetTransactionDetailAdmin",new { TransactionID = transactionId }).FirstOrDefault();
        }

        public IList<TransactionHistoryModel> GetTransactionHistoryByType(int userId, TransactionStatusMode statusMode,
            int take, int skip, out int totalRecords)
        {
            var p = new DynamicParameters();
            p.Add("UserId", userId);
            p.Add("Status", (int)statusMode);
            p.Add("Take", take);
            p.Add("Skip", skip);
            p.Add("TotalRecords", dbType: DbType.Int32, direction: ParameterDirection.Output);

            var data = QueryStoreProc<TransactionHistoryModel>("usp_TransactionHistoryByUserIdStatus_Paging", p).ToList();
            totalRecords = p.Get<int>("TotalRecords");
            return data;
        }

        public Transaction GetbyTransactionId(int transactionId)
        {
            return GetByObject(new { TransactionId= transactionId });
        }

        public IList<TransactionHistoryUserModel> GetTransactionHistoryForUser(int userId, TransactionStatusClient transactionStatusClient,int skip, int take, out int totalRecords )
        {
            var p = new DynamicParameters();
            p.Add("UserId", userId);
            p.Add("Status", (int)transactionStatusClient);
            p.Add("Take", take);
            p.Add("Skip", skip);    
            p.Add("TotalRecords", dbType: DbType.Int32, direction: ParameterDirection.Output);

            var data = QueryStoreProc<TransactionHistoryUserModel>("usp_Transaction_GetHistoryBy_UserIdStatus", p).ToList();
            totalRecords = p.Get<int>("TotalRecords");
            return data;
        }
    }
}