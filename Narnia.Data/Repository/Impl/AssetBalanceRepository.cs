﻿using Narnia.Core.Domain;
using Narnia.Data.Base;
using Narnia.Data.Base.Repositories;
using Narnia.Data.Repository.Contracts;

namespace Narnia.Data.Repository.Impl
{
    public class AssetBalanceRepository : GenericRepository<AssetBalance>, IAssetBalanceRepository
    {
        public AssetBalanceRepository(IDbContext dbContext) : base(dbContext)
        {
        }

        public AssetBalance GetByUserIdAssetType(int userID, string assetTypeID)
        {
            return GetFirst(new { UserId = userID, AssetTypeId = assetTypeID });
        }
    }
}