﻿using Dapper;
using Narnia.Core.Domain;
using Narnia.Core.Models;
using Narnia.Data.Repository.Contracts;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Narnia.Core.Enums;
using Narnia.Data.Base.Repositories;
using Narnia.Data.Base;

namespace Narnia.Data.Repository.Impl
{
   
    public class SellAdvertisementRepository : GenericRepository<SellAdvertisement>, ISellAdvertisementRepository
    {
        public SellAdvertisementRepository(IDbContext dbContext) : base(dbContext)
        {
        }

        public IEnumerable<BuySellViewModel> SearchSellAdvertisement(int pageIndex, int pageSize, out int totalResultsCount, out int filteredResultsCount, bool countTotal = true
            ,bool isShowOfflineUser = false, string assetTypeId = null)
        {
            if (string.IsNullOrEmpty(assetTypeId))
                assetTypeId = "ETH";

            totalResultsCount = 0;
            filteredResultsCount = 0;
            int skip = (pageIndex - 1) * pageSize;

            //string query = "", queryTotal = "", sJoinTable = " inner join [User] u with (nolock) on s.UserId=u.Id";
            //query = $"select s.*,u.* From SellAdvertisement s with (nolock) {sJoinTable} Where AssetTypeId = {assetTypeId} order by s.CreatedDate desc offset {skip} rows fetch next {pageSize} rows only option(recompile) ";
            

            IEnumerable<BuySellViewModel> data = null;
            int total = -1;
            using (var conn = DbContext.OpenConnection())
            {
                data = conn.Query<BuySellViewModel>("usp_SellAdvertisement"
                        , new { AssetTypeId = assetTypeId, IsGetOfflineUser = isShowOfflineUser, From = skip, To = pageSize }
                    , commandType: CommandType.StoredProcedure);

                if (countTotal)
                {
                    string queryTotal = "";
                    if (isShowOfflineUser)
                        queryTotal = $"select count (*) from SellAdvertisement with (nolock) Where AssetTypeId = '{assetTypeId}' and Approved = 1 and (MaxCoinNumber - ISNULL(AmountSell,0)) > 0 ";
                    else
                        queryTotal = $"select count (*) from SellAdvertisement s with (nolock) Where AssetTypeId = '{assetTypeId}' and Approved = 1 and (MaxCoinNumber - ISNULL(s.AmountSell,0)) > 0 and exists (select top 1 * from UserLoginHistory uh with (nolock) where uh.UserId=s.UserId) ";

                    total = conn.ExecuteScalar<int>(queryTotal);
                }
            }
                
            filteredResultsCount = total;
            totalResultsCount = filteredResultsCount;
            // var enumerable = data as SellAdvertisement[] ?? data.ToArray();
            //return new PagedList<SellAdvertisement>(enumerable, pageIndex, pageSize);
            return data;
        }

        public InfoAdvertisementForSellBuyModel GetInfoSellAdvertisement(int SellAdId,int userId)
        {
            InfoAdvertisementForSellBuyModel data = null;
            using (var conn = DbContext.OpenConnection())
            {
                data = conn.Query<InfoAdvertisementForSellBuyModel>("usp_GetInfoSellAdvertisement"
                    , new { SellAdId = SellAdId, UserId =userId}
                    , commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return data;
        }

        public IEnumerable<SellAdvertisement> LoadSellAdvertisement(int pageIndex, int pageSize, out int totalResultsCount, out int filteredResultsCount, bool countTotal = true, string sort = null
            , BuySellAdvertiseSearchCriteria criteria = null)
        {
            totalResultsCount = 0;
            filteredResultsCount = 0;
            int skip = 0;
            if (pageIndex > 0)
                skip = (pageIndex - 1) * pageSize;

            string query = "", queryTotal = "", whereString = "";
            IEnumerable<SellAdvertisement> data = null;

            string sJoinTable = " inner join [User] u with (nolock) on s.UserId=u.Id";           
            sJoinTable += " inner join [Currency] cuy with (nolock) on s.CurrencyId=cuy.CurrencyCode";
            sJoinTable += " left join [Banks] b with (nolock) on s.BankId=b.Id";
            sJoinTable += " left join [PaymentMethod] p with (nolock) on s.PaymentMethodId=p.Id";
            try
            {
                query = $"select s.*,u.*,cuy.*,b.*,p.* From SellAdvertisement s with (nolock) {sJoinTable} {whereString} order by s.CreatedDate desc offset {skip} rows fetch next {pageSize} rows only option(recompile) ";
                queryTotal = $"select count (*) from SellAdvertisement with (nolock) {whereString}";

                int total = -1;
                using (var conn = DbContext.OpenConnection())
                {
                    data = conn.Query<SellAdvertisement, User, Currency, Banks, PaymentMethod, SellAdvertisement>(
                        query, (s, u, cuy, b, p) => { s.AdvertiseUser = u;  s.CurrencyType = cuy; s.Bank = b; s.PayType = p; return s; });

                    if (countTotal)
                        total = conn.ExecuteScalar<int>(queryTotal);

                }
                  
                filteredResultsCount = total;
                totalResultsCount = filteredResultsCount;
            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }

            return data;
        }

        public SellAdvertisement LoadSellAdvertisementDetail(int sellAdId)
        {
            SellAdvertisement data = null;

            string sJoinTable = " inner join [User] u with (nolock) on s.UserId=u.Id";           
            sJoinTable += " inner join [Currency] cuy with (nolock) on s.CurrencyId=cuy.CurrencyCode";
            sJoinTable += " left join [Banks] b with (nolock) on s.BankId=b.Id";
            sJoinTable += " left join [PaymentMethod] p with (nolock) on s.PaymentMethodId=p.Id";
            string query = $"select s.*,u.*,cuy.*,b.*,p.* From SellAdvertisement s with (nolock) {sJoinTable} where s.ID= {sellAdId} ";
            try
            {
                using (var conn = DbContext.OpenConnection())
                {
                    var retVal = conn.Query<SellAdvertisement, User, Currency, Banks, PaymentMethod, SellAdvertisement>(
                        query, (s, u, cuy, b, p) => { s.AdvertiseUser = u;  s.CurrencyType = cuy; s.Bank = b; s.PayType = p; return s; });

                    data = retVal.FirstOrDefault();
                }
            }
            catch// (Exception ex)
            {
                //string s = ex.Message;
            }

            return data;
        }

        public IEnumerable<InfoQuickBuySellAdvertisement> LoadQuickSellAdvertisement(string assetTypeId, string currencyType, decimal coinNumber)
        {
            IEnumerable<InfoQuickBuySellAdvertisement> data = null;

            using (var conn = DbContext.OpenConnection())
            {
                data = conn.Query<InfoQuickBuySellAdvertisement>("usp_QuickSellAdvertisement"
                    , new { AssetTypeId =assetTypeId, CurrencyType = currencyType, CoinNumber = coinNumber }
                    , commandType: System.Data.CommandType.StoredProcedure);
            }

            return data;
        }

        public SellAdvertisement GetActiveById(int Id)
        {
            return GetFirst(new {Id = Id, Status = (int) AdvertisementStatus.New});
        }

        public SellAdvertisement GetyId(int Id)
        {
            return GetFirst(new { Id = Id});
        }

        public IEnumerable<BuySellViewModel> GetSellAdvertisementByUserId(int pageIndex, int pageSize, int userId, out int totalResultsCount, bool countTotal = true, string assetTypeId = null)
        {
            if (string.IsNullOrEmpty(assetTypeId))
                assetTypeId = "ETH";

            totalResultsCount = 0;
            int skip = (pageIndex - 1) * pageSize;

            string queryTotal = "", sJoinTable = " inner join [User] u with (nolock) on s.UserId=u.Id";

            queryTotal = $"select count (*) from SellAdvertisement s with (nolock) {sJoinTable} Where AssetTypeId = '{assetTypeId}' AND s.UserId = {userId}";

            IEnumerable<BuySellViewModel> data = null;
            int total = -1;
            using (var conn = DbContext.OpenConnection())
            {
                data = conn.Query<BuySellViewModel>("usp_GetSellAdvertisementByUserId"
                        , new { AssetTypeId = assetTypeId, UserId = userId, From = skip, To = pageSize }
                    , commandType: CommandType.StoredProcedure);

                if (countTotal)
                    total = conn.ExecuteScalar<int>(queryTotal);
            }

            totalResultsCount = total;

            return data;
        }

        public IEnumerable<SellAdvertisement> SearchAdvertisments(int skip, int take, out int totalResultsCount, out int filteredResultsCount, bool countTotal = true,
   DateTime? createdFromUtc = null, DateTime? createdToUtc = null,
   IList<int> statuses = null, IList<string> assetTypeIds = null, string userName = "")
        {
            string query = "";
            string queryTotal = "";
            string fromString = "";
            string orderString = " s.Id desc ";
            string whereString = " where 1=1 ";

            fromString = @" [SellAdvertisement] s left join [User] u on s.UserId = u.Id";

            if (!string.IsNullOrEmpty(userName))
            {
                whereString += " and u.UserName LIKE CONCAT('%',@UserName,'%')";
            }

            if (createdFromUtc.HasValue)
            {
                whereString += " and s.CreatedDate >= @CreatedFrom ";
            }
            if (createdToUtc.HasValue)
            {
                whereString += " and s.CreatedDate <= @CreatedTo ";
            }
            if (statuses != null && statuses.Count > 0)
            {
                var statsusString = String.Join(",", statuses);
                whereString += $" and s.Status in ({statsusString}) ";
            }
            if (assetTypeIds != null && assetTypeIds.Count > 0)
            {
                var assetString = String.Join(",", assetTypeIds.Select(item => { return "'" + item + "'"; }));
                whereString += $" and s.AssetTypeId in ({assetString}) ";
            }

            query = $"select s.*,u.Id,u.Email,u.UserName from {fromString} { whereString} order by {orderString} offset {skip} rows fetch next {take} rows only option(recompile)";
            queryTotal = $"select count(*) from {fromString} { whereString}";

            IEnumerable<SellAdvertisement> data = null;

            using (var conn = DbContext.OpenConnection())
            {
                DynamicParameters parameter = new DynamicParameters();

                parameter.Add("@UserName", userName, DbType.String, ParameterDirection.Input);
                parameter.Add("@CreatedFrom", createdFromUtc, DbType.DateTime, ParameterDirection.Input);
                parameter.Add("@CreatedTo", createdToUtc, DbType.DateTime, ParameterDirection.Input);

                data = conn.Query<SellAdvertisement, User, SellAdvertisement>(query, (s, u) =>
                {
                    s.AdvertiseUser = u;
                    return s;
                }, parameter, null);

                totalResultsCount = conn.ExecuteScalar<int>(queryTotal, parameter);
                filteredResultsCount = totalResultsCount;
            }

            return data;
        }
    }
}
