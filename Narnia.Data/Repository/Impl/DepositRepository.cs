﻿using Dapper;
using Narnia.Core.Domain;
using Narnia.Core.Domain.Payments;
using Narnia.Data.Base;
using Narnia.Data.Base.Repositories;
using Narnia.Data.Repository.Contracts;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Narnia.Data.Repository.Impl
{
    public class DepositRepository : GenericRepository<Deposit>, IDepositRepository
    {
        public DepositRepository(IDbContext dbContext) : base(dbContext)
        {
        }

        public IEnumerable<Deposit> SearchDeposits(int skip, int take, out int totalResultsCount, out int filteredResultsCount, bool countTotal = true, DateTime? createdFromUtc = null, DateTime? createdToUtc = null
            , IList<string> assetTypeIds = null, IList<int> statuses = null, string userName = "")
        {
            string query = "";
            string queryTotal = "";
            string fromString = "";
            string orderString = " d.Id desc ";
            string whereString = " where 1=1 ";

            fromString = @" [Deposit] d left join [User] u on d.UserId = u.Id";

            if (!string.IsNullOrEmpty(userName))
            {
                whereString += " and u.UserName LIKE CONCAT('%',@UserName,'%')";
            }

            if (createdFromUtc.HasValue)
            {
                whereString += " and d.CreatedDate >= @CreatedFrom ";
            }
            if (createdToUtc.HasValue)
            {
                whereString += " and d.CreatedDate <= @CreatedTo ";
            }
            if (statuses != null && statuses.Count > 0)
            {
                var statsusString = String.Join(",", statuses);
                whereString += $" and d.Status in ({statsusString}) ";
            }
            if (assetTypeIds != null && assetTypeIds.Count > 0)
            {
                var assetString = String.Join(",", assetTypeIds.Select(item => { return "'" + item + "'"; }));
                whereString += $" and d.AssetTypeId in ({assetString}) ";
            }

            query = $"select d.*,u.Id,u.UserName from {fromString} { whereString} order by {orderString} offset {skip} rows fetch next {take} rows only option(recompile)";
            queryTotal = $"select count(*) from {fromString} { whereString}";

            IEnumerable<Deposit> data = null;

            using (var conn = DbContext.OpenConnection())
            {
                DynamicParameters parameter = new DynamicParameters();

                parameter.Add("@UserName", userName, DbType.String, ParameterDirection.Input);
                parameter.Add("@CreatedFrom", createdFromUtc, DbType.DateTime, ParameterDirection.Input);
                parameter.Add("@CreatedTo", createdToUtc, DbType.DateTime, ParameterDirection.Input);

                data = conn.Query<Deposit, User, Deposit>(query, (d, u) =>
                {
                    d.User = u;
                    return d;
                }, parameter, null);

                totalResultsCount = conn.ExecuteScalar<int>(queryTotal, parameter);
                filteredResultsCount = totalResultsCount;
            }

            return data;
        }

        public IEnumerable<BankTransfer> SearchBankTransfers(int skip, int take, out int totalResultsCount, out int filteredResultsCount, bool countTotal = true, DateTime? createdFromUtc = null, DateTime? createdToUtc = null
            , IList<string> assetTypeIds = null, IList<int> statuses = null, string userName = "", DateTime? billingDate = null)
        {
            string query = "";
            string queryTotal = "";
            string fromString = "";
            string orderString = " t.Id desc ";
            string whereString = " where 1=1 ";

            fromString = @" [BankTransfer] t left join [User] u on t.UserId = u.Id";

            if (!string.IsNullOrEmpty(userName))
            {
                whereString += " and u.UserName LIKE CONCAT('%',@UserName,'%')";
            }

            if (createdFromUtc.HasValue)
            {
                whereString += " and t.CreatedOn >= @CreatedFrom ";
            }
            if (createdToUtc.HasValue)
            {
                whereString += " and t.CreatedOn <= @CreatedTo ";
            }
            if (statuses != null && statuses.Count > 0)
            {
                var statsusString = String.Join(",", statuses);
                whereString += $" and t.Status in ({statsusString}) ";
            }
            if (assetTypeIds != null && assetTypeIds.Count > 0)
            {
                var assetString = String.Join(",", assetTypeIds.Select(item => { return "'" + item + "'"; }));
                whereString += $" and t.AssetTypeId in ({assetString}) ";
            }

            query = $"select t.*,u.Id,u.UserName from {fromString} { whereString} order by {orderString} offset {skip} rows fetch next {take} rows only option(recompile)";
            queryTotal = $"select count(*) from {fromString} { whereString}";

            IEnumerable<BankTransfer> data = null;

            using (var conn = DbContext.OpenConnection())
            {
                DynamicParameters parameter = new DynamicParameters();

                parameter.Add("@UserName", userName, DbType.String, ParameterDirection.Input);
                parameter.Add("@CreatedFrom", createdFromUtc, DbType.DateTime, ParameterDirection.Input);
                parameter.Add("@CreatedTo", createdToUtc, DbType.DateTime, ParameterDirection.Input);

                data = conn.Query<BankTransfer, User, BankTransfer>(query, (t, u) =>
                {
                    t.User = u;
                    return t;
                }, parameter, null);

                totalResultsCount = conn.ExecuteScalar<int>(queryTotal, parameter);
                filteredResultsCount = totalResultsCount;
            }

            return data;
        }
    }
}
