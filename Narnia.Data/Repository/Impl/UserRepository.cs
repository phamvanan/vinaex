﻿using Dapper;
using Microsoft.Extensions.Configuration;
using Narnia.Core.Domain;
using Narnia.Core.Models;
using Narnia.Data.Base;
using Narnia.Data.Base.Repositories;
using Narnia.Data.Repository.Contracts;
using Narnia.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Narnia.Data.Repository.Impl
{
    public class UserRepository : GenericRepository<User>, IUserRepository
    {
        public UserRepository(IDbContext dbContext) : base(dbContext)
        {
        }
        public List<User> GetUplineUsers(int userId)
        {
            List<User> data = null;
            string sql = $"with UserCTE as (select * from [User] where Id = {userId} union all select u.*from[User] u inner join UserCTE cte on cte.UplineId = u.Id) select* from UserCTE";
            sql += $" where UserCTE.Id <> {userId}";

            using (var conn = DbContext.OpenConnection())
            {
                data = conn.Query<User>(sql, new { UserId = userId }, DbContext.GetTransaction()).ToList();
            }
            return data;
        }
        public User GetUserById(int userId)
        {
            User data = null;
            using (var conn = DbContext.OpenConnection())
            {
                data = conn.Query<User, UserInfor, User>("usp_GetUserInforById", (u, ui) =>
                {
                    if (ui != null && ui.Id > 0)
                        u.UserInfo = ui;
                    return u;
                }, new { UserId = userId }, DbContext.GetTransaction(),commandType:CommandType.StoredProcedure).FirstOrDefault();
            }
            return data;
        }

        public User GetUserByName(string userName)
        {
            User data = null;
            using (var conn = DbContext.OpenConnection())
            {
                data = conn.Query<User, UserInfor, User>("usp_GetUserInforByUserName", (u, ui) =>
                {
                    if (ui != null && ui.Id > 0)
                        u.UserInfo = ui;
                    return u;
                }, new { UserName = userName }, DbContext.GetTransaction(), commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return data;
        }

        public UserInforModel GetUserInforByUserId(int userId)
        {
            var data = QueryStoreProc<UserInforModel>("usp_GetUserInforById", new { UserId = userId }).FirstOrDefault();

            return data;
        }

        public UserInforModel GetUserInforByUserName(string userName)
        {
            var data = QueryStoreProc<UserInforModel>("usp_GetUserInforByUserName", new { UserName = userName }).FirstOrDefault();

            return data;
        }

        public UserInforModel GetUserFullInforByUserId(int userId)
        {
            var data = QueryStoreProc<UserInforModel>("usp_GetUserFullInforByUserId", new { UserId = userId }).FirstOrDefault();

            return data;
        }

        public IEnumerable<User> FindUsers(int skip, int take, out int totalResultsCount, out int filteredResultsCount, bool countTotal = true,
           DateTime? createdFromUtc = null, DateTime? createdToUtc = null, string userName = null,
           string phoneNumber = "")
        {
            string query = "";
            string queryTotal = "";
            string fromString = "";
            string orderString = " u.Id desc ";
            string whereString = " where 1=1 ";

            if (!string.IsNullOrEmpty(userName))
            {
                whereString += " and u.UserName  LIKE CONCAT('%',@UserName,'%') ";
            }
            if (!string.IsNullOrEmpty(phoneNumber))
            {
                whereString += " and u.Phone is not null and u.Phone  LIKE CONCAT('%',@Phone,'%') ";
            }

            if (createdFromUtc.HasValue)
            {
                whereString += " and u.CreatedDate >= @CreatedFrom ";
            }
            if (createdToUtc.HasValue)
            {
                whereString += " and u.CreatedDate <= @CreatedTo ";
            }
           
            fromString = @" [User] u left join [UserInfor] ui on u.Id = ui.UserId";


            query = $"select u.*,ui.* from {fromString} { whereString} order by {orderString} offset {skip} rows fetch next {take} rows only option(recompile)";
            queryTotal = $"select count(*) from {fromString} { whereString}";

            IEnumerable<User> data = null;

            using (var conn = DbContext.OpenConnection())
            {
                DynamicParameters parameter = new DynamicParameters();

                parameter.Add("@UserName", userName, DbType.String, ParameterDirection.Input);
                parameter.Add("@Phone", phoneNumber, DbType.String, ParameterDirection.Input);
                parameter.Add("@CreatedFrom", createdFromUtc, DbType.DateTime, ParameterDirection.Input);
                parameter.Add("@CreatedTo", createdToUtc, DbType.DateTime, ParameterDirection.Input);

                data = conn.Query<User, UserInfor, User>(query, (u,ui) =>
                {
                    u.UserInfo = ui;
                    return u;
                }, parameter, null);

                totalResultsCount = conn.ExecuteScalar<int>(queryTotal, parameter);
                filteredResultsCount = totalResultsCount;
            }

            return data;
        }

        public IList<UserInforModel> FindUsers(SearchUserModel userSearch, out int totalRecords)
        {
            IEnumerable<UserInforModel> data = null;

            using (var conn = DbContext.OpenConnection())
            {
                var dynamicParams = new DynamicParameters();
                dynamicParams.Add("@Email", userSearch.Email, DbType.String);
                dynamicParams.Add("@FromDate", userSearch.FromDate, DbType.DateTime);
                dynamicParams.Add("@ToDate", userSearch.ToDate, DbType.DateTime);
                dynamicParams.Add("@Skip", userSearch.Skip, DbType.Int32);
                dynamicParams.Add("@Take", userSearch.Take, DbType.Int32);

                dynamicParams.Add("@TotalRecords", DbType.Int32, direction: ParameterDirection.Output);
                data = conn.Query<UserInforModel>("usp_SearchUser_Paging" , dynamicParams , commandType: CommandType.StoredProcedure);

                totalRecords = dynamicParams.Get<int>("@TotalRecords");
            }

            return data != null ? data.ToList(): null;
        }

        public User GetById(int id)
        {
           return GetByObject(new { Id = id });
        }
        public User GetUplineUser(int userId)
        {
            User data = null;

            //var query = "select * from [User] where UplineId in (select UplineId from [User] where Id = @Id)";

            //using (var conn = DbContext.OpenConnection())
            //{
            //    var dynamicParams = new DynamicParameters();
            //    dynamicParams.Add("@Id", userId, DbType.Int32);
            //    data = conn.Query<User>(query, dynamicParams, commandType: CommandType.Text).FirstOrDefault();
            //}
            var dynamicParams = new DynamicParameters();
            dynamicParams.Add("@Id", userId, DbType.Int32);
            data = QueryStoreProc<User>("sp_GetUplineUser", dynamicParams).FirstOrDefault();
            return data;
        }
        public LinkedList<User> GetAllUplineUsers(int userId)
        {
            IEnumerable<User> data = null;

            //var query = @"with myTable as
            //                (
            //                 select u.Id , u.UserName,u.IsActive,u.IsVerify,u.UplineId, 0 as Generation
            //                 from [User] u where u.Id = @UserId
            //                 union all
            //                 select u.Id as UserId, u.UserName,u.IsActive,u.IsVerify, u.UplineId, myTable.Generation + 1 as Generation
            //                 from [User] u inner join myTable on myTable.UplineId = u.Id
            //                )
            //                select * from myTable
            //                where Generation > 0 
            //                order by Generation";

            //using (var conn = DbContext.OpenConnection())
            //{
            //    var dynamicParams = new DynamicParameters();
            //    dynamicParams.Add("@UserId", userId, DbType.Int32);
            //    data = conn.Query<User>(query, dynamicParams, commandType: CommandType.Text);
            //}
            var dynamicParams = new DynamicParameters();
            dynamicParams.Add("@UserId", userId, DbType.Int32);
            data = QueryStoreProc<User>("sp_GetAllUplineUsers", dynamicParams);
            var retVal = new LinkedList<User>(data);
            return retVal;
        }
    }
}
