﻿using Dapper;
using Narnia.Core.Domain;
using Narnia.Core.Domain.Commissions;
using Narnia.Core.Domain.Payments;
using Narnia.Data.Base;
using Narnia.Data.Base.Repositories;
using Narnia.Data.Repository.Contracts;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Narnia.Data.Repository.Impl
{
    public class AggregateRepository : IRepository, IAggregateRepository
    {
        public IDbContext DbContext { get; private set; }

        public AggregateRepository(IDbContext dbContext)
        {
            DbContext = dbContext;
        }

        public bool CreateSellAdvertisement(SellAdvertisement sellInfo, Double feePercentage)
        {                                                   
            using (var conn = DbContext.OpenConnection())
            {
                var query = "usp_CreateSellAdvertisement";
                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@UserId", sellInfo.UserId, DbType.Int32);
                parameters.Add("@CurrencyId", sellInfo.CurrencyId, DbType.String);
                parameters.Add("@AssetTypeId", sellInfo.AssetTypeId, DbType.String);
                parameters.Add("@BankId", sellInfo.BankId, DbType.Int32);
                parameters.Add("@BankAccountNumber", sellInfo.BankAccountNumber, DbType.AnsiStringFixedLength, size: 100);
                parameters.Add("@BankAccountName", sellInfo.BankAccountName, DbType.StringFixedLength, size: 255);
                parameters.Add("@CoinExchangeId", sellInfo.CoinExchangeId, DbType.Int32);
                parameters.Add("@CoinPrice", sellInfo.MinCoinPrice, DbType.Decimal);
                parameters.Add("@MinCoinPrice", sellInfo.MinCoinPrice, DbType.Decimal);
                parameters.Add("@MinCoinNumber", sellInfo.MinCoinNumber, DbType.Double);
                parameters.Add("@MaxCoinNumber", sellInfo.MaxCoinNumber, DbType.Double);
                parameters.Add("@PaymentAllowTime", sellInfo.PaymentAllowTime, DbType.Int32);
                parameters.Add("@PaymentMethodId", sellInfo.PaymentMethodId, DbType.Int32);
                parameters.Add("@CountryId", sellInfo.CountryId, DbType.Int32);
                parameters.Add("@FeePercentage", feePercentage, DbType.Double); 

                try
                {
                    conn.Query(query, parameters, commandType: CommandType.StoredProcedure);                    
                    return true;
                }
                catch (System.Exception ex)
                {                   
                    
                }

                return false;
            }
        }

        public IList<Feedback> GetTopFeedbacks(int top)
        {
            using (var conn = DbContext.OpenConnection())
            {
                var query = $"select top {top} fb.*,u1.*,u2.* from Feedback fb inner join[User] u1 on u1.Id = fb.CreatedBy inner join[User] u2 " +
                    $"on u2.Id = fb.UserId order by fb.CreatedDate desc";
                return conn.Query<Feedback, User, User, Feedback>(query, (fb, fromUser, toUser) => 
                {
                    fb.CreatedUser = fromUser;
                    fb.ReceivedUser = toUser;
                    return fb;
                }).ToList();
              
            }
        }

        public IEnumerable<AssetBalance> FindAssetBalances(int skip, int take, out int totalResultsCount, out int filteredResultsCount, bool countTotal = true, string userEmail = null, IList<string> assetTypeIds = null)
        {
            totalResultsCount = 0;
            filteredResultsCount = 0;
            string whereString = " where 1=1 ";

            if (!string.IsNullOrEmpty(userEmail))
            {
                whereString += $" and u.Email like '%{userEmail}%' ";
            }
            if (assetTypeIds != null && assetTypeIds.Count > 0)
            {
                var assetString = String.Join(",", assetTypeIds.Select(item => { return "'" + item + "'"; }));
                whereString += $" and ab.AssetTypeId in ({assetString}) ";
            }

            string query = "";
            string queryTotal = "";
            string orderString = " ab.Id desc ";

            query = $"select * from AssetBalance ab inner join [User] u on ab.UserId = u.Id {whereString} order by {orderString} offset {skip} rows fetch next {take} rows only option(recompile) ";
            queryTotal = $"select count(*) from AssetBalance ab inner join [User] u on ab.UserId = u.Id {whereString}";

            IEnumerable<AssetBalance> data = null;

            using (var conn = DbContext.OpenConnection())
            {
                data = conn.Query<AssetBalance, User, AssetBalance>(query, (ab, u) => { ab.User = u; return ab; }, null, null);

                totalResultsCount = conn.ExecuteScalar<int>(queryTotal);
                filteredResultsCount = totalResultsCount;
            }

            return data;
        }

        public IEnumerable<Commission> FindCommissions(int skip, int take, out int totalResultsCount, out int filteredResultsCount, bool countTotal = true,
            DateTime? createdFromUtc = null, DateTime? createdToUtc = null,
          IList<int> commLevels = null, string beneficiaryUserName = "")
        {
            totalResultsCount = 0;
            filteredResultsCount = 0;
            string whereString = " where 1=1 ";

            string levelsString = "";

            if (!string.IsNullOrEmpty(beneficiaryUserName))
            {
                whereString += " and u.Email  LIKE CONCAT('%',@BeneficiaryUserName,'%') ";
            }

            if (createdFromUtc.HasValue)
            {
                whereString += " and c.CreatedDate >= @CreatedFrom ";
            }
            if (createdToUtc.HasValue)
            {
                whereString += " and c.CreatedDate <= @CreatedTo ";
            }
            if(commLevels != null && commLevels.Count > 0)
            {
                levelsString = String.Join(",", commLevels);
                whereString += $" and c.CommLevel in ({levelsString}) ";
            }

            string query = "";
            string queryTotal = "";
            string orderString = " c.CreatedDate desc,u.UserName asc ";

            query = $"select c.*,u.* from Commission c inner join [User] u on c.BeneficiaryUserId = u.Id {whereString} order by {orderString} offset {skip} rows fetch next {take} rows only option(recompile) ";
            queryTotal = $"select count(*) from Commission c inner join [User] u on c.BeneficiaryUserId = u.Id {whereString}";

            IEnumerable<Commission> data = null;

            using (var conn = DbContext.OpenConnection())
            {
                DynamicParameters parameter = new DynamicParameters();

                parameter.Add("@BeneficiaryUserName", beneficiaryUserName, DbType.String, ParameterDirection.Input);
                parameter.Add("@CreatedFrom", createdFromUtc, DbType.DateTime, ParameterDirection.Input);
                parameter.Add("@CreatedTo", createdToUtc, DbType.DateTime, ParameterDirection.Input);

                data = conn.Query<Commission, User, Commission>(query, (c, u) => { c.BeneficiaryUser = u; return c; }, parameter, null);

                totalResultsCount = conn.ExecuteScalar<int>(queryTotal, parameter);
                filteredResultsCount = totalResultsCount;
            }

            return data;
        }

        public IEnumerable<Setting> FindSettings(int skip, int take, out int totalResultsCount, out int filteredResultsCount, bool countTotal = true,
           string settingName = "", string settingValue = "")
        {
            totalResultsCount = 0;
            filteredResultsCount = 0;
            string whereString = " where 1=1 ";

            if (!string.IsNullOrEmpty(settingName))
            {
                whereString += " and Name  LIKE CONCAT('%',@SettingName,'%') ";
            }

            if (!string.IsNullOrEmpty(settingValue))
            {
                whereString += " and Value  LIKE CONCAT('%',@SettingValue,'%') ";
            }

            string query = "";
            string queryTotal = "";
            string orderString = " Name asc ";

            query = $"select * from Setting { whereString} order by {orderString} offset {skip} rows fetch next {take} rows only option(recompile) ";
            queryTotal = $"select count(*) from Setting { whereString}";

            IEnumerable<Setting> data = null;

            using (var conn = DbContext.OpenConnection())
            {
                DynamicParameters parameter = new DynamicParameters();

                parameter.Add("@SettingName", settingName, DbType.String, ParameterDirection.Input);
                parameter.Add("@SettingValue", settingValue, DbType.String, ParameterDirection.Input);
                
                data = conn.Query<Setting>(query, parameter, null);

                totalResultsCount = conn.ExecuteScalar<int>(queryTotal, parameter);
                filteredResultsCount = totalResultsCount;
            }

            return data;
        }
        public IList<GatewayAccount> GetActiveSystemGatewayAccounts(int gatewayId)
        {
            string query = "select ga.* from GatewayAccount ga inner join PaymentGateway pg on ga.GatewayId = pg.Id ";
            string whereString = $" where pg.Id= {gatewayId} and ga.IsSystemAccount = 'true' and pg.IsActive = 'true' ";

            query = query + whereString;

            IList<GatewayAccount> data = null;

            using (var conn = DbContext.OpenConnection())
            {
                data = conn.Query<GatewayAccount>(query).ToList();
            }
            return data;
        }

        public IEnumerable<GatewayAccount> FindGatewayAccounts(int skip, int take, out int totalResultsCount, out int filteredResultsCount, bool countTotal = true,
           DateTime? createdFromUtc = null, DateTime? createdToUtc = null, string userName = "", bool isSystemAccount = false)
        {
            string query = "";
            string queryTotal = "";
            string fromString = "";
            string orderString = " ga.CreatedDate desc ";
            string whereString = " where 1=1 ";
            
            if (!string.IsNullOrEmpty(userName))
            {
                whereString += " and (u.UserName LIKE CONCAT('%',@UserName,'%') or u1.UserName LIKE CONCAT('%',@UserName,'%') or u2.UserName LIKE CONCAT('%',@UserName,'%'))";
            }

            if (createdFromUtc.HasValue)
            {
                whereString += " and ga.CreatedDate >= @CreatedFrom ";
            }
            if (createdToUtc.HasValue)
            {
                whereString += " and ga.CreatedDate <= @CreatedTo ";
            }

            whereString += $" and ga.IsSystemAccount = {(isSystemAccount ? 1:0)} ";

            fromString = @" [GatewayAccount] ga left join [User] u on ga.UserId = u.Id";


            query = $"select ga.*,u.Id,u.Email,u.UserName from {fromString} { whereString} order by {orderString} offset {skip} rows fetch next {take} rows only option(recompile)";
            queryTotal = $"select count(*) from {fromString} { whereString}";

            IEnumerable<GatewayAccount> data = null;

            using (var conn = DbContext.OpenConnection())
            {
                DynamicParameters parameter = new DynamicParameters();

                parameter.Add("@UserName", userName, DbType.String, ParameterDirection.Input);
                parameter.Add("@CreatedDate", createdFromUtc, DbType.DateTime, ParameterDirection.Input);
                parameter.Add("@CreatedTo", createdToUtc, DbType.DateTime, ParameterDirection.Input);

                data = conn.Query<GatewayAccount, User, GatewayAccount>(query, (ga, u) =>
                {
                    ga.User = u;
                    return ga;
                }, parameter, null);

                totalResultsCount = conn.ExecuteScalar<int>(queryTotal, parameter);
                filteredResultsCount = totalResultsCount;
            }

            return data;
        }

        public IEnumerable<ExchangeOffer> FindExchangeOffers(int skip, int take, out int totalResultsCount, bool countTotal = true,
  DateTime? createdFromUtc = null, DateTime? createdToUtc = null,
  IList<int> statuses = null, IList<string> assetTypeIds = null, string code = "", int? type = null)
        {
            string query = "";
            string queryTotal = "";
            string fromString = "";
            string orderString = " Id desc ";
            string whereString = " where 1=1 ";

            fromString = @" [ExchangeOffer] ";

            if (!string.IsNullOrEmpty(code))
            {
                whereString += " and ExchangeOfferCode LIKE CONCAT('%',@Code,'%')";
            }
            if(type.HasValue && type.Value > 0)
            {
                whereString += $" and Type = {type.Value} ";
            }

            if (createdFromUtc.HasValue)
            {
                whereString += " and CreatedDate >= @CreatedFrom ";
            }
            if (createdToUtc.HasValue)
            {
                whereString += " and CreatedDate <= @CreatedTo ";
            }
            if (statuses != null && statuses.Count > 0)
            {
                var statsusString = String.Join(",", statuses);
                whereString += $" and Status in ({statsusString}) ";
            }
            if (assetTypeIds != null && assetTypeIds.Count > 0)
            {
                var assetString = String.Join(",", assetTypeIds.Select(item => { return "'" + item + "'"; }));
                whereString += $" and AssetTypeId in ({assetString}) ";
            }

            query = $"select * from {fromString} { whereString} order by {orderString} offset {skip} rows fetch next {take} rows only option(recompile)";
            queryTotal = $"select count(*) from {fromString} { whereString}";

            IEnumerable<ExchangeOffer> data = null;

            using (var conn = DbContext.OpenConnection())
            {
                DynamicParameters parameter = new DynamicParameters();

                parameter.Add("@Code", code, DbType.String, ParameterDirection.Input);
                parameter.Add("@CreatedFrom", createdFromUtc, DbType.DateTime, ParameterDirection.Input);
                parameter.Add("@CreatedTo", createdToUtc, DbType.DateTime, ParameterDirection.Input);
                parameter.Add("@Code", code, DbType.String, ParameterDirection.Input);

                data = conn.Query<ExchangeOffer>(query, parameter);

                totalResultsCount = conn.ExecuteScalar<int>(queryTotal, parameter);
            }

            return data;
        }
    }
}
