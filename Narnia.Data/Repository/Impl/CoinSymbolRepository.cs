﻿using System.Collections.Generic;
using System.Linq;
using Narnia.Core.Domain;
using Narnia.Data.Base;
using Narnia.Data.Base.Repositories;
using Narnia.Data.Repository.Contracts;

namespace Narnia.Data.Repository.Impl
{
    public class CoinSymbolRepository : GenericRepository<CoinSymbol>, ICoinSymbolRepository
    {
        public CoinSymbolRepository(IDbContext dbContext) : base(dbContext)
        {

        }
        public CoinSymbol GetbyAssetIdBase(string assetIdBase)
        {
            return GetFirst(new {AssetIdBase = assetIdBase});
        }

        public List<CoinSymbol> GetListActive()
        {
            return base.GetWhere(new { Active =true}).ToList();
        }
    }
}