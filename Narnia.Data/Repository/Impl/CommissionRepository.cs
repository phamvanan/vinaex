﻿using Dapper;
using Narnia.Core.Domain.Commissions;
using Narnia.Core.Enums;
using Narnia.Core.Models;
using Narnia.Data.Base;
using Narnia.Data.Base.Repositories;
using Narnia.Data.Repository.Contracts;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Narnia.Data.Repository.Impl
{
    public class CommissionRepository : GenericRepository<Commission>, ICommissionRepository
    {
        public CommissionRepository(IDbContext dbContext) : base(dbContext)
        {
        }

        public List<Commission> GetCommissionsForCronJob()
        {
            return GetWhere(new { Status = (int)CommisionStatusEnum.NotPaid }).ToList();
        }

        public IEnumerable<CommissionHistoryByBeneficiaryUserIdModel> GetCommissionHistoryByBeneficiaryUserId(int userId, int skip, int take, out int totalRecords)
        {
            var p = new DynamicParameters();
            p.Add("skip", skip);
            p.Add("take", take);
            p.Add("TotalRecords", dbType: DbType.Int32, direction: ParameterDirection.Output);
            p.Add("UserId", userId);
            var data = QueryStoreProc<CommissionHistoryByBeneficiaryUserIdModel>("usp_Commission_Get_HistoryByBeneficiaryUserId", p).ToList();
            totalRecords = p.Get<int>("TotalRecords");
            return data;
        }

         public IEnumerable<CommissionHistoryByModel> CommissionHistoryBy(string userName,DateTime? startDate,DateTime? endDate,bool? status,int skip, int take,out int totalRecords)
        {
            var p = new DynamicParameters();
            p.Add("skip", skip);
            p.Add("take", take);
            p.Add("TotalRecords", dbType: DbType.Int32, direction: ParameterDirection.Output);
            if(string.IsNullOrEmpty(userName) == false)
            {
                p.Add("UserName", new DbString() { Value = userName, IsAnsi = true, Length = 100 });
            }
            if (startDate.HasValue)
            {
                p.Add("StartDate", startDate);
            }
            if (endDate.HasValue)
            {
                p.Add("EndDate", endDate);
            }
            if (status.HasValue)
            {
                p.Add("Status", status);
            }
            var data = QueryStoreProc<CommissionHistoryByModel>("usp_Commission_Get_HistoryBy", p).ToList();
            totalRecords = p.Get<int>("TotalRecords");
            return data;
        }
    }
}