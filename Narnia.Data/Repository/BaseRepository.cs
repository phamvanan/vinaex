﻿using Microsoft.Extensions.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Narnia.Repository
{
    public class BaseRepository
    {
        public string ConnectionString { get; protected set; }
        public BaseRepository(IConfiguration configuration)
        {
            ConnectionString = configuration.GetConnectionString("CoinDb"); 
        }
        public IDbConnection CreateConnection()
        {
            return new SqlConnection(ConnectionString);
        }
    }
}
