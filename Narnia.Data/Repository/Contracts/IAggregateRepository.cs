﻿using Narnia.Core.Domain;
using Narnia.Core.Domain.Commissions;
using Narnia.Core.Domain.Payments;
using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Data.Repository.Contracts
{
    public interface IAggregateRepository
    {
        bool CreateSellAdvertisement(SellAdvertisement sellInfo, Double feePercentage);
        IList<Feedback> GetTopFeedbacks(int top);

        IEnumerable<AssetBalance> FindAssetBalances(int skip, int take, out int totalResultsCount, out int filteredResultsCount, bool countTotal = true, string userEmail = null, IList<string> assetTypeIds = null);
        IEnumerable<Commission> FindCommissions(int skip, int take, out int totalResultsCount, out int filteredResultsCount, bool countTotal = true,
            DateTime? createdFromUtc = null, DateTime? createdToUtc = null,
          IList<int> commLevels = null, string beneficiaryUserName = "");

        IEnumerable<Setting> FindSettings(int skip, int take, out int totalResultsCount, out int filteredResultsCount, bool countTotal = true,
           string settingName = "", string settingValue = "");

        #region Payments
        IList<GatewayAccount> GetActiveSystemGatewayAccounts(int gatewayId);

        IEnumerable<GatewayAccount> FindGatewayAccounts(int skip, int take, out int totalResultsCount, out int filteredResultsCount, bool countTotal = true,
           DateTime? createdFromUtc = null, DateTime? createdToUtc = null, string userName = "", bool isSystemAccount = false);
        #endregion

        #region Offers
        IEnumerable<ExchangeOffer> FindExchangeOffers(int skip, int take, out int totalResultsCount, bool countTotal = true,
   DateTime? createdFromUtc = null, DateTime? createdToUtc = null,
   IList<int> statuses = null, IList<string> assetTypeIds = null, string code = "", int? type = null);
        #endregion
    }
}
