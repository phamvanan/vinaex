﻿using Narnia.Core.Domain;
using Narnia.Data.Base.Repositories;

namespace Narnia.Data.Repository.Contracts
{
    public interface IEvidenceRepository : IGenericRepository<Evidence>
    {
        Evidence GetbyCode(string code);

        Evidence GetbyId(int id);
    }
}