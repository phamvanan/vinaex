﻿using Narnia.Core.Domain;
using Narnia.Core.Models;
using Narnia.Data.Base.Repositories;
using System.Collections.Generic;

namespace Narnia.Data.Repository.Contracts
{
    public interface IBanksRepository : IGenericRepository<Banks>
    {
        List<Banks> GetList();
        Banks GetById(int id);

        List<BankAdminPagingModel> GetBankAdminPagings(int pageIndex, int pageSize, string orderBy,
            out int totalRecords);

        Banks GetByCode(string code);
    }
}