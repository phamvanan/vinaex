﻿using Narnia.Core.Domain;
using Narnia.Core.Models;
using Narnia.Data.Base.Repositories;
using System;
using System.Collections.Generic;

namespace Narnia.Data.Repository.Contracts
{
    public interface ITransactionWalletRepository : IGenericRepository<TransactionWallet>
    {
        List<TransactionWalletPagingModel> GetTransactionWalletPaging(TransactionHistorySearchModel searchModel, out int totalRecords);

        TransactionWalletDetailVNDModel GetDetailVNDByTrancodeUserId(string transcode);

        TransactionWallet GetByTranscode(string transcode);

        IEnumerable<TransactionWalletAdminSearchModel> SearchTransactions(int skip, int take, out int totalResultsCount,
            DateTime? createdFromUtc = null, DateTime? createdToUtc = null, string transactionCode = null,
            IList<int> statuses = null, IList<string> assetTypeIds = null, string userName = "");

        TransactionWallletDetailAdminModel GetDetailAdmin(int transactionId);

        TransactionWallet GetByTranId(int transactionId);
    }
}