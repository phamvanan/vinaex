﻿using Narnia.Core.Domain;
using Narnia.Core.Models;
using Narnia.Data.Base.Repositories;
using System;
using System.Collections.Generic;

namespace Narnia.Data.Repository.Contracts
{
    public interface IUserRepository : IGenericRepository<User>
    {
        List<User> GetUplineUsers(int userId);
        User GetUserById(int userId);
        User GetUserByName(string userName);
        UserInforModel GetUserInforByUserId(int userId);
        UserInforModel GetUserInforByUserName(string userName);
        IEnumerable<User> FindUsers(int skip, int take, out int totalResultsCount, out int filteredResultsCount, bool countTotal = true,
           DateTime? createdFromUtc = null, DateTime? createdToUtc = null, string userName = null,
           string phoneNumber = "");
        IList<UserInforModel> FindUsers(SearchUserModel userSearch, out int totalRecords);
        
        User GetById(int id);

        User GetUplineUser(int userId);
        LinkedList<User> GetAllUplineUsers(int userId);
        UserInforModel GetUserFullInforByUserId(int userId);
    }
}
