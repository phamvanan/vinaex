﻿using Narnia.Core.Domain.Commissions;
using Narnia.Core.Models;
using Narnia.Data.Base.Repositories;
using System;
using System.Collections.Generic;

namespace Narnia.Data.Repository.Contracts
{
    public interface ICommissionRepository : IGenericRepository<Commission>
    {
        List<Commission> GetCommissionsForCronJob();
        IEnumerable<CommissionHistoryByBeneficiaryUserIdModel> GetCommissionHistoryByBeneficiaryUserId(int userId, int skip, int take, out int totalRecords);
        IEnumerable<CommissionHistoryByModel> CommissionHistoryBy(string userName, DateTime? startDate, DateTime? endDate, bool? status, int skip, int take, out int totalRecords);
    }
}