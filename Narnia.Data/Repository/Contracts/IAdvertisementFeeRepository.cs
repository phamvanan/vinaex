﻿using Narnia.Core.Domain;
using Narnia.Data.Base.Repositories;

namespace Narnia.Data.Repository.Contracts
{
    public  interface IAdvertisementFeeRepository:IGenericRepository<AdvertisementFee>
    {
        AdvertisementFee GetBySellAdtId(int sellAdtId);
        AdvertisementFee GetByBuyAdtId(int buyAdtId);
        AdvertisementFee GetByBuyTransactionId(int transactionId);

        AdvertisementFee GetAmountFreezeByUserIdCoinType(int userId, string assetTypeId);
        AdvertisementFee GetByUserIdAssetTypeTranId(int userId, string assetType, int transId);
    }
}
