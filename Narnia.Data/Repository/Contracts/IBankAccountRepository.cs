﻿using Narnia.Core.Domain;
using Narnia.Core.Models;
using Narnia.Data.Base.Repositories;
using System.Collections.Generic;

namespace Narnia.Data.Repository.Contracts
{
    public interface IBankAccountRepository : IGenericRepository<BankAccount>
    {
        List<BankAccountModel> GetListByUserId(int userId);
        BankAccount GetByUserIdAccountNo(int userId, string accountNo);
        List<BankAccountAdminPagingModel> GetBankAccountAdminPagings(int pageIndex, int pageSize, string orderBy, out int totalRecords);
        BankAccount GetById(int id);
    }
}