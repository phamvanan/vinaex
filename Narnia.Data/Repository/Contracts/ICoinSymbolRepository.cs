﻿using Narnia.Core.Domain;
using Narnia.Data.Base.Repositories;
using System.Collections.Generic;

namespace Narnia.Data.Repository.Contracts
{
    public interface ICoinSymbolRepository : IGenericRepository<CoinSymbol>
    {
        CoinSymbol GetbyAssetIdBase(string assetIdBase);
        List<CoinSymbol> GetListActive();
    }
}