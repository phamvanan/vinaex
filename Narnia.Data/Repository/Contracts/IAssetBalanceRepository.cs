﻿using System;
using System.Collections.Generic;
using System.Text;
using Narnia.Core.Domain;
using Narnia.Data.Base.Repositories;

namespace Narnia.Data.Repository.Contracts
{
    public interface IAssetBalanceRepository: IGenericRepository<AssetBalance>
    {
        AssetBalance GetByUserIdAssetType(int userID, string assetTypeID);
    }
}
