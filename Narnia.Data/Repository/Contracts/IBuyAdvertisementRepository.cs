﻿using Narnia.Core.Domain;
using Narnia.Core.Models;
using Narnia.Data.Base.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Data.Repository.Contracts
{
    public interface IBuyAdvertisementRepository : IGenericRepository<BuyAdvertisement>
    {
        IEnumerable<BuySellViewModel> SearchBuyAdvertisement(int pageIndex, int pageSize, out int totalResultsCount, out int filteredResultsCount, bool countTotal = true
            , bool isShowOfflineUser = false, string assetTypeId = null);

        InfoAdvertisementForSellBuyModel GetInfoBuyAdvertisement(int buyAdId);

        IEnumerable<BuyAdvertisement> LoadBuyAdvertisement(int pageIndex, int pageSize, out int totalResultsCount, out int filteredResultsCount, bool countTotal = true, string sort = null, BuySellAdvertiseSearchCriteria criteria = null);
        BuyAdvertisement LoadBuyAdvertisementDetail(int buyId);
        IEnumerable<InfoQuickBuySellAdvertisement> LoadQuickBuyAdvertisement(string assetTypeId, string currencyType, decimal coinNumber);
        IEnumerable<BuySellViewModel> GetBuyAdvertisementByUserId(int pageIndex, int pageSize, int userId, out int totalResultsCount, bool countTotal = true, string assetTypeId = null);
        IEnumerable<BuyAdvertisement> SearchAdvertisments(int skip, int take, out int totalResultsCount, out int filteredResultsCount, bool countTotal = true,
            DateTime? createdFromUtc = null, DateTime? createdToUtc = null,
            IList<int> statuses = null, IList<string> assetTypeIds = null, string userName = "");
        BuyAdvertisement GetyId(int Id);
    }
}
