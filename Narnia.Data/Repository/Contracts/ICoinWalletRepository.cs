﻿using Narnia.Core.Domain;
using Narnia.Data.Base.Repositories;
using System;
using System.Collections.Generic;

namespace Narnia.Data.Repository.Contracts
{
    public interface ICoinWalletRepository : IGenericRepository<CoinWallet>
    {
        CoinWallet GetbyUserId(int userId);

        CoinWallet GetByAddress(string address);

        CoinWallet GetbyUserIdCointType(int userId, int coinType);
        IEnumerable<CoinWallet> FindWallets(int skip, int take, out int totalResultsCount, out int filteredResultsCount, bool countTotal = true,
            DateTime? createdFromUtc = null, DateTime? createdToUtc = null,
          IList<string> assetTypeIds = null, string userName = "", int? userId = null);
        CoinWallet GetByUserIdType(int userId, string type);
    }
}