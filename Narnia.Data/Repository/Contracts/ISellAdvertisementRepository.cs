﻿using Narnia.Core.Domain;
using Narnia.Core.Models;
using Narnia.Data.Base.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Data.Repository.Contracts
{
   
    public interface ISellAdvertisementRepository : IGenericRepository<SellAdvertisement>
    {
        IEnumerable<BuySellViewModel> SearchSellAdvertisement(int pageIndex, int pageSize, out int totalResultsCount, out int filteredResultsCount, bool countTotal = true
            , bool isShowOfflineUser = false, string assetTypeId = null);

        InfoAdvertisementForSellBuyModel GetInfoSellAdvertisement(int SellAdId,int userId);

        IEnumerable<SellAdvertisement> LoadSellAdvertisement(int pageIndex, int pageSize, out int totalResultsCount, out int filteredResultsCount, bool countTotal = true, string sort = null, BuySellAdvertiseSearchCriteria criteria = null);
        SellAdvertisement LoadSellAdvertisementDetail(int sellAdId);

        IEnumerable<InfoQuickBuySellAdvertisement> LoadQuickSellAdvertisement(string assetTypeId, string currencyType, decimal coinNumber);

        SellAdvertisement GetActiveById(int Id);
        IEnumerable<BuySellViewModel> GetSellAdvertisementByUserId(int pageIndex, int pageSize, int userId, out int totalResultsCount, bool countTotal = true, string assetTypeId = null);
        IEnumerable<SellAdvertisement> SearchAdvertisments(int skip, int take, out int totalResultsCount, out int filteredResultsCount, bool countTotal = true,
            DateTime? createdFromUtc = null, DateTime? createdToUtc = null,
            IList<int> statuses = null, IList<string> assetTypeIds = null, string userName = "");
        SellAdvertisement GetyId(int Id);
    }
}
