﻿using Narnia.Core.Domain;
using Narnia.Core.Enums;
using Narnia.Core.Models;
using Narnia.Data.Base.Repositories;
using System;
using System.Collections.Generic;

namespace Narnia.Data.Repository.Contracts
{
    public interface ITransactionRepository : IGenericRepository<Transaction>
    {
        InfoAdvertisementDetailModel GetInfoDetailTransaction(int transId);
        IList<TransactionStatisticModel> GetStatistic(int userId);

        List<TransactionAdminSearchModel> SearchAdmin(int pageIndex, int pageSize, int tranMode, string AssetTypeId,
            int Status, string orderBy,
            DateTime? FromDate, DateTime? ToDate, string transactionCode, out int totalRecords);
        IEnumerable<Transaction> SearchTransactions(int skip, int take, out int totalResultsCount, out int filteredResultsCount, bool countTotal = true, 
            DateTime? createdFromUtc = null, DateTime? createdToUtc = null, string transactionCode = null,
            IList<int> statuses = null, IList<string> assetTypeIds = null, string userName = "");

        TransactionDetailAdminModel GetDetailAdmin(int transactionId);
        InfoAdvertisementDetailModel GetInfoDetailTransactionByCode(string transCode);

        IList<TransactionHistoryModel> GetTransactionHistoryByType(int userId, TransactionStatusMode statusMode,
            int take, int skip, out int totalRecords);

        Transaction GetbyTransactionId(int transactionId);
        IList<TransactionHistoryUserModel> GetTransactionHistoryForUser(int userId, TransactionStatusClient transactionStatusClient, int skip, int take, out int totalRecords);
    }
}