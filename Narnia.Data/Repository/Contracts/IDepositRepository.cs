﻿using Narnia.Core.Domain;
using Narnia.Core.Domain.Payments;
using Narnia.Data.Base.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace Narnia.Data.Repository.Contracts
{
    public interface IDepositRepository: IGenericRepository<Deposit>
    {
        IEnumerable<Deposit> SearchDeposits(int skip, int take, out int totalResultsCount, out int filteredResultsCount, bool countTotal = true, DateTime? createdFromUtc = null, DateTime? createdToUtc = null
            , IList<string> assetTypeIds = null, IList<int> statuses = null, string userName = "");

        IEnumerable<BankTransfer> SearchBankTransfers(int skip, int take, out int totalResultsCount, out int filteredResultsCount, bool countTotal = true, DateTime? createdFromUtc = null, DateTime? createdToUtc = null
            , IList<string> assetTypeIds = null, IList<int> statuses = null, string userName = "", DateTime? billingDate = null);
    }
}
